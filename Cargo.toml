
[workspace]
resolver = "3"
default-members = ["server"]
members = [
"builder",
"cargo_author",
"cargo_toml",
"categories",
"crate_db",
"crates_io_client",
"creviews",
"datadump",
"docs_rs_client",
"event_log",
"fetcher",
"front_end",
"github_info",
"kitchen_sink",
"manifest",
"reindex",
"render_readme",
"repo_url",
"rich_crate",
"simple_cache",
"user_db",
"crate_git_checkout",
"udedokei",
"search_index",
"synscrape",
"ranking",
"server",
"svg_render",
"tarball",
"feat_extractor",
"deps_index",
"debcargo_list",
"util",
"validator",
"style",
]

[workspace.package]
edition = "2024"

[workspace.dependencies]
serde = { version = "1.0.152", features = ["derive"] }
regex = { version = "1.10.0", default-features = false, features = ["std", "unicode-gencat", "unicode-perl"] }
tokio = { version = "1.36.0", features = ["rt-multi-thread", "macros", "time", "sync", "tracing"] }
chrono = { version = "0.4.22", default-features = false, features = ["std", "serde", "clock"] }
crates-index = { version = "3.1.0", default-features = false, features = ["git", "parallel"] }
cargo_toml = { version = "0.21", features = ["features"] }
reqwest = { version = "0.12.9", features = ["blocking", "brotli", "gzip", "json"] }
rusqlite = "0.32.1"
itertools = "0.14"
brotli = "7"
heck = "0.5"
rayon = "1.10"
rand = { version = "0.8", features = ["small_rng"] }
env_logger = { version = "0.11.3", default-features = false }

[workspace.lints.rust]
absolute_paths_not_starting_with_crate = "warn"
elided_lifetimes_in_paths = "warn"
macro_use_extern_crate = "warn"
trivial_casts = "warn"
unit_bindings = "deny"
for_loops_over_fallibles = "deny"
# unused_lifetimes = "warn"  # it's buggy
unused_qualifications = "warn"
trivial_numeric_casts = "warn"
# single_use_lifetimes = "warn" # it's buggy
meta_variable_misuse = "warn"
keyword_idents = { level = "deny", priority = 1 }
redundant_lifetimes = "warn"
unnameable_types = "warn"

[workspace.lints.clippy]
pedantic = { level = "warn", priority = -100 }
nursery = { level = "warn", priority = -100 }
perf = { level = "warn", priority = -100 }
suspicious = { level = "warn", priority = -100 }
complexity = { level = "warn", priority = -100 }
cargo = { level = "warn", priority = -100 }


# Experimental
future_not_send = "allow"

suboptimal_flops = "warn"
manual_inspect = "warn"
borrow_interior_mutable_const = "deny" # yikes
doc_markdown = "allow" # too many false positives
manual_range_contains = "allow" # range contains looks awful
needless_raw_string_hashes = "allow" # I want them consistent
get_first = "allow" # 0 is clearer
type_complexity = "allow" # I know
new_without_default = "allow" # I still want new
map_unwrap_or = "allow" # breaks code due to type inference
cast_possible_truncation = "allow" # that's why I cast
cast_sign_loss = "allow" # that's why I cast
cast_possible_wrap = "allow" # that's why I cast
cast_precision_loss = "allow"
missing_errors_doc = "allow" # struct Error documents it
missing_panics_doc = "allow" # too much work
implicit_hasher = "allow" # ahash all the way
ignored_unit_patterns = "allow" # don't care
redundant_closure_for_method_calls = "allow" # often makes code longer
if_not_else = "allow" # I put common case first
items_after_statements = "allow" # it's fine…
too_many_lines = "allow" # maybe later
manual_assert = "allow" # Ifs are more readable
default_trait_access = "allow" # I'd like it if it make code shorter
module_name_repetitions = "allow" # these names need to be readable outside the module
struct_field_names = "allow" # false positives
match_same_arms = "allow" # order and consistency matters
similar_names = "allow" # I can tell them apart
unreadable_literal = "allow" # decimal separators for fractional part of floats is WTFBBQ
wildcard_imports = "allow" # I'm not going to maintain long lists
needless_for_each = "allow" # it may optimize better
bool_to_int_with_if = "allow" # too codegolf
unused_self = "allow" # that's my APIs
unnecessary_wraps = "allow" # that's my APIs
unused_async = "allow" # that's my APIs
struct_excessive_bools = "allow" # false positives
many_single_char_names = "allow" # I know what I'm doing
zero_prefixed_literal = "allow" # I know it's not octal
inline_always = "allow" # I know what I'm doing
no_effect_underscore_binding = "allow" # cmon, they're already unused
naive_bytecount = "allow" # libstd should have this built-in
redundant_pub_crate = "allow" # I don't like ambiguous pub
missing_const_for_fn = "allow" # buggy: this shouldn't warn when self is non-const-constructible
enum_glob_use = "allow" # autofix breaks tests that use the variants, plus I like the glob
option_if_let_else = "allow" # closure is way worse
significant_drop_tightening = "allow" # cool idea, but as of 1.79 it's unusable
significant_drop_in_scrutinee = "allow" # cool idea, but as of 1.79 it's unusable
blocks_in_conditions = "allow" # don't complain about is_some_and
or_fun_call = "allow" # https://github.com/rust-lang/rust-clippy/issues/12724
case_sensitive_file_extension_comparisons = "allow" # makes code too ugly
iter_on_single_items = "allow" # `[].into_iter()` is nicer
collapsible_if = "allow" # long lines are not more readable
upper_case_acronyms = "allow" # they're acronyms!

[profile.dev]
opt-level = 1
debug = "line-tables-only"


[profile.dev.package]
flate2 = { opt-level = 2, debug = false }
brotli = { opt-level = 2, debug = false }
ahash = { opt-level = 2, debug = false }
rustc-hash = { opt-level = 2, debug = false }
hashbrown = { opt-level = 2, debug = false }

[profile.release]
opt-level = 2
debug = "line-tables-only"
debug-assertions = false
incremental = false

[profile.release.package."*"]
debug = false

[patch.crates-io]
cargo_toml = { path = "cargo_toml" }
cargo_author = { path = "cargo_author" }
ammonia = { git = "https://github.com/rust-ammonia/ammonia", rev = "d11ff7605ae42730b44cb51b473493366eae668c" }
