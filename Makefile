STYLES=style/public/index.css style/public/search.css
CACHE_FILES=data/crate_data.db data/users.db data/2019.rmpz

all: website

website: data/index/1 styles Cargo.lock
	cd server && cargo run --release
	cd style && npm start

Cargo.lock: Cargo.toml
	rm Cargo.lock; git submodule update --init --recursive
	cargo update

backup_data:
	rsync -Praz librs:/var/lib/crates-server/crate_data.db data/crate_data.db;

backup:
	rm -f data/*-shm data/*-wal
	rsync -Praz librs:/var/lib/crates-server/ data/ --exclude=tarballs --exclude=rustsec --exclude='debcargo*' --exclude=*-shm --exclude=git --exclude=index --exclude=event_log.db --exclude='.*' --exclude='*.txt' --exclude='tag-synonyms.csv' --exclude='ablocklist.csv' --exclude=data
	rsync -Pra librs:/var/lib/crates-server/ data/ --exclude=tarballs --exclude=rustsec --exclude='debcargo*' --exclude=*-shm --exclude='*.mpbr' --exclude=git --exclude=index --exclude=event_log.db --exclude='.*' --exclude='*.txt' --exclude='tag-synonyms.csv' --exclude='ablocklist.csv' --exclude=data
	cd data/index && git fetch && git reset --hard origin/master

$(CACHE_FILES):
	if [ ! -d data/data.tar.xz -a -f data.tar.xz ]; then mv data.tar.xz data/; fi
	if [ ! -f data/data.tar.xz ]; then curl --fail --output data/data.tar.xz https://lib.rs/data/data.tar.xz; fi

	cd data; unxz < data.tar.xz | tar xv
	touch $@

compat_stats:
	# set autobins = true
	cargo run -r --bin compat_export -p builder | gzip -9 > all_supported_rust_versions.csv.gz
	rsync -P all_supported_rust_versions.csv.gz librs:/var/www/lib.rs/public/data/

download_stats:
	# set autobins = true
	cargo run -p datadump -r --bin downloads_csv
	zip -9 downloads_csv.zip downloads_20??.csv
	rsync -Pr downloads_csv.zip librs:/var/www/lib.rs/public/data/

repo_checks:
	find data/rust-repo-checks -type f | sort | tar cf rust-repo-checks.tar.xz --numeric-owner --no-acls --no-xattrs --no-fflags --xz --strip-components=1 --files-from=-
	rsync -P rust-repo-checks.tar.xz librs:/var/www/lib.rs/public/data/

styles: $(STYLES)

$(STYLES): style/node_modules/.bin/gulp
	cd style && npm run build

style/package.json:
	git submodule update --init --recursive

style/node_modules/.bin/gulp: style/package.json
	@echo Installing Sass
	cd style && npm install
	touch $@

data/index/1:
	@echo Getting crates index
	git submodule update --init

.PHONY: all download-caches styles clean clean-cache

clean:
	rm -rf style/public/*.css Cargo.lock
	git submodule update --init --recursive
	git submodule sync

