# Lib.rs

[Lib.rs](https://lib.rs) is an [opinionated, unofficial index](https://lib.rs/about) of Rust libraries and applications. It aims to improve discovery of crates, and provide [richer data and insights](https://lib.rs/data-processing) than crates.io.

Lib.rs automatically shows crates [published to crates.io](https://doc.rust-lang.org/cargo/reference/publishing.html). Lib.rs itself is not a registry. It's not affiliated with the Rust project.

## Reporting issues

If you notice any issues with the data, search, ranking, categorization, etc., or have concerns how your crates are presented, please [file a bug](https://gitlab.com/lib.rs/main). I'm keen to improve the site, and ensure the crates are presented fairly and accurately.

