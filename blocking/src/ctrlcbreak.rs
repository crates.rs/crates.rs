use futures::Future;
use std::pin::Pin;
use std::sync::atomic::Ordering::SeqCst;
use std::sync::Arc;
use std::task::{Context, Poll};

#[derive(Debug, Copy, Clone)]
pub struct NotRunningError;

impl fmt::Display for NotRunningError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("STOPPING")
    }
}
impl std::error::Error for NotRunningError {}

use std::{fmt, process};
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::Once;

pub static STOPPED: AtomicU32 = AtomicU32::new(0);
static INIT: Once = Once::new();

pub fn dont_hijack_ctrlc() {
    INIT.call_once(|| {});
}

pub fn stop() {
    let stops = STOPPED.fetch_add(1, SeqCst);
    if stops > 1 {
        eprintln!("STOPPING");
        if stops > 3 {
            process::exit(1);
        }
    }
}

pub fn stopped() -> bool {
    INIT.call_once(|| {
        ctrlc::set_handler(stop)
        .expect("Error setting Ctrl-C handler");
    });
    STOPPED.load(Ordering::Relaxed) > 0
}

#[inline]
pub fn running() -> Result<(), NotRunningError> {
    if !stopped() {
        Ok(())
    } else {
        Err(NotRunningError)
    }
}

#[derive(Clone, Default)]
pub struct Stop(Arc<AtomicU32>);

impl Stop {
    #[must_use] pub fn new() -> (Self, Arc<AtomicU32>) {
        let a = Arc::new(AtomicU32::new(0));
        (Self(a.clone()), a)
    }
}

impl Drop for Stop {
    fn drop(&mut self) {
        self.0.store(1, SeqCst);
    }
}

#[repr(transparent)]
pub struct SpawnAbortOnDrop<T>(pub tokio::task::JoinHandle<T>);

impl<T> Drop for SpawnAbortOnDrop<T> {
    fn drop(&mut self) {
        self.0.abort();
    }
}

impl<T> Future for SpawnAbortOnDrop<T> {
    type Output = Result<T, tokio::task::JoinError>;

    #[inline]
    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        match unsafe { Pin::new_unchecked(&mut self.0) }.poll(cx) {
            ready @ Poll::Ready(_) => ready,
            pending => {
                if stopped() {
                    self.0.abort();
                }
                pending
            },
        }
    }
}
