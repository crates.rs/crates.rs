use rustc_hash::FxHashMap as HashMap;
use log::{error, info, log, warn};
use tokio::time::error::Elapsed;
use std::cmp::Reverse;
use std::collections::VecDeque;
use std::fmt::Display;
use std::future::Future;
use std::sync::{Arc, Mutex, MutexGuard, Weak};
use std::time::{Duration, Instant};

pub mod ctrlcbreak;

type LockMap = HashMap<String, VecDeque<Weak<Instant>>>;
static ACTIVE_WORK: Mutex<Option<LockMap>> = Mutex::new(None);


pub fn ablock_in_place<F, R>(label: impl Display, cb: F) -> impl Future<Output=R> where F: Send + FnOnce() -> R {
    let l = label.to_string();
    async move {
        tokio::task::yield_now().await;
        block_in_place(l, cb)
    }
}

pub fn block_in_place<F, R>(label: impl Display, cb: F) -> R where F: FnOnce() -> R {
    let mut label = label.to_string();
    label.push('!');
    let _guard = set(label);
    tokio::task::block_in_place(cb)
}

pub fn watch<F, R>(label: impl Display, cb: F) -> R where F: FnOnce() -> R {
    let label = label.to_string();
    let _guard = set(label);
    cb()
}

pub fn closure<F, R>(label: impl Display, cb: F) -> impl FnOnce() -> R
where F: FnOnce() -> R {
    let label = label.to_string();
    move || {
        let _guard = set(label);
        cb()
    }
}

pub fn closure1<F, A, R>(label: impl Display, cb: F) -> impl FnOnce(A) -> R
where F: FnOnce(A) -> R {
    let label = label.to_string();
    move |a| {
        let _guard = set(label);
        cb(a)
    }
}

pub fn awatch<'a, F, R>(label_display: impl Display, cb: F) -> impl Future<Output = R> + 'a where F: Future<Output = R> + 'a {
    let label = format!("{label_display}@");
    async move {
        let _guard = set(label);
        cb.await
    }
}

pub fn timeout<'a, T: 'a, E: From<Elapsed> + 'a>(label: impl Display, time: u16, f: impl Future<Output = Result<T, E>> + Send + 'a) -> impl Future<Output = Result<T, E>>  + Send + 'a {
    let fut = async move {
        match tokio::time::timeout(Duration::from_secs(time.into()), f).await {
            Ok(r) => r,
            Err(elapsed) => Err(E::from(elapsed)),
        }
    };
    awatch(label, fut)
}

fn lock_it() -> MutexGuard<'static, Option<LockMap>> {
    ACTIVE_WORK.lock().unwrap_or_else(|_| {
        error!("block: poison");
        std::process::exit(10)
    })
}

fn set(key: String) -> impl Drop {
    let maybe_map = &mut *lock_it();
    let map = maybe_map.get_or_insert_with(|| {
        spawn_watchdog();
        HashMap::default()
    });

    let a = Arc::new(Instant::now());
    let v = map.entry(key).or_default();

    // quick partial cleanup while we're locked
    while v.front().is_some_and(|old| Weak::strong_count(old) == 0) {
        v.pop_front();
    }
    v.push_back(Arc::downgrade(&a));
    a
}

pub fn dump_all() {
    let mut map = lock_it();
    let Some(map) = map.as_mut() else { return };
    let now = Instant::now();
    for (key, v) in map {
        for w in v {
            let Some(a) = w.upgrade() else { continue };
            let elapsed = now.duration_since(*a);
            warn!("blocking: {key} has been running for {}ms", elapsed.as_millis() as u32);
        }
    }
}

fn spawn_watchdog() {
    std::thread::Builder::new().name("block_watch".to_string()).spawn(|| {
        loop {
            std::thread::sleep(Duration::from_secs(3));
            {
                let mut map = lock_it();
                let map = map.as_mut().unwrap();
                let now = Instant::now();
                let min_duration = Duration::from_secs(2);

                let map_len = map.len();
                if map_len > 2000 {
                    info!("blocking: {map_len} tasks running!");
                }
                let mut printed_any = false;
                let mut worst = 0;
                let mut worst_name = String::new();
                let mut to_print = Vec::new();
                map.retain(|key, timers| {
                    let timers_len = timers.len();
                    timers.retain_mut(|w| {
                        let Some(a) = w.upgrade() else { return false };

                        let elapsed = now.duration_since(*a);
                        if elapsed > min_duration {
                            printed_any = true;
                            let millis = elapsed.as_millis() as u32;
                            if millis > 5000 && millis > worst {
                                worst = millis;
                                worst_name.clone_from(key);
                            }

                            let text = if millis > 10000 {
                                format!("blocking: {key} HAS BEEN RUNNING FOR {}s // {map_len}*{timers_len}", millis / 1000)
                            } else {
                                format!("blocking: {key} has been running for {millis}ms // {map_len}*{timers_len}")
                            };
                            to_print.push((millis, text));
                        }
                        true
                    });
                    if timers.len() > 100 {
                        printed_any = true;
                        warn!("blocking: {key} has {} concurrent instances", timers.len());
                    }
                    !timers.is_empty()
                });

                if !to_print.is_empty() {
                    to_print.sort_by_key(|a| Reverse(a.0));
                    let max_millis = to_print.iter().map(|&(m, _)| m).max().unwrap_or(0);
                    let sev = if max_millis > 10000 {
                        log::Level::Warn
                    } else if max_millis > 3000 {
                        log::Level::Info
                    } else {
                        log::Level::Debug
                    };

                    log!(sev, "\n---- blocking: ({})", to_print.len());
                    for (m, t) in to_print {
                        let sev = if m > 10000 {
                            log::Level::Warn
                        } else if m > 3000 {
                            log::Level::Info
                        } else {
                            log::Level::Debug
                        };
                        log!(sev, "{t}");
                    }
                }

                if map.capacity() > 1000 && map.len() < 500 {
                    map.shrink_to_fit();
                }
                if printed_any && worst > 5000 {
                    warn!("---- there were blocking tasks up to {worst}ms ({worst_name})\n");
                }
            }
        }
    }).unwrap();
}
