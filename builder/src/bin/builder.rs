
#[path = "../dockerize.rs"]
pub(crate) mod dockerize;
#[path = "../clippy_builder.rs"]
pub(crate) mod clippy_builder;
#[path = "../parse.rs"]
mod parse;

use crate::dockerize::DOCKERFILE_COMMON;
use crate::dockerize::chownperm;
use parking_lot::Mutex;
use rand::rngs::SmallRng;
use rand::SeedableRng;
use util::FxHashMap;
use util::FxHashSet as HashSet;
use dockerize::Mount;
use crate::clippy_builder::clippy_builds;
use crate::dockerize::{Docker, Line};
use crate_db::builddb::{BuildDb, ClippyDb, Compat, CompatRanges, RustcMinorVersion, SetCompatMulti};
use futures::future::try_join_all;
use kitchen_sink::{CError, Crate, CratesIndexCrate, KitchenSink, Origin, RendCtx, SemVer, stopped};
use log::{debug, error, info, warn};
use parse::{DIVIDER, parse_analyses_chan};
use rand::Rng;
use rand::seq::SliceRandom;
use std::collections::BTreeMap;
use std::path::Path;
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;
use std::time::Instant;
use util::pick_top_n_unstable_by;

const CONCURRENCY: u8 = 8; // builds in parallel

const MAX_OLD_VERSIONS: usize = 3;
const MAX_TARBALL_CHECKS: usize = 1;

const DOCKERFILE_DEFAULT_RUSTC: RustcMinorVersion = 80;
const DOCKERFILE_PRELUDE: &str = r#"
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --profile minimal --default-toolchain 1.80.1 --verbose
ENV PATH="$PATH:/home/rustyuser/.cargo/bin"
RUN rustup set profile minimal
"#; // must have newline!

pub(crate) const TEMP_JUNK_DIR: &str = "/var/tmp/crates_env";

const RUST_VERSIONS: &[RustcMinorVersion] = &[
    31,
    42,
    52,
    57,
    62,
    64,
    67,
    69,
    71,
    73,
    75,
    77,
    79,
    81,
];

struct ToCheck {
    score: u32,
    crate_name: Arc<str>,
    version: SemVer,
    rustc_compat: CompatRanges,
    is_app: bool,
    prefer_older_rustc: bool,
    oldest_ok_rustc_version: Option<RustcMinorVersion>,
    newest_bad_rustc_version: RustcMinorVersion,
}

pub(crate) fn out_of_disk_space() -> bool {
    match fs4::available_space(TEMP_JUNK_DIR) {
        Ok(size) => {
            info!("free disk space: {}MB", size / 1_000_000);
            size < 5_000_000_000 // cargo easily chews gigabytes of disk space per build
        },
        Err(e) => {
            error!("disk space check: {}", e);
            true
        },
    }
}

#[tokio::main]
#[allow(clippy::await_holding_lock)]
async fn main() -> Result<(), anyhow::Error> {
    env_logger::init();

    let crates = Arc::new(KitchenSink::new_default().await?);
    let db = BuildDb::new(crates.main_data_dir().join("builds.db"))?;

    let mut filters: Vec<_> = std::env::args().skip(1).collect();
    let mut do_all = false;
    let mut do_clippy = false;
    filters.retain(|v| {
        if v == "--all" { do_all = true; return false; }
        if v == "--clippy" { do_clippy = true; return false; }
        true
    });

    let index = crates.index()?;
    let crates_io_crates = index.crates_io_crates()?;
    let mut map_iter;
    let mut recent_iter;
    let do_filter = move |&all: &&Crate| {
        if let [only] = filters.as_slice() {
            if !all.name().contains(only) {
                return false;
            }
        } else if filters.len() > 1 && !filters.iter().any(|f| f == all.name()) {
            return false;
        }
        true
    };
    let all_crates: &mut dyn Iterator<Item = _> = if !do_all {
        let rend = RendCtx::new(Instant::now() + Duration::from_secs(300));
        let mut recent = Box::pin(crates.notable_recently_updated_crates(8000, &rend)).await?;
        recent.shuffle(&mut SmallRng::from_entropy());
        recent_iter = recent.into_iter().filter_map(|(o, _)| {
            crates_io_crates.get(o.package_name_icase())
        }).filter(do_filter);
        &mut recent_iter
    } else {
        map_iter = crates_io_crates.values().filter(do_filter);
        &mut map_iter
    };

    if do_clippy {
        let clippy_db = ClippyDb::new(crates.main_data_dir().join("clippy.db"))?;
        clippy_builds(crates.clone(), db, clippy_db, all_crates).await
    } else {
        msrv_builds(crates.clone(), db, all_crates).await
    }
}

struct CompatBuilder {
    docker: Docker,
    job_inputs_root: PathBuf,
    /// Lockfile: rustc -> package -> version
    built_crates: Mutex<FxHashMap<u16, FxHashMap<String, SemVer>>>,
}

async fn msrv_builds(crates: Arc<KitchenSink>, db: BuildDb, all_crates: &mut dyn Iterator<Item = &Crate>) -> Result<(), anyhow::Error> {
    info!("Starting Docker…");

    let mut builder = CompatBuilder {
        docker: Docker::new("rustesting2", TEMP_JUNK_DIR.into()),
        job_inputs_root: Path::new(TEMP_JUNK_DIR).join("job_inputs"),
        built_crates: Mutex::default(),
    };
    builder.prepare_docker()?;

    let (s, r) = crossbeam_channel::bounded::<Vec<_>>(200);

    let builds = std::thread::spawn(move || {
        std::thread::sleep(Duration::from_millis(500)); // wait for more data
        let mut candidates: Vec<ToCheck> = Vec::new();
        let mut rng = SmallRng::from_entropy();
        let mut one_crate_version = HashSet::default();

        while let Ok(mut next_batch) = r.recv() {
            std::thread::sleep(Duration::from_millis(500)); // wait for more data
            if out_of_disk_space() {
                error!("Stopping early due to disk space");
                let _ = std::fs::remove_dir_all(Path::new(TEMP_JUNK_DIR).join("target"));
                let _ = std::fs::remove_dir_all(Path::new(TEMP_JUNK_DIR).join("registry"));
                break;
            }

            if stopped() {
                break;
            }

            candidates.append(&mut next_batch);
            for mut tmp in r.try_iter().take(100) {
                candidates.append(&mut tmp);
            }

            // biggest gap, then latest ver; best at the end, because pops
            candidates.sort_unstable_by(|a, b| a.score.cmp(&b.score).then(a.version.cmp(&b.version)));

            let mut available_rust_versions = RUST_VERSIONS.to_vec();
            available_rust_versions.shuffle(&mut rng);

            let max_to_waste_trying = (candidates.len() / 2).max(RUST_VERSIONS.len());
            let (versions_lib, versions_app): (Vec<_>, Vec<_>) = std::iter::from_fn(|| candidates.pop())
            .take(max_to_waste_trying)
            .filter_map(|x| {
                let max_ver = x.rustc_compat.oldest_ok_certain().map(|v| v-1).unwrap_or(999);
                let min_ver = x.rustc_compat.newest_bad_likely().map(|v| v+1).unwrap_or(31);
                let lowest_not_bad_yet = x.rustc_compat.newest_bad().map(|v| v+1);

                // cargo install --message-format is v1.58+ :(
                let upper_limit = x.rustc_compat.oldest_ok().unwrap_or(65).min(x.oldest_ok_rustc_version.unwrap_or(999));
                // don't pick 1.29 as the first choice
                let lower_limit = x.rustc_compat.newest_bad().unwrap_or(if x.is_app {58} else {55}).max(x.newest_bad_rustc_version);

                // min_ver ignores bad deps, but that often leads it to be stuck on last edition
                // which is super wasteful
                let min_ver = min_ver.max((lower_limit.min(max_ver)).saturating_sub(10));
                // same for approx max ver that may come from msrv or binaries
                let max_ver = max_ver.min(upper_limit.max(min_ver) + 12);

                let best_ver = if let Some(closes_the_gap) = x.rustc_compat.newest_bad_certain().map(|v| v+1).filter(|&v| Some(v) == x.rustc_compat.oldest_ok()) {
                    closes_the_gap // verify rustc-version
                } else if x.prefer_older_rustc || x.rustc_compat.oldest_ok_certain().is_some() || x.rustc_compat.newest_bad_likely().is_some() {
                    ((upper_limit-1) + (lower_limit+1) * 4)/5 // bias towards lower ver, because lower versions see features from newer versions
                } else {
                    ((upper_limit-1) + (lower_limit+1))/2
                };

                let origin = Origin::from_crates_io_name(&x.crate_name);
                let mut existing_info = db.get_compat_raw(&origin).unwrap_or_default();

                // don't keep retrying the same rustc versions for every crate release
                let already_tried_rust_versions: HashSet<_> = existing_info.iter().map(|inf| inf.rustc_version).collect();
                existing_info.retain(|inf| inf.crate_version == x.version);

                let possible_rusts = available_rust_versions.iter().enumerate()
                .filter(|&(_, &minor)| {
                    minor == best_ver || Some(minor) == lowest_not_bad_yet || (minor >= min_ver && minor <= max_ver)
                })
                .filter(|&(_, &v)| {
                    !existing_info.iter().any(|inf| inf.rustc_version == v && inf.compat.certainity() > 0)
                });
                let maybe_rustc_idx = if !x.rustc_compat.has_ever_built() && /* has bad uncertain builds */ x.rustc_compat.newest_bad().is_some_and(|n| Some(n) != x.rustc_compat.newest_bad_certain()) {
                    // pick latest to avoid building with a compiler that may not understand new manifest or edition (cargo failures give worse info)
                    possible_rusts.min_by_key(|&(_, &v)| (i32::from(upper_limit) - i32::from(v)).abs() + if v != best_ver && already_tried_rust_versions.contains(&v) { 5 } else { 0 })
                    .map(|(k,v)| { debug!("{}-{} never built, trying latest R.{}", x.crate_name, x.version, v); (k,v) })
                } else {
                    possible_rusts.min_by_key(|&(_, &minor)| (i32::from(minor) - i32::from(best_ver)).abs() + if minor != best_ver && already_tried_rust_versions.contains(&minor) { 3 } else { 0 })
                };
                let Some((rustc_idx, _)) = maybe_rustc_idx else {
                    debug!("Can't find rust for {}@{}, because it needs r{}-{}, but only got {:?}", x.crate_name, x.version, min_ver, max_ver, available_rust_versions);
                    return None;
                };

                // it's better to make multiple passes, and re-evaluate the crate after pass/fail of this version
                if !one_crate_version.insert(x.crate_name.clone()) {
                    return None;
                }

                let rustc_ver = available_rust_versions.swap_remove(rustc_idx);
                let mut required_deps = Vec::new();
                for (c,(v, newest_bad)) in x.rustc_compat.required_deps() {
                    if newest_bad <= rustc_ver { // this check is useless, since newest_bad is for wrong version
                        required_deps.push((c.into(), v.clone()));
                    }
                }

                Some(CrateToRun {
                    rustc_ver,
                    crate_name: x.crate_name,
                    version: x.version,
                    required_deps,
                    is_app: x.is_app,
                })
            })
            .take((RUST_VERSIONS.len() *3/4).min(CONCURRENCY.into())) // max concurrency
            .partition(|c| !c.is_app);

            // apps use install
            for versions in [versions_lib, versions_app] {
                if versions.is_empty() {
                    continue;
                }

                eprintln!("\nselected: {}/{} ({})", versions.len(), candidates.len(), versions.iter().take(10).map(|c| format!("{} {}", c.crate_name, c.version)).collect::<Vec<_>>().join(", "));

                if let Err(e) = builder.run_and_analyze_versions(&db, &versions) {
                    eprintln!("•• {e}");
                }
            }

            if candidates.len() > 3000 {
                candidates.drain(..candidates.len()/2);
            }
        }
        let _ = std::fs::remove_dir_all(Path::new(TEMP_JUNK_DIR).join("registry/src"));
        eprintln!("builder end");
    });


    for all in all_crates {
        if stopped() {
            break;
        }

        match find_versions_to_build(all, &crates).await {
            Ok(vers) => {
                s.send(vers).unwrap();
            },
            Err(e) => {
                eprintln!("•• {}: {e}", all.name());
                continue;
            },
        }
    }
    drop(s);
    eprintln!("sender end");
    builds.join().unwrap();
    eprintln!("bye");
    Ok(())
}

async fn find_versions_to_build(all: &CratesIndexCrate, crates: &KitchenSink) -> Result<Vec<ToCheck>, anyhow::Error> {
    let crate_name: Arc<str> = all.name().into();
    let origin = &Origin::from_crates_io_name(&crate_name);
    let mut rng = SmallRng::from_entropy();

    let popularity_factor = crates.crate_ranking_for_builder(origin).unwrap_or(0.3);
    if popularity_factor < rng.gen_range(0.1..0.33) {
        return Ok(vec![]);
    }

    let krate = crates.rich_crate_async(origin).await?;

    let min_relevant_rustc = RUST_VERSIONS.iter().copied().min().unwrap_or_default()
        .max(51);
    let mut compat_info = crates.rustc_compatibility_for_builder(&krate, min_relevant_rustc).await?;

    let has_anything_built_ok_yet = compat_info.values().any(|c| c.has_ever_built());

    // for all versions (biased towards latest state), to copy results across versions
    let newest_bad_rustc_version = compat_info.values().filter_map(|c| c.newest_bad()).max().unwrap_or(0);
    let oldest_ok_rustc_version = compat_info.values()
        .flat_map(|c| c.all_ok_likely_rustc_versions()) // there can be multiple builds, but oldest being below newest_bad
        .filter(|&ok_ver| ok_ver > newest_bad_rustc_version)
        .min();

    let mut candidates: Vec<_> = all.versions().iter().rev() // rev() starts from most recent
        .filter(|v| !v.is_yanked())
        .take(MAX_OLD_VERSIONS)
        .filter_map(|v| SemVer::parse(v.version()).ok())
        .map(|v| {
            let c = compat_info.remove(&v).unwrap_or_default();
            (v, c)
        })
        .filter(|(_, c)| c.oldest_ok().unwrap_or(999) > 20) // old crates, don't bother
        .enumerate()
        .map(|(idx, (version, compat))| {
            let has_ever_built = compat.has_ever_built();
            let has_failed = compat.newest_bad_likely().is_some();
            let no_compat_bottom = compat.newest_bad().is_none();
            let oldest_ok = compat.oldest_ok().unwrap_or(999);
            let newest_bad = compat.newest_bad().unwrap_or(0).max(19); // we don't test rust < 19
            let oldest_ok_certain = compat.oldest_ok_certain().unwrap_or(999);
            let newest_bad_certain = compat.newest_bad_likely().unwrap_or(0).max(25); // we don't test rust < 25
            let gap = u32::from(oldest_ok.saturating_sub(newest_bad)); // unknown version gap
            let gap_certain = u32::from(oldest_ok_certain.saturating_sub(newest_bad_certain)); // unknown version gap
            let overall_gap = u32::from(oldest_ok_rustc_version.unwrap_or(oldest_ok).saturating_sub(newest_bad_rustc_version)); // for any ver
            let score = rng.gen_range(0..if has_anything_built_ok_yet { 3 } else { 10 }) // spice it up a bit
                + gap_certain.min(20)
                + overall_gap.min(20)
                + if gap > 4 { gap * 2 } else { gap }
                + if gap > 10 { gap } else { 0 }
                + if !has_ever_built && has_failed && has_anything_built_ok_yet { 15 } else { 0 } // unusable data? try fixing first
                + if has_ever_built { 0 } else { 2 } // move to better ones
                + if newest_bad < compat.newest_bad_certain().unwrap_or(0) { 5 } else { 0 } // really unusable data
                + if compat.oldest_ok_certain().unwrap_or(999) > oldest_ok { 5 } else { 0 } // haven't really checked min ver yet
                + if newest_bad > 29 { 1 } else { 0 } // don't want to check old crap
                + if no_compat_bottom { 2 } else { 0 } // don't want to show 1.0 as compat rust
                + if oldest_ok  > 35 { 1 } else { 0 }
                + if oldest_ok  > 47 { 2 } else { 0 }
                + if oldest_ok  > 50 { 5 } else { 0 }
                + if oldest_ok  > 55 { 8 } else { 0 }
                + if oldest_ok  > 65 { 8 } else { 0 }
                + if newest_bad_certain <= 55 { 5 } else { 0 } // check post latest edition
                + if version.pre.is_empty() { 20 } else { 0 } // don't waste time testing alphas
                + if idx == 0 { 10 } else { 0 } // prefer latest
                + 5u32.saturating_sub(idx as u32); // prefer newer

            // lower score if already spent effort testing this one
            let score = score.saturating_sub(compat.all_rustc_versions().count() as u32);

            ToCheck {
                prefer_older_rustc: idx == 0,
                oldest_ok_rustc_version,
                newest_bad_rustc_version,
                score,
                version,
                rustc_compat: compat,
                crate_name: crate_name.clone(),
                is_app: false,
            }
        })
        .filter(|c| {
            c.rustc_compat.oldest_ok_certain().unwrap_or(999) > 2+c.rustc_compat.newest_bad().unwrap_or(0)
        })
        .collect();

    if candidates.is_empty() {
        return Ok(vec![]);
    }

    for c in &mut candidates {
        c.score = (f64::from(c.score) * popularity_factor) as u32;
    }

    if has_anything_built_ok_yet {
        // biggest gap, then latest ver
        pick_top_n_unstable_by(&mut candidates, 6, |a, b| b.score.cmp(&a.score).then(b.version.cmp(&a.version)));
    } else {
        candidates.shuffle(&mut rng); // dunno which versions will build
        candidates.truncate(5); // don't waste time on broken crates
    }

    try_join_all(candidates.iter_mut().take(MAX_TARBALL_CHECKS).map(|c| async move {
        let meta = crates.crate_files_summary_from_crates_io_tarball(&c.crate_name, &c.version.to_string()).await?;
        let msrv = crates.index_msrv_from_manifest(&Origin::from_crates_io_name(&c.crate_name), &meta.manifest)?;
        if msrv > 1 {
            c.rustc_compat.add_compat(msrv - 1, Compat::DefinitelyIncompatible, None);
        }
        if meta.manifest.lib.is_none() && !meta.manifest.bin.is_empty() {
            c.is_app = true;
        }
        Ok::<_, CError>(())
    }))
    .await?;

    for c in &candidates {
        println!(
            "{crate_name} {}\t^{}\tinferred={}~{} checked={}~{} (x{}) | overall={oldest_ok_rustc_version}-{newest_bad_rustc_version}",
            c.version,
            c.score,
            c.rustc_compat.oldest_ok().unwrap_or(0),
            c.rustc_compat.newest_bad().unwrap_or(0),
            c.rustc_compat.oldest_ok_certain().unwrap_or(0),
            c.rustc_compat.newest_bad_likely().unwrap_or(0),
            c.rustc_compat.all_rustc_versions().count(),
            oldest_ok_rustc_version = oldest_ok_rustc_version.unwrap_or(0),
        );
        for (k, (v, r)) in c.rustc_compat.required_deps() {
            println!(" + {k}@{v} for r{r}");
        }
    }

    if candidates.is_empty() {
        println!("{crate_name} ???\tno candidates");
    }

    Ok(candidates)
}

struct CrateToRun {
    rustc_ver: RustcMinorVersion,
    crate_name: Arc<str>,
    version: SemVer,
    required_deps: Vec<(Box<str>, SemVer)>,
    is_app: bool,
}

impl CompatBuilder {
fn run_and_analyze_versions(&self, db: &BuildDb, versions: &[CrateToRun]) -> Result<(), anyhow::Error> {
    if versions.is_empty() {
        return Ok(());
    }

    // reuse tarballs cached by our crates-io client
    for c in versions {
        let dest = Path::new(TEMP_JUNK_DIR)
            .join("registry/cache/index.crates.io-6f17d22bba15001f")
            .join(format!("{}-{}.crate", c.crate_name, c.version));
        if !dest.exists() {
            let src = Path::new("/var/lib/crates-server/tarballs").join(format!("{}/{}.crate", c.crate_name, c.version));
            let _ = std::fs::hard_link(&src, &dest)
                .or_else(|_| std::fs::copy(&src, &dest).map(drop))
                .map_err(|e| warn!("tarball {} -> {}: {}", src.display(), dest.display(), e));
        }
    }

    let (lines_send, lines_recv) = crossbeam_channel::bounded(10);
    let mut to_set = BTreeMap::new();

    std::thread::scope(|s| {
        let t = s.spawn(|| {
            for f in parse_analyses_chan(lines_recv) {
                if let Some(rustc_version) = f.rustc_version {
                    for (rustc_override, crate_name, crate_version, new_compat, reason) in f.crates {
                        let origin = Origin::from_crates_io_name(&crate_name);
                        let rustc_version = rustc_override.unwrap_or(rustc_version);

                        // don't lock to 0.x.0 versions, the can be buggy or too old due to minimal-versions
                        if new_compat == Compat::VerifiedWorks && (crate_version.major != 0 || crate_version.patch != 0) {
                            self.built_crates.lock()
                                .entry(rustc_version).or_insert_with(Default::default)
                                // always overwrite with the most recent, because it's best if it matches cache
                                .insert(crate_name, crate_version.clone());
                        }

                        to_set.entry((rustc_version, origin, crate_version, reason))
                            .and_modify(|existing_compat: &mut Compat| {
                                if new_compat.is_better(existing_compat) {
                                    *existing_compat = new_compat;
                                }
                            })
                            .or_insert(new_compat);
                    }
                }
            }
        });

        self.do_builds(versions, lines_send)?;
        t.join().unwrap();
        Ok::<(), anyhow::Error>(())
    })?;

    let tmp = to_set.iter().map(|((rv, o, cv, reason), c)| {
        SetCompatMulti { origin: o, ver: cv, rustc_version: *rv, compat: *c, reason }
    }).collect::<Vec<_>>();
    if db.set_compat_multi(&tmp).is_err() {
        // retry, sqlite is flaky
        std::thread::sleep(Duration::from_secs(1));
        db.set_compat_multi(&tmp)?;
    }
    Ok(())
}

fn prepare_docker(&mut self) -> Result<(), anyhow::Error> {
    for &p in &["registry/src", "target/debug", "registry/git", "registry/index/github.com-1ecc6299db9ec823/.cache", "registry/index/index.crates.io-6f17d22bba15001f/.cache"] {
        let _ = std::fs::remove_dir_all(Path::new(TEMP_JUNK_DIR).join(p));
    }
    for &p in &["git", "target", "job_inputs", "registry", "registry/cache/github.com-1ecc6299db9ec823", "registry/cache/index.crates.io-6f17d22bba15001f"] {
        let p = Path::new(TEMP_JUNK_DIR).join(p);
        let _ = std::fs::create_dir_all(&p);
        let _ = chownperm(&p, true).inspect_err(|e| warn!("can't chown workdir {p:?}: {e}"));
    }
    // let _ = Command::new("chmod").arg("-R").arg("a+rwX").arg(TEMP_JUNK_DIR).status()?;
    // let _ = Command::new("chown").arg("-R").arg("4321:4321").arg(TEMP_JUNK_DIR).status()?;

    self.docker.set_concurrency(CONCURRENCY);
    self.docker.set_env("CARGO_INCREMENTAL", "0");
    self.docker.set_env("CARGO_REGISTRIES_CRATES_IO_PROTOCOL", "git"); // sparse duplicates data
    self.docker.set_env("CARGO_BUILD_JOBS", "3"); // Not CONCURRENCY, because multiple builds run at the same time

    use std::fmt::Write;
    let mut dockerfile = format!("{DOCKERFILE_COMMON}\n{DOCKERFILE_PRELUDE}");
    for v in RUST_VERSIONS.iter().copied() {
        if v != DOCKERFILE_DEFAULT_RUSTC {
            writeln!(&mut dockerfile, "RUN rustup toolchain add --no-self-update {}", rustc_minor_ver_to_version(v))?;
        }
    }
    writeln!(&mut dockerfile, "RUN rustup toolchain add --no-self-update nightly")?;
    dockerfile.push_str("RUN rustup toolchain list\nRUN chmod -R a-w ~/.rustup ~/.cargo/bin ~/.cargo/env\n");

    self.docker.build(&dockerfile)?;

    // force index update
    self.docker.run_script("cargo install libc --vers 99.9.9 --color=always || true", &[
        (&format!("{TEMP_JUNK_DIR}/git"), "/home/rustyuser/.cargo/git", Mount::Delegated),
        (&format!("{TEMP_JUNK_DIR}/registry"), "/home/rustyuser/.cargo/registry", Mount::Delegated),
    ], None)?;

    Ok(())
}

// must match bash script below
fn job_inputs_dir(&self, c: &CrateToRun) -> PathBuf {
    self.job_inputs_root.join(format!("crate-{}-{}--{}-job", c.crate_name, c.version, rustc_minor_ver_to_version(c.rustc_ver)))
}

// versions is (rustc version, crate version)
fn do_builds(&self, versions: &[CrateToRun], out_chan: crossbeam_channel::Sender<Line>) -> Result<(), anyhow::Error> {
    for c in versions {
        let dir = self.job_inputs_dir(c);
        let _ = std::fs::create_dir(&dir);

        let mut cargo_toml = format!(
            "[package]\nname=\"_____\"\nversion=\"0.0.0\"\nedition=\"{}\"\nrust-version=\"1.{}\"\n[profile.dev]\ndebug=false\n[dependencies]\n{} = \"={}\"\n",
            if c.rustc_ver > 56 { "2021" } else { "2018" },
            c.rustc_ver, c.crate_name, c.version
        );
        for (c, v) in &c.required_deps {
            use std::fmt::Write;
            let _ = writeln!(
                &mut cargo_toml,
                "{c} = \"<= {v}, {}{}\"",
                if v.major == 0 { "0." } else { "" },
                if v.major == 0 { v.minor } else { v.major }
            );
        }
        debug!("{}", cargo_toml);
        std::fs::write(dir.join("Cargo.toml"), cargo_toml)?;

        // Fill the lockfile with versions that have built okay
        // this speeds up subsequent builds (cache) and makes them more reliable (compat)
        if let Some(built_crates) = self.built_crates.lock().get(&c.rustc_ver) {
            let mut cargo_lock = String::new();
            use std::fmt::Write;
            for (name, version) in built_crates.iter().take(1000) {
                write!(&mut cargo_lock, "
[[package]]
name = \"{name}\"
version = \"{version}\"
source = \"registry+https://github.com/rust-lang/crates.io-index\"
")?;
            }
            std::fs::write(dir.join("Cargo.lock"), cargo_lock)?;
        }
    }

    let keep_going_flag = if versions.iter().all(|v| v.rustc_ver > 60) {
        "-Z unstable-options --keep-going"
    } else {
        ""
    };

    let script = format!(r##"
        set -euo pipefail
        function yeet {{
            {{ mv "$1" "$1-delete" && rm -rf "$1-delete"; }} || rm -rf "$1" # atomic delete
        }}
        function cleanup() {{
            local rustver="$1"
            local crate_name="$2"
            local libver="$3"
            export CARGO_TARGET_DIR=/home/rustyuser/target/$rustver;
            rm -rf "$CARGO_TARGET_DIR"/debug/*/"$crate_name-"*; # eats disk space
            rm -rf "$CARGO_TARGET_DIR"/debug/*/"lib$crate_name-"*;
            yeet ~/.cargo/registry/src/*/"$crate_name-$libver";
        }}
        function check_crate_with_rustc() {{
            local rustver="$1"
            local crate_name="$2"
            local libver="$3"
            local stdoutfile="$4"
            local stderrfile="$5"
            local job_inputs_dir="/home/rustyuser/job_inputs/crate-$crate_name-$libver--$rustver-job"
            mkdir -p "crate-$crate_name-$libver/src";
            cd "crate-$crate_name-$libver";
            touch src/lib.rs
            cp "$job_inputs_dir/Cargo."* ./
            export CARGO_TARGET_DIR=/home/rustyuser/target/$rustver;
            {{
                echo >"$stdoutfile" "CHECKING $rustver $crate_name $libver"
                RUSTC_BOOTSTRAP=1 CARGO_RESOLVER_INCOMPATIBLE_RUST_VERSIONS=fallback cargo +nightly -Zno-index-update -Zmsrv-policy generate-lockfile || true
                RUSTC_BOOTSTRAP=1 timeout 20 cargo +nightly -Z no-index-update fetch || CARGO_NET_GIT_FETCH_WITH_CLI=true timeout 60 cargo +"$rustver" fetch --color=always -vv;
                RUSTC_BOOTSTRAP=1 timeout 90 nice cargo +$rustver {check_command} -j3 {keep_going_flag} --locked --message-format=json >>"$stdoutfile" 2>"$stderrfile";
            }} || {{
                echo >>"$stdoutfile" "CHECKING $rustver $crate_name $libver (minimal-versions)"
                echo >>"$stderrfile" "CHECKING $rustver $crate_name $libver (minimal-versions)"
                printf > Cargo.toml '[package]\nname="_____"\nversion="0.0.0"\n[profile.dev]\ndebug=false\n[dependencies]\n%s = "=%s"\n[dev-dependencies]\nminimal-versions-are-broken="1"\nregex="1.7.2"\n' "$crate_name" "$libver";
                rm -f Cargo.lock
                export DOCS_RS=1
                RUSTC_BOOTSTRAP=1 timeout 20 cargo +"$rustver" -Z no-index-update -Z minimal-versions generate-lockfile;
                timeout 40 nice cargo +$rustver {check_command} -j3 --locked --message-format=json >>"$stdoutfile" 2>>"$stderrfile";
            }}
        }}
        for job in {jobs}; do
            (
                check_crate_with_rustc $job "/tmp/output-$job" "/tmp/outputerr-$job" && echo "# R.$job done OK" || echo "# R.$job failed" '{check_command}';
            ) &
        done
        wait
        for job in {jobs}; do
            echo "{divider}"
            cat "/tmp/output-$job";
            echo >&2 "{divider}"
            cat >&2 "/tmp/outputerr-$job";
            cleanup $job
        done
    "##,
        check_command = if versions.iter().all(|v| v.is_app && v.rustc_ver >= 58) { "install \"$crate_name\" -f --bins --version \"$libver\" --debug --root=./install-test" } else { "check" },
        divider = DIVIDER,
        jobs = versions.iter().map(|c| format!("\"{} {} {}\"", rustc_minor_ver_to_version(c.rustc_ver), c.crate_name, c.version)).collect::<Vec<_>>().join(" "),
    );

    self.docker.run_script(&script, &[
        (&format!("{TEMP_JUNK_DIR}/git"), "/home/rustyuser/.cargo/git", Mount::Delegated),
        (&format!("{TEMP_JUNK_DIR}/registry"), "/home/rustyuser/.cargo/registry", Mount::Delegated),
        (&format!("{TEMP_JUNK_DIR}/target"), "/home/rustyuser/target", Mount::Delegated),
        (&format!("{}", self.job_inputs_root.display()), "/home/rustyuser/job_inputs:ro", Mount::Rw),
    ], Some(out_chan))?;

    Ok(())
}
}

fn rustc_minor_ver_to_version(rustc_minor_ver: u16) -> String {
    if rustc_minor_ver == 56 {
        "1.56.1".into()
    } else {
        format!("1.{rustc_minor_ver}.0")
    }
}

