use crate::DOCKERFILE_COMMON;
use rand::rngs::SmallRng;
use rand::seq::SliceRandom;
use rand::{Rng, SeedableRng};
use util::FxHashMap as HashMap;
use anyhow::Context;
use cargo_toml::Lints;
use crate::dockerize::{Docker, Line};
use crate::TEMP_JUNK_DIR;
use crate::parse::CompilerMessage;
use crate_db::builddb::{BuildDb, ClippyDb, SetCompatMulti};
use crate::dockerize::chownperm;
use crate::dockerize::Mount;
use crates_index::{DependencyKind, Version};
use futures::future::try_join_all;
use itertools::Itertools;
use kitchen_sink::{running, Compat, Crate, CratesIoIndexLookup, KitchenSink, Origin, RichCrate, SemVer, VersionReq};
use log::{debug, error, info, warn};
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::collections::hash_map::Entry;
use util::FxHashSet as HashSet;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::sync::Arc;
use util::{CowAscii, SmolStr};

const STANDARD_LINTS: &str = r#"
[workspace.lints.rust]
absolute_paths_not_starting_with_crate = "warn"
elided_lifetimes_in_paths = "warn"
keyword_idents = { level = "warn", priority = -1 }
macro_use_extern_crate = "warn"
meta_variable_misuse = "warn"
missing_docs = "warn"
trivial_casts = "warn"
trivial_numeric_casts = "warn"
unit_bindings = "warn"
unsafe_code = "warn"
unstable_features = "warn"
unused_crate_dependencies = "warn"
unused_imports = "allow"
unused_qualifications = "warn"
variant_size_differences = "warn"
for_loops_over_fallibles = "warn"
redundant_lifetimes = "warn"
keyword_idents_2018 = "warn"
# nightly
lossy_provenance_casts = "warn"
fuzzy_provenance_casts = "warn"
impl_trait_overcaptures = "warn"
must_not_suspend = "warn"
non_exhaustive_omitted_patterns = "warn"
non_local_definitions = "warn"
rust_2021_incompatible_closure_captures = "warn"
rust_2021_incompatible_or_patterns = "warn"
rust_2021_prefixes_incompatible_syntax = "warn"
rust_2021_prelude_collisions = "warn"
rust_2024_incompatible_pat = "warn"
single_use_lifetimes = "warn"
unnameable_types = "warn"
# unsafe_attr_outside_unsafe = "warn"
unused_extern_crates = "warn"
unused_macro_rules = "warn"

[workspace.lints.clippy]
pedantic = { level = "warn", priority = -90 }
correctness = { level = "warn", priority = -96 }
nursery = { level = "warn", priority = -95 }
perf = { level = "warn", priority = -94 }
suspicious = { level = "warn", priority = -95 }
complexity = { level = "warn", priority = -100 }
cargo = { level = "warn", priority = -95 }
all = { level = "warn", priority = -98 }

incompatible_msrv = "warn"
as_ptr_cast_mut = "warn"
clear_with_drain = "warn"
collection_is_never_read = "warn"
debug_assert_with_mut_call = "warn"
deref_by_slicing = "warn"
empty_enum_variants_with_brackets = "warn"
fn_to_numeric_cast_any = "warn"
format_push_string = "warn"
mem_forget = "warn"
needless_collect = "warn"
needless_pass_by_ref_mut = "warn"
nonstandard_macro_braces = "warn"
panic_in_result_fn = "warn"
path_buf_push_overwrite = "warn"
# pathbuf_init_then_push = "warn"
unwrap_in_result = "warn"
verbose_file_reads = "warn"
useless_let_if_seq = "warn"
rc_buffer = "warn"
rc_mutex = "warn"
read_zero_byte_vec = "warn"
redundant_clone = "warn"
redundant_type_annotations = "warn"
string_add = "warn"
string_slice = "warn"
string_to_string = "warn"
trailing_empty_array = "warn"
transmute_undefined_repr = "warn"
unnecessary_self_imports = "warn"
# unused_result_ok = "warn"

cast_lossless = "allow"
cast_possible_truncation = "allow"
cast_possible_wrap = "allow"
cast_precision_loss = "allow"
cast_sign_loss = "allow"
collapsible_else_if = "allow"
default_trait_access = "allow"
doc_markdown = "allow"
enum_glob_use = "allow"
if_not_else = "allow"
ignored_unit_patterns = "allow"
inline_always = "allow"
len_without_is_empty = "allow"
len_zero = "allow"
manual_assert = "allow"
map_unwrap_or = "allow"
match_same_arms = "allow"
missing_safety_doc = "allow"
module_name_repetitions = "allow"
must_use_candidate = "allow"
needless_raw_string_hashes = "allow"
new_without_default = "allow"
redundant_closure = "allow"
redundant_closure_for_method_calls = "allow"
redundant_field_names = "allow"
similar_names = "allow"
struct_field_names = "allow"
unreadable_literal = "allow"
wildcard_imports = "allow"
"#;

const DOCKERFILE: &str = r#"
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --profile minimal --verbose --default-toolchain beta
ENV PATH="$PATH:/home/rustyuser/.cargo/bin"
RUN rustup component add rustfmt clippy
RUN rustup toolchain install nightly --component clippy --component rustfmt --no-self-update
RUN chmod -R a-w ~/.rustup ~/.cargo/bin ~/.cargo/env
"#;

const JOB_REL_DIR: &str = "lib.rs/crates";

struct CrateToRun {
    krate: RichCrate,
    version: SemVer,
}

#[derive(Debug, Default)]
struct ClippyResults {
    built_rustc: Option<u16>,
    ran_build_script: bool,
    edition: Option<u16>,
    warnings: HashMap<String, (String, u16)>,
    errors: HashMap<String, (String, u16)>,
}

struct BatchOptions<'a> {
    jobs_dir: &'a Path,
    keep_going: bool,
    default_features: bool,
    nightly: bool,
}

async fn build_batch(batch: &[CrateToRun], alread_built_versions: &HashMap<SmolStr, SemVer>, docker: &Docker, opts: &BatchOptions<'_>, crates: &KitchenSink) -> Result<HashMap<(Origin, SemVer), ClippyResults>, anyhow::Error> {
    running()?;
    info!("Building batch of {}:\n{}\n", batch.len(), batch.iter().map(|c| format!("https://lib.rs/compat/{}#{}", c.krate.name(), c.version)).join("\n"));

    let job_workdir = tempfile::tempdir_in(opts.jobs_dir).context("Can't make batch tmpdir")?;
    let job_workdir = job_workdir.path();
    std::fs::create_dir_all(job_workdir).context("Can't make batch workdir")?;
    let _ = chownperm(job_workdir, true).inspect_err(|e| warn!("can't chown workdir {e}"));

    make_workspace(crates, batch, alread_built_versions, job_workdir, JOB_REL_DIR.as_ref()).await.context("Error making ws")?;

    let mut by_crate = HashMap::default();
    std::thread::scope(|s| {
        let by_crate_ref = &mut by_crate;
        let (send, recv) = crossbeam_channel::bounded(10);

        let collect_thread = s.spawn(move || -> anyhow::Result<()> {
            let mut rustc_version = None::<u16>;
            for line in recv {
                match line {
                    Line::StdOut(line) => {
                        if rustc_version.is_none() {
                            if let Some((rustc_ver, _)) = line.strip_prefix("cargo 1.").and_then(|l| l.split_once('.')) {
                                rustc_version = rustc_ver.parse().ok();
                                info!("Using rustc 1.{}", rustc_version.unwrap_or(0));
                            }
                        } else {
                            match line.starts_with('{').then(|| serde_json::from_str::<CompilerMessage>(&line)) {
                                Some(Ok(msg)) => {
                                    collect_messages(msg, by_crate_ref, rustc_version)?;
                                },
                                Some(Err(e)) => warn!("> msg parse error {e}: {line}"),
                                None => println!("> {}", line.get(..200).unwrap_or(&line)),
                            }
                        }
                    },
                    Line::StdErr(line) => {
                        let l = line.trim_start();
                        if l.starts_with("could not execute process `/home/rustyuser/.rustup") || l.starts_with("failed to create directory `/home/rustyuser/.cargo") {
                            let _ = std::fs::remove_dir_all(TEMP_JUNK_DIR);
                            panic!("messed up {line}");
                        }

                        let line = line
                            .replace("/home/rustyuser/", "~/")
                            .replace("~/.rustup/toolchains/beta-aarch64-unknown-linux-gnu/", ".rustup/")
                            .replace("-C panic=abort -C embed-bitcode=no -C codegen-units=", "cu=")
                            .replace("--crate-type lib --emit=dep-info,metadata ", "")
                            .replace("-C debug-assertions=off ", "")
                            .replace("--out-dir $TARGET/deps ", "")
                            .replace(" -L dependency=$TARGET/deps --cap-lints warn", "")
                            .replace(" --error-format=json --json=diagnostic-rendered-ansi,artifacts,future-incompat", "")
                            .replace("~/.cargo/registry/", "$REG/")
                            .replace("$REG/src/index.crates.io-6f17d22bba15001f/", "$REG//")
                            .replace("~/target/debug/", "$TARGET/");

                        if let Some((_, rest)) = line.split_once("rustc --crate-name") {
                            eprintln!("rustc: […] {}", rest.get(..300).unwrap_or(rest));
                            continue;
                        }

                        if line.split_once("Running") // careful about ANSI color
                            .and_then(|(_, l)| l.split_once("/bin/cargo CARGO_"))
                            .is_some_and(|(_, l)| l.contains("-C extra-filename=-")) { // looks like Cargo invocation
                                debug!("Running [log skipped]");
                                continue;
                            }
                        eprintln!("err: {line}");
                    },
                    Line::Eof(status) => {
                        info!("Finished with {} ({status:?})", status.code().unwrap_or(-1));
                    },
                }
            }
            Ok(())
        });

        let target_dir = format!("{TEMP_JUNK_DIR}/target{}", if opts.nightly { "-nightly" } else { "" });
        let dirs = [
            (&*format!("{TEMP_JUNK_DIR}/git"), "/home/rustyuser/.cargo/git", Mount::Delegated),
            (&*format!("{TEMP_JUNK_DIR}/registry"), "/home/rustyuser/.cargo/registry", Mount::Delegated),
            (&*target_dir, "/home/rustyuser/target", Mount::Delegated),
            (job_workdir.to_str().unwrap(), &format!("/home/rustyuser/{JOB_REL_DIR}"), Mount::Rw),
        ];

        let extra_flags = if opts.keep_going { "--future-incompat-report --keep-going" } else { "" };
        let feature_flags = if opts.default_features { "" } else { "--no-default-features" };
        let toolchain_name = if opts.nightly { "nightly" } else { "beta" };
        let build_res = docker.run_script(&format!(r##"
            export RUSTUP_TOOLCHAIN={toolchain_name}
            cargo +{toolchain_name} --version # used by the parser!
            timeout 20 cargo +{toolchain_name} fetch --manifest-path="{JOB_REL_DIR}/Cargo.toml" --color=always || {{ rm -f "{JOB_REL_DIR}/Cargo.lock"; timeout 30 cargo fetch --manifest-path="{JOB_REL_DIR}/Cargo.toml" || exit 12; }}
            timeout 200 nice cargo +{toolchain_name} clippy -vvv --manifest-path="{JOB_REL_DIR}/Cargo.toml" --color=never --message-format=json --frozen --bins --lib {extra_flags} {feature_flags} --workspace
        "##), &dirs, Some(send));

        // // registry readonly
        // dirs[0].2 = Mount::Ro;
        // dirs[1].2 = Mount::Ro;

        // let build_res = docker.run_script(&format!(r##"
        //     ls -l '/home/rustyuser/.rustup/toolchains/beta-aarch64-unknown-linux-gnu/bin/rustc'
        //     cargo clippy --manifest-path={JOB_REL_DIR}/Cargo.toml -v --color=never --message-format=json --frozen --bins --lib --future-incompat-report --keep-going --workspace
        // "##), &dirs, Some(send));


        if let Err(e) = build_res {
            warn!("Build reported exit: {e}");
        }

        collect_thread.join().expect("log parse thread")
    }).context("Build aborted; lost results")?;
    Ok(by_crate)
}

fn collect_messages(mut msg: CompilerMessage, by_crate_ref: &mut HashMap<(Origin, SemVer), ClippyResults>, rustc_version: Option<u16>) -> Result<(), anyhow::Error> {
    let message_is_error = msg.message.as_ref().is_some_and(|m| m.level == "error" || m.level == "failure-note");
    let log_level = if message_is_error { log::Level::Info } else { log::Level::Debug };
    if msg.reason.as_deref() == Some("build-finished") && msg.package_id.is_none() {
        return Ok(());
    }
    let Some((crate_origin, crate_version)) = msg.package_id().and_then(|(n,v)| Some((Origin::from_crates_io_name(n), SemVer::parse(v).ok()?))) else {
        warn!("msg without package_id {msg:?}");
        return Ok(());
    };
    let res = by_crate_ref.entry((crate_origin.clone(), crate_version)).or_default();
    if msg.is_build_script() || msg.reason.as_deref() == Some("build-finished") {
        res.ran_build_script = true;
        log::log!(log_level, "build script {} {crate_origin:?} {msg:?}", if message_is_error { "failure" } else { "ok" });
        return Ok(());
    }
    if res.edition.is_none() {
        if let Some(ed) = msg.target.as_ref().and_then(|t| t.edition.as_deref()?.parse().ok()) {
            res.edition = Some(ed);
        }
    }
    match msg.reason.as_deref().unwrap_or("") {
        "build-script-executed" => {},
        "compiler-artifact" => {
            res.built_rustc = rustc_version;
        },
        "compiler-message" => {
            if let Some(mut message) = msg.message.take() {
                if let Some(code) = message.code.take().map(|c| c.code) {
                    let dst = if message_is_error {
                        warn!("Build failed {crate_origin:?} {message:?}\n\n");
                        &mut res.errors
                    } else { &mut res.warnings };
                    let (code, message_text) = deaggregate_compiler_message(&crate_origin, code, message.message.take().unwrap_or_default(), message.span_text());
                    if code.is_empty() {
                        return Ok(());
                    }
                    if code == "E0602" {
                        anyhow::bail!("Invalid clippy lint in {crate_origin:?}!? {message_text:?}");
                    }
                    match dst.entry(code) {
                        Entry::Occupied(mut e) => {
                            let v = e.get_mut();
                            v.1 = v.1.saturating_add(1);
                        },
                        Entry::Vacant(e) => {
                            log::log!(log_level, "new msg: '{message_text}' {:?}", msg.package_id);
                            e.insert((message_text, 1));
                        },
                    }
                } else if message_is_error {
                    info!("Build error: {}", message.message.as_deref().unwrap_or("???"));
                    res.built_rustc = None;
                } else if let Some(text) = message.message.take() {
                    if !known_warning(&text) {
                        warn!("Unknown message '{text}' {msg:?} no code {message:?} {crate_origin:?}");
                    }
                } else {
                    warn!("Missing message text? {msg:?} no code {message:?} {crate_origin:?}");
                }
            } else {
                error!("Missing message!? {msg:?} {crate_origin:?}");
            }
        },
        other => warn!("Unknown reason {other}: {msg:#?} {crate_origin:?}"),
    };
    Ok(())
}

fn edition_incompatible_rustc(ed: u16) -> Option<u16> {
    match ed {
        2018 => Some(30),
        2021 => Some(55),
        _ => None,
    }
}

// ptr_arg split for &mut, especially &mut String vs &mut str

fn deaggregate_compiler_message(origin: &Origin, mut code: String, mut message: String, span: Option<&str>) -> (String, String) {
    match code.as_str() {
        "missing_docs" => {
            if let Some(ty) = message.strip_prefix("missing documentation for ") {
                let ty = ty.trim_start_matches("a ").trim_start_matches("an ").trim_start_matches("the ");
                code.push(' ');
                code.push_str(ty);
                message.clear();
            }
        },
        "trivial_casts" => {
            if message.ends_with("_t`") || message.contains("::os::") {
                code.push_str(" ffi");
            }
            else if message.contains("Box<dyn") {
                code.push_str(" dyn");
            }
        },
        "unused_imports" => {
            if let Some(item) = message.strip_prefix("the item `") {
                if item.starts_with("Vec`") || item.starts_with("Box`") {
                    code.push_str(" std");
                } else if item.starts_with("TryInto`") || item.starts_with("TryFrom`") {
                    code.clear();
                }
            }
        },
        "unused_variables" => {
            if message.contains("is assigned to, but never used") {
                code.push_str(" assign");
            }
        }
        "unused_must_use" => {
            if message.contains("Result`") {
                code.push_str(" res");
            } else if message.contains("Future`") {
                code.push_str(" fut");
            } else if message.contains("::from_raw") {
                code.push_str(" raw");
            }
        }
        "unsafe_code" => {
            if let Some(ty) = message.strip_prefix("implementation of an `unsafe` ") {
                code.push(' ');
                code.push_str(ty);
                message.clear();
            } else if let Some(ty) = message.strip_prefix("declaration of an `unsafe` ") {
                code.push_str(" decl");
                code.push_str(ty);
                message.clear();
            } else if message == "usage of an `unsafe` block" {
                code.push_str(" block");
                message.clear();
            }
        },
        "non_snake_case" => {
            if message.contains('_') {
                code.push_str(" _");
            }
        },
        "clippy::unused_io_amount" => {
            if let Some(kind) = message.split(' ').next() {
                code.push(' ');
                code.push_str(kind);
            }
        },
        "clippy::cargo_common_metadata" => {
            if !message.as_ascii_lowercase().contains(&*origin.package_name_icase().as_ascii_lowercase()) {
                // clippy complains about missing metadata of wrong packages?
                code.clear();
            }
        },
        "deprecated" => {
            if message.starts_with("use of deprecated macro `try`") {
                code.push_str(" try");
                message.clear();
            } else if message.contains("::uninitialized") {
                code.push_str(" uninit");
            } else if message.contains("Error::description") {
                code.push_str(" err");
            } else if message.contains("`std::") || message.contains("`core::") {
                code.push_str(" std");
            }
        },
        "clippy::transmute_ptr_to_ref" => {
            if message.contains("MaybeUninit") {
                code.clear();
            }
            if message.contains("`*const") && message.contains("`&mut") {
                code.push_str(" mut");
            }
        },
        "clippy::unnecessary_cast" => {
            if let Some(ty) = message.strip_prefix("casting ").and_then(|m| m.split(' ').next()) {
                code.push(' ');
                code.push_str(ty);
            }
        },
        "clippy::needless_lifetimes" => {
            if message.ends_with("'a") {
                code.push_str(" a");
            }
        },
        "clippy::incompatible_msrv" => {
            if let Some((n, _)) = message.split_once("is stable since `1.").and_then(|(_, n)| n.split_once('.')) {
                code.push(' ');
                code.push_str(n);
                message.clear();
                message.push_str("clippy^msrv");
                if let Some(s) = span {
                    message.push(' ');
                    message.push_str(s.split_once(['(', '[', '{', '.']).map(|(s,_)| s).unwrap_or(s));
                }
            }
        },
        "invalid_reference_casting" => {
            if message.contains("&mut") {
                code.push_str(" mut");
            }
        },
        "clippy::transmute_ptr_to_ptr" => {
            if let Some(ty) = message.split(' ').next() {
                code.push(' ');
                code.push_str(ty);
            }
        },
        "clippy::redundant_allocation" => {
            if message.contains("<&") {
                code.push_str(" ref");
            }
        },
        "clippy::ptr_arg" => {
            // &mut String arg is reasonable, even where &mut str would do
            if message.contains("`&mut ") && !message.contains("`&mut Box") {
                code.push_str(if message.contains("&mut Vec") { " mutvec" } else { " mut" });
            }
            else if message.contains("`&Vec") {
                code.push_str(" vec");
            }
        },
        "clippy::used_underscore_binding" | "used_underscore_binding" => {
            if let Some(rest) = message.strip_prefix("used binding `_") {
                if rest.starts_with(|c: char| c.is_ascii_digit()) { // `_3d`
                    code.clear();
                }
            }
        },
        _ => {},
    }
    (code, message)
}

fn known_warning(text: &str) -> bool {
    text == "expected identifier" ||
    text.starts_with("aborting due to") ||
    text.ends_with("warnings emitted") ||
    text.ends_with("warning emitted")
}

struct Batch<'a> {
    batch: HashMap<SmolStr, CrateToRun>,
    links: HashSet<SmolStr>,
    clippy_db: &'a ClippyDb,
    kitchen_sink: &'a KitchenSink,
    lookup: &'a CratesIoIndexLookup,
    already_tried: HashSet<SmolStr>,
    // adding deps searches same things a lot
    already_tried_ver: HashSet<String>,
    built_versions: HashMap<SmolStr, SemVer>,
    built_versions_nightly: HashMap<SmolStr, SemVer>,

    rng: SmallRng,

    pub borked: HashSet<SmolStr>,
}

#[derive(Debug)]
enum AddResult {
    Added(Origin),
    Done,
    Incompatible,
    Dupe,
    Sucks,
}

impl Batch<'_> {
    async fn add_one(&mut self, crate_version: &Version) -> Result<AddResult, anyhow::Error> {
        running()?;

        if self.borked.contains(crate_version.name()) {
            return Ok(AddResult::Incompatible);
        }

        if self.already_tried.contains(crate_version.name()) {
            return Ok(AddResult::Dupe);
        }

        if self.can_add_to_batch(crate_version).is_none() {
            return Ok(AddResult::Incompatible);
        }

        let origin = Origin::from_crates_io_name(crate_version.name());
        let rank = self.kitchen_sink.crate_ranking_for_builder(&origin).unwrap_or(0.3);
        if rank < self.rng.gen_range(0.1..0.4) {
            return Ok(AddResult::Sucks);
        }

        if !self.already_tried_ver.insert(format!("{}-{}", crate_version.name(), crate_version.version())) {
            return Ok(AddResult::Dupe);
        }

        let version = SemVer::parse(crate_version.version())?;
        if self.clippy_db.version_built(&origin)?.is_some_and(|old| old >= version) {
            debug!("{}@{version} already done", crate_version.name());
            return Ok(AddResult::Done);
        }

        let krate = self.kitchen_sink.rich_crate_async(&origin).await?;

        if !self.already_tried.insert(crate_version.name().into()) {
            return Ok(AddResult::Dupe);
        }

        if self.rng.gen_bool(0.9) {
            let krate_ver = self.kitchen_sink.rich_crate_version_stale_is_ok(&origin).await?;
            if krate_ver.category_slugs().iter().any(|s| s == "cryptography::cryptocurrencies") {
                return Ok(AddResult::Sucks);
            }
        }

        info!("https://lib.rs/compat/{}#{version} needs to be built", crate_version.name());

        if let Some(l) = crate_version.links() {
            self.links.insert(l.into());
        }
        self.batch.insert(krate.name().into(), CrateToRun { krate, version });

        self.add_dependencies(crate_version).await?;

        Ok(AddResult::Added(origin))
    }

    async fn add_dependencies(&mut self, crate_version: &Version) -> Result<(), anyhow::Error> {
        for dep in crate_version.dependencies().iter().filter(|d| d.kind() != DependencyKind::Dev && !d.is_optional() && d.target().is_none()).take(50) {
            if self.batch.contains_key(dep.crate_name()) {
                continue;
            }
            if let Some(c) = self.lookup.get(&dep.crate_name().as_ascii_lowercase()) {
                let req = VersionReq::parse(dep.requirement())?;
                if let Some(dep_ver) = c.versions().iter().rev().take(50).find(|&v| !v.is_yanked() && v.version().parse().ok().is_some_and(|v| req.matches(&v))) {
                    match Box::pin(self.add_one(dep_ver)).await {
                        Ok(AddResult::Added(_)) => {
                            debug!("Added dependency https://lib.rs/crates/{}#{}", dep_ver.name(), dep_ver.version());
                        },
                        Ok(AddResult::Dupe | AddResult::Sucks) => {},
                        Ok(_reason) => {
                            // info!("Skipped dep https://lib.rs/crates/{}#{}, because {reason:?}", dep_ver.name(), dep_ver.version());
                            Box::pin(self.add_dependencies(dep_ver)).await.context("Can't add deps recursively")?;
                        },
                        Err(e) => warn!("Can't add dep https://lib.rs/crates/{} to batch: {e}", dep_ver.name()),
                    }
                }
            }
            if self.batch.len() > 150 {
                break;
            }
        }
        Ok(())
    }

    async fn add(&mut self, crate_version: &Version) -> Result<bool, anyhow::Error> {
        let AddResult::Added(origin) = self.add_one(crate_version).await? else {
            return Ok(false);
        };

        // proc macros need a user
        if self.batch.len() < 25 || is_a_proc_macro(crate_version) {
            if let Some(rev) = self.kitchen_sink.index()?.deps_stats()?.crates_io_full_dependents_stats_of(&origin) {
                let self_mut = RefCell::new(self);
                // adding influences selecting, so add some first
                for try_size in [2, 25] {
                    let mut candidates: Vec<_> = rev.rev_dep_names_default.iter()
                        .take(150)
                        .filter_map(|rev_name| {
                            self_mut.borrow().lookup.get(&rev_name.as_ascii_lowercase())
                        })
                        .filter_map(|rev| {
                            // take 5few latest to avoid linearly searching and parsing too much
                            rev.versions().iter().rev().take(3).find_map(|v| {
                                // don't add sys crates, they're too hard to build
                                if v.is_yanked() || v.links().is_some() || v.dependencies().iter().any(|d| d.name().ends_with("-sys")) {
                                    return None;
                                }
                                let new_deps = self_mut.borrow_mut().can_add_to_batch(v)?;
                                Some((new_deps, v))
                            })
                        })
                        .take(try_size)
                        .collect();
                    candidates.sort_by_key(|c| c.0);
                    candidates.truncate(try_size/2);

                    for (_, parent) in candidates {
                        let res = self_mut.borrow_mut().add_one(parent).await;
                        match res {
                            Ok(AddResult::Added(_)) => {
                                debug!("Added parent {}@{}", parent.name(), parent.version());
                            },
                            Ok(reason) => debug!("Skipped parent {}@{}, because {reason:?}", parent.name(), parent.version()),
                            Err(e) => warn!("Can't add {} to batch: {e}", parent.name()),
                        }
                    }
                }
            }
        }
        Ok(true)
    }

    // true if some, returns number of new direct deps it will cost
    fn can_add_to_batch(&self, crate_version: &Version) -> Option<usize> {
        if self.batch.contains_key(crate_version.name()) || self.already_tried.contains(crate_version.name()) || self.borked.contains(crate_version.name()) {
            return None;
        }

        if matches!(crate_version.name(), "syn" | "quote" | "proc-macro-error" | "proc-macro2") {
            return None;
        }

        if let Some(l) = crate_version.links() {
            if self.links.contains(l) {
                return None;
            }
        }

        let mut new_deps = 0;
        let can = crate_version.dependencies().iter()
            .filter(|&d| d.kind() != DependencyKind::Dev) // even optional ones must match
            .take(1000)
            .filter_map(|d| {
                new_deps += 1;
                let existing = self.batch.get(d.crate_name())?;
                new_deps -= 1;
                let req = d.requirement();
                if req.starts_with('=') || req.contains(',') {
                    new_deps += 2; // increase cost of problematic requirements
                }
                Some((existing, req.parse::<VersionReq>().ok()?))
            })
            .all(|(existing, req)| req.matches(&existing.version));

        can.then_some(new_deps)
    }

    pub fn take(&mut self) -> Vec<CrateToRun> {
        self.already_tried_ver.clear();
        self.links.clear();
        self.batch.drain().map(|(_, v)| v).collect()
    }
}

fn is_a_proc_macro(crate_version: &Version) -> bool {
    crate_version.name().contains("derive") || crate_version.name().contains("proc-macro") ||
        crate_version.dependencies().iter().any(|d| d.crate_name().starts_with("proc-macro"))
}

pub(crate) async fn clippy_builds(crates: Arc<KitchenSink>, build_db: BuildDb, clippy_db: ClippyDb, all_crates: &mut dyn Iterator<Item = &Crate>) -> Result<(), anyhow::Error> {
    let jobs_dir = prepare_dirs().context("Can't set dirs for clippy")?;

    let mut docker = Docker::new("clippy", TEMP_JUNK_DIR.into());

    docker.set_concurrency(6);
    docker.set_env("CARGO_INCREMENTAL", "0");
    docker.set_env("CARGO_HOME", "/home/rustyuser/.cargo");
    docker.set_env("CARGO_TARGET_DIR", "/home/rustyuser/target");
    docker.set_env("RUSTUP_TOOLCHAIN", "stable");

    docker.set_env("DOCS_RS", "1"); // some sys deps skip heavy compile
    docker.set_env("CFLAGS", "-O0");
    docker.set_env("CXXFLAGS", "-O0");

// note which crates failed to build, and avoid adding them to batches in the future

    docker.build(&format!("{DOCKERFILE_COMMON}\n{DOCKERFILE}"))?;
    let mut batch = Batch {
        batch: Default::default(),
        already_tried: Default::default(),
        already_tried_ver: Default::default(),
        borked: Default::default(),
        links: Default::default(),
        kitchen_sink: &crates,
        clippy_db: &clippy_db,
        lookup: &*crates.index()?.crates_io_crates()?,
        built_versions: HashMap::default(),
        built_versions_nightly: HashMap::default(),
        rng: SmallRng::from_entropy(),
    };

    let mut ideal_batch_size = 20;
    let only_never_built_crates = false;

    let mut to_retry = Vec::new();
    let mut todo = Vec::with_capacity(1000);
    let mut ignored_early = 0;

    loop {
        while let Some(c) = all_crates.next() {
            let Some(v) = c.highest_normal_version() else {
                continue;
            };

            if only_never_built_crates {
                let Ok(all) = crates.rich_crate_async(&Origin::from_crates_io_name(v.name())).await else { continue };
                let Ok(comp) = crates.rustc_compatibility(&all).await else { continue };
                if comp.values().any(|c| c.has_ever_built()) {
                    info!("{:?} has built; boring", all.origin());
                    ignored_early += 1;
                    continue;
                }
            }

            todo.push(v);
            if todo.len() + ignored_early > 1000 {
                ignored_early = 0;
                break;
            }
        }
        running()?;

        if todo.is_empty() {
            assert!(all_crates.next().is_none());
            break;
        }

        // maybe it'll group related crates together?
        todo.sort_by(|a,b| a.name().cmp(b.name()));

        if crate::out_of_disk_space() {
            error!("Stopping early due to disk space");
            let _ = std::fs::remove_dir_all(Path::new(TEMP_JUNK_DIR).join("target"));
            let _ = std::fs::remove_dir_all(Path::new(TEMP_JUNK_DIR).join("target-nightly"));
            let _ = std::fs::remove_dir_all(Path::new(TEMP_JUNK_DIR).join("registry"));
            anyhow::bail!("disk space");
        }

        while let Some(v) = todo.pop() {
            if !batch.add(v).await.inspect_err(|e| error!("Error adding to batch {}: {e}", v.name())).unwrap_or(false) {
                log::trace!("ignored https://lib.rs/compat/{}", v.name());
                continue;
            };
            to_retry.push(v);

            if batch.batch.len() < ideal_batch_size {
                continue;
            }

            info!("Will build https://lib.rs/crates/{}@{} ({} total)", v.name(), v.version(), batch.batch.len());
            let was_ok = batch.start_build(&docker, &jobs_dir, &crates, &clippy_db, &build_db).await?;
            if was_ok {
                to_retry.clear();
            } else {
                to_retry.shuffle(&mut batch.rng);
                to_retry.truncate(to_retry.len()/2);
                for v in &to_retry {
                    debug!("will retry {}@{}", v.name(), v.version());
                }
                todo.append(&mut to_retry);
            }
            if !was_ok && ideal_batch_size > 5 {
                ideal_batch_size /= 2;
            } else if ideal_batch_size < 20 {
                ideal_batch_size += 3;
            }
        }
    }
    debug!("Last batch finishing");
    batch.start_build(&docker, &jobs_dir, &crates, &clippy_db, &build_db).await?;
    info!("Clippy is done");
    Ok(())
}

impl Batch<'_> {
async fn start_build(&mut self, docker: &Docker, jobs_dir: &PathBuf, crates: &KitchenSink, clippy_db: &ClippyDb, build_db: &BuildDb) -> Result<bool, anyhow::Error> {
    running()?;

    let mut list = self.take();
    if list.is_empty() {
        return Ok(true);
    }
    let opts = BatchOptions {
        jobs_dir,
        keep_going: true,
        default_features: true,
        nightly: false,
    };

    list.shuffle(&mut self.rng);
    let mut was_ok = true;
    match build_batch(&list, &self.built_versions, docker, &opts, crates).await {
        Ok(mut results_by_crate) => {
            if results_by_crate.is_empty() {
                was_ok = false;
                error!("NOTHING out of {} batch succeeded", list.len());
            }
            save_batch_results(&results_by_crate, clippy_db, build_db, &mut self.built_versions)?;
            // rebuilding entire large batches wastes too much time
            let mut retries = 0;
            let retries_max = (1+list.len()/8).min(3);

            let failed_builds: Vec<_> = list.iter().filter_map(|c| {
                let key = (c.krate.origin().clone(), c.version.clone());
                if !results_by_crate.contains_key(&key) {
                    Some((key, c))
                } else {
                    None
                }
            }).collect();
            for (key, c) in &failed_builds {
                if results_by_crate.contains_key(key) {
                    debug!("{key:?} has been fixed in retry");
                    continue;
                }
                running()?;
                retries += 1;
                // TODO: detect if it's a proc macro - won't build by itself
                warn!("Crate in batch did not build: https://lib.rs/crates/{}#{}", c.krate.name(), c.version);
                let quick_opts = &BatchOptions {
                    jobs_dir,
                    keep_going: false,
                    default_features: true,
                    nightly: true,
                };
                match build_batch(std::slice::from_ref(c), &self.built_versions_nightly, docker, quick_opts, crates).await {
                    Ok(redone) => {
                        save_batch_results(&redone, clippy_db, build_db, &mut self.built_versions_nightly)?;
                        if !redone.contains_key(key) {
                            self.borked.insert(c.krate.name().into());
                            error!("https://lib.rs/crates/{} is not visible in results", c.krate.name());
                            let fatal = [("lib.rs builder".into(), ("ignored?".into(), 1))].into_iter().collect();
                            clippy_db.set(c.krate.origin(), &c.version, &Default::default(), &fatal).context("DB write failed")?;
                        }
                        results_by_crate.extend(redone); // maybe this build also fixed other missing builds
                    },
                    Err(e) => {
                        running()?;
                        self.borked.insert(c.krate.name().into());
                        error!("https://lib.rs/crates/{} can't be built, marking as done anyway: {e}", c.krate.name());
                        let fatal = [("lib.rs builder".into(), (e.to_string(), 1))].into_iter().collect();
                        clippy_db.set(c.krate.origin(), &c.version, &Default::default(), &fatal).context("DB write failed")?;
                    }
                }
                if retries >= retries_max {
                    break; // enough
                }
            }
        },
        Err(e) => {
            for e in e.chain() {
                error!("Batch failed: {e}");
            }
            was_ok = false;
            warn!("No results from the batch");
        }
    }
    Ok(was_ok)
}
}

fn save_batch_results(by_crate: &HashMap<(Origin, SemVer), ClippyResults>, clippy_db: &ClippyDb, build_db: &BuildDb, built_versions: &mut HashMap<SmolStr, SemVer>) -> Result<(), anyhow::Error> {
    if by_crate.is_empty() {
        error!("All crates in the batch have failed, there are no results");
        return Ok(())
    }
    let mut compat = Vec::new();
    for ((origin, ver), res) in by_crate {
        if let Some(rustc_version) = res.built_rustc {
            if res.errors.is_empty() {
                compat.push(SetCompatMulti {
                    origin,
                    ver,
                    rustc_version,
                    compat: Compat::VerifiedWorks,
                    reason: "clippy"
                });
                built_versions.insert(origin.package_name_icase().into(), ver.clone());
            } else {
                error!("Inconsistent results for {origin:?}@{ver}: built {rustc_version} but {:#?}", res.errors);
            }
        }
        if let Some(rustc_version) = res.edition.and_then(edition_incompatible_rustc) {
            compat.push(SetCompatMulti {
                origin,
                ver,
                rustc_version,
                compat: Compat::DefinitelyIncompatible,
                reason: "clippy^edition",
            });
        }
        for (rustc_version, msg) in res.warnings.iter().chain(res.errors.iter())
            .filter_map(|(code, (msg, _))| Some((code.strip_prefix("clippy::incompatible_msrv ")?, msg)))
            .filter_map(|(ver, msg)| Some((ver.parse::<u16>().inspect_err(|e| error!("msrv {e}")).ok()?, msg))) {
            compat.push(SetCompatMulti {
                origin,
                ver,
                rustc_version: rustc_version.saturating_sub(1),
                compat: if res.ran_build_script && rustc_version > 40 {
                    // not deps, but this is least certain setting.
                    // and it's uncertain, because crates can detect rust ver
                    Compat::BrokenDeps
                } else if rustc_version < 60 { // lower risk of mistake
                    Compat::DefinitelyIncompatible
                } else {
                    Compat::LikelyIncompatible
                },
                reason: msg,
            });
        }
        if !res.warnings.is_empty() || !res.errors.is_empty() {
            debug!("{origin:?} {} warnings; {} errors", res.warnings.len(), res.errors.len());
        }
        clippy_db.set(origin, ver, &res.warnings, &res.errors).context("DB write failed")?;
    }
    build_db.set_compat_multi(&compat).context("Build DB write failed")?;
    info!("Built {} crates, reported {} compat", by_crate.len(), compat.len());
    Ok(())
}

/// Remove conflicting and unnecessary options, add lints
fn rewrite_dep_cargo_toml(crates: &KitchenSink, in_dir: &Path) -> Result<bool, anyhow::Error> {
    let manifest_path = in_dir.join("Cargo.toml");
    let mut manifest = cargo_toml::Manifest::from_path(&manifest_path)
        .with_context(|| format!("Can't read manifest in {in_dir:?}"))?;

    // while we have it, we may as well check it
    if let Some(package) = &manifest.package {
        crates.index_msrv_from_manifest(&Origin::from_crates_io_name(package.name()), &manifest)?;
    }

    // skip building proc macros directly
    if manifest.lib.as_ref().is_some_and(|lib| lib.proc_macro) {
        return Ok(false);
    }

    let features_for_dev_deps: HashSet<SmolStr> = manifest.features.values().flatten().filter(|f| {
        let name = f.trim_start_matches("dep:");
        let name = name.split_once(['/','?']).map(|(n,_)| n).unwrap_or(name);
        let known = manifest.features.contains_key(name) ||
            manifest.dependencies.contains_key(name) ||
            manifest.build_dependencies.contains_key(name) ||
            manifest.target.values().any(|t| t.dependencies.contains_key(name) || t.build_dependencies.contains_key(name));
        !known
    }).take(300).map(From::from).collect();
    if !features_for_dev_deps.is_empty() {
        debug!("must remove dev features {features_for_dev_deps:?}");
        manifest.features.values_mut().for_each(|f| {
            f.retain(|f| !features_for_dev_deps.contains(f.as_str()));
        });
    }

    manifest.workspace = None;
    manifest.dev_dependencies = Default::default();
    #[allow(deprecated)] {
        manifest.replace = Default::default();
    }
    manifest.patch = Default::default();
    manifest.profile = Default::default();
    manifest.bench = Default::default();
    manifest.test = Default::default();
    manifest.example = Default::default();
    manifest.lints = Some(Lints {
        workspace: true,
        groups: Default::default(),
    });
    for t in manifest.target.values_mut() {
        t.dev_dependencies = Default::default();
    }


    if let Some(package) = &mut manifest.package {
        package.resolver = None;
        package.default_run = None;

        let min_msrv = package.edition().min_rust_version_minor().max(20);
        package.set_rust_version(Some(format!("1.{min_msrv}")));
        package.workspace = None;
        package.autoexamples = false;
        package.autotests = false;
        package.autobenches = false;
    }

    let m = toml::to_string(&manifest).context("Can't serialize new cargo toml")?;
    std::fs::write(manifest_path, m).context("Can't write new cargo toml")?;

    Ok(true)
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Hash)]
struct LockfilePackage {
    name: String,
    version: String,
    source: Option<String>,
}

#[derive(Serialize, Deserialize)]
struct Lockfile {
    #[serde(default)]
    version: u32,
    package: Vec<LockfilePackage>,
}


async fn make_workspace(kitchen_sink: &KitchenSink, crates: &[CrateToRun], alread_built_versions: &HashMap<SmolStr, SemVer>, host_dest_dir: &Path, _vm_dest_dir: &Path) -> Result<(), anyhow::Error> {
    let _ = std::fs::create_dir_all(host_dest_dir);
    let crates = try_join_all(crates.iter().map(|c| async move {
        debug!("decompressing https://lib.rs/crates/{} to {host_dest_dir:?}", c.krate.name());
        kitchen_sink.decompress_crate_tarball(c.krate.name(), &c.version.to_string(), host_dest_dir).await?;
        let subdir = format!("{}-{}", c.krate.name(), c.version);
        if subdir.contains(['"', '%', '[', '*', '?', '/', '\\']) || subdir.contains("..") {
            anyhow::bail!("hacky crate name {subdir}");
        }
        let host_dir = host_dest_dir.join(&subdir);
        if !host_dir.starts_with(host_dest_dir) {
            anyhow::bail!("invalid dir {host_dir:?} in {host_dest_dir:?}");
        }
        Ok::<_, anyhow::Error>((c, host_dir, subdir))
    })).await.context("Can't get crate tarballs for a ws")?;

    let all_ws_crate_names = crates.iter().map(|(c, _, _)| c.krate.name()).collect::<HashSet<_>>();

    let mut members = String::new();
    let mut patches = String::new();
    let mut combined_lockfile = HashMap::<SmolStr, LockfilePackage>::default();
    for (c, host_dir, rel_dir) in &crates {
        use std::fmt::Write;

        let keep = rewrite_dep_cargo_toml(kitchen_sink, host_dir).with_context(|| format!("toml of {}-{}", c.krate.name(), c.version))?;
        if !keep {
            debug!("Skipping unsupported manifest of https://lib.rs/crates/{}", c.krate.name());
            continue;
        }
        writeln!(&mut members, "\"{rel_dir}\",")?;
        writeln!(&mut patches, "{}.path = \"{rel_dir}\"", c.krate.name())?;

        let crate_cargo_toml_path = host_dir.join("Cargo.toml");
        if let Ok(toml) = std::fs::read_to_string(&crate_cargo_toml_path) {
            // nightly lint breaks builds with stable!
            if toml.contains("dangling_pointers_from_temporaries") {
                let toml = toml.lines().filter(|l| !l.contains("dangling_pointers_from_temporaries")).join("\n");
                std::fs::write(&crate_cargo_toml_path, toml)?;
                let _ = chownperm(&crate_cargo_toml_path, false);
            }
        }

        let lock = std::fs::read_to_string(host_dir.join("Cargo.lock")).ok()
            .and_then(|l| toml::from_str::<Lockfile>(&l).inspect_err(|e| warn!("lock {e}")).ok());
        if let Some(lock) = lock {
            let lockfile_packages = lock.package.into_iter()
                .filter(|p| p.source.as_deref().map_or(true, |s| s == "registry+https://github.com/rust-lang/crates.io-index"))
                .filter(|p| !all_ws_crate_names.contains(p.name.as_str()))
                .take(500);
            for mut p in lockfile_packages {
                let Ok(mut lockfile_semver) = SemVer::parse(&p.version) else {
                    continue;
                };
                if let Some(already_built) = alread_built_versions.get(p.name.as_str()) {
                    // prefer versions already built (in cargo cache)
                    if lockfile_semver != *already_built {
                        p.version = already_built.to_string();
                        lockfile_semver.clone_from(already_built);
                    }
                }
                match combined_lockfile.entry(SmolStr::from(&p.name)) {
                    Entry::Occupied(mut e) => if e.get().version.parse().map_or(true, |ver: SemVer| ver < lockfile_semver) {
                        e.insert(p);
                    },
                    Entry::Vacant(e) => {
                        e.insert(p);
                    },
                }
            }

        }
    }

    std::fs::write(host_dest_dir.join("Cargo.toml"), format!(r#"
[workspace]
resolver = "2"
members = [
{members}
]

{STANDARD_LINTS}

[profile.dev]
debug = 0
debug-assertions = false
overflow-checks = false
incremental = false
codegen-units = 8
panic = "abort"
strip = "none"

[profile.dev.build-override]
debug = 0
debug-assertions = false
overflow-checks = false
incremental = false
codegen-units = 8
strip = "none"

[profile.dev.package."*"]
debug = 0
debug-assertions = false
overflow-checks = false
incremental = false
codegen-units = 8
strip = "none"

[patch.crates-io]
{patches}
"#))?;

    if !combined_lockfile.is_empty() {
        let lockfile = toml::to_string(&Lockfile {
            version: 3,
            package: combined_lockfile.into_values().collect(),
        }).context("new lockfile")?;
        debug!("Lockfile {lockfile}");
        let cargo_lock_path = host_dest_dir.join("Cargo.lock");
        if let Err(e) = cargo_lock_path.try_exists() {
            panic!("workspace permissions are not set up correctly: {e}");
        }
        std::fs::write(&cargo_lock_path, lockfile).context("lockfile write")?;
        let _ = chownperm(&cargo_lock_path, true);
        assert!(cargo_lock_path.try_exists().expect("borked perms"));
    }
    Ok(())
}

fn prepare_dirs() -> Result<PathBuf, anyhow::Error> {
    for &p in &["clippy inputs", "target", "target-nightly", "registry/src", "registry/index/github.com-1ecc6299db9ec823/.cache"] {
        let p = Path::new(TEMP_JUNK_DIR).join(p);
        let _ = std::fs::remove_dir_all(p);
    }

    for &(p, rw) in &[("git", false), ("registry", false), ("target", false), ("target-nightly", false), ("clippy inputs", true)] {
        let p = Path::new(TEMP_JUNK_DIR).join(p);
        std::fs::create_dir_all(&p).with_context(|| format!("Can't create dir {p:?}"))?;
        let _ = chownperm(&p, rw).inspect_err(|e| warn!("chmod fail {p:?} {e}"));
        let _ = Command::new("chmod").arg("a+rwX").arg(&p).status();
        let _ = Command::new("chown").arg("4321:4321").arg(&p).status();
    }

    let jobs_dir = Path::new(TEMP_JUNK_DIR).join("clippy inputs");
    Ok(jobs_dir)
}
