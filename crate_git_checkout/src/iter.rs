use util::FxHashSet as HashSet;
use git2::{Commit, Oid};
use std::cmp::Ordering;
use std::collections::BinaryHeap;

pub struct HistoryIter<'repo> {
    seen: HashSet<Oid>,
    to_visit: BinaryHeap<Generation<'repo>>,
}

pub struct HistoryItem<'repo> {
    pub commit: Commit<'repo>,
    pub is_merge: bool,
}

struct Generation<'repo> {
    num: u32,
    nth: u32,
    commit: Commit<'repo>,
}

impl<'repo> HistoryIter<'repo> {
    #[must_use] pub fn new(start: Commit<'repo>) -> Self {
        let mut to_visit = BinaryHeap::with_capacity(16);
        to_visit.push(Generation{
            commit:start,
            num:0, nth:0,
        });
        Self {
            seen: HashSet::with_capacity_and_hasher(500, Default::default()),
            to_visit,
        }
    }
}

impl<'repo> Iterator for HistoryIter<'repo> {
    type Item = HistoryItem<'repo>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(Generation { commit, num, .. }) = self.to_visit.pop() {
            let seen = &mut self.seen; // technically needed only after merges
            let mut is_merge = false;
            self.to_visit.extend(commit.parents()
                .take(1)
                .filter(|commit| {
                    seen.insert(commit.id())
                })
                .enumerate()
                .map(|(nth, commit)| {
                    if nth > 0 {is_merge = true;}
                    Generation {num: num+1, nth: nth as u32, commit}
                }));
            Some(HistoryItem {
                commit, is_merge,
            })
        } else {
            None
        }
    }
}

impl PartialEq for Generation<'_> {
    fn eq(&self, other: &Generation<'_>) -> bool {
        other.num == self.num && self.nth == other.nth
    }
}
impl PartialOrd for Generation<'_> {
    fn partial_cmp(&self, other: &Generation<'_>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl Eq for Generation<'_> {}
impl Ord for Generation<'_> {
    fn cmp(&self, other: &Generation<'_>) -> Ordering {
        other.num.cmp(&self.num).then(other.nth.cmp(&self.nth))
    }
}
