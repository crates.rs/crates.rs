pub use rustsec::advisory::Severity;
use rustsec::database::Query;
use rustsec::repository::git::DEFAULT_URL;
pub use rustsec::{Advisory, Error};
use rustsec::{Collection, Database, Repository};
use std::path::{Path, PathBuf};
use std::time::Duration;

pub struct RustSec {
    path: PathBuf,
    db: Database,
}

impl RustSec {
    pub fn new(root: &Path) -> Result<Self, Error> {
        let path = root.join("rustsec");
        let db = Self::db_at_path(&path)?;
        Ok(Self { path, db })
    }

    pub fn update(&mut self) -> Result<(), Error> {
        self.db = Self::db_at_path(&self.path)?;
        if self.advisories_for_crate("libwebp-sys2").is_empty() {
            return Err(Error::new(rustsec::ErrorKind::Registry, &"rustsec repo missing expected advisory"));
        }
        Ok(())
    }

    fn db_at_path(path: &Path) -> Result<Database, Error> {
        // this is a dangerous lock that will get left behind and cause pages to time out
        let r = if let Ok(r) = Repository::fetch(DEFAULT_URL, path, true, Duration::from_secs(45)) { r } else {
            let _ = std::fs::remove_file(path.join("rustsec.rustsec.lock"));
            let _ = std::fs::remove_file(path.parent().unwrap().join("rustsec.rustsec.lock"));
            let _ = std::fs::remove_file(path.parent().unwrap().join("rustsec..lock"));
            let _ = std::fs::remove_dir_all(path);
            Repository::fetch(DEFAULT_URL, path, true, Duration::from_secs(45))?
        };
        log::info!("Fetched rustsec from {} at {}", DEFAULT_URL, path.display());
        Database::load_from_repo(&r)
    }

    #[must_use] pub fn advisories_for_crate(&self, crate_name: &str) -> Vec<&Advisory> {
        self.db.query(&Query::new()
            .collection(Collection::Crates)
            .package_source(Default::default())
            .package_name(crate_name.parse().unwrap())
        )
    }
}


#[test]
fn rustsec_test() {
    let path = Path::new("../data");
    assert!(path.exists());
    let d = RustSec::new(path).unwrap();
    let a = d.advisories_for_crate("rgb");
    assert_eq!(1, a.len());
    let a = d.advisories_for_crate("libwebp-sys2");
    assert_eq!(1, a.len());
}

#[test]
fn severity_sort() {
    assert!(Severity::High > Severity::Low);
}
