use std::fs::File;
use std::io::BufWriter;
use std::io::Write;

use chrono::prelude::*;
use chrono::Days;

fn main() {
    env_logger::init();
    let db = kitchen_sink::AllDownloads::new("data");

    let current_year = Utc::now().year() as u16;
    for year in 2015..=current_year {
        let mut out = BufWriter::new(File::create(format!("downloads_{year}.csv")).unwrap());
        out.write_all(b"crate_name,version").unwrap();
        let mut y_date = NaiveDate::from_ymd_opt(year.into(), 1, 1).unwrap();
        loop {
            write!(&mut out, "{}", y_date.format(",%F")).unwrap();
            match y_date.checked_add_days(Days::new(1)) {
                Some(next) if next.year() as u16 == year => y_date = next,
                _ => break,
            }
        }
        out.write_all(b"\n").unwrap();
        let mut all = Vec::new();

        db.for_each_in_year(year, |name, dl| {
            all.push((name.clone(), dl));
        }).unwrap();

        all.sort_by_cached_key(|(_, dl)| {
            !dl.values().map(|v| u64::from(v.0.iter().copied().max().unwrap_or(0))).sum::<u64>()
        });

        let mut x = false;
        for (name, dl) in all {
            let mut dl = dl.into_iter().collect::<Vec<_>>();
            dl.sort_by_cached_key(|(_, dl)| dl.0.iter().copied().map(u64::from).sum::<u64>());
            x = !x;
            if x {
                dl.reverse();
            }

            debug_assert!(!name.contains(','));
            for (ver, dl) in &dl {
                let ver = ver.to_semver();
                debug_assert!(!ver.to_string().contains(','));

                let useless_end = dl.0.iter().rev().take_while(|&&v| v == 0).count();
                let dl = &dl.0[..dl.0.len() - useless_end];
                if dl.is_empty() {
                    continue;
                }
                write!(&mut out, "{name},{ver}").unwrap();
                for &num in dl {
                    if num > 0 {
                        write!(&mut out, ",{num}").unwrap();
                    } else {
                        out.write_all(b",").unwrap();
                    }
                }
                out.write_all(b"\n").unwrap();
            }
        }
    }
}
