use util::FxHashMap as HashMap;
use std::io;
use std::path::Path;

pub async fn refresh_usage(path: &Path) -> Result<(), anyhow::Error> {
    // https://p.datadoghq.com/sb/3a172e20-e9e1-11ed-80e3-da7ad0900002-973f4c1011257befa8598303217bfe3a
    let url = "https://p.datadoghq.com/dashboard/shared_widget_update/3a172e20-e9e1-11ed-80e3-da7ad0900002-973f4c1011257befa8598303217bfe3a/3303264091969212?preferred_time_frame=1w";
    let json = reqwest::get(url).await?.error_for_status()?.text().await?;
    let data = scrap(&json).ok_or_else(|| anyhow::format_err!("Can't find data in JSON\n{json}"))?;
    dump_csv(path, &data)?;
    Ok(())
}

fn dump_csv(path: &Path, data: &HashMap<(u16, bool), u64>) -> io::Result<()> {
    let mut csv: Vec<String> = data.iter().map(|((ver, is_nightly), cnt)| {
        format!("{},{},{}\n", ver, if *is_nightly { "n" } else { "" }, cnt)
    }).collect();
    csv.sort_unstable();
    let csv = csv.into_iter().collect::<String>();
    std::fs::write(path, csv)
}

fn scrap(data: &str) -> Option<HashMap<(u16, bool), u64>> {
    let data: serde_json::Value = serde_json::from_str(data).unwrap();
    let mut out = HashMap::with_capacity_and_hasher(100, Default::default());
    for c in inputs_iter(&data) {
        let label = c.get("defaultLabel").and_then(|n| n.as_str()).and_then(|l| l.strip_prefix("cargo.version:"));
        let cnt = c.get("value").and_then(|v| v.as_i64());
        if let Some((ver, cnt)) = label.zip(cnt) {
            let is_nightly = ver.contains("nightly");
            let ver: Option<u16> = ver.split('.').nth(1).and_then(|v| v.parse().ok());
            if let Some(ver) = ver {
                *out.entry((ver, is_nightly)).or_insert(0u64) += cnt as u64;
            }
        }
    }
    Some(out).filter(|o| !o.is_empty())
}

fn inputs_iter(data: &serde_json::Value) -> impl Iterator<Item=&serde_json::Value> {
    data.get("responses").and_then(|r| r.as_object()).into_iter().flat_map(|r| r.values())
        .filter_map(|r| r.as_array()).flat_map(|a| a.iter())
        .filter_map(|a| a.get("data")?.as_array())
        .flat_map(|d| d.iter())
        .filter_map(|a| a.get("attributes")?.get("inputs")?.as_array())
        .flat_map(|d| d.iter())
}

#[tokio::test]
async fn refresh_usage_data_test() {
    refresh_usage("/tmp/usage-test.csv".as_ref()).await.unwrap();
}
