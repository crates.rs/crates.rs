use crate::index::{ArcDepSetInterned, Dep, DepNameVersionSym, DepQuery, DepSetInterned, ICrate, Index, MiniVer};
use crate::{CratesIoIndexLookup, DepsErr, IVersion, Origin};
use util::FxHashMap as HashMap;
use crates_index::{Crate, Version};
use lasso::{Spur as Sym, ThreadedRodeo as StringInterner};
use log::info;
use rayon::prelude::*;
use semver::VersionReq;
use std::time::Instant;
use triomphe::Arc;
use util::{CowAscii, FxHashMap, FxHashSet, PushString, SmolStr};

pub type DepInfMap = HashMap<SmolStr, (AddDepsAs, MiniVer)>;

pub struct DepsStats {
    pub total: usize,
    pub counts: HashMap<SmolStr, RevDependencies>,
}

impl DepsStats {
    /// For crate being outdated. Returns (is_latest, popularity)
    /// 0 = not used/deprecated/undesirable
    /// 1 = everyone uses it
    pub fn version_popularity_raw(&self, krate: &Crate, requirement: &VersionReq) -> Result<Option<(bool, f32)>, DepsErr> {
        fn matches(ver: &Version, req: &VersionReq) -> bool {
            ver.version().parse().ok().is_some_and(move |ver| req.matches(&ver))
        }

        let matches_latest = matches(CratesIoIndexLookup::find_highest_crates_io_version(krate, true), requirement) ||
            // or match latest unstable
            matches(CratesIoIndexLookup::find_highest_crates_io_version(krate, false), requirement);

        let pop = self.counts.get(&*krate.name().as_ascii_lowercase())
        .map_or(0., |stats| {
            let mut matches = 0;
            let mut matches_max = 0;
            let mut unmatches = 0;
            let mut unmatches_max = 0;
            for (ver, count) in &stats.versions {
                if requirement.matches(&ver.to_semver()) {
                    matches += count; // TODO: this should be (slighly) weighed by crate's popularity?
                    matches_max = matches_max.max(*count);
                } else {
                    unmatches += count;
                    unmatches_max = unmatches_max.max(*count);
                }
            }
            matches += 1; // one to denoise unpopular crates; div/0
            matches_max += 1; // one to denoise unpopular crates; div/0

            let real_ratio = f64::from(matches) / f64::from(matches + unmatches);

            // when major versions are shifting in popularity, matching second-most-popular crate
            // is still not that terrible.
            // This also helps with crates that have very fragmented users and no
            // version is popular.
            let ratio_of_maxes = f64::from(matches_max) / f64::from(matches_max + unmatches_max);

            ((real_ratio + ratio_of_maxes) * 0.5) as f32
        });

        Ok(Some((matches_latest, pop)))
    }

    /// How likely it is that this exact crate will be installed in any project
    pub fn version_global_popularity(&self, crate_name: &str, version: &MiniVer) -> Result<Option<f32>, DepsErr> {
        match crate_name {
            // bindings' SLoC looks heavier than actual overhead of standard system libs
            "libc" | "winapi" | "kernel32-sys" | "winapi-i686-pc-windows-gnu" | "winapi-x86_64-pc-windows-gnu" => return Ok(Some(0.99)),
            _ => {},
        }

        Ok(self.counts.get(&*crate_name.as_ascii_lowercase())
        .and_then(|c| {
            c.versions.get(version)
            .map(|&ver| (f64::from(ver) / self.total as f64) as f32)
        }))
    }

    #[inline]
    pub fn crates_io_dependents_stats_of(&self, origin: &Origin) -> Option<RevDependenciesCounters> {
        match origin {
            Origin::CratesIo(crate_name) => {
                self.counts.get(&**crate_name).map(|c| c.counters)
            },
            _ => None,
        }
    }

    #[inline]
    pub fn crates_io_full_dependents_stats_of(&self, origin: &Origin) -> Option<&RevDependencies> {
        match origin {
            Origin::CratesIo(crate_name) => {
                self.counts.get(&**crate_name)
            },
            _ => None,
        }
    }
}

#[derive(Debug, Copy, Clone, Default)]
pub struct RevDepCount {
    pub def: u32,
    pub opt: u32,
}

impl RevDepCount {
    #[must_use] pub fn all(&self) -> u32 {
        self.def + self.opt
    }
}

#[derive(Debug, Copy, Clone, Default)]
pub struct DirectDepCount {
    pub runtime: u16,
    pub build: u16,
    pub dev: u16,
}

impl DirectDepCount {
    #[must_use] pub fn all(&self) -> u32 {
        u32::from(self.runtime) + u32::from(self.build) + u32::from(self.dev)
    }
}

#[derive(Debug, Copy, Clone, Default)]
pub struct RevDependenciesCounters {
    /// Default, optional
    pub runtime: RevDepCount,
    pub build: RevDepCount,
    pub dev: u16,
    pub direct: DirectDepCount,
}

#[derive(Debug, Clone, Default)]
pub struct RevDependencies {
    pub counters: RevDependenciesCounters,
    pub versions: HashMap<MiniVer, u32>,
    pub rev_dep_names_default: CompactStringSet,
    pub rev_dep_names_optional: CompactStringSet,
    pub rev_dep_names_dev: CompactStringSet,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum DepTy {
    Runtime,
    Build,
    Dev,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct AddDepsAs {
    pub direct: bool,
    pub enabled_by_default: bool,
    pub ty: DepTy,
}

pub struct DepVisitor {
    node_visited: FxHashSet<(AddDepsAs, *const DepSetInterned)>,
}

impl DepVisitor {
    pub(crate) fn new() -> Self {
        Self {
            node_visited: FxHashSet::with_capacity_and_hasher(120, Default::default()),
        }
    }

    pub(crate) fn visit(&mut self, depset: &ArcDepSetInterned, depinf: AddDepsAs, mut cb: impl FnMut(&mut Self, DepNameVersionSym, &Dep)) {
        if self.node_visited.insert((depinf, Arc::as_ptr(depset))) {
            depset.iter().for_each(move |(name, dep)| {
                cb(self, *name, dep);
            });
        }
    }

    #[inline]
    pub(crate) fn start(&mut self, dep: &Dep, depinf: AddDepsAs, cb: impl FnMut(&mut Self, &ArcDepSetInterned, AddDepsAs)) {
        self.recurse_inner(dep, AddDepsAs { direct: true, ..depinf }, cb);
    }

    #[inline]
    pub(crate) fn recurse(&mut self, dep: &Dep, depinf: AddDepsAs, cb: impl FnMut(&mut Self, &ArcDepSetInterned, AddDepsAs)) {
        self.recurse_inner(dep, AddDepsAs { direct: false, ..depinf }, cb);
    }

    #[inline]
    fn recurse_inner(&mut self, dep: &Dep, depinf: AddDepsAs, mut cb: impl FnMut(&mut Self, &ArcDepSetInterned, AddDepsAs)) {
        cb(self, &dep.runtime, depinf);
        let ty = if depinf.ty == DepTy::Dev { DepTy::Dev } else { DepTy::Build };
        if let Some(build) = &dep.build {
            cb(self, build, AddDepsAs { ty, ..depinf });
        }
    }
}

impl Index {
    pub fn all_dependencies_flattened(&self, c: &impl ICrate) -> Result<DepInfMap, DepsErr> {
        let inter = &*self.inter.load();
        let collected = self.all_dependencies_flattened_internal(&*self.crates_io_crates()?, c, inter)?;

        if collected.is_empty() {
            return Ok(HashMap::default());
        }

        let mut converted = HashMap::with_capacity_and_hasher(collected.len(), Default::default());
        converted.extend(collected.into_iter().map(|(k, v)| {
            let name = inter.resolve(&k);
            debug_assert!(!name.is_empty());
            debug_assert_eq!(name, name.as_ascii_lowercase());
            (name.into(), v)
        }));
        Ok(converted)
    }

    fn all_dependencies_flattened_internal(&self, indexed_crates: &CratesIoIndexLookup, c: &impl ICrate, inter: &StringInterner) -> Result<FxHashMap<Sym, (AddDepsAs, MiniVer)>, DepsErr> {
        let (latest, mut non_default_feature_keys) = c.latest_version_with_all_features_for_optional_crates_io_deps();
        let key = {
            (inter.get_or_intern(&*latest.name().as_ascii_lowercase()), inter.get_or_intern(latest.version()))
        };

        let normal = self.deps_of_crate_version(indexed_crates, key, latest, &mut None, DepQuery {
            enable_all_targets: false,
            dev: false,
        })?;
        let mut collected = FxHashMap::with_capacity_and_hasher(2*normal.runtime.len().max(normal.build.as_ref().map_or(0, |b| b.len())), Default::default());
        let mut visitor = DepVisitor::new();
        flatten(&normal, AddDepsAs {
            enabled_by_default: true,
            direct: true,
            ty: DepTy::Runtime,
        }, &mut collected, &mut visitor);
        drop(normal);

        flatten(&self.deps_of_crate_version(indexed_crates, key, latest, &mut non_default_feature_keys, DepQuery {
            enable_all_targets: true,
            dev: false,
        })?, AddDepsAs {
            enabled_by_default: false, // false, because real defaults have already been set
            direct: true,
            ty: DepTy::Runtime,
        }, &mut collected, &mut visitor);

        if latest.dependencies().has_any_dev_deps() {
            flatten(&self.deps_of_crate_version(indexed_crates, key, latest, &mut non_default_feature_keys, DepQuery {
                enable_all_targets: true,
                dev: true,
            })?, AddDepsAs {
                enabled_by_default: false,  // false, because real defaults have already been set
                direct: true,
                ty: DepTy::Dev,
            }, &mut collected, &mut visitor);
        }
        Ok(collected)
    }

    #[inline(never)]
    pub(crate) fn get_deps_stats(&self) -> DepsStats {
        let inter = &*self.inter.load();
        let crates = &*self.crates_io_crates().unwrap();
        let start = Instant::now();
        let crates: Vec<(&str, FxHashMap<_,_>)> = crates.crates_by_lc
            .par_iter()
            .filter_map(move |(name_lowercase, c)| {
                self.all_dependencies_flattened_internal(crates, c, inter)
                .ok()
                .filter(|collected| !collected.is_empty())
                .map(move |deps| {
                    debug_assert!(Origin::is_valid_crate_name(name_lowercase));
                    (name_lowercase.as_str(), deps)
                })
            }).collect();

        let cache_entries = self.deps_set_cache.len();
        let total = crates.len();

        self.deps_set_cache.clear();
        self.deps_set_cache.shrink_to_fit();

        info!("Deps stats resolution took {}ms ({cache_entries} items, {total} crates)", start.elapsed().as_millis() as u32);
        let start = Instant::now();

        let mut deps_count = 0;
        let mut counts = HashMap::with_capacity_and_hasher(total/2, Default::default());
        for (parent_name, deps) in crates {
            for (sym, (depinf, semver)) in deps {
                deps_count += 1;
                let n = counts.entry(sym).or_insert_with(RevDependencies::default);
                let t = n.versions.entry(semver).or_insert(0);
                *t = t.checked_add(1).expect("overflow");

                match depinf.ty {
                    DepTy::Runtime => {
                        if depinf.direct {
                            if depinf.enabled_by_default { &mut n.rev_dep_names_default } else { &mut n.rev_dep_names_optional }.push(parent_name);
                            n.counters.direct.runtime = n.counters.direct.runtime.saturating_add(1);
                            debug_assert!(n.counters.direct.runtime < u16::MAX);
                        }
                        if depinf.enabled_by_default {
                            n.counters.runtime.def += 1;
                        } else {
                            n.counters.runtime.opt += 1;
                        }
                    },
                    DepTy::Build => {
                        if depinf.direct {
                            if depinf.enabled_by_default { &mut n.rev_dep_names_default } else { &mut n.rev_dep_names_optional }.push(parent_name);
                            n.counters.direct.build = n.counters.direct.build.saturating_add(1);
                        }
                        if depinf.enabled_by_default {
                            n.counters.build.def += 1;
                        } else {
                            n.counters.build.opt += 1;
                        }
                    },
                    DepTy::Dev => {
                        if depinf.direct {
                            n.rev_dep_names_dev.push(parent_name);
                            n.counters.direct.dev = n.counters.direct.dev.saturating_add(1);
                        }
                        n.counters.dev = n.counters.dev.saturating_add(1);
                    },
                }
            }
        }

        let counts: HashMap<_, _> = {
            counts.into_iter().map(move |(k, v)| {
                let name = inter.resolve(&k);
                (name.into(), v)
            }).collect()
        };

        self.clear_cache();
        info!("Deps stats collection took {}ms ({deps_count} deps, {} counts)", start.elapsed().as_millis() as u32, counts.len());
        DepsStats { total, counts }
    }
}

fn flatten(dep: &Dep, depinf: AddDepsAs, collected: &mut FxHashMap<Sym, (AddDepsAs, MiniVer)>, visitor: &mut DepVisitor) {
    visitor.start(dep, depinf, |vis, dep, depinf| flatten_set(dep, depinf, collected, vis));
}

fn flatten_set(depset: &ArcDepSetInterned, depinf: AddDepsAs, collected: &mut FxHashMap<Sym, (AddDepsAs, MiniVer)>, visitor: &mut DepVisitor) {
    visitor.visit(depset, depinf, |vis, (name_sym, _), dep| {
        collected.entry(name_sym)
            .and_modify(|(old, old_semver)| {
                let mut better = false;
                if depinf.enabled_by_default && !old.enabled_by_default {
                    old.enabled_by_default = true;
                    better = true;
                }
                if depinf.direct && !old.direct {
                    old.direct = true;
                    better = true;
                }
                if old.ty == DepTy::Dev && depinf.ty != DepTy::Dev {
                    better = true;
                }
                // build becoming runtime isn't necessarily better - for build-only deps used weirdly
                match (old.ty, depinf.ty) {
                    (DepTy::Dev | DepTy::Build, DepTy::Runtime) => { old.ty = DepTy::Runtime; },
                    (DepTy::Dev, DepTy::Build) => { old.ty = DepTy::Build; },
                    _ => {},
                }
                // the problem is this could be coming from multiple reverse dependencies using
                // it optionally or in weird ways, and latest-latest isn't always the best
                if better && dep.semver > *old_semver {
                    *old_semver = dep.semver.clone();
                }
            })
            .or_insert_with(move || (depinf, dep.semver.clone()));
        vis.recurse(dep, depinf, |vis, dep, depinf| flatten_set(dep, depinf, collected, vis));
    });
}

#[derive(Debug, Clone, Default)]
pub struct CompactStringSet(String);

impl CompactStringSet {
    pub fn push(&mut self, s: &str) {
        debug_assert!(s.bytes().all(|c| c > 0));

        self.0.reserve(1 + s.len());
        if !self.0.is_empty() {
            self.0.push_ascii_in_cap(b'\0');
        }
        self.0.push_str_in_cap(s);
    }

    pub fn iter(&self) -> impl Iterator<Item = &str> {
        if !self.0.is_empty() { Some(self.0.as_str()) } else { None }
            .into_iter().flat_map(|s| s.split('\0'))
    }
}


#[test]
fn compact_str() {
    let mut c = CompactStringSet::default();
    assert_eq!(0, c.iter().count());
    c.push("aaa");
    assert_eq!(1, c.iter().count());
    c.push("bb");
    assert_eq!(2, c.iter().count());
}
