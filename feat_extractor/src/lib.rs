use log::{info, warn};
use regex::Regex;
use util::crate_name_fuzzy_eq;
use std::borrow::Cow;
use util::FxHashMap as HashMap;
use util::FxHashSet as HashSet;
use categories::normalize_keyword;
use categories::Synonyms;
use either::Either;
use heck::ToSnakeCase;
use log::debug;
use std::sync::LazyLock as Lazy;
use rich_crate::MaintenanceStatus;
use rich_crate::Manifest;
use rich_crate::Markup;
use rich_crate::RichCrateVersion;
use semver::VersionReq;
use util::SmolStr;
use util::pick_top_n_unstable_by;
use std::path::Path;
use synscrape::ItemKind;
use synscrape::ModuleScrape;
use synscrape::TextFromSourceCode;
use util::CowAscii;
use util::smol_fmt;
use stopwords::COND_STOPWORDS;
use crate::stopwords::KEYWORD_STOPWORDS;
use std::fmt;

pub mod stopwords;
pub mod clippy_codes;

pub mod wlita;

/// ignore these as keywords
pub(crate) static IDENT_STOPWORDS: Lazy<HashSet<&str>> = Lazy::new(|| [
    "ref", "name", "entry", "iter", "new", "test", "self", "this", "struct", "vec", "string", "str", "raw", "ptr", "pos", "len",
    "u8", "f32", "f64", "u32", "usize", "impls", "impl", "bool", "cow", "count", "fmt", "borrow", "none", "delegate", "void",
    "max", "map", "tests", "build", "panic", "id", "print", "help", "usage", "main", "output", "do", "args", "cb", "callback", "function",
    "extern", "ext", "result", "res", "err", "assert", "assert-eq", "error", "mut", "try", "bench", "add", "remove", "boxed", "box", "slice", "safe", "unsafe", "start", "run",
    "next", "reason", "source", "value", "values", "demo", "mod", "match", "fn", "common", "inner", "assign", "vec", "neg", "mul", "sub", "add",
    "saturating", "check", "copy", "checked", "wrapping", "invert", "size", "err", "ok", "uninit", "init", "message", "data", "static", "total", "sum",
    "unwind", "skip", "expand", "buffer", "buf", "exhaustive", "open", "close", "fix", "enumerate", "extend", "extended", "unchecked",
    "const", "update", "full", "normal", "override", "type", "kind", "cmp", "issue", "null", "unknown", "list", "feature", "filter",
    "handle", "enabled", "maximum", "minimum", "refs", "parent", "child", "children", "hint", "forget", "underlying", "util", "utils",
    "old", "move", "insert", "apply", "all", "any", "newtype", "wrapper", "advanced", "call", "clone", "boolean", "returns", "recent",
    "internal", "private", "unwrap", "let", "wrong", "nested", "empty", "example", "start", "end", "nonexhaustive", "opts", "prelude",
    "default", "enabled", "rust", "after", "done", "partial-ord", "out", "pub", "structs", "struct", "usize", "extern", "assert",
    "equals", "two", "plus", "four", "options", "types", "last", "number", "selection", "writer", "int", "float", "base", "gets",
    "timeout", "curr", "prev", "rev", "spawn", "first", "off", "global", "and", "or", "not", "true", "false", "id", "op", "added",
    "removed", "checks", "multi", "derive", "lifetime", "state", "let", "mut", "self", "mode", "module", "func", "msrv", "mod", "res", "ok",
    "apply", "idx", "x", "y", "z", "unchecked", "unsafe", "create", "println", "done", "strongly", "replace", "internal", "best",
    "check", "faster", "fast", "starting", "skip", "pre", "results", "config", "conf", "configure", "configuration", "getting", "access",
    "disabled", "enabled", "assert_eq", "eq", "ne", "lt", "drop", "panicking", "std", "a", "essential", "extracts", "takes", "single",
    "begin", "finish", "actual", "bufread", "destination", "source", "each", "downcast", "endpoint", "resolve", "params", "resource",
    "ty", "inp", "cause", "reinterpret", "trait", "extra", "n", "ex", "sty",
    ].into_iter().collect());

pub(crate) static STOPWORDS: Lazy<HashSet<&str>> = Lazy::new(|| [
    "a", "sys", "ffi", "placeholder", "app", "loops", "master", "library", "rs", "deref",
    "accidentally", "additional", "adds", "against", "all", "allow", "allows",
    "already", "also", "alternative", "always", "an", "and", "any", "appropriate",
    "arbitrary", "are", "as", "at", "available", "based", "be", "because", "been",
    "both", "but", "by", "can", "certain", "changes", "comes", "contains", "core", "cost",
    "crate", "crates.io", "current", "currently", "custom", "dependencies", "others",
    "dependency", "developers", "do", "don't", "e.g", "easily", "easy", "either",
    "enables", "etc", "even", "every", "example", "examples", "features", "feel",
    "files", "for", "from", "fully", "function", "get", "given", "had", "has",
    "have", "here", "if", "implementing", "implements", "in", "includes", "invalid",
    "including", "incurring", "installation", "interested", "into", "is", "it",
    "it's", "its", "itself", "just", "known", "large", "later", "library", "another",
    "license", "lightweight", "like", "made", "main", "make", "makes", "many", "repeated",
    "may", "me", "means", "method", "minimal", "mit", "more", "mostly", "much", "calls",
    "need", "needed", "never", "new", "no", "noop", "not", "of", "on", "one", "either",
    "only", "or", "other", "over", "plausible", "please", "possible", "program",
    "provides", "put", "readme", "release", "runs", "rust", "rust's", "same", "let",
    "see", "selected", "should", "similar", "simple", "simply", "since", "small", "so",
    "some", "specific", "still", "stuff", "such", "take", "than", "that", "the", "requirements",
    "their", "them", "then", "there", "therefore", "these", "they", "things",
    "this", "those", "to", "todo", "too", "travis", "two", "under", "us", "unofficial",
    "usable", "use", "used", "useful", "using", "usage", "v1", "v2", "v3", "v4", "various",
    "very", "via", "want", "way", "well", "we'll", "what", "when", "where", "which",
    "while", "will", "wip", "with", "without", "working", "works", "writing", "essentials",
    "written", "yet", "you", "your", "build status", "meritbadge", "common", "during",
    "file was generated", "easy to use", "blazingly fast", "built with rust", "fixme",
    "visitor", "visit", "pure-rust", "evenly", "maybe", "off", "peculiar", "supported",
    "within", "misc", "outside", "description", "github-issue", "wanted", "hence", "undefined",
    "needs", "created", "previous", "initialized", "prior", "won-t", "don-t", "msrv",
    "ambiguous", "matches", "rust-lang", "how", "however", "provided", "return", "most",
    "instead", "along", "consisting", "correctly", "vs", "few", "uses", "does", "hello",
    "dont", "doesnt", "thus", "super-easy", "would", "thanks", "our", "goes", "hello-world",
    "must", "cannot", "another", "unwrap", "thank", "yourself", "implementations", "was",
    "resulting", "ok-res", "management", "synonym", "alternative", "unwraps", "changing",
    "guaranteed", "similarly", "carefully", "whether", "magical", "refers", "terse", "nicer",
    "skipped", "outcome", "visiting", "forces", "how", "we", "demand", "declaring", "advanced",
    "exposing", "writes", "choice", "partially", "limitation", "simplification", "beyond",
    "loading", "results", "doesnt","disabled", "pretty", "smaller", "enough", "purposes",
    "doesn", "separate", "youll", "wont", "faster", "absence", "means", "potential", "building",
    "wasnt", "whose", "essentially", "desired", "until", "originally", "around", "easier",
    "being", "creating", "underlying", "seeking", "reduced", "popular", "public", "internal",
    "external", "extern", "detailed", "resources", "about", "multiple", "crates", "special",
    "successfully", "note", "implement", "convenience", "between", "internally",
    ].into_iter().collect());

// drops boilerplate sections
#[must_use]
pub fn filter_sections_by_relevance<'a>(own_name: &str, readme_by_section: &'a [(String, String)]) -> Vec<(&'a str, Weight)> {
    let mut out = Vec::with_capacity(readme_by_section.len());

    let install_boilerplate1 = format!("cargo install {own_name}");
    let install_boilerplate2 = format!("cargo add {own_name}");
    let install_boilerplate3 = format!("{own_name} = ");
    let mut seen_text = HashSet::default();
    for (section_orig, text) in readme_by_section {
        if !seen_text.insert(text) {
            continue;
        }
        let section_s = section_orig.trim().to_lowercase();
        let section = section_s
            .trim_start_matches(|c: char| c as u32 > 0xFFFF) // strip decorative emoji
            .trim_start_matches("the ").trim_start_matches("rust ")
            .trim_end_matches('s') // singular
            .trim_end_matches(" crate");

        // skip boilerplate
        if section.starts_with("minimum supported rust") || section.starts_with("minimum rust version") {
            continue;
        }
        if matches!(section, "license" | "contact" | "todo" | "get help" | "code formatting" | "formatting" | "contribution" | "contributing" | "copyright" | "sponsor" | "financial support" | "license and link" |
            "semantic versioning" | "compatibility policy" | "maintenance" | "building" | "msrv" | "out of scope" | "minimum rustc" | "your contribution" | "cargo.toml" |
             "version compatibility" | "about the author" | "build process" | "version support" | "tests" | "code of conduct" | "version policy" | "release schedule" | "supported rust version" | "get it from crates.io" | "install rust") {
            continue;
        }
        let few_sections_added = out.len() <= 2;
        let mut relevance = if matches!(section, "info" | "how does it work" | "use case" | "tldr" | "motivation" | "summary" | "description" | "introduction" | "main feature" | "overview" | "how it work") || section.starts_with("about") || section.starts_with("what is ") || section == own_name { 1.0 }
        else if matches!(section, "feature flag" | "feature" | "" | "basic example") || section.starts_with("example") || section.starts_with("usage example") { 0.6 }
        else if matches!(section, "usage" | "how to use" | "how to use it") {
            if text.contains("[dependencies") { 0.1 } else { 0.45 }
        }
        else if matches!(section, "getting started" | "quickstart" | "documentation") || section.starts_with("compiling") { 0.3 }
        else if section.starts_with("install") || section.starts_with("contributing") || section.starts_with("license") || section_s.starts_with("rust version") ||
            matches!(section, "change" | "开发示例" | "ci" | "table of content" | "setup" | "dependencie" | "limitation" | "safety" | "community" |
            "alternative" | "other project" | "related project" | "credit" | "participating" | "acknowledgement" | "changelog" | "benchmark" | "platform" | "rust version" | "getting help" |
            "release history" | "troubleshooting" | "testing" | "current state" | "authors and acknowledgment" | "badge" | "building from source" | "see also" |
            "author" | "warning" | "debugging" | "versioning" | "regenerating sources" | "disclaimer" | "requirement" | "recent change" | "contact") { 0.1 }
        else {
            out.push((section_orig.as_str(), Weight::new(if section_s.contains(own_name) {1.} else {0.1}, true, Sources::README)));
            0.4
        };

        // bonus for the first (useful) section, assuming readmes start from synopsis
        if few_sections_added {
            relevance = (relevance * 1.8_f32).min(1.05);
        }

        // render readme to DOM, extract nodes
        for par in text.split('\n') {
            let par_orig = par.trim_start_matches(|c: char| c.is_whitespace() || c == '#' || c == '=' || c == '*' || c == '-' || c == '/' || c == '!');
            if par_orig.is_empty() {
                continue;
            }
            let par = par_orig.as_ascii_lowercase().replace('`', "");
            let par = &*par;
            if par == "cargo test" || par == "cargo build" || par == "cargo fmt" || par.starts_with("cargo build ") || par.starts_with("cargo clippy ") || par.starts_with("cargo install ") {
                continue;
            }
            if par.starts_with("[dependencies") || par.contains(&install_boilerplate1) || par.contains(&install_boilerplate2) || par.starts_with(&install_boilerplate3) {
                continue;
            }
            // example code makes messy keywords
            if par.starts_with("assert") || par.starts_with("println") || par.starts_with("let ") || par.contains(" --help") || par.starts_with("rust_log=") ||
                par.starts_with("sudo ") || par.starts_with("git clone ") || par.contains(" = ") || par.contains("()") || par.contains(", --") {
                relevance *= 0.8;
                continue;
            }
            if par.starts_with("licensed under either of") || par.starts_with("license:") || par.starts_with("this code is licensed under")
            || par.starts_with("unless you explicitly state otherwise, any contribution") || par.contains("apache license 2.0")
            || par.starts_with("add to cargo.toml") || par.contains(" your cargo.toml") || par.starts_with("at your option")
            || par.contains("the following line to your") || par.starts_with("in cargo.toml") || par.contains("your crate's cargo.toml") {
                continue;
            }

            if par.contains('<') || par.contains("std::") { // code?
                if par.contains("<out>") { // CLI example
                    relevance *= 0.5;
                }
                relevance *= 0.8;
            }
            if par.contains("...") { // terminal? code?
                relevance *= 0.95;
            }

            out.push((par_orig, Weight::new(relevance, few_sections_added || relevance > 0.4, Sources::README)));
            relevance *= 0.999;
        }
    }
    out
}

/// (names, docstrings)
#[must_use]
pub fn extract_text_from_source_code(modules: Vec<ModuleScrape>) -> TextFromSourceCode {
    let mut names = HashMap::with_capacity_and_hasher(modules.len() * 2, Default::default());
    let mut texts = Vec::with_capacity(modules.len() * 2);
    let keyword_stopwords = &*KEYWORD_STOPWORDS;
    let ident_stopwords = &*IDENT_STOPWORDS;
    let stopwords = &*STOPWORDS;

    for m in modules {
        let is_test_module = m.file_stem == "tests" || m.file_stem == "test";
        for item in m.items {
            // ModuleScrape adds name of struct or trait before '.'
            let (prefix, name) = item.name.rsplit_once('.')
                .map(|(prefix, name)| (if !item.is_test { Some(normalize_keyword(prefix)).filter(|w| !ident_stopwords.contains(&**w)) } else { None }, name))
                .unwrap_or((None, item.name.as_str()));
            let prefix = prefix.as_deref().map(chop3words);
            let weight = if item.public { 4u32 } else { 2 } + match item.kind {
                ItemKind::Macro => 1, // Dunno, these are guesses
                ItemKind::Module => 1, // Dunno, these are guesses
                ItemKind::Fn => 4,
                ItemKind::Const => 2,
                ItemKind::Struct => 2,
                ItemKind::Trait => 1,
                ItemKind::Type => 2,
                ItemKind::Field => 3,
            };
            let name = name.to_snake_case();
            let name = ["_", "get_", "set_", "is_", "as_", "parse_", "read_", "non_", "write_", "partial_", "max_", "build_", "num_", "bench_", "test_", "async_",  "should_", "temp_", "my_", "to_", "try_", "into_", "from_", "map_", "with_", "new_"].into_iter().fold(name.as_str(), |name, pref| name.strip_prefix(pref).unwrap_or(name));
            let name = ["_", "_ref", "_mut", "_async", "_t", "_get", "_opt", "_num", "_generic", "_eq", "_ext", "_str", "_iter", "_bench", "_write", "_test", "_int", "_char", "_slice", "_only", "_tests", "_at", "_options"].into_iter().fold(name, |name, pref| name.strip_suffix(pref).unwrap_or(name));

            let mut prev_word = None;
            for word in name.split('_').filter(|w| !w.is_empty() && !stopwords.contains(w) && !ident_stopwords.contains(w) && w.bytes().any(|b| !b.is_ascii_digit())) {
                if word.len() > 1 && !keyword_stopwords.contains(word) {
                    names.entry(SmolStr::from(word)).and_modify(|w| *w += 2 * weight).or_insert(weight);
                }
                if let Some(prev) = prev_word {
                    if prev != word {
                        names.entry(smol_fmt!("{prev}-{word}")).and_modify(|w| *w += 4 * weight).or_insert(3 * weight);
                    }
                }
                if let Some(prefix) = prefix {
                    if !prefix.contains(word) {
                        names.entry(smol_fmt!("{prefix}-{word}")).and_modify(|w| *w += 3 * weight).or_insert(2 * weight);
                    }
                }
                prev_word = Some(word);
            }

            if let Some(doc) = item.doc {
                if is_bad_doc_text(&doc) {
                    continue;
                }
                texts.push((if item.is_test || is_test_module { 1 } else { 2 * weight }, doc));
            }
        }
        let val = names.entry(m.file_stem).or_insert(1);
        if *val < 1<<16 {
            *val *= 2;
        }
        if names.len() > 10000 {
            break;
        }
    }

    let cond_stopwords = &*COND_STOPWORDS;
    let to_kill = names.iter_mut().filter_map(|(k, v)| {
        let words = cond_stopwords.get(k.as_str()).copied()?;
        if words.is_none() {
            *v += 2;
            *v *= 2; // no list means all words, so demote all others by promoting this one (scores are relative)
        }
        words
    }).flat_map(|w| w.iter().copied()).collect::<Vec<_>>();
    for kw in to_kill {
        if let Some(v) = names.get_mut(kw) {
            *v /= 4;
        }
    }

    let (mut names_single, mut names_double): (Vec<_>, Vec<_>) = names.into_iter().map(|(k, v)| (v, k)).partition(|(_, k)| k.contains('-'));
    for names in [&mut names_single, &mut names_double] {
        let limit = names.len()/2 + 20;
        pick_top_n_unstable_by(names, limit, |a, b| b.0.cmp(&a.0));
    }
    names_double.append(&mut names_single);

    TextFromSourceCode {
        idents: names_double,
        doc: texts,
    }
}

fn is_bad_doc_text(doc: &str) -> bool {
    doc.contains("cargo test") || doc.contains("RUST_LOG=")
}

#[must_use]
fn combined_keywords(ranked_phrases: &[(Cow<'_, str>, Weight)], mut keywords_out: KeywordBag, all_keywords: &HashMap<SmolStr, u32>, tag_synonyms: &Synonyms) -> KeywordBag {
    let keyword_stopwords = &*KEYWORD_STOPWORDS;

    let all_words: Vec<_> = ranked_phrases.iter()
        .map(move |(line, kw)| {
            let mut rest_of_line_weight = 1.;
            line.split([' ', '\t', '\n', '.', ',', '(', ')' , ';', '"', '{', '}', '#', '!', '?', '`', '['])
                .map(move |word| (word, *kw))
                .flat_map(|(mut word, mut kw)| {
                    if kw.sources.intersects(Sources::README | Sources::DOCS | Sources::REPO_META | Sources::GUESS) {
                        word = trim_sentence_intro(word);
                    }
                    let word_lc = &*word.as_ascii_lowercase();
                    if matches!(word_lc, "excluding" | "except" | "excludes" | "unlike") {
                        rest_of_line_weight *= 0.2;
                    } else if matches!(word_lc, "not" | "without" | "doesn't" | "isn't" | "wasn't" | "never") {
                        rest_of_line_weight *= 0.8;
                    }

                    kw.weight *= rest_of_line_weight;
                    if word.len() < 30 && !word.contains("::") {
                        Either::Left([(word, kw)].into_iter())
                    } else {
                        kw.weight *= 0.98;
                        if word.starts_with("std::") {
                            kw.visible = false;
                            kw.weight *= 0.3;
                        }
                        // too long for a keyword
                        Either::Right(
                            // don't include '-', because it breaks e-mail etc.
                            word.split(['_','/','\\','\'',':','&','#','*','@','<','>'])
                                .map(|w| w.trim_matches('-'))
                                .filter(|w| !w.is_empty())
                                .map(move |w| (w, kw)))
                    }
                })
                .map(|(word, mut kw)| {
                    let word = word.trim_matches(|c: char| c.is_ascii_punctuation() || c == '®' || c.is_control() || c.is_whitespace())
                        .trim_end_matches("'s").trim_end_matches("'d").trim_end_matches("'ll");
                    if !word.starts_with(|c: char| c.is_ascii_alphabetic()) { kw.weight *= 0.8 };
                    (word, kw)
                })
                .filter(|&(word, _)| {
                    word.len() > 1 && !word.starts_with("std::")
                })
                .map(|(word, kw)| {
                    (normalize_keyword(word), kw)
                })
                .filter(move |(word, kw)| {
                    (kw.sources.contains(Sources::KEYWORDS) && !kw.sources.contains(Sources::DERIVED)) ||
                    !KEYWORD_STOPWORDS.contains(&**word)
                })
                .collect()
        })
        .filter(|set: &Vec<_>| !set.is_empty())
        .collect();

    for &(ref w, mut kw) in all_words.iter().flatten() {
        for _ in w.split('-').filter(move |&w| keyword_stopwords.contains(w)) {
            kw.weight *= 0.6;
        }
        let norm = tag_synonyms.normalize(w, 4);
        keywords_out.insert(norm, kw);
        if norm != w {
            kw.weight *= 0.6;
            keywords_out.insert(w.as_str(), kw);
        }
    }

    // First pass of stopwords
    // (stopwords are not removed entirely yet, because they can be used in compound-keywords)
    let cond_stopwords = &*COND_STOPWORDS;
    let to_kill = keywords_out.keywords.iter_mut().filter_map(move |(k, kw)| {
        let words = cond_stopwords.get(k.as_str()).copied()?;
        if words.is_none() {
            kw.weight += 0.1;
            kw.weight *= 2.; // no list means all words, so demote all others by promoting this one (scores are relative)
        }
        words
    }).flat_map(|w| w.iter().copied()).collect::<Vec<_>>();
    for w in to_kill {
        if let Some(kw) = keywords_out.keywords.get_mut(w) {
            kw.weight *= 0.2;
        }
    }

    // Add synonyms
    let mut syn = Vec::new();
    keywords_out.keywords.iter().for_each(|(w, kw)| {
        if let Some((s, w2)) = tag_synonyms.get(w) {
            let mut kw = *kw;
            kw.weight *= w2;
            kw.sources |= Sources::DERIVED;
            syn.push((SmolStr::from(s), kw));
            if let Some((s, w3)) = tag_synonyms.get(w) {
                kw.weight *= w3;
                syn.push((SmolStr::from(s), kw));
            }
        }
    });
    for (s, kw) in syn {
        keywords_out.insert(s, kw);
    }
    log::trace!("auto_keywords before {keywords_out:?}");

    // Make compound-word keywords
    for set in all_words {
        let set = set.iter()
            .flat_map(|(w, kw)| {
                // if it's already a compount word, it makes keywords that are too long
                let splits = w.as_bytes().iter().filter(|&&b| b == b'-').count();
                if w.len() < 20 && (w.len() < 15 || splits <= 1) {
                    Either::Right([(w.as_str(), *kw)].into_iter())
                } else {
                    Either::Left(w.split('-').filter(|p| !p.is_empty())
                        .map(move |part| {
                            let mut kw2 = *kw;
                            kw2.weight *= 0.9;
                            (part, kw2)
                        }))
                }
            })
            // push extra to flush loop using prev word
            .chain([("", Weight::new(0.0, false, Sources::empty()))]);
        let mut prev_prev_word: Option<(_, Weight)> = None;
        let mut prev_word: Option<(_, Weight)> = None;
        // lower weight on compound keywords with a stopword skipped
        // because they may make less sense without a word
        let mut had_a_stopword = false;
        for (w_orig, mut kw) in set {
            let w = tag_synonyms.normalize(w_orig, 4);

            if let Some(kw2) = keywords_out.get(w_orig) {
                if kw2.combined_score() > kw.combined_score() {
                    kw = kw2;
                }
                if w_orig != w {
                    if let Some(kw3) = keywords_out.get(w) {
                        if kw3.combined_score() > kw.combined_score() {
                            kw = kw3;
                        }
                    }
                }
            }

            if let Some(((prev_prev, mut prev_prev_w), (mut prev, mut prev_w))) = prev_prev_word.zip(prev_word) {
                // Don't make silly "word-word" keywords
                if w == prev {
                    continue;
                }
                // word-foo-word is also weird (but don't skip because it will be gone in the next iteration)
                if w == prev_prev || prev == prev_prev || keyword_stopwords.contains(prev_prev) {
                    had_a_stopword = true;
                    prev_prev_w.visible = false;
                    prev_prev_w.weight *= 0.1;
                }

                // fudge to skip middle word that is a stopword
                let prev_stopword = !prev_w.sources.contains(Sources::KEYWORDS) && keyword_stopwords.contains(prev);
                if prev_stopword {
                    if keyword_stopwords.contains(w) {
                        had_a_stopword = true;
                        continue;
                    }
                    prev = w;
                    prev_w = kw;
                }

                let s_orig = smol_fmt!("{prev_prev}-{prev}-{w}");
                let long_words = if let Some((synonym, similarity)) = tag_synonyms.get(&s_orig) {
                    let similarity = similarity.powi(2); // force it to pick one or the other
                    [Some((synonym.into(), similarity)), Some((s_orig, 1.-similarity))]
                } else {
                    [Some((s_orig, 1.)), None]
                };
                let recognized_long = long_words[1].is_some();
                let mut added_long = false;
                let mut added_as_visible = false;
                for (s, synonym_weight) in long_words.into_iter().flatten() {
                    let pop = all_keywords.get(&*s).copied().unwrap_or(0);
                    if !prev_stopword && pop > 0 && !w.is_empty() && !keyword_stopwords.contains(&*s) {
                        added_long = true;

                        let vis = pop > 15 && kw.visible && prev_w.visible && prev_prev_w.visible;
                        if vis {
                            added_as_visible = true;
                        }
                        let w3 = if had_a_stopword { 1.01 } else { 1.2 } * synonym_weight *
                            (kw.weight * prev_w.weight * prev_prev_w.weight).cbrt(); // geomean
                        let kw3 = Weight::new(w3, vis, (prev_prev_w.sources & prev_w.sources & kw.sources) | Sources::DERIVED);
                        keywords_out.insert(s, kw3);
                    }
                }
                if !added_long { // if there's "windows-operating-system", don't add "windows-operating"
                    let s = smol_fmt!("{prev_prev}-{prev}");
                    let pairs = if let Some((synonym, similarity)) = tag_synonyms.get(&s) {
                        let similarity = similarity.powi(2);
                        [Some((synonym.into(), similarity)), Some((s, 1.-similarity))]
                    } else {
                        [Some((s, 1.)), None]
                    };
                    for (s, synonym_weight) in pairs.into_iter().flatten() {
                        let pop = all_keywords.get(&*s).copied().unwrap_or(0);
                        if pop > 0 && !keyword_stopwords.contains(&*s) {
                            let vis = !recognized_long && pop > 20 && prev_w.visible && prev_prev_w.visible;
                            if vis {
                                added_as_visible = true;
                            }
                            let w2 = if had_a_stopword { 0.97 } else { 1.04 } * synonym_weight *
                                (prev_prev_w.weight * prev_w.weight).sqrt(); // geomean
                            let kw2 = Weight::new(w2, vis, (prev_w.sources & prev_prev_w.sources) | Sources::DERIVED);
                            keywords_out.insert(s, kw2);
                        }
                    }
                }
                if added_as_visible {
                    // if this keyword is visible, downgrade the next one to avoid overlapping
                    kw.weight *= if added_long { 0.7 } else { 0.9 };
                }
            }

            had_a_stopword = false;

            if matches!(w, "or" | "nor" | "not" | "non") {
                // "hot or cold" makes nonsense "hot-cold" keywords.
                prev_word = None;
            }
            prev_prev_word = prev_word.replace((w, kw));
        }
    }

    // Now real stopwords
    for (w, kw) in &mut keywords_out.keywords {
        let real_keyword = !kw.sources.contains(Sources::DERIVED) && kw.sources.contains(Sources::KEYWORDS);
        if !real_keyword && keyword_stopwords.contains(&**w) {
            kw.visible = false;
            kw.weight *= 0.1;
        }

        // these look redundant
        if categories::CATEGORIES.root.contains_key(&**w) {
            kw.weight *= 0.6;
        }
    }

    keywords_out
}

fn chop3words(s: &str) -> &str {
    let mut words = 0;
    for (pos, ch) in s.char_indices() {
        if ch == ' ' || ch == '-' {
            words += 1;
            if words >= 2 {
                return &s[0..pos];
            }
        }
    }
    s
}

fn popular_keyword_weight(pop: u32) -> f32 {
    if pop < 20 {
        (10+pop) as f32 / 30.
    } else if pop > 1000 {
        2000_f32.sqrt() / ((1000+pop) as f32).sqrt()
    } else {
        1.
    }
}

static DEPRECATED_DESC_REGEX: Lazy<Regex> = Lazy::new(||
    Regex::new("(?i)^deprecated|^please use `gix-|(?:this |the |^)(?:crate|package|tool|project|this|it) (?:was|has been|is) (?:renamed|abandoned|deprecated)|^obsolete|^unsafe and deprecated|^crate is abandoned|^abandoned|^(?:temp crate )?do not use|^an empty crate|^discontinued|^very early wip|^(?:crate )?renamed to |this is a dummy (?:crate|package)|no longer developed\\.|please don'?t use$|deprecated$|deprecated in favou?r|project is deprecated").unwrap());

static DEPRECATED_README_REGEX: Lazy<Regex> = Lazy::new(||
    Regex::new("(?i)this (?:is )?(?:a )?(?:project|crate) (?:that )(?:is|has(?: been)?) (?:now)?deprecated in favou?r of|this crate has been deprecated|this crate is no longer used.*and is deprecated").unwrap());


/// Returns reason
#[must_use]
pub fn is_deprecated(k: &RichCrateVersion) -> Option<&'static str> {
    if k.version().contains("deprecated") {
        return Some("version");
    }
    if k.maintenance() == MaintenanceStatus::Deprecated {
        return Some("Cargo.toml badge");
    }
    if k.repository().and_then(|r| r.url_owner_name()).is_some_and(|r| r == "rust-lang-deprecated") {
        return Some("deprecated-repo");
    }

    if let Some(desc) = k.description() {
        let desc = desc.trim_matches(|c: char| !c.is_ascii_alphabetic());

        if DEPRECATED_DESC_REGEX.is_match(desc) {
            return Some("description-deprecated");
        }
        let is_unmaintained =
            desc.starts_with("unmaintained ") ||
            desc.contains("no longer maintained") ||
            desc.starts_with("temporary fork") ||
            desc.starts_with("unfinished") ||
            desc.starts_with("an unfinished") ||
            desc.starts_with("unmaintained! ");
        if is_unmaintained {
            return Some("description-unmaintained");
        }
    }
    if let ok @ Some(_) = is_squatspam(k) {
        info!("Marked as spam {ok:?}");
        return ok;
    }
    if let Some(readme) = k.readme() {
        let text = readme.raw_text();
        let text = text.get(..1000).unwrap_or(text);
        if text.contains("deprecated") || text.contains("DEPRECATED") {
            if DEPRECATED_README_REGEX.is_match(text) ||
                text.contains(&format!("{} is now deprecated in favour", k.short_name().as_ascii_lowercase())) ||
                text.contains(&format!("{} is deprecated", k.short_name().as_ascii_lowercase())) {
                return Some("readme-depr");
            }
        }
    }
    if let Ok(req) = k.version().parse() {
        if is_deprecated_requirement(k.short_name(), &req) {
            return Some("known-bad-crate");
        }
    }
    None
}

/// `is_deprecated_requirement` is alredy checked before that
#[must_use]
pub fn is_bad_dependency(name: &str, _requirement: &VersionReq) -> bool {
    match name {
        "owning_ref" => true, // unsound, unmaintained
        "autotools" => true, // tens of thousands of unreviewable lines of code are a supply chain risk: CVE-2024-3094
        // fundamentally unsound
        "str-concat" => true,
        "extprim" => true, // does not compile due to const_fn unstable feature
        "traitobject" | "unsafe-any" => true, // vulnerable
        "typemap" => true, // 2015
        "bitcoin" => true, // this will lower ranking of crates using it (please stop using PoW)
        "rustfmt" | "clippy" => true, // rustup
        _ => false,
    }
}

#[must_use]
pub fn is_deprecated_requirement(name: &str, requirement: &VersionReq) -> bool {
    let bad_ahash = "0.7.8".parse().unwrap();
    let v02 = "0.2.99".parse().unwrap();
    let v01 = "0.1.99".parse().unwrap();
    let v4 = "4.99.99".parse().unwrap();
    match name {
        "futures" | "opengl32-sys" | "uuid-sys" if requirement.matches(&v01) => true,
        "time" | "tokio" | "winapi" | "winmm-sys" | "secur32-sys" if requirement.matches(&v01) || requirement.matches(&v02) => true,
        "ahash" if requirement.matches(&bad_ahash) => true,
        "nom" if requirement.matches(&v4) => true, // future-incompat problem
        "rustc-serialize" | "rmp-serialize" | "gcc" | "rustc-benchmarks" | "rust-crypto" |
        "flate2-crc" | "complex" | "simple_stats" | "concurrent" | "feed" |
        "isatty" | "thread-scoped" | "target_build_utils" | "chan" | "chan-signal" |
        "glsl-to-spirv" => true,
        // futures 0.1
        "futures-preview" | "futures-tls" | "tokio-process" | "futures-mio" | "futures-channel-preview" | "futures-core-preview" |
        "tokio-io" | "tokio-timer" | "tokio-sync" | "tokio-codec" | "futures-stable" | "futures-async-runtime" | "futures01" | "futures-sink-preview" |
        "tokio-executor" | "tokio-reactor" | "tokio-signal" | "tokio-core" | "futures-cpupool" | "futures-util-preview" |
        "tokio-compat" | "tokio-async-await" | "tokio-serde-json" | "tokio-udp" | "tokio-service" | "tokio-fs" |
        "tokio-threadpool" | "tokio-tcp" | "tokio-current-thread" | "tokio-uds" => true,
        // uses old winapi
        "user32-sys" | "shell32-sys" | "advapi32-sys" | "gdi32-sys" | "ole32-sys" | "ws2_32-sys" | "kernel32-sys" | "userenv-sys" => true,
        // uses the newest windows api, still deprecated :)
        "winrt" => true,
        // renamed
        "hyprland-workspaces" => true,
        "hdrsample" => true,
        "ordermap" => true,
        "istat" => true,
        "sifis-generate" => true,
        "rustworkx-core" => true, // renamed
        // in stdlib
        "insideout" | "file" | "ref_slice" | "matches" => true,
        "serde_derive_internals" | "serde_codegen_internals" | "serde_macros" | "serde_codegen" => true,
        "twoway" => true,
        // says so in the readme
        "fast-xml" => true,
        "instant" => true,
        "rusty_v8" => true,
        "auto_uds" => true,
        "bv2av" => true,
        "interpolate_idents" => true,
        "puroro-protobuf-compiled" => true,
        "cetkaik_core" => true,
        "block-modes" => true,
        "surfman-chains-api" => true,
        "proto_schema_plugin" => true,
        "svgdom" => true,
        "proto_deno" => true,
        "dinghy" => true,
        "rpsl-parser" => true,
        "lzf" => true,
        "battlesearch" => true,
        "coord" => true,
        "rand_os" => true,
        "windows_macros" => true,
        "fast-modulo" => true,
        "spawn-editor" => true,
        "cranelift-faerie" => true,
        "wasmer-deploy-client-cli" => true,
        "dbz-lib" => true,
        "darktoken" => true,
        "try" => true,
        "axum-debug" => true,
        "aws-smithy-client" => true,
        "typename_derive" => true,
        "typename" => true,
        "tauri-mobile" => true,
        "triton_grow" => true,
        "parser-combinators" => true,
        "stream-cipher" | "block-cipher" | "error-chain" | "ansi_term" | "failure" | "failure_derive" => true, // dead
        "jujutsu-lib" | "jujutsu" | "public_items" => true,
        _ => false,
    }
}

#[must_use]
pub fn is_autopublished(k: &RichCrateVersion) -> bool {
    let d = k.description().unwrap_or_default();
    let name = k.short_name();

    if !(d.starts_with("Automatically published ") ||
    d.ends_with(" generated by tonic-build") ||
        name.starts_with("rustc-ap-") || name.starts_with("ra-ap-")) { name.starts_with("huaweicloud-sdk-"); }
        name.starts_with("googleapis-tonic-google-")
}

#[must_use]
pub fn is_squatspam_partial(version: &str, description: Option<&str>) -> Option<&'static str> {
    if version.contains("reserved") || version.contains("placeholder") {
        return Some("reserved-version");
    }
    if let Some(desc) = description {
        if is_reserved_boilerplate_text(desc) {
            return Some("reserved-description");
        }
    }
    None
}

/// Returns reason why
#[must_use]
pub fn is_squatspam(k: &RichCrateVersion) -> Option<&'static str> {
    if k.is_spam() {
        return Some("marked as spam");
    }
    let mut authors = k.authors();
    let first_author = authors.next();
    let multiple_authors = authors.next().is_some();
    if !multiple_authors && first_author.is_some_and(|a| a.name.as_deref() == Some("...")) {
        return Some("spam-author"); // spam by mahkoh
    }
    if let Some(reason) = is_squatspam_partial(k.version(), k.description()) {
        return Some(reason);
    }
    if let Some(readme) = k.readme() {
        match &readme.markup {
            Markup::Html(s) | Markup::Markdown(s) | Markup::AsciiDoc(s) | Markup::Rst(s) => if is_reserved_boilerplate_text(s) {
                return Some("reserved-readme");
            }
        }
    }

    if let Some(l) = k.lib_file() {
        if l.trim_start().is_empty() && k.bin_file().is_none() {
            // sys crates don't need Rust source
            if !k.is_sys() {
                return Some("lib.rs junk");
            }
        }

        let hash = blake3::hash(l.as_bytes());
        let hash = hash.as_bytes();
        debug!("hashed lib.rs {hash:02x?}");
        if [
            [0xc9,0x37,0x4f,0x1e,0xa1,0x30,0x27,0xf6,0xe1,0xd5,0x4e,0xbd,0x98,0xab,0x71,0x5e,0xf0,0xf6,0xc6,0x38,0x84,0xbb,0xda,0x6c,0x2f,0x46,0xf5,0x25,0xc1,0x9d,0x1b,0x7b],
            [0x07,0xbc,0x44,0x9b,0xb2,0x62,0x3d,0xd1,0x8f,0x80,0xf4,0x1a,0xb0,0xa1,0xf8,0xd5,0x50,0x87,0x4a,0xe5,0xd0,0x2b,0xa2,0x9d,0x69,0xc2,0x40,0x27,0x11,0x3d,0x1e,0x4b],
            [0x25,0x80,0x23,0x73,0x7f,0xc5,0x1a,0xfb,0x32,0xfa,0x96,0xe7,0x53,0xbc,0x5d,0x7d,0xd6,0xcf,0x53,0x58,0x5a,0xec,0x10,0x0b,0x63,0x16,0xad,0x07,0x7e,0x91,0x30,0x9e],
            [0x5b,0x49,0xda,0x51,0x4c,0x3f,0x8f,0x34,0x1a,0x82,0xbd,0x94,0x45,0xd3,0x3d,0x5b,0x23,0x77,0x54,0x9b,0xb3,0x5f,0x63,0x58,0x77,0x6f,0x94,0x5f,0x3a,0xa0,0x7f,0xfc],
            [0x94,0x0a,0x22,0x83,0x8e,0x3b,0x0a,0xb0,0x5a,0xdd,0xf1,0xa6,0x3e,0xc6,0x24,0xfe,0x52,0x5e,0x25,0xf9,0xa7,0x74,0xc0,0x78,0x0e,0xd2,0x57,0x26,0x7c,0x1b,0x94,0x4d],
            [0xad,0x7d,0x56,0x3f,0x13,0x8f,0x96,0x04,0xbe,0xce,0x74,0xd7,0xf3,0x6d,0xc0,0x9a,0x03,0x2d,0xed,0x6d,0x31,0x96,0xbf,0xb1,0xfa,0x3f,0x29,0x37,0x2b,0x0a,0xcf,0x4b],
            [0xb8,0x68,0xa0,0x68,0x43,0xcd,0x43,0x7a,0x58,0xe0,0xf6,0x6f,0x75,0x1b,0xcb,0x3b,0xc9,0x34,0x36,0xc1,0xb4,0xc2,0xe7,0x76,0x2d,0x56,0x26,0xfc,0xe3,0x73,0x42,0x90],
            [0xbe,0xc2,0xe5,0xb4,0x19,0xef,0x46,0x60,0x6c,0xea,0x0a,0x74,0x57,0x7f,0x36,0x0c,0xaa,0xb5,0xb9,0x2d,0x23,0x59,0x71,0x60,0x41,0xe2,0x96,0x85,0xc4,0x92,0x20,0x23],
            [0xd5,0x94,0xd3,0x46,0x5d,0x20,0x3f,0x1d,0xc6,0x96,0x03,0x22,0xc9,0xf2,0xe9,0xa3,0xcc,0xdc,0x91,0xdf,0xc7,0x58,0xb2,0xc7,0x7d,0xa8,0xb7,0xa9,0xd6,0x16,0xb4,0xab],
        ].iter().any(move |h| hash == h) {
            return Some("lib.rs junk");
        }
    }

    if let Some(l) = k.bin_file() {
        let hash = blake3::hash(l.as_bytes());
        let hash = hash.as_bytes();
        debug!("hashed main.rs {hash:02x?}");
        if *hash == [0xfb,0x5c,0xe9,0xdc,0xd7,0x94,0x90,0xba,0x25,0xa7,0x00,0x04,0x07,0x0b,0x11,0xe6,0x2e,0x7f,0xca,0x85,0xc1,0xf6,0x50,0xcb,0x44,0x3e,0x4d,0xd0,0x45,0xbc,0x9f,0xd9] {
            return Some("main.rs junk");
        }
    }

    None
}

fn is_reserved_boilerplate_text(desc: &str) -> bool {
    let desc = &*desc.trim_matches(|c: char| !c.is_ascii_alphabetic()).as_ascii_lowercase();
    let desc2 = desc.trim_start_matches("this crate ")
        .trim_start_matches("is being ")
        .trim_start_matches("is ")
        .trim_start_matches("has been ")
        .trim_start_matches("has ")
        .trim_start_matches("a ").trim_start();
    desc.contains("this crate is a placeholder") ||
        desc.contains("reserving this crate") ||
        desc.contains("reserving this crate") ||
        desc.contains("crate is a placeholder for the future") ||
        desc.contains("only to reserve the name") ||
        desc.contains("crate is currently a placeholder") ||
        desc.contains("this crate has been retired") ||
        desc.contains(" if you want this crate name") ||
        desc.contains("want to use this name") ||
        desc.contains("this is a dummy package") ||
        desc.contains("if you would like to use this crate name, please contact") ||
        desc.starts_with("parked for future use") ||
        desc.starts_with("reserving this crate name for") ||
        desc.starts_with("contact me if you want this name") ||
        desc.starts_with("unused crate name") ||
        desc.starts_with("more coming soon") ||
        desc.starts_with("short description of your package") ||
        desc.starts_with("unused. contact me") ||
        desc.starts_with("fun game where you guess what number the computer has chosen") ||
        desc.starts_with("crate name not in use") ||
        desc.starts_with("if you want to use this crate name, please contact ") ||
        desc.contains("if you want this name, please contact me") ||
        desc.contains("i consent to the transfer of this crate to the ") ||
        desc2.starts_with("reserved crate ") ||
        desc.contains("this crate is reserved ") ||
        desc == "name available" ||
        desc == "a library for modeling artistic concepts" ||
        desc2 == "coming soon" ||
        desc2 == "more coming soon" ||
        desc2 == "reserved" ||
        desc2 == "parked" ||
        desc2 == "crate name reserved" ||
        desc2.starts_with("reserved for future use") ||
        desc2.starts_with("placeholder") ||
        desc.ends_with(" placeholder") ||
        desc.ends_with(" reserved for use") ||
        desc2.starts_with("dummy crate") ||
        desc2.starts_with("available for ownership transfer") ||
        desc2.starts_with("reserved, for") ||
        desc2.starts_with("crate name reserved for") ||
        desc2.starts_with("wip: reserved") ||
        desc2.starts_with("placeholder") ||
        desc2.starts_with("reserved coming") ||
        desc2.starts_with("empty crate") ||
        desc2.starts_with("an empty crate") ||
        desc2.starts_with("reserved for ") ||
        desc2.starts_with("stub to squat") ||
        desc2.starts_with("claiming it before someone") ||
        desc2.starts_with("reserved name") ||
        desc2.starts_with("reserved package") ||
        desc2.starts_with("reserve the name")
}

use bitflags::bitflags;

bitflags! {
    #[derive(Debug, Clone, Copy)]
    pub struct Sources: u8 {
        /// Cargo.toml
        const KEYWORDS = 1 << 0;
        const DESCRIPTION = 1 << 1;
        const README = 1 << 2;
        /// From source code
        const IDENTS = 1 << 3;
        const DOCS = 1 << 4;
        /// GitHub topics
        const REPO_META = 1 << 5;
        const GUESS = 1 << 6;
        /// Combined other keywords
        const DERIVED = 1 << 7;
    }
}

impl Sources {
    #[must_use] pub fn visible(self) -> bool {
        self.intersects(Self::KEYWORDS | Self::DESCRIPTION | Self::README | Self::DOCS | Self::REPO_META)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Weight {
    /// Max relevance
    pub weight: f32,
    /// Can be displayed on the page (is not an internal marker)
    pub visible: bool,
    /// All places where it has been found
    pub sources: Sources,
}

impl Weight {
    #[must_use] pub fn new(weight: f32, visible: bool, sources: Sources) -> Self {
        Self {
            weight,
            visible,
            sources,
        }
    }

    pub fn combine(&mut self, mut other: Self) {
        let min_w = self.weight.min(other.weight);
        let max_w = self.weight.max(other.weight);
        self.weight = min_w.mul_add(0.3, max_w);

        if self.sources.contains(Sources::DERIVED) && !other.sources.contains(Sources::DERIVED) && other.sources.visible() {
            self.sources.remove(Sources::DERIVED);
        }
        else if other.sources.contains(Sources::DERIVED) && !self.sources.contains(Sources::DERIVED) && self.sources.visible() {
            other.sources.remove(Sources::DERIVED);
        }

        self.sources |= other.sources;
        if other.visible {
            self.visible = true;
        }
    }

    #[must_use]
    pub fn combined_score(&self) -> f32 {
        const MAX_SCORE: i8 = 15;
        let src_num = (self.sources & !Sources::DERIVED).bits().count_ones() as i8;
        let score =
            if src_num > 1 { 3 } else { -1 } +
            if src_num > 2 { src_num } else { 0 } +
            if self.sources.contains(Sources::KEYWORDS) { 14 } else { -1 } +
            if self.sources.contains(Sources::DESCRIPTION) { 7 } else { 0 } +
            if self.sources.contains(Sources::README) { 2 } else { -2 } +
            if self.sources.contains(Sources::IDENTS) { 2 } else { 0 } +
            if self.sources.contains(Sources::DOCS) { 1 } else { 0 } +
            if self.sources.contains(Sources::REPO_META) { 4 } else { 0 } +
            if self.sources.contains(Sources::GUESS) { 5 } else { 0 } +
            if self.sources.contains(Sources::DERIVED) { -1 } else { 0 } +
            // if visible, the guess matches explicit sources, so it's a good guess
            if self.sources.contains(Sources::GUESS) && self.visible { 5 } else if src_num > 1 { 2 } else { 0 };

        self.weight * f32::from(1 + score.clamp(0, MAX_SCORE)) / f32::from(1 + MAX_SCORE)
    }
}

struct KeywordBag {
    keywords: HashMap<SmolStr, Weight>,
}

impl KeywordBag {
    #[must_use] pub fn new() -> Self {
        Self {
            keywords: HashMap::default(),
        }
    }

    #[must_use] pub fn get(&self, keyword: &str) -> Option<Weight> {
        self.keywords.get(keyword).copied()
    }

    pub fn insert(&mut self, name: impl Into<SmolStr>, kw: Weight) {
        let name = name.into();
        self.keywords.entry(name).and_modify(|k| {
            k.combine(kw);
        }).or_insert(kw);
    }

}

impl fmt::Debug for KeywordBag {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut list = self.keywords.iter().map(|(k, w)| {
            let vis = if !w.visible { "_" } else { "" };
            let src = if w.sources.intersects(Sources::KEYWORDS | Sources::DESCRIPTION | Sources::README) { "" }
                else if w.sources.intersects(Sources::REPO_META) { "@" }
                else if w.sources.intersects(Sources::DOCS | Sources::IDENTS) { "&" }
                else if w.sources.intersects(Sources::GUESS) { "?" }
                else { "" };
            let der = if w.sources.contains(Sources::DERIVED) { "+" } else { "" };
            let s = w.combined_score();
            let s = if s > 0.25 && s < 0.9 { (s * 10.).round() / 10. } else { (s * 100.).round() / 100. };
            (format!("{src}{der}{vis}{k}"), s)
        }).collect::<Vec<_>>();
        pick_top_n_unstable_by(&mut list, 50, |a, b| b.1.total_cmp(&a.1));
        f.debug_map().entries(list).finish()
    }
}

pub struct KeywordsInputs<'a> {
    pub repo_relevance: f32,
    pub manifest: &'a Manifest,
    pub readme_by_section: &'a [(String, String)],
    pub github_description: Option<&'a str>,
    pub github_keywords: Option<&'a [String]>,
    pub bad_categories: &'a [SmolStr],
    pub text_from_source_code: &'a TextFromSourceCode,
    pub data_dir: &'a Path,
    pub all_keywords: &'a HashMap<SmolStr, u32>,
    pub tag_synonyms: &'a Synonyms,
    pub parent_crate_name: Option<&'a str>,
}

/// Visible and invisible, with weights
pub fn auto_keywords_for_package(inp: &KeywordsInputs<'_>) -> (Vec<(f32, SmolStr)>, Vec<(f32, SmolStr)>) {
    let package = inp.manifest.package();
    let mut all_phrases = filter_sections_by_relevance(package.name(), inp.readme_by_section);

    let name_trimmed = package.name().trim_start_matches("rust").trim_end_matches("rust")
        .trim_end_matches("-core").trim_end_matches("4rs");
    for k in name_trimmed.split(|c: char| !c.is_alphanumeric()).filter(|k| k.len() > 2) {
        if k == "internal" || k == "rs" || k == "impl" || k == "shared" {
            continue;
        }
        // there are some janky crate names, which shoudln't imply they're visible keywords
        all_phrases.push((k, Weight::new(0.8, false, Sources::DESCRIPTION)));
    }

    let name_spaces = package.name.chars().map(|c| if !c.is_ascii_whitespace() {c} else {' '}).collect::<String>();
    all_phrases.push((name_spaces.as_str(), Weight::new(1.0f32, false, Sources::DESCRIPTION)));

    if let Some(s) = package.description() {
        all_phrases.push((s, Weight::new(1.0f32, true, Sources::DESCRIPTION)));
    }

    if let Some(s) = inp.github_description {
        all_phrases.push((s, Weight::new(inp.repo_relevance, true, Sources::REPO_META)));
    }

    let mut all_phrases = clean_phrases(all_phrases);

    // add as a phrase to enable combining of adjacent keywords
    all_phrases.push((package.keywords().join(" ").into(), Weight::new(1.0, true, Sources::KEYWORDS)));

    // start collecting actual keywords
    let mut existing_keywords = keywords_from_doc_idents(inp.text_from_source_code, inp.all_keywords, inp.tag_synonyms);

    if let Some(links) = package.links() {
        existing_keywords.insert(normalize_keyword(links), Weight::new(0.9, true, Sources::DESCRIPTION));
        if let Some(lib) = links.strip_prefix("lib") {
            existing_keywords.insert(normalize_keyword(lib), Weight::new(0.9, false, Sources::DESCRIPTION));
        }
    }

    for k in inp.manifest.features.keys() {
        let k = k.trim_start_matches("use_").trim_start_matches("with_");
        if KEYWORD_STOPWORDS.contains(k) || matches!(k, "full" | "rt" | "bin" | "serde1" | "core" | "zeroize" | "default-tls" | "capi" | "trace" | "bench" | "unstable" | "debug" | "std" | "tokio" | "static" | "dynamic" | "test" | "log" | "serialize" | "time" | "vendored" | "doc" | "docs" | "alloc" | "serde" | "arbitrary" |"testing" | "native-tls") {
            continue;
        }
        let kw = Weight::new(0.2, false, Sources::IDENTS | Sources::DERIVED);
        existing_keywords.insert(normalize_keyword(k), kw);
    }

    for k in inp.github_keywords.unwrap_or_default() {
        existing_keywords.insert(inp.tag_synonyms.normalize(&normalize_keyword(k), 3), Weight::new(inp.repo_relevance, true, Sources::REPO_META));
    }

    for k in inp.bad_categories {
        existing_keywords.insert(normalize_keyword(k), Weight::new(0.99, true, Sources::KEYWORDS));
    }

    if let Some(n) = inp.parent_crate_name {
        let kw = Weight::new(0.95, false, Sources::REPO_META);
        existing_keywords.insert(normalize_keyword(n), kw);
        all_phrases.push((n.into(), kw));
    }

    // but also explicitly add them word by word
    let must_keep_keywords: Vec<_> = package.keywords().iter().map(|k| {
        let mut k = normalize_keyword(k);
        let alt = inp.tag_synonyms.normalize(&k, 4);
        if alt != k {
            k = alt.into();
        }
        k
    }).collect();

    for k in &must_keep_keywords {
        existing_keywords.insert(k.as_str(), Weight::new(3., true, Sources::KEYWORDS));
    }

    let tag_synonyms = inp.tag_synonyms;
    let all_keywords = inp.all_keywords;

    #[cfg(debug_assertions)]
    all_phrases.sort_by(|a, b| b.1.weight.total_cmp(&a.1.weight));

    auto_keywords_raw(&all_phrases, existing_keywords.keywords, all_keywords, tag_synonyms, must_keep_keywords, package.name())
}

// visible + invisible
pub fn auto_keywords_raw(all_phrases: &[(Cow<'_, str>, Weight)], existing_keywords: HashMap<SmolStr, Weight>, all_keywords: &HashMap<SmolStr, u32>, tag_synonyms: &Synonyms, must_keep_keywords: Vec<SmolStr>, own_name: &str) -> (Vec<(f32, SmolStr)>, Vec<(f32, SmolStr)>) {
    let existing_keywords = KeywordBag { keywords: existing_keywords };

    // if we have very little input text, then keywords will suck, so make them all less certain
    let input_data_amount = all_phrases.iter().map(|(text, kw)| if kw.visible { kw.weight } else { 0.1 * kw.weight } * text.len() as f32).sum::<f32>();
    let overall_certainity = 0.4 + ((input_data_amount+1.).sqrt() / 16.).min(0.5);

    #[cfg(debug_assertions)] {
        debug!("guessing auto_keywords {} from {} phrases: dat={input_data_amount:0.2} maxq={overall_certainity:0.2}", own_name, all_phrases.len());
        let mut acc = String::new();
        let mut last_w = all_phrases.get(0).map(|(_, w)| w.weight).unwrap_or(0.);
        let mut last_sources = all_phrases.get(0).map(|(_, w)| w.sources).unwrap_or(Sources::empty());
        for (text, kw) in all_phrases {
            if last_sources.bits() != kw.sources.bits() || acc.len() > 120 || kw.weight < last_w - 0.05 {
                debug!("{last_sources:?} {last_w:0.2}: {acc}");
                last_w = kw.weight;
                last_sources = kw.sources;
                acc.clear();
            }
            acc.push_str(if kw.visible { " . " } else { " ! " });
            acc.push_str(text);
        }
        debug!("{last_sources:?}  {last_w:0.2}: {acc}");
    }

    debug!("auto_keywords {} - existing_keywords: {existing_keywords:?}", own_name);
    let mut keywords = combined_keywords(&all_phrases, existing_keywords, all_keywords, tag_synonyms);

    // TF-IDF etc
    keywords.keywords.iter_mut().for_each(|(w, kw)| {
        let pop = all_keywords.get(&**w).copied().unwrap_or(0);
        kw.weight *= 0.5 + popular_keyword_weight(pop);
        if !kw.visible {
            kw.weight *= if w.contains('-') { 0.95 } else { 0.8 };
        }
        if !w.chars().any(char::is_alphabetic) {
            kw.weight *= 0.5;
        }
    });

    // not needed?
    // Make sure keywords specified by the package are there
    for k in must_keep_keywords {
        let kw = if let Some(kw) = keywords.keywords.get_mut(&k) {
            kw
        } else {
            let syn = tag_synonyms.normalize(&k, 1);
            if let Some(kw) = keywords.keywords.get_mut(syn) {
                kw
            } else {
                warn!("how did it lose a keyword!? #{k}");
                keywords.insert(k, Weight::new(1., true, Sources::KEYWORDS));
                continue;
            }
        };
        if !kw.visible {
            debug!("auto_keywords kw needed vis #{k}");
            kw.visible = true;
        }
        if kw.weight < 0.8 {
            debug!("auto_keywords kw needed fixing #{k}");
            kw.weight = 0.8;
        }
    }

    if !keywords.keywords.contains_key("cargo-subcommand") {
        for mentionable in ["cargo", "cargo-toml", "cargo-clean", "cargo-check", "cargo-install", "cargo-build", "cargo-test"] {
            if let Some(kw) = keywords.keywords.get_mut(mentionable) {
                // if only in readme, this could be just bad readme scrape
                if (kw.sources & !Sources::DERIVED).bits() == Sources::README.bits() {
                    kw.weight *= 0.1;
                }
            }
        }
    }

    // Swap normalized synonyms to have higher score than irregular keywords
    let mut synonyms = HashMap::<&str, &mut Weight>::with_capacity_and_hasher(keywords.keywords.len(), Default::default());
    keywords.keywords.iter_mut().for_each(move |(w, kw)| {
        let norm = tag_synonyms.normalize(w, 3);
        if norm != w {
            if let Some(preferred) = synonyms.get_mut(&norm) {
                if preferred.weight < kw.weight {
                    preferred.weight = kw.weight;
                }
                preferred.sources |= kw.sources;
                preferred.visible |= kw.visible;
                kw.visible = false;
            }
        }
        // this process has to be symmetric, so random hashmap order doesn't matter
        synonyms.insert(norm, kw);
    });

    debug!("auto_keywords {} after tfidf {keywords:?}", own_name);

    let mut all: Vec<_> = keywords.keywords.into_iter()
        .filter(|(_, kw)| kw.weight > 0.0001) // that's before normalization?
        .collect();

    // cache the score?
    pick_top_n_unstable_by(&mut all, 150, |a, b| b.1.combined_score().total_cmp(&a.1.combined_score()));

    let mut dupes = HashSet::default();

    // sorted by weight, so more important always wins
    let mut prev_word = None;
    let mut num_original_visible_keywords = 0;
    all.iter_mut().for_each(|(name, kw)| {
        let name = &**name;

        if crate_name_fuzzy_eq(name, own_name) {
            kw.weight *= 0.9;
            kw.visible = false;
        }

        if !kw.visible { return; }

        // use orig keywords to remove other dupes,
        // but don't let auto-keywords dupes remove orig keywords
        let is_original_keyword = kw.sources.contains(Sources::KEYWORDS) && !kw.sources.contains(Sources::DERIVED);

        // don't have "data-structures, data, structures"
        let mut rest = name;
        while let Some((part1, part2)) = rest.rsplit_once('-') {
            dupes.extend([Cow::Borrowed(part1), Cow::Borrowed(part2)]);
            rest = part1;
        }

        // don't have "data, structures, data-structures"
        if let Some(prev) = prev_word.replace(name) {
            dupes.insert(format!("{prev}-{name}").into());
        }

        if !dupes.insert(Cow::Borrowed(name)) && !is_original_keyword {
            kw.visible = false;
        }
        if let Some((replaced, _)) = tag_synonyms.get(name) {
            if !dupes.insert(Cow::Borrowed(replaced)) && !is_original_keyword {
                kw.visible = false;
            }
            let replaced2 = tag_synonyms.normalize(replaced, 1);
            if replaced2 != replaced && !dupes.insert(Cow::Borrowed(replaced2)) && !is_original_keyword {
                kw.visible = false;
            }
        }

        // derived will be the same keywords but joined
        if kw.visible && is_original_keyword {
            num_original_visible_keywords += 1;
        }
    });
    drop(dupes);

    // Original keywords were good enough to keep
    if num_original_visible_keywords > 2 {
        debug!("kept {} keywords", num_original_visible_keywords);
        for (_, kw) in &mut all {
            if !kw.sources.contains(Sources::KEYWORDS) && !kw.sources.contains(Sources::DERIVED) {
                kw.visible = false;
            }
        }
    }

    // Make at least 3 visible keywords
    let visible_cnt = all.iter().filter(|(_, kw)| kw.visible).take(3).count();
    if visible_cnt < 3 {
        let mut dupes = HashSet::default();
        all.iter_mut()
            .filter(move |(k, kw)| {
                let novel = dupes.insert(SmolStr::from(tag_synonyms.normalize(k, 1)));
                kw.visible || (novel && kw.sources.visible())
            })
            .take(3)
            .for_each(|(w, kw)| {
                debug!("Added '{w}' {kw:?} fallback to visible keywords");
                kw.visible = true;
            });
    }

    // the highest score can be far above others, so prefer to saturate than drop others
    let norm = all.get(0).map_or(1., |(_, kw)| kw.combined_score());
    let others = all.get(1).map_or(f32::INFINITY, |(_, kw)| kw.combined_score() * 1.2)
        .min(all.get(2).map_or(f32::INFINITY, |(_, kw)| kw.combined_score() * 1.5));
    let norm = (norm*0.33).max(others).min(norm);

    let mut out_visible = Vec::with_capacity(all.len().min(65));
    let mut out_invisible = Vec::with_capacity(all.len().min(65));
    all.into_iter().for_each(|(word, kw)| {
        let mut vis = kw.visible;
        if vis && out_visible.len() > 6 { vis = false; }
        if !vis && out_invisible.len() > 25 { return; }

        let score = (kw.combined_score() * overall_certainity * (1. / norm)).min(1.);

        let keep = score > 0.01 && ((vis && out_visible.len() < 3) || (!vis && out_invisible.len() < 5) || score > 0.05);
        if keep {
            if vis { &mut out_visible } else { &mut out_invisible }.push((score, word));
        }
    });

    debug!("auto_keywords fin {} ({}vis/{num_original_visible_keywords}orig) {out_visible:?} + {out_invisible:?}", own_name, out_visible.len());
    (out_visible, out_invisible)
}

fn keywords_from_doc_idents(text_from_source_code: &TextFromSourceCode, all_keywords: &HashMap<SmolStr, u32>, tag_synonyms: &Synonyms) -> KeywordBag {
    let mut doc_phrases = Vec::new();

    let max_doc_weight = text_from_source_code.doc.iter().map(|&(w, _)| w).max().unwrap_or(1).max(1) as f32;
    for (w, text) in &text_from_source_code.doc {
        let w = *w as f32 / max_doc_weight;
        let mut seen_lines = HashSet::default();
        for (i, line) in text.lines().enumerate() {
            let line = line.trim_start();
            if line.is_empty() {
                continue;
            }
            let w2 = 1. - (i as f32 / 100.).min(0.15);
            if seen_lines.insert(line) {
                doc_phrases.push((line, Weight::new(w * w2 * 0.39, false, Sources::DOCS)));
            }
        }
    }

    let doc_phrases = clean_phrases(doc_phrases);
    let mut doc_keywords = combined_keywords(&doc_phrases, KeywordBag::new(), all_keywords, tag_synonyms);

    // normalize weight, because doc can be very long and add up to very dominating keywords
    let max_weight = doc_keywords.keywords.values().map(|kw| kw.weight).max_by(|a, b| b.total_cmp(a)).unwrap_or(1.).max(1.);
    doc_keywords.keywords.values_mut().for_each(move |kw| kw.weight *= 1. / max_weight);

    let ident_max = 0.8 * text_from_source_code.idents.iter().map(|&(w, _)| w).max().unwrap_or(1).max(1) as f32;
    text_from_source_code.idents.iter().for_each(|(w, k)| {
        let weight = (*w as f32 / ident_max).sqrt() * if k.contains('-') { 1. } else { 0.75 };
        doc_keywords.insert(k.as_str(), Weight::new(weight, false, Sources::IDENTS));
    });

    // normalize again, because idents can be repetitive too,
    // and now clamp to 0.5 max weight, because other sources should be able to have higher weight than these
    let max_weight = doc_keywords.keywords.values().map(|kw| kw.weight).max_by(|a, b| b.total_cmp(a)).unwrap_or(1.).max(1.);
    doc_keywords.keywords.values_mut().for_each(move |kw| kw.weight *= 0.5 * 1. / max_weight);

    debug!("auto_keywords from docs/idents {doc_keywords:?}");
    doc_keywords
}

fn clean_phrases(all_phrases: Vec<(&str, Weight)>) -> Vec<(Cow<'_, str>, Weight)> {
    all_phrases.into_iter().map(|(text, kw)| {
        let mut text = Cow::Borrowed(if kw.sources.contains(Sources::KEYWORDS) { text } else { trim_sentence_intro(text) });
        if text.contains("://") {
            text = text.replace("http://", " ").replace("https://", " ").replace("index.html", "").into();
        }
        if text.contains('’') {
            text = text.replace('’', "'").into();
        }
        let text_lc = text.as_ascii_lowercase();
        if text_lc.contains("rust programming language") {
            text = text_lc.replace("rust programming language", "rust").into();
        }
        (text, kw)
    }).collect()
}

// Remove "this is a …"
pub fn trim_sentence_intro(mut text: &str) -> &str {
    while let Some((word, rest)) = text.split_once(|c: char| c.is_ascii_whitespace() || c == ',' || c == '.' || c == ':') {
        let word = word.trim_matches(|c: char| c.is_ascii_punctuation()).trim_end_matches("'s");
        if !word.is_empty() {
            let word = &*word.as_ascii_lowercase();
            if !STOPWORDS.contains(word) && !IDENT_STOPWORDS.contains(word) &&
                !matches!(word,  "i" | "getting" | "if" | "install" | "set" | "get" | "return" | "tool" | "was" | "going" | "software" |
                    "package" | "lets" | "integrates" | "facilitates" | "designed" | "let" | "provide" | "support" | "for" |
                     "help" | "helps" | "methods" | "functions" | "convenient" | "a" | "an" | "the" | "it" | "this" | "is" |
                     "simple" | "use" | "library" | "crate" | "rusty" | "which" | "provides" | "implementation" | "rust" | "to" |
                     "-" | "–" | "of" | "helpful" | "and" | "easy-to-use" | "yet" | "another" | "make" | "you" | "with" | "utility" | "utilities") {
                break;
            }
        }
        text = rest;
    }
    text
}

#[test]
fn depr_text() {
    for txt in [
        "abandoned. Some text later.",
        "an empty crate. Some text later.",
        "Bar! deprecated",
        "Bar! please dont use",
        "crate is abandoned. Some text later.",
        "crate renamed to . Some text later.",
        "deprecated. Some text later.",
        "discontinued. Some text later.",
        "do not use. Some text later.",
        "Foo. deprecated in favor",
        "Foo. No longer developed. ",
        "Foo. project is deprecated",
        "Foo. this crate has been abandoned",
        "Foo. this crate is abandoned",
        "Foo. this is a dummy package",
        "Foo. this tool is abandoned",
        "obsolete. Some text later.",
        "Please use `gix-. Some text later.",
        "renamed to . Some text later.",
        "temp crate do not use. Some text later.",
        "this crate is deprecated. Some text later.",
        "this crate was renamed. Some text later.",
        "this package was renamed. Some text later.",
        "unsafe and deprecated. Some text later.",
        "very early wip. Some text later.",
    ] {
        assert!(DEPRECATED_DESC_REGEX.is_match(txt), "{txt}");
    }
}
