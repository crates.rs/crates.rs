use crate::download_graph::DownloadsGraph;
use crate::urler::Urler;
use crate::{base_urls_fallback, limit_text_len_mid, make_link_fixer, parsed_url_domain, templates, url_domain, Page};
use util::{FxHashMap as HashMap, FxHashSet as HashSet};
use anyhow::Context;
use cargo_toml::Edition;
use categories::{Category, CATEGORIES};
use chrono::prelude::*;
use chrono::Duration;
use futures::stream::StreamExt;
use kitchen_sink::{
    ABlockAction, ArcRichCrateVersion, CResult, CrateAuthor, DepInfMap, DepTy, DepsStats, KitchenSink, KitchenSinkErr, Kontext, Origin, RendCtx, RevDependencies, Severity
};
use locale::Numeric;
use log::warn;
use manifest::IdentifiedBinFile;
use render_readme::{Links, LinksContext, Renderer};
use rich_crate::{MaintenanceStatus, Manifest, ManifestExt, Readme, Repo, RichCrate, RichCrateVersion, RichDep};
use semver::Version as SemVer;
use std::borrow::Cow;
use std::cmp::Ordering;
use std::f64::consts::PI;
use std::fmt::Display;
use std::hash::Hash;
use std::path::Path;
use std::time::Instant;
use udedokei::{Language, LanguageExt, Lines, Stats};
use util::SmolStr;

pub(crate) struct CrateLicense {
    pub origin: Origin,
    pub license: Box<str>,
    pub optional: bool,
}

pub(crate) struct CrateSizes {
    pub tarball: u64,
    pub uncompressed: u64,
    pub minimal: DepsSize,
    pub typical: DepsSize,
}

pub(crate) struct Link<'a> {
    pub url: Cow<'a, str>,
    pub label: Cow<'a, str>,
    pub rel: templates::Html<&'static str>,
}

/// Data sources used in `crate_page.rs.html`
pub struct CratePage<'a> {
    pub(crate) all: &'a RichCrate,
    pub(crate) ver: &'a RichCrateVersion,
    pub(crate) kitchen_sink: &'a KitchenSink,
    pub(crate) markup: &'a Renderer,
    pub(crate) top_keyword: Option<(u32, String)>,
    pub(crate) crate_owners_and_contributors: (Vec<CrateAuthor>, Vec<CrateAuthor>, usize),
    fediverse_creators: Vec<String>,
    /// own, deps (tarball, uncompressed source); last one is sloc
    pub(crate) sizes: Option<CrateSizes>,
    pub(crate) lang_stats: Option<(usize, Stats)>,
    pub(crate) viral_license: Option<CrateLicense>,
    pub(crate) is_obsolete: bool,
    top_category: Option<(u32, &'static Category)>,
    is_build_or_dev: (bool, bool),
    api_reference_url: Option<String>,
    former_glory: f64,
    dependents_stats: Option<RevDependencies>,
    related_crates: Vec<Origin>,
    ns_crates: Vec<ArcRichCrateVersion>,
    keywords_populated: Vec<(&'a str, bool)>,
    parent_crate: Option<ArcRichCrateVersion>,
    pub(crate) top_versions: Vec<VersionGroup<'a>>,
    pub(crate) show_audit_link: bool,
    pub(crate) banned: bool,
    pub(crate) blocklist_actions: Vec<ABlockAction<'a>>,
    pub(crate) blocklist_must_remove_data: bool,
    pub(crate) security_advisory_url: Option<String>,
    has_verified_repository_link: bool,
    downloads_per_month_cached: Option<usize>,
    github_stargazers_and_watchers: Option<(u32, u32)>,
    direct_dependencies: (Vec<RichDep>, Vec<RichDep>, Vec<RichDep>),
    up_to_date_class_cache: HashMap<String, &'static str>,
    nofollow: bool,
    pub(crate) is_unmaintained: bool,
    // path - type
    pub(crate) suspicious_binary_files: Vec<IdentifiedBinFile>,
    pub(crate) deprecation_reason: Option<String>,
    deadline: Instant,
    pub changelog_url: Option<String>,
    manifest: Manifest,
}

/// Helper used to find most "interesting" versions
#[derive(Debug)]
pub struct VersionGroup<'a> {
    pub ver: Version<'a>,
    pub count: usize,
}

#[derive(Debug)]
pub struct Version<'a> {
    pub num: &'a str,
    pub semver: SemVer,
    pub yanked: bool,
    pub created_at: DateTime<Utc>,
}

#[derive(Debug, Default)]
struct ReleaseCounts {
    total: u32,
    stable: u32,
    major: u32,
    major_recent: u32,
    patch: u32,
    unstable: u32,
    breaking: u32,
    breaking_recent: u32,
}

pub(crate) struct Contributors<'a> {
    pub authors: &'a [CrateAuthor],
    pub owners: &'a [CrateAuthor],
    pub co_owned: bool,
    pub contributors: usize,
    pub period_after_authors: bool,
    pub contributors_as_a_team: bool,
}

impl<'a> CratePage<'a> {
    pub async fn new(all: &'a RichCrate, ver: &'a RichCrateVersion, kitchen_sink: &'a KitchenSink, similar: Vec<(Origin, f32)>, markup: &'a Renderer, rend: &RendCtx) -> CResult<CratePage<'a>> {
        let origin = all.origin();
        let (is_obsolete, top_category, parent_crate, keywords_populated, (related_crates, ns_crates, downloads_per_month_or_equivalent), has_docs_rs, changelog_url) = futures::join!(
            blocking::awatch("%1", kitchen_sink.is_crate_obsolete(all)),
            blocking::awatch("%2", kitchen_sink.top_category(ver, rend)),
            blocking::awatch("%3", Box::pin(kitchen_sink.parent_crate(ver))),
            blocking::awatch("%4", Box::pin(kitchen_sink.keywords_populated(ver))),
            blocking::awatch("%5", async move {
                let downloads_per_month_or_equivalent = kitchen_sink.downloads_per_month_or_equivalent(origin).await.ok().and_then(|x| x);
                let (related_crates, ns_crates) = Self::make_related_crates(kitchen_sink, ver, downloads_per_month_or_equivalent, similar).await;
                (related_crates, ns_crates, downloads_per_month_or_equivalent)
            }),
            blocking::awatch("%6", kitchen_sink.has_docs_rs(origin, ver.short_name(), ver.version())),
            blocking::awatch("%7", kitchen_sink.changelog_url(ver)),
        );

        let deadline = rend.deadline.min(Instant::now() + std::time::Duration::from_secs(20));
        let mut is_build_or_dev = kitchen_sink.is_build_or_dev(origin)?;
        let stats = kitchen_sink.index()?.deps_stats().context("Dependency stats")?;
        let (traction_stats, top_keyword, crate_owners_and_contributors, downloads_per_month_cached, github_stargazers_and_watchers, vet_reviews, has_verified_repository_link, manifest) = futures::try_join!(
            blocking::awatch("%B", kitchen_sink.traction_stats(origin, stats)),
            blocking::awatch("%C", async { if top_category.is_some() { Ok(None) } else { kitchen_sink.top_keyword(all).await }}),
            blocking::awatch("%D", kitchen_sink.crate_owners_and_contributors(ver, deadline.into())),
            blocking::awatch("%7", kitchen_sink.downloads_per_month(origin, false)),
            blocking::awatch("%8", kitchen_sink.github_stargazers_and_watchers(origin)),
            blocking::awatch("%9", kitchen_sink.vets_for_crate(origin)),
            blocking::awatch("%A", kitchen_sink.has_verified_repository_link(ver)),
            blocking::awatch("%M", kitchen_sink.crate_manifest(ver.origin())),
        )?;
        let (all_owners_match, blocklist_actions) = blocking::awatch("%F", kitchen_sink.crate_blocklist_actions(all.origin())).await;

        let deprecation_reason = kitchen_sink.is_marked_deprecated_reason(origin);

        let crev_reviews = blocking::ablock_in_place("%r", || kitchen_sink.reviews_for_crate(origin)).await;
        let advisories = blocking::ablock_in_place("%R", || kitchen_sink.advisories_for_crate(origin)).await.unwrap_or_default();
        let semver: SemVer = ver.version_semver()?;
        let is_unmaintained = deprecation_reason.as_deref() == Some("description-unmaintained") || kitchen_sink.is_unmaintained_in_advisories(origin, &advisories, &crev_reviews, &semver).unwrap_or(false);
        let advisory = advisories.into_iter()
            .filter(|a| !a.withdrawn() && a.versions.is_vulnerable(&semver))
            .max_by_key(|a| a.severity().unwrap_or(Severity::None));
        let has_advisories = advisory.is_some();
        let security_advisory_url = advisory.filter(|a| a.severity().is_some() || a.metadata.informational.is_none()).and_then(|a| a.id().url());

        let top_category = top_category
            .and_then(|(top, slug)| CATEGORIES.from_slug(slug).0.last().map(|&c| (top, c)));

        if ver.category_slugs().iter().any(|s| &**s == "development-tools::build-utils") {
            is_build_or_dev.0 = true;
        }

        let blocklist_must_remove_data = all_owners_match && blocklist_actions.iter().any(|a| a.is_block() || a.is_redirect());
        let banned = all_owners_match && blocklist_actions.iter().all(|a| a.is_ban());

        let api_reference_url = if has_docs_rs {
            Some(format!("https://docs.rs/{}", ver.short_name()))
        } else {
            None
        };

        let has_reviews = !vet_reviews.is_empty() || !crev_reviews.is_empty();

        // trust >20 is about top 2000 authors
        let min_trust = if has_verified_repository_link || has_reviews { 15. } else { 25. };
        let any_owner_is_trusted = crate_owners_and_contributors.0.iter().chain(&crate_owners_and_contributors.1).filter_map(|c| c.trust).any(move |t| t > min_trust);
        let has_low_traffic = downloads_per_month_or_equivalent.unwrap_or(0) < 3500;
        let nofollow = has_low_traffic && !any_owner_is_trusted;

        let fediverse_creators = if !nofollow && !all_owners_match {
            let logins = crate_owners_and_contributors.0.iter().chain(&crate_owners_and_contributors.1)
                .filter(|o| o.owner).filter_map(|o| o.github_login());
            futures::stream::iter(logins)
                .filter_map(|gh| async move {
                    Some(kitchen_sink.rustacean_for_github_login(gh).await?
                        .mastodon()?.1.to_string())
                })
                .collect().await
        } else {
            Vec::new()
        };

        let mut suspicious_binary_files = blocking::block_in_place("sus", || kitchen_sink.get_suspicious_binary_files(origin, ver.version())).kontext("Checking for binaries in tarball")?;
        suspicious_binary_files.iter_mut().take(10).for_each(|f| {
            if let Some(path) = f.path.to_str() {
                if path.len() > 40 {
                    if let Some(fname) = f.path.file_name().and_then(|f| f.to_str()) {
                        f.path = limit_text_len_mid(fname, 80).into_owned().into();
                    }
                }
            }
        });

        let stats = kitchen_sink.index()?.deps_stats().context("Dependency stats")?;
        let dependents_stats = blocking::ablock_in_place("fdso", || stats.crates_io_full_dependents_stats_of(origin).cloned()).await;
        let mut page = Self {
            is_obsolete,
            up_to_date_class_cache: HashMap::default(),
            direct_dependencies: manifest.direct_dependencies(),
            downloads_per_month_cached,
            github_stargazers_and_watchers,
            security_advisory_url,
            top_keyword,
            crate_owners_and_contributors,
            fediverse_creators,
            all,
            ver,
            kitchen_sink,
            markup,
            sizes: None,
            lang_stats: None,
            viral_license: None,
            top_category,
            is_build_or_dev,
            api_reference_url,
            former_glory: traction_stats.map_or(1., |t| t.former_glory),
            dependents_stats,
            related_crates,
            ns_crates,
            keywords_populated,
            parent_crate,
            show_audit_link: has_reviews || has_advisories || !suspicious_binary_files.is_empty(),
            blocklist_must_remove_data,
            blocklist_actions,
            banned,
            top_versions: Vec::new(),
            has_verified_repository_link,
            nofollow,
            is_unmaintained,
            deprecation_reason,
            suspicious_binary_files,
            deadline: rend.deadline.min(Instant::now() + std::time::Duration::from_secs(4)),
            changelog_url,
            manifest,
        };
        blocking::awatch("%U", page.up_to_date_class_prefetch(stats)).await;
        let all_deps_flat = kitchen_sink.all_dependencies_flattened(&page.manifest).kontext("Error getting full resolved list of dependencies")?;
        let (sizes, lang_stats, viral_license) = page.crate_size_and_viral_license(all_deps_flat, stats).await.context("Dependency checks")?;
        page.sizes = Some(sizes);
        page.viral_license = viral_license;

        let total = lang_stats.langs.iter().filter(|(lang, _)| lang.is_code()).map(|(_, lines)| lines.code).sum::<u32>();
        page.lang_stats = Some((total as usize, lang_stats));
        page.top_versions = page.make_top_versions();
        Ok(page)
    }

    pub(crate) fn page(&self, url: &Urler) -> Page {
        let keywords = self.ver.keywords().join(", ");
        let c = self.all_contributors();
        let author = c.authors.iter().chain(c.owners).find(|a| a.owner).map(|a| a.trusted_name().to_string());
        Page {
            title: self.page_title(),
            keywords: if !keywords.is_empty() { Some(keywords) } else { None },
            created: self.date_created_string(),
            description: self.ver.description().map(|d| format!("{d} | Rust/Cargo package")),
            item_name: Some(self.ver.short_name().to_string()),
            item_description: self.ver.description().map(|d| d.to_string()),
            alternate: url.crates_io_crate(self.ver.origin()),
            canonical: Some(format!("https://lib.rs{}", url.crate_abs_path_by_origin(self.ver.origin()))),
            noindex: self.ver.is_yanked() || self.ver.is_hidden() || self.ver.is_spam() || self.blocklist_must_remove_data,
            search_meta: false,
            og_section: self.ver.category_slugs().get(0).map(|c| c.to_string()),
            og_image: (self.ver.origin().is_crates_io() && self.ver.description().is_some_and(|d| d.is_ascii())).then(|| (self.ver.short_name().to_string(), "c")),
            author,
            fediverse_creators: self.fediverse_creators.clone(),
            ..Default::default()
        }
    }

    pub(crate) fn page_title(&self) -> String {
        let slugs = self.ver.category_slugs();
        let (is_build, is_dev) = self.is_build_or_dev;
        let kind = if self.ver.has_bin() {
            if slugs.iter().any(|s| &**s == "development-tools::cargo-plugins") {
                "Cargo add-on"
            } else if slugs.iter().any(|s| &**s == "command-line-utilities") {
                "command-line utility in Rust"
            } else if is_build {
                "Rust build tool"
            } else if is_dev {
                "Rust dev tool"
            } else if self.ver.is_app() {
                "Rust application"
            } else {
                "Rust utility"
            }
        } else if self.ver.is_sys() {
            "system library interface for Rust"
        } else if is_build && self.ver.has_lib() {
            "Cargo library"
        } else if let Some(cat) = slugs.get(0).and_then(|slug| CATEGORIES.from_slug(slug).0.last().copied()) {
            &cat.title
        } else if self.ver.has_lib() {
            "Rust library"
        } else {
            "Rust crate"
        };
        let name_capital = self.ver.capitalized_name();

        let deprecation_reason = self.deprecation_reason.as_deref();

        let label = if self.ver.is_yanked() { "deleted" }
            else if self.is_unmaintained { "unmaintained" }
            else if self.ver.maintenance() == MaintenanceStatus::Deprecated || deprecation_reason == Some("deprecated-repo") || deprecation_reason == Some("description-deprecated") { "deprecated" }
            else if self.former_glory < 0.3 { "unpopular" }
            else if self.ver.maintenance() == MaintenanceStatus::AsIs { "minimal maintenance" }
            else if deprecation_reason == Some("archived") { "archived" }
            else if self.ver.is_spam() || self.ver.is_hidden() || self.banned || self.deprecation_reason.is_some() { "not recommended" }
            else { "" };

        if !label.is_empty() {
            format!("{name_capital} {} [{label}]", self.ver.version())
        } else {
            format!("{name_capital} — {kind}")
        }
    }

    pub(crate) fn is_build_or_dev(&self) -> (bool, bool) {
        self.is_build_or_dev
    }

    pub(crate) fn edition(&self) -> Edition {
        self.manifest.package().edition()
    }

    pub(crate) fn dependents_stats(&self) -> Option<DepsStatsResult<'_>> {
        let d = self.dependents_stats.as_ref()?;
        let d = DepsStatsResult {
            deps: d.counters.runtime.def + d.counters.runtime.opt + d.counters.build.def + d.counters.build.opt + u32::from(d.counters.dev),
            direct: d.counters.direct.all(),
            name: d.rev_dep_names_default.iter().chain(d.rev_dep_names_optional.iter()).chain(d.rev_dep_names_dev.iter()).next(),
            former_glory: self.former_glory,
        };
        if d.deps == 0 {
            return None;
        }
        Some(d)
    }

    /// If true, there are many other crates with this keyword. Populated first.
    pub(crate) fn keywords_populated(&self) -> Option<&[(&str, bool)]> {
        if !self.keywords_populated.is_empty() {
            Some(&self.keywords_populated)
        } else {
            None
        }
    }

    pub(crate) fn parent_crate(&self) -> Option<&RichCrateVersion> {
        self.parent_crate.as_deref()
    }

    pub(crate) fn render_maybe_markdown_str(&self, s: &str) -> templates::Html<String> {
        let mut tmp = None;
        let base_urls = base_urls_fallback(self.ver, &mut tmp);
        crate::render_maybe_markdown_str(s, self.markup, base_urls, Some(self.ver.short_name()))
    }

    pub(crate) fn render_lib_intro(&self) -> Option<templates::Html<String>> {
        let k = &self.ver;
        k.lib_file_markdown().map(|markup| {
            let mut tmp = None;
            let docs_url = k.docs_rs_url();
            let base_url = docs_url.as_deref().map(|u| (u, u)).or_else(|| base_urls_fallback(k, &mut tmp));
            let own_crate_name = Some(k.short_name());
            let (html, warnings) = self.markup.page(&markup, &LinksContext {
                base_url,
                nofollow: self.nofollow_markup(),
                own_crate_name,
                link_own_crate_to_crates_io: true,
                link_fixer: Some(&make_link_fixer(own_crate_name, base_url, Some(&|name| self.exists_check(name)))),
            }, true, self.deadline);
            if !warnings.is_empty() {
                warn!("{} lib: {:?}", k.short_name(), warnings);
            }
            templates::Html(html)
        })
    }

    pub(crate) fn is_readme_short_or_irrelevant(&self) -> bool {
        log::debug!("missing readme? {}", self.ver.readme_was_missing());
        self.ver.readme_was_missing() || self.kitchen_sink.is_readme_short(self.ver.readme().as_ref().map(|r| &r.markup))
    }

    pub(crate) fn has_no_readme_or_lib(&self) -> bool {
        self.ver.readme().is_none() && self.ver.lib_file_markdown().is_none()
    }

    pub(crate) fn render_readme(&self, readme: &Readme) -> templates::Html<String> {
        blocking::block_in_place("rr", move || {
        let k = &self.ver;
        let mut tmp = None;
        let base_url = readme.base_urls().or_else(|| base_urls_fallback(k, &mut tmp));
        log::debug!("readme base {base_url:?} of {}", k.short_name());
        let own_crate_name = Some(k.short_name());
        let (html, warnings) = self.markup.page(&readme.markup, &LinksContext {
            base_url,
            nofollow: self.nofollow_markup(),
            own_crate_name,
            link_own_crate_to_crates_io: true,
            link_fixer: Some(&make_link_fixer(own_crate_name, base_url, Some(&|name| self.exists_check(name)))),
        }, false, self.deadline);
        if !warnings.is_empty() {
            warn!("{} readme: {:?}", k.short_name(), warnings);
        }
        templates::Html(html)
        })
    }

    fn exists_check(&self, name: &str) -> bool {
        Origin::try_from_crates_io_name(name).is_some_and(move |o| self.kitchen_sink.crate_exists(&o))
    }

    pub(crate) fn rel_nofollow(&self) -> templates::Html<&'static str> {
        templates::Html(if self.nofollow {"rel=\"nofollow ugc\""} else {""})
    }

    pub(crate) fn nofollow_markup(&self) -> Links {
        // TODO: take multiple factors into account, like # of contributors, author reputation, dependents
        if self.nofollow {
            Links::Ugc
        } else {
            Links::FollowUgc
        }
    }

    pub(crate) fn all_contributors(&self) -> Contributors<'_> {
        let (authors, owners, contributors) = &self.crate_owners_and_contributors;
        let contributors = *contributors;
        let mut authors = authors.as_slice();
        let mut owners = owners.as_slice();

        let co_owned = !owners.is_empty() && authors.iter().any(|a| a.owner);
        let period_after_authors = !owners.is_empty() && contributors == 0;
        let contributors_as_a_team = authors.last().is_some_and(|last| !last.owner && last.not_a_person);
        // avoid "x contributors. owned by y"
        if authors.is_empty() && !owners.is_empty() && contributors > 0 {
            authors = owners;
            owners = &[];
        }
        Contributors { authors, owners, co_owned, contributors, period_after_authors, contributors_as_a_team }
    }

    pub(crate) fn format_number(&self, num: impl Display) -> String {
        Numeric::english().format_int(num)
    }

    pub(crate) fn format_knumber(&self, num: usize) -> (String, &'static str) {
        let (num, unit) = match num {
            0..=899 => (num, ""),
            0..=8000 => return (format!("{}", ((num + 250) / 500) as f64 * 0.5), "K"), // 3.5K
            0..=899_999 => ((num + 500) / 1000, "K"),
            0..=9_999_999 => return (format!("{}", ((num + 250_000) / 500_000) as f64 * 0.5), "M"), // 3.5M
            _ => ((num + 500_000) / 1_000_000, "M"),                                                // 10M
        };
        (Numeric::english().format_int(num), unit)
    }

    pub(crate) fn format_kbytes(&self, bytes: u64) -> String {
        crate::format_kbytes(bytes)
    }

    fn format_number_frac(num: f64) -> String {
        if num > 0.05 && num < 10. && num.fract() > 0.09 && num.fract() < 0.9 {
            if num < 3. {
                format!("{num:.1}")
            } else {
                format!("{}", (num * 2.).round() / 2.)
            }
        } else {
            Numeric::english().format_int(if num > 500. {
                (num / 10.).round() * 10.
            } else if num > 100. {
                (num / 5.).round() * 5.
            } else {
                num.round()
            })
        }
    }

    pub(crate) fn format_kbytes_range(&self, a: u64, b: u64) -> String {
        let min_bytes = a.min(b);
        let max_bytes = a.max(b);

        // if the range is small, just display the upper number
        if min_bytes * 4 > max_bytes * 3 || max_bytes < 250_000 {
            return self.format_kbytes(max_bytes);
        }

        let (denom, unit) = match max_bytes {
            0..=800_000 => (1000., "KB"),
            _ => (1_000_000., "MB"),
        };
        let mut low_val = min_bytes as f64 / denom;
        let high_val = max_bytes as f64 / denom;
        if low_val > 1. && high_val > 10. {
            low_val = low_val.round(); // spread is so high that precision of low end isn't relevant
        }
        format!("{}–{}{unit}", Self::format_number_frac(low_val), Self::format_number_frac(high_val))
    }

    /// Display number 0..1 as percent
    pub(crate) fn format_fraction(&self, num: f64) -> String {
        if num < 1.9 {
            format!("{num:0.1}%")
        } else {
            format!("{}%", Numeric::english().format_int(num.round() as usize))
        }
    }

    pub(crate) fn format(date: &DateTime<Utc>) -> String {
        date.format("%b %e, %Y").to_string()
    }

    pub(crate) fn non_dep_features(&self) -> Option<Vec<SmolStr>> {
        let deps_features: HashSet<_> = self.direct_dependencies.0.iter().chain(&self.direct_dependencies.1).chain(&self.direct_dependencies.2)
            .flat_map(|dep| dep.only_for_features.keys().map(|k| k.as_str()).chain(Some(dep.user_alias.as_str())))
            .collect();

        let features: Vec<_> = self.manifest.features.keys()
            .filter(|&k| !k.starts_with('_') && k != "default" && !deps_features.contains(k.as_str()))
            .map(SmolStr::from)
            .collect();

        if !features.is_empty() {
            Some(features)
        } else {
            None
        }
    }

    pub(crate) fn direct_dependencies(&self) -> Option<(&[RichDep], &[RichDep], &[RichDep])> {
        Some((
            &self.direct_dependencies.0,
            &self.direct_dependencies.1,
            &self.direct_dependencies.2,
        ))
    }

    pub(crate) fn up_to_date_class(&self, richdep: &RichDep) -> &str {
        if richdep.dep.req() == "*" || !richdep.dep.is_crates_io() {
            return "common";
        }
        let key = format!("{}={}", richdep.user_alias, richdep.dep.req());
        self.up_to_date_class_cache.get(&key).copied().unwrap_or("obsolete")
    }

    async fn up_to_date_class_prefetch(&mut self, stats: &DepsStats) {
        for richdep in self.direct_dependencies.0.iter().chain(&self.direct_dependencies.1).chain(&self.direct_dependencies.2) {
            if richdep.dep.req() == "*" || !richdep.dep.is_crates_io() {
                continue;
            }
            let key = format!("{}={}", richdep.user_alias, richdep.dep.req());
            if let Ok(req) = richdep.dep.req().parse() {
                let origin = Origin::from_crates_io_name(&richdep.package);
                if let Ok(Some(pop)) = self.kitchen_sink.version_popularity(&origin, &req, stats).await {
                    let res = match pop.pop {
                        x if x >= 0.75 && !pop.lost_popularity && !pop.deprecated => "common", // hide the version completely
                        _ if pop.matches_latest && !pop.lost_popularity && !pop.deprecated => "verynew", // display version in black
                        x if x >= 0.33 => "outdated", // orange
                        _ if pop.deprecated => "outdated deprecated", // red
                        _ => "obsolete", // red
                    };
                    self.up_to_date_class_cache.insert(key, res);
                }
            }
        }
    }

    // tooltip in deps sidebar
    pub(crate) fn dep_title_attr(&self, richdep: &RichDep) -> String {
        use std::fmt::Write;

        let mut out = String::new();
        if richdep.package != richdep.user_alias {
            let _ = write!(&mut out, "renamed {}, ", richdep.user_alias);
        }
        let label = match self.up_to_date_class(richdep) {
            "common" => richdep.dep.req(),
            "outdated deprecated" => "deprecated",
            "verynew" => "new",
            other => other,
        };
        out.push_str(label);
        out
    }

    /// The rule is - last displayed digit may change (except 0.x)
    pub(crate) fn pretty_print_req(&self, reqstr: &str) -> String {
        if let Ok(req) = semver::VersionReq::parse(reqstr) {
            if req.comparators.len() == 1 {
                let pred = &req.comparators[0];
                if pred.pre.is_empty() {
                    use semver::Op::{Caret, Tilde, Wildcard};
                    match pred.op {
                        Tilde | Caret | Wildcard => {
                            return if pred.op == Tilde || (pred.major == 0 && pred.patch.is_some_and(|p| p > 0)) {
                                format!("{}.{}.{}", pred.major, pred.minor.unwrap_or(0), pred.patch.unwrap_or(0))
                            } else {
                                format!("{}.{}", pred.major, pred.minor.unwrap_or(0))
                            };
                        },
                        _ => {},
                    }
                }
            }
        }
        reqstr.to_string()
    }

    pub(crate) fn is_version_new(&self, ver: &Version<'_>, nth: usize) -> bool {
        nth == 0 /*latest*/ && ver.created_at > Utc::now() - Duration::try_weeks(1).unwrap()
    }

    pub(crate) fn license_file(&self) -> Option<&Path> {
        self.manifest.package().license_file()
    }

    pub(crate) fn license_name(&self) -> Option<&str> {
        self.manifest.license_name()
    }

    pub(crate) fn links(&self) -> Option<&str> {
        self.manifest.package().links()
    }

    pub(crate) fn has_runtime_deps(&self) -> bool {
        self.manifest.package().links().is_some() ||
            !self.manifest.dependencies.is_empty() ||
            self.manifest.target.values().any(|target| !target.dependencies.is_empty())
    }

    fn group_versions<K, I>(keep_first_n: usize, all: I) -> Vec<VersionGroup<'a>>
    where
        I: Iterator<Item = (K, VersionGroup<'a>)>,
        K: Eq + Hash,
    {
        use std::collections::hash_map::Entry::{Occupied, Vacant};
        let mut grouped = HashMap::<(K, bool), VersionGroup<'a>>::default();
        for (i, (key, v)) in all.enumerate() {
            let key = (key, i < keep_first_n);
            match grouped.entry(key) {
                Occupied(mut e) => {
                    let old = e.get_mut();
                    old.count += v.count;
                    if old.ver.semver < v.ver.semver && (old.ver.yanked || !v.ver.yanked) && (!old.ver.semver.pre.is_empty() || v.ver.semver.pre.is_empty()) {
                        old.ver = v.ver;
                    }
                },
                Vacant(e) => {
                    e.insert(v);
                },
            };
        }
        let mut grouped: Vec<_> = grouped.into_values().collect();
        grouped.sort_unstable_by(|a, b| b.ver.semver.cmp(&a.ver.semver));
        grouped
    }

    fn make_top_versions(&self) -> Vec<VersionGroup<'a>> {
        let all = self.all_versions();
        let grouped1 = Self::group_versions(
            0,
            all.map(|ver| {
                let key = (
                    ver.created_at.year(),
                    ver.created_at.month(),
                    ver.created_at.day(),
                    // semver exposes major bumps, specially for 0.x
                    if ver.semver.major == 0 { ver.semver.minor + 1 } else { 0 },
                    ver.semver.major,
                    // exposes minor changes
                    if ver.semver.major == 0 { ver.semver.patch + 1 } else { 0 },
                    ver.semver.minor,
                );
                (key, VersionGroup { count: 1, ver })
            }),
        );
        let grouped2 = if grouped1.len() > 5 {
            Self::group_versions(
                1,
                grouped1.into_iter().map(|v| {
                    (
                        (
                            v.ver.created_at.year(),
                            v.ver.created_at.month(),
                            // semver exposes major bumps, specially for 0.x
                            if v.ver.semver.major == 0 { v.ver.semver.minor + 1 } else { 0 },
                            v.ver.semver.major,
                        ),
                        v,
                    )
                }),
            )
        } else {
            grouped1
        };

        let mut top = if grouped2.len() > 8 {
            Self::group_versions(2, grouped2.into_iter().map(|v| ((v.ver.created_at.year(), v.ver.created_at.month() / 4, v.ver.semver.major), v)))
        } else {
            grouped2
        };
        if top.len() > 5 {
            top.swap_remove(4); // move last to 5th pos, so that first release is always seen
            top.truncate(5);
        }
        top
    }

    /// String describing how often breaking changes are made
    pub(crate) fn version_stats_summary(&self) -> Option<(String, Option<String>)> {
        self.version_stats().map(|v| v.summary())
    }

    /// Counts minor and major releases for the summary
    fn version_stats(&self) -> Option<ReleaseCounts> {
        let mut cnt = ReleaseCounts::default();
        let mut prev: Option<Version<'a>> = None;
        let mut all: Vec<_> = self.all_versions().filter(|v| !v.yanked).collect();
        all.sort_unstable_by(|a, b| a.semver.cmp(&b.semver));
        cnt.total = all.len() as u32;
        let recent = *all.iter().map(|d| &d.created_at).max()? - Duration::try_weeks(40).unwrap();
        for v in all {
            if v.semver.major == 0 {
                cnt.unstable += 1;
                if let Some(prev) = &prev {
                    if v.semver.minor != prev.semver.minor {
                        cnt.breaking += 1;
                        if v.created_at >= recent {
                            cnt.breaking_recent += 1;
                        }
                    }
                }
            } else {
                if v.semver.pre.is_empty() {
                    cnt.stable += 1;
                }
                if let Some(prev) = &prev {
                    if v.semver.major != prev.semver.major {
                        cnt.major += 1;
                        if v.created_at >= recent {
                            cnt.major_recent += 1;
                        }
                    }
                }
            }
            if let Some(prev) = &prev {
                if v.semver.major == prev.semver.major &&
                    v.semver.minor == prev.semver.minor &&
                    (v.semver.patch != prev.semver.patch || v.semver.pre != prev.semver.pre)
                {
                    cnt.patch += 1;
                }
            }
            prev = Some(v);
        }
        Some(cnt)
    }

    /// Most relevant category for the crate and rank in that category
    pub(crate) fn top_category(&self) -> Option<(u32, &'static Category)> {
        self.top_category
    }

    /// docs.rs link, if available
    pub(crate) fn api_reference_url(&self) -> Option<&str> {
        self.api_reference_url.as_deref()
    }

    /// `(url, label)`
    pub(crate) fn homepage_link(&self) -> Option<Link<'_>> {
        self.ver.homepage().map(|url| {
            let label = url_domain(url)
                .map(|host| {
                    let docs_on_same_host = self.ver.documentation().and_then(url_domain).is_some_and(|doc_host| doc_host == host);

                    if docs_on_same_host {
                        "Home".into() // there will be verbose label on docs link, so repeating it would be noisy
                    } else {
                        format!("Home ({host})").into()
                    }
                })
                .unwrap_or_else(|| "Homepage".into());
            Link {
                url: url.to_string().into(),
                label,
                rel: self.rel_nofollow(),
            }
        })
    }

    /// `(url, label)`
    pub(crate) fn documentation_link(&self) -> Option<Link<'_>> {
        self.ver.documentation().map(|url| {
            let host = url_domain(url);
            let host = host.as_deref();
            let is_docs_rs = host.is_some_and(|host| host == "docs.rs");
            let label = if is_docs_rs {
                "API Reference".into()
            } else if let Some(host) = host {
                format!("{} ({host})", if host.len() < 20 { "Documentation" } else { "Docs" }).into()
            } else { "Documentation".into() };
            Link {
                url: url.into(),
                label,
                rel: if is_docs_rs { templates::Html("") } else { self.rel_nofollow() },
            }
        })
    }

    pub(crate) fn author_link<'ca>(&self, urler: &Urler, author: &'ca CrateAuthor) -> Option<Link<'ca>> {
        let url = urler.author(author)?;
        let rel = if author.trust.unwrap_or(0.) > 60. || url.starts_with('/') { templates::Html("") } else { self.rel_nofollow() };
        Some(Link {
            url,
            label: author.trusted_name().into(),
            rel,
        })
    }

    /// `(normal, direct to sha1)`
    pub(crate) fn repository_links(&self, urler: &Urler) -> Vec<(Link<'_>, Option<Link<'_>>)> {
        let mut repo_links = Vec::with_capacity(2);

        if !self.has_verified_repository_link && self.ver.origin().is_crates_io() {
            repo_links.push((Link {
                url: urler.docs_rs_source(self.ver.short_name(), self.ver.version()).into(),
                label: "Source".into(),
                rel: templates::Html(""),
            }, None));
        }

        if let Some(r) = self.ver.repository_http_url() {
            let (label, owner_label) = if self.has_verified_repository_link {
                let label_prefix = r.repo.site_link_label(r.exact_url.is_some());
                match r.repo.host() {
                    Repo::GitHub(host) | Repo::GitLab(host) | Repo::BitBucket(host) => {
                        (label_prefix, Some(host.owner.as_str()))
                    },
                    Repo::Other(url) => (label_prefix, parsed_url_domain(url)),
                }
            } else {
                ("Repository link", None)
            };

            debug_assert!(owner_label.map_or(true, |o| !o.is_empty()));

            let repo_link = if let Some(exact_url) = r.exact_url {
                (Link {
                    url: r.project_url.into(),
                    label: label.into(),
                    rel: self.rel_nofollow(),
                }, Some(Link {
                    url: exact_url.into(),
                    label: owner_label.unwrap_or("…/…").into(),
                    rel: self.rel_nofollow(),
                }))
            } else {
                (Link {
                    url: r.project_url.into(),
                    label: if let Some(owner_label) = owner_label { format!("{label} ({owner_label})").into() } else { label.into() },
                    rel: self.rel_nofollow(),
                }, None)
            };
            repo_links.push(repo_link);
        }
        repo_links
    }

    /// Most relevant keyword for this crate and rank in listing for that keyword
    pub(crate) fn top_keyword(&self) -> Option<(u32, String)> {
        self.top_keyword.clone()
    }

    /// Categories and subcategories, but deduplicated
    /// so that they look neater in breadcrumbs
    pub(crate) fn category_slugs_unique(&self) -> Vec<Vec<&Category>> {
        let mut seen = HashSet::default();
        self.ver
            .category_slugs()
            .iter()
            .map(|slug| {
                CATEGORIES
                    .from_slug(slug).0.into_iter()
                    .filter(|c| {
                        if seen.contains(&c.slug) {
                            return false;
                        }
                        seen.insert(&c.slug);
                        true
                    })
                    .collect()
            })
            .filter(|v: &Vec<_>| !v.is_empty())
            .collect()
    }

    pub(crate) fn date_created(&self) -> Option<DateTime<Utc>> {
        self.most_recent_version().map(|v| v.created_at)
    }

    pub(crate) fn date_created_string(&self) -> Option<String> {
        self.date_created().map(|v| v.format("%Y-%m-%d").to_string())
    }

    fn most_recent_version(&self) -> Option<Version<'a>> {
        self.all_versions().max_by(|a, b| a.created_at.cmp(&b.created_at))
    }

    pub(crate) fn all_versions(&self) -> impl Iterator<Item = Version<'a>> {
        self.all.versions().iter().filter_map(|v| Some(Version {
            yanked: v.yanked,
            num: &v.num,
            semver: SemVer::parse(&v.num).map_err(|e| warn!("semver parse {} {:?}", e, v.num)).ok()?,
            created_at: v.created_at,
        }))
    }

    /// Data for weekly breakdown of recent downloads
    pub(crate) fn download_graph(&self, width: usize, height: usize) -> Option<DownloadsGraph> {
        Some(DownloadsGraph::new(self.kitchen_sink.weekly_downloads(self.all, 16).ok()?, self.ver.has_bin(), width, height))
    }

    pub(crate) fn downloads_per_month(&self) -> Option<usize> {
        self.downloads_per_month_cached
    }

    pub(crate) fn github_stargazers_and_watchers(&self) -> Option<(u32, u32)> {
        self.github_stargazers_and_watchers
    }

    pub(crate) fn related_crates(&self) -> Option<&[Origin]> {
        if self.related_crates.is_empty() { None } else { Some(&self.related_crates) }
    }

    pub(crate) fn same_namespace_crates(&self) -> Option<&[ArcRichCrateVersion]> {
        if self.ns_crates.is_empty() { None } else { Some(&self.ns_crates) }
    }

    async fn make_related_crates(kitchen_sink: &KitchenSink, ver: &RichCrateVersion, downloads_per_month_or_equivalent: Option<usize>, similar: Vec<(Origin, f32)>) -> (Vec<Origin>, Vec<ArcRichCrateVersion>) {
        // require some level of downloads to avoid recommending spam
        // but limit should be relative to the current crate, so that minor crates
        // get related suggestions too

        let dl = downloads_per_month_or_equivalent.unwrap_or(150);
        let min_recent_downloads = (dl as u32 / 2).max(150);
        Box::pin(kitchen_sink.related_crates(ver, min_recent_downloads, similar)).await.map_err(|e| warn!("related crates fail: {e}")).ok().unwrap_or_default()
    }

    /// data for piechart
    pub(crate) fn langs_chart(&self, stats: &Stats, width_px: u32) -> Option<LanguageStats> {
        let mut res: Vec<_> = stats.langs.iter().filter(|(lang, lines)| lines.code > 0 && lang.is_code()).map(|(a, b)| (*a, *b)).collect();
        if !res.is_empty() {
            res.sort_unstable_by_key(|(_, lines)| lines.code);
            let biggest = res.last().copied().unwrap();
            let total = res.iter().map(|(_, lines)| lines.code).sum::<u32>();
            if biggest.0 != Language::Rust || biggest.1.code < total * 9 / 10 {
                let mut remaining_px = width_px;
                let mut remaining_lines = total;
                let min_width = 3;
                Some(
                    res.into_iter()
                        .map(|(lang, lines)| {
                            let width = (lines.code * remaining_px / remaining_lines.max(1)).max(min_width);
                            let xpos = width_px - remaining_px;
                            remaining_px = remaining_px.saturating_sub(width);
                            remaining_lines -= lines.code;
                            (lang, lines, (xpos, width))
                        })
                        .rev()
                        .collect(),
                )
            } else {
                None // if crate is 90% Rust, don't bother with stats
            }
        } else {
            None
        }
    }

    pub(crate) fn svg_path_for_slice(start: u32, len: u32, total: u32, diameter: u32) -> String {
        fn coords(val: u32, total: u32, radius: f64) -> (f64, f64) {
            ((2. * PI * f64::from(val) / f64::from(total)).sin().mul_add(radius, radius), (PI + 2. * PI * f64::from(val) / f64::from(total)).cos().mul_add(radius, radius))
        }
        let radius = diameter / 2;
        let big_arc = len > total / 2;
        let end = coords(start + len, total, f64::from(radius));
        let start = coords(start, total, f64::from(radius));
        format!(
            "M {startx:.2} {starty:.2} A {radius} {radius} 0 {arcflag} 1 {endx:.2} {endy:.2} L {radius} {radius}",
            startx = start.0.round(),
            starty = start.1.round(),
            radius = radius,
            arcflag = if big_arc { "1" } else { "0" },
            endx = end.0,
            endy = end.1,
        )
    }

    /// analyze dependencies checking their weight and their license
    async fn crate_size_and_viral_license(&self, deps: DepInfMap, stats: &DepsStats) -> CResult<(CrateSizes, Stats, Option<CrateLicense>)> {
        let mut viral_license: Option<CrateLicense> = None;

        let mut main_lang_stats = self.ver.language_stats().clone();
        let mut main_crate_size = self.ver.crate_size();
        let mut deps_size_typical = DepsSize::default();
        let mut deps_size_minimal = DepsSize::default();
        let own_license = self.manifest.package().license();

        blocking::awatch("crate_size_and_viral_license", futures::stream::iter(deps.into_iter().filter(|(_, (depinf, _))| depinf.ty != DepTy::Dev)
            .map(move |(name, (depinf, semver))| async move {
                let krate = self.get_crate_of_dependency(&name, ()).await
                    .map_err(|e| warn!("bad dep not counted: {name} {e}")).ok()?;
                Some((krate, name, depinf, semver))
            }))
            .filter_map(|res| res)
            .for_each_concurrent(8, |(krate, name, depinf, semver)| {
                let commonality = stats.version_global_popularity(&name, &semver).expect("depsstats").unwrap_or(0.);
                let is_heavy_build_dep = matches!(&*name, "bindgen" | "clang-sys" | "cmake" | "cc" if depinf.enabled_by_default); // you deserve full weight of it

                // if optional, make it look less problematic (indirect - who knows, maybe platform-specific?)
                let weight = if depinf.enabled_by_default {1.} else if depinf.direct {0.25} else {0.15} *
                // if it's common, it's more likelty to be installed anyway,
                // so it's likely to be less costly to add it
                (1. - commonality) *
                // proc-macros are mostly build-time deps
                if krate.is_proc_macro() {0.1} else {1.} *
                // Build deps aren't a big deal [but still include some in case somebody did something awful]
                if depinf.ty == DepTy::Runtime || is_heavy_build_dep {1.} else {0.1};

                // count only default ones
                let weight_minimal = if depinf.enabled_by_default {1.} else {0.} *
                // overestimate commonality
                (1. - commonality).powi(2) *
                if krate.is_proc_macro() {0.} else {1.} *
                if depinf.ty == DepTy::Runtime {1.} else {0.};

                if let Some(dep_license) = krate.license() {
                    // if the parent crate itself is copyleft
                    // then there's no need to raise alerts about copyleft dependencies.
                    if viral_license.is_some() || compare_virality(dep_license, own_license) == Ordering::Greater {
                        let prev_license = viral_license.as_ref().map(|c| &*c.license);
                        let virality = compare_virality(dep_license, prev_license);
                        let existing_is_optional = viral_license.as_ref().is_some_and(|d| d.optional);
                        let new_is_optional = !depinf.enabled_by_default || depinf.ty == DepTy::Build;
                        // Prefer showing non-optional, direct dependency
                        if virality
                            .then(if !new_is_optional && existing_is_optional {Ordering::Greater} else {Ordering::Equal})
                            .then(if depinf.direct {Ordering::Greater} else {Ordering::Equal})
                            == Ordering::Greater {
                            viral_license = Some(CrateLicense {
                                origin: krate.origin().clone(),
                                optional: new_is_optional,
                                license: dep_license.into(),
                            });
                        }
                    }
                }

                let crate_size = krate.crate_size();
                let tarball_weighed = (crate_size.0 as f32 * weight) as u64;
                let uncompr_weighed = (crate_size.1 as f32 * weight) as u64;
                let tarball_weighed_minimal = (crate_size.0 as f32 * weight_minimal) as u64;
                let uncompr_weighed_minimal = (crate_size.1 as f32 * weight_minimal) as u64;
                let crate_stats = krate.language_stats();

                if depinf.direct && Self::is_same_project(self.ver, &krate) {
                    main_crate_size.0 += tarball_weighed;
                    main_crate_size.1 += uncompr_weighed;

                    for (&lang, val) in &crate_stats.langs {
                        let e = main_lang_stats.langs.entry(lang).or_default();
                        e.code += (val.code as f32 * weight) as u32;
                        e.comments += (val.comments as f32 * weight) as u32;
                    }
                } else {
                    let sloc = crate_stats.langs.iter().filter(|(l, _)| l.is_code()).map(|(_, v)| v.code).sum::<u32>();

                    deps_size_typical.tarball += tarball_weighed;
                    deps_size_typical.uncompressed += uncompr_weighed;
                    deps_size_typical.lines += (sloc as f32 * weight) as usize;
                    deps_size_minimal.tarball += tarball_weighed_minimal;
                    deps_size_minimal.uncompressed += uncompr_weighed_minimal;
                    deps_size_minimal.lines += (sloc as f32 * weight_minimal) as usize;
                }
                async {}
            })).await;

        Ok((CrateSizes {
            tarball: main_crate_size.0,
            uncompressed: main_crate_size.1,
            minimal: deps_size_minimal,
            typical: deps_size_typical,
        }, main_lang_stats, viral_license))
    }

    async fn get_crate_of_dependency(&self, name: &str, _semver: ()) -> Result<ArcRichCrateVersion, KitchenSinkErr> {
        // FIXME: caching doesn't hold multiple versions, so fetchnig of precise old versions is super expensive
        self.kitchen_sink.rich_crate_version_stale_is_ok(&Origin::from_crates_io_name(name)).await.kontext("Dependency info")

        // let krate = self.kitchen_sink.index()?.crate_by_name(&Origin::from_crates_io_name(name))?;
        // let ver = krate.versions()
        //     .iter().rev()
        //     .find(|k| SemVer::parse(k.version()).ok().is_some_and(|v| &v == semver))
        //     .unwrap_or_else(|| krate.latest_version());
        // self.kitchen_sink.rich_crate_version_from_index(ver)
    }

    fn is_same_project(one: &RichCrateVersion, two: &RichCrateVersion) -> bool {
        matches!((one.repository(), two.repository()), (Some(a), Some(b)) if a == b)
    }
}

pub struct DepsStatsResult<'a> {
    pub deps: u32,
    pub direct: u32,
    pub name: Option<&'a str>,
    pub former_glory: f64,
}

#[derive(Debug, Copy, Clone, Default)]
pub struct DepsSize {
    pub tarball: u64,
    pub uncompressed: u64,
    pub lines: usize,
}

type LanguageStats = Vec<(Language, Lines, (u32, u32))>;

const fn plural(n: u32) -> &'static str {
    if n == 1 { "" } else { "s" }
}

impl ReleaseCounts {
    /// Judge how often the crate makes breaking or stable releases
    pub(crate) fn summary(&self) -> (String, Option<String>) {
        // show (n this year|n last year)
        let (n, label, n2, label2, majorinfo) = if self.stable > 0 {
            let breaking = self.major_recent > 2 && self.major * 2 >= self.stable;
            if breaking {
                (self.major, "major breaking", 0, "", false)
            } else {
                (self.stable, "stable", self.major, "major", self.major > 2.max(self.stable / 16))
            }
        } else {
            let very_breaking = self.unstable > 2 && (self.breaking_recent > 3 || self.breaking * 2 >= self.unstable);
            if very_breaking {
                (self.breaking, "breaking", 0, "", false)
            } else {
                let bad = self.breaking_recent > 1.max(self.unstable / 9) && self.breaking > 2.max(self.unstable / 8);
                let good = self.patch * 2 >= self.total;
                (self.unstable, if !bad && good { "" } else { "unstable" }, self.breaking, "breaking", bad)
            }
        };
        if n == self.total || (n > 7 && n * 10 >= self.total * 8) {
            if majorinfo {
                (format!("{n} {label} release{}", plural(n)), Some(format!("({n2} {label2})")))
            } else {
                (format!("{n} {label} release{}", plural(n)), None)
            }
        } else if n * 3 >= self.total * 2 {
            (format!("{} release{}", self.total, plural(self.total)), Some(format!("({label})")))
        } else {
            (format!("{} release{}", self.total, plural(self.total)), if !label.is_empty() { Some(format!("({n} {label})")) } else { None })
        }
    }
}

fn is_not_viral(l: &str) -> bool {
    l.starts_with("MIT") || l.starts_with("Apache") || l.starts_with("BSD") || l.starts_with("Zlib") ||
    l.starts_with("IJG") || l.starts_with("CC0") || l.starts_with("ISC") || l.starts_with("FTL") || l.starts_with("MPL")
}

// FIXME: this is very lousy parsing, but crates-io allows non-SPDX syntax, so I can't use a proper SPDX parser.
fn virality_score(license: &str) -> u8 {
    license.split("AND").filter_map(|l| {
        l.split('/').flat_map(|l| l.split("OR"))
        .filter_map(|l| {
            let l = l.trim_start();
            if is_not_viral(l) {
                Some(0)
            } else if l.starts_with("AGPL") {
                Some(6)
            } else if l.starts_with("GPL") {
                Some(5)
            } else if l.starts_with("CC-") {
                Some(4)
            } else if l.starts_with("LGPL") {
                Some(2)
            } else if l.starts_with("GFDL") {
                Some(1)
            } else {
                None
            }
        }).min()
    }).max().unwrap_or(0)
}

#[test]
fn test_vir() {
    assert_eq!(6, virality_score("AGPL AND MIT"));
    assert_eq!(0, virality_score("FTL / GPL-2.0"));
    assert_eq!(0, virality_score("AGPL OR MIT"));
    assert_eq!(0, virality_score("MIT/Apache/LGPL"));
    assert_eq!(0, virality_score("MPL/LGPL"));
    assert_eq!(2, virality_score("Apache/MIT AND LGPL"));
    assert_eq!(Ordering::Greater, compare_virality("LGPL", Some("MIT")));
    assert_eq!(Ordering::Greater, compare_virality("AGPL", Some("LGPL")));
    assert_eq!(Ordering::Equal, compare_virality("GPL-3.0", Some("GPL-2.0")));
    assert_eq!(Ordering::Less, compare_virality("CC0-1.0 OR GPL", Some("LGPL-2.0+")));
}

fn compare_virality(license: &str, other_license: Option<&str>) -> Ordering {
    let score = virality_score(license);
    let other_score = other_license.map_or(0, virality_score);
    score.cmp(&other_score)
}

#[test]
fn counts() {
    let r = ReleaseCounts { total: 10, stable: 0, major: 0, major_recent: 0, patch: 0, unstable: 9, breaking: 9, breaking_recent: 0 };
    assert_eq!(r.summary(), ("9 breaking releases".to_string(), None));
    let r = ReleaseCounts { total: 25, stable: 3, major: 0, major_recent: 0, patch: 7, unstable: 2, breaking: 1, breaking_recent: 2 };
    assert_eq!(r.summary(), ("25 releases".to_string(), Some("(3 stable)".to_string())));
    let r = ReleaseCounts { total: 27, stable: 0, major: 1, major_recent: 0, patch: 23, unstable: 13, breaking: 1, breaking_recent: 0 };
    assert_eq!(r.summary(), ("27 releases".to_string(), None));
}

#[test]
fn pie() {
    assert_eq!("M 10.00 5.00 A 5 5 0 0 1 5.00 10.00 L 5 5", CratePage::svg_path_for_slice(1, 1, 4, 10));
    assert_eq!("M 28.00 0.00 A 28 28 0 1 1 28.00 0.00 L 28 28", CratePage::svg_path_for_slice(0, 10, 10, 56));
}
