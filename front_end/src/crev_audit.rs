use kitchen_sink::comparator;
use crate::crev_audit::vet::VetVersionRef;
use crate::urler::Urler;
use crate::{templates, Page};
use util::{FxHashMap as HashMap, FxHashSet as HashSet};
use kitchen_sink::vet::{AuditSource, Review as VetReview};
pub use kitchen_sink::Rating;
use kitchen_sink::{vet, Advisory, Author, KitchenSink, Level, Origin, Review, SemVer};
use manifest::IdentifiedBinFile;
use render_readme::{Links, LinksContext, Markup, Renderer};
use rich_crate::RichCrateVersion;
use std::borrow::Cow;
use std::cmp::Reverse;
use std::time::{Duration, Instant};
use unicase::Ascii;

pub(crate) struct VetReviewEx<'a> {
    pub authors: Vec<(Option<String>, String)>,
    pub latest_warn: bool,
    pub semver: Option<SemVer>,
    pub r: &'a VetReview<'a>,
    pub fold: bool,
}

pub struct AuditsPage<'a> {
    pub(crate) ver: &'a RichCrateVersion,
    pub(crate) markup: &'a Renderer,
    pub(crate) advisories: Vec<Advisory>,
    /// hide text, show non-latest warning, review
    pub(crate) crev_reviews: Vec<(bool, bool, &'a Review)>,
    pub(crate) vet_reviews: Vec<VetReviewEx<'a>>,
    pub(crate) vet_legend: Vec<Legend<'a>>,
    pub(crate) version: SemVer,
    pub(crate) cargo_crev_origin: Origin,
    pub(crate) cargo_vet_origin: Origin,
    pub(crate) cargo_audit_origin: Origin,
    pub(crate) crate_tarball_download_url: Option<String>,
    pub(crate) suspicious_binary_files: Vec<IdentifiedBinFile>,
    deadline: Instant,
    pub(crate) reviews_order: [ReviewSet; 2],
    pub(crate) repo_report: Option<comparator::Report>,
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum ReviewSet {
    Crev,
    Vet,
}

pub(crate) struct Legend<'a> {
    pub criterion_name: &'a str,
    pub desc: &'a str,
    pub implies: Vec<&'a str>,
    pub src: &'a AuditSource,
    pub aggregated_from_host: Option<&'a str>,
    pub fold: bool,
}

#[derive(Default)]
struct LegendAccumulator<'a> {
    added_to_legend: HashSet<(&'a str, &'a str)>,
    legend_dedupe: HashSet<(&'a str, &'a str)>,
    vet_legend: Vec<Legend<'a>>,
}

impl<'a> LegendAccumulator<'a> {
    fn add(&mut self, criterion_name: &'a str, src: &'a AuditSource, fold: bool) {
        let real_criterion = src.audits.criteria.get(criterion_name);
        let aggregated_from = real_criterion.and_then(|c| c.aggregated_from.get(0));

        // different sources may use same key name, need separate descriptions
        if self.added_to_legend.insert((src.url.as_str(), criterion_name)) && aggregated_from.map_or(true, |a| self.added_to_legend.insert((a, criterion_name))) {
            let desc = real_criterion.and_then(|c| {
                c.description.as_deref().map(|d| {
                    let implies = c.implies.iter().map(|s| s.as_str()).collect();
                    (implies, d)
                })
            }).or_else(|| match criterion_name {
                "safe-to-deploy" => Some((vec!["safe-to-run"], "This crate will not introduce a serious security vulnerability to production software exposed to untrusted input. [More…](https://mozilla.github.io/cargo-vet/built-in-criteria.html#safe-to-deploy)")),
                "safe-to-run" => Some((vec![], "This crate can be compiled, run, and tested on a local workstation or in controlled automation without surprising consequences. [More…](https://mozilla.github.io/cargo-vet/built-in-criteria.html#safe-to-run)")),
                _ => None,
            });
            if let Some((implies, desc)) = desc {
                // different sources may describe same key same way
                if self.legend_dedupe.insert((criterion_name, desc)) {
                    self.vet_legend.push(Legend {
                        criterion_name,
                        desc, implies: implies.clone(), src, fold,
                        aggregated_from_host: aggregated_from.and_then(|a| a.strip_prefix("https://").unwrap_or(a).split('/').next()),
                    });
                    for i in implies {
                        self.add(i, src, true); // unfold here may be wrong if later review has it explicitly, fixed in fin()
                    }
                }
            }
        }
    }

    pub(crate) fn finish(mut self, reviews: &[VetReviewEx<'_>]) -> Vec<Legend<'a>> {
        let visible: HashSet<_> = reviews.iter().flat_map(|r| r.r.criteria().iter().map(|s| s.as_str())).collect();
        self.vet_legend.iter_mut().filter(|l| l.fold).for_each(|l| {
            if visible.contains(l.criterion_name) {
                l.fold = false;
            }
        });
        self.vet_legend
    }
}

impl<'a> AuditsPage<'a> {
    pub(crate) async fn new(crev_reviews: &'a [Review], vet_reviews: &'a [VetReview<'a>], ver: &'a RichCrateVersion, k: &'a KitchenSink, markup: &'a Renderer, url: &Urler) -> AuditsPage<'a> {
        let version: SemVer = ver.version_semver().expect("semver");
        let cargo_crev_origin = Origin::from_crates_io_name("cargo-crev");
        let cargo_vet_origin = Origin::from_crates_io_name("cargo-vet");
        let cargo_audit_origin = Origin::from_crates_io_name("cargo-audit");
        let mut author_seen = HashSet::with_capacity_and_hasher(crev_reviews.len(), Default::default());
        let mut crev_reviews: Vec<_> = crev_reviews.iter().map(|r| {
            (!author_seen.insert(r.author_url.as_deref()), false, r)
        }).collect();

        let suspicious_binary_files = k.get_suspicious_binary_files(ver.origin(), ver.version()).unwrap_or_default();

        // Eliminate reviews that are repetitive (some authors copypaste same boilerplate for every version)
        let mut review_author_repetition = HashMap::default();
        let mut review_author_notes = HashSet::<String>::default();
        crev_reviews.retain(|&(seen, _, r)| {
            let is_latest = r.version == version || review_author_repetition.is_empty(); // empty when latest of crev reviews

            if is_latest && (r.rating == Rating::Negative || !r.issues.is_empty()) {
                // old negative reviews may not be relevant any more
                return true;
            }

            let repetition_points = review_author_repetition.entry(r.author_url.clone()).or_insert(0);
            if seen { *repetition_points += 10; }
            if !is_latest { *repetition_points += 3; }

            let mut low_value_points = *repetition_points;
            if r.rating == Rating::Neutral {
                low_value_points += 1;
            }
            if r.thoroughness == Level::None {
                // this is not useful!
                if !is_latest && r.comment_markdown.contains("I didn't review the source code.") {
                    return false;
                }
                low_value_points += 4;
            }
            if r.understanding == Level::None {
                low_value_points += 3;
            }
            if r.comment_markdown.is_empty() {
                low_value_points += 1;
            } else if review_author_notes.contains(&r.comment_markdown) {
                low_value_points += 7;
            }
            if low_value_points < 22 {
                review_author_notes.insert(r.comment_markdown.clone());
                true
            } else {
                false
            }
        });

        let mut non_latest_shown = false; // remind about latest ver before first outdated reivew
        crev_reviews.iter_mut().for_each(|(_, non_latest, r)| {
            *non_latest = if !non_latest_shown && r.version < version {
                non_latest_shown = true;
                true
            } else {
                false
            };
        });

        let owners_s = k.crate_owners(ver.origin(), kitchen_sink::CrateOwners::All).await.unwrap_or_default();
        let owners: HashSet<_> = owners_s.iter().filter_map(|o| o.github_login()).map(Ascii::new).collect();

        let mut legend = LegendAccumulator::default();
        let mut vet_reviews: Vec<_> = vet_reviews.iter().map(|r| {
            for criterion_name in r.criteria() {
                legend.add(criterion_name, r.src, false);
            }
            let authors = r.who().iter().map(|a| {
                let a = Author::new(a);
                let maybe_name = a.name.or_else(|| a.email.as_ref().and_then(|e| e.split_once('@')).map(|x| x.0.to_string()));
                let gh_lookup = a.email.as_ref().and_then(|e| k.user_by_email(e).ok()).flatten();
                if let Some(u) = gh_lookup {
                    let name = u.name.map(|n| n.to_string()).or(maybe_name).unwrap_or_else(|| u.login.to_string());
                    return (Some(if owners.contains(&Ascii::new(u.login.as_str())) {
                        url.crate_owner_by_github_login(&u.login)
                    } else {
                        format!("https://github.com/{}", u.login)
                    }), name)
                }
                (a.url, maybe_name.unwrap_or_else(|| "anonymous".into()))
            }).collect();

            let semver = r.version().and_then(|v| SemVer::parse(v.version).ok());
            VetReviewEx {
                latest_warn: false,
                authors,
                semver,
                fold: false,
                r
            }
        }).collect();
        drop(owners_s);

        // stable sort
        vet_reviews.sort_by(|a,b| b.semver.cmp(&a.semver).then_with(|| {
            let a_weak = a.r.criteria().iter().any(|c| c == "unknown");
            let b_weak = b.r.criteria().iter().any(|c| c == "unknown");
            a_weak.cmp(&b_weak)
        }));

        let mut vet_notes_dupes = HashSet::default();
        let mut author_dupes = HashSet::default();
        let mut non_latest_shown = false;
        vet_reviews.retain_mut(|r| {
            // hide dupes from the same author, but not diffs, since they need to be put together
            if r.semver.is_some() && !author_dupes.insert((r.r.who(), r.r.aggregated_from())) {
                return false;
            }
            r.latest_warn = match r.semver.as_ref() {
                Some(this_ver) if !non_latest_shown && this_ver < &version => {
                    non_latest_shown = true;
                    true
                }
                _ => false,
            };
            if let Some(note) = r.r.notes() {
                r.fold = !note.is_empty() && !vet_notes_dupes.insert(note);
            }
            true
        });

        let mut advisories = k.advisories_for_crate(ver.origin()).unwrap_or_default().into_iter()
            .filter(|a| !a.withdrawn() && a.versions.is_vulnerable(&version))
            .collect::<Vec<_>>();
        advisories.sort_unstable_by_key(|a| Reverse(a.severity()));

        let max_vet_ver = vet_reviews.iter().find_map(|r| r.semver.as_ref());
        let max_crev_ver = crev_reviews.get(0).map(|r| &r.2.version);

        let reviews_order = if max_vet_ver > max_crev_ver {
            [ReviewSet::Vet, ReviewSet::Crev]
        } else {
            [ReviewSet::Crev, ReviewSet::Vet]
        };

        Self {
            repo_report: if ver.origin().is_crates_io() {
                k.repo_comparison_report_for_crate(ver.short_name(), ver.version())
            } else { None },
            suspicious_binary_files,
            advisories,
            vet_legend: legend.finish(&vet_reviews),
            crev_reviews,
            vet_reviews,
            version,
            ver,
            markup,
            cargo_crev_origin,
            cargo_vet_origin,
            cargo_audit_origin,
            crate_tarball_download_url: k.crate_tarball_download_url(ver),
            deadline: Instant::now() + Duration::from_secs(4),
            reviews_order,
        }
    }

    pub(crate) fn issue_url<'b>(&self, id: &'b str) -> Option<Cow<'b, str>> {
        if id.starts_with("https://") {
            return Some(id.into());
        }
        if id.starts_with("RUSTSEC-") {
            return Some(format!("https://github.com/RustSec/advisory-db/blob/HEAD/crates/{}/{id}.md", self.ver.short_name()).into());
        }
        None
    }

    pub(crate) fn issue_id<'b>(&self, id: &'b str) -> &'b str {
        id.trim_start_matches("https://")
    }

    pub(crate) fn rating_class_vet(&self, r: &VetReview<'_>) -> &str {
        self.rating_class(match r.audit {
            vet::AuditKind::Audit(a) => {
                if a.violation.is_some() {
                    Rating::Negative
                } else {
                    a.criteria.iter().filter_map(|c| Some(match c.as_str() {
                        "safe-to-deploy" | "rule-of-two-safe-to-deploy" => Rating::Strong,
                        "safe-to-run" => Rating::Positive,
                        "ub-risk-2" | "ub-risk-1" => Rating::Neutral,
                        "ub-risk-3" | "ub-risk-4" => Rating::Negative,
                        "ub-risk-0" | "does-not-implement-crypto" | "crypto-safe" => return None,
                        _ => Rating::Neutral,
                    })).min().unwrap_or(Rating::Neutral)
                }
            },
            vet::AuditKind::WildcardAudit(_) |
            vet::AuditKind::Trusted(_) => Rating::Neutral,
        })
    }

    pub(crate) fn rating_class(&self, rating: Rating) -> &str {
        match rating {
            Rating::Negative => "negative",
            Rating::Neutral => "neutral",
            Rating::Positive => "positive",
            Rating::Strong => "strong",
        }
    }

    pub(crate) fn rating_label(&self, rating: Rating) -> &str {
        match rating {
            Rating::Negative => "Negative",
            Rating::Neutral => "Neutral",
            Rating::Positive => "Positive",
            Rating::Strong => "Strong Positive",
        }
    }

    pub(crate) fn level_class(&self, level: Level) -> &str {
        match level {
            Level::None => "none",
            Level::Low => "low",
            Level::Medium => "medium",
            Level::High => "high",
        }
    }

    pub(crate) fn level_label(&self, level: Level) -> &str {
        match level {
            Level::None => "None",
            Level::Low => "Low",
            Level::Medium => "Medium",
            Level::High => "High",
        }
    }

    /// class, label - for cargo vet
    pub(crate) fn version_compare_opt(&self, v: Option<&SemVer>) -> Option<(&str, &str)> {
        let Some(v) = v else {
            return Some(("current", "(all versions)"))
        };
        Some(self.version_compare(v))
    }

    /// class, label
    pub(crate) fn version_compare(&self, other: &SemVer) -> (&str, &str) {
        if &self.version <= other {
            ("current", "(current)")
        } else if self.version.major > 0 && self.version.major == other.major && self.version.minor == other.minor {
            ("old", "(older version)")
        } else {
            ("outdated", "(older version)")
        }
    }

    pub(crate) fn page(&self) -> Page {
        Page {
            title: format!("Code review{} of the {} crate", if self.crev_reviews.len() + self.vet_reviews.len() == 1 { "" } else { "s" }, self.ver.capitalized_name()),
            item_name: Some(self.ver.short_name().to_string()),
            item_description: self.ver.description().map(|d| d.to_string()),
            noindex: self.crev_reviews.is_empty() && self.vet_reviews.is_empty(),
            search_meta: false,
            critical_css_data: Some(include_str!("../../style/public/crev.css")),
            critical_css_dev_url: Some("/crev.css"),
            ..Default::default()
        }
    }

    pub(crate) fn author_name<'b>(&self, review: &'b Review) -> Cow<'b, str> {
        if let Some(url) = review.author_url.as_ref() {
            let url = url.trim_start_matches("https://");
            if url.starts_with("github.com/") {
                let mut parts = url.split('/');
                if let Some(name) = parts.nth(1) {
                    return name.into();
                }
            }
            let url = url.trim_end_matches("/crev-proofs");
            url.into()
        } else {
            format!("Untrusted source ({})", review.author_id).into()
        }
    }

    pub fn git_url_for_vet_version<'v>(&self, vet_ver: VetVersionRef<'v>) -> Option<(String, &'v str)> {
        let rev_str = vet_ver.git_rev?;
        let rev: [u8; 20] = hex::decode(rev_str).ok()?.as_slice().try_into().ok()?;
        let url = self.ver.repository_http_url_at_sha1(&rev)?;

        Some((url, rev_str.get(..6)?))
    }

    pub(crate) fn render_title(&self, title_markdown_ish: &str) -> templates::Html<String> {
        crate::render_maybe_markdown_str(title_markdown_ish, self.markup, None, Some(self.ver.short_name()))
    }

    pub(crate) fn render_comment(&self, markdown: &str) -> templates::Html<String> {
        let (html, warnings) = self.markup.page(&Markup::Markdown(markdown.to_string()), &LinksContext {
            base_url: None,
            own_crate_name: Some(self.ver.short_name()),
            link_own_crate_to_crates_io: false,
            nofollow: Links::Ugc,
            link_fixer: None,
        }, false, self.deadline);
        if !warnings.is_empty() {
            eprintln!("{} creview: {warnings:?}", self.ver.short_name());
        }
        templates::Html(html)
    }
}
