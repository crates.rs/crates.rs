use crate::{make_link_fixer, templates, Page};
use kitchen_sink::KitchenSink;
use log::warn;
use render_readme::{Links, LinksContext, Renderer};
use rich_crate::{Origin, Readme, RichCrateVersion};
use std::time::{Duration, Instant};

pub struct InstallPage<'a> {
    pub is_dev: bool,
    pub is_build: bool,
    pub ver: &'a RichCrateVersion,
    pub kitchen_sink: &'a KitchenSink,
    pub markup: &'a Renderer,
    api_reference_url: Option<String>,
    deadline: Instant,
}

impl<'a> InstallPage<'a> {
    pub async fn new(ver: &'a RichCrateVersion, kitchen_sink: &'a KitchenSink, markup: &'a Renderer) -> InstallPage<'a> {
        let (is_build, is_dev) = blocking::ablock_in_place("idev", || kitchen_sink.is_build_or_dev(ver.origin())).await.expect("deps");
        let api_reference_url = if kitchen_sink.has_docs_rs(ver.origin(), ver.short_name(), ver.version()).await {
            Some(format!("https://docs.rs/{}", ver.short_name()))
        } else {
            None
        };
        Self {
            is_dev, is_build, ver, kitchen_sink, markup, api_reference_url,
            deadline: Instant::now() + Duration::from_secs(4),
        }
    }

    pub(crate) fn page(&self) -> Page {
        Page {
            title: self.page_title(),
            item_name: Some(self.ver.short_name().to_string()),
            item_description: self.ver.description().map(|d| d.to_string()),
            noindex: true,
            search_meta: false,
            critical_css_data: Some(include_str!("../../style/public/install.css")),
            critical_css_dev_url: Some("/install.css"),
            ..Default::default()
        }
    }

    /// docs.rs link, if available
    pub(crate) fn api_reference_url(&self) -> Option<&str> {
        self.api_reference_url.as_deref()
    }

    pub(crate) fn render_readme(&self, readme: &Readme) -> templates::Html<String> {
        blocking::block_in_place("readmei", || {
        let base_url = readme.base_urls();
        let own_crate_name = Some(self.ver.short_name());
        let check = &move |name: &str| Origin::try_from_crates_io_name(name).is_some_and(move |o| self.kitchen_sink.crate_exists(&o));
        let (html, warnings) = self.markup.page(&readme.markup, &LinksContext {
            base_url, own_crate_name,
            nofollow: Links::Ugc,
            link_own_crate_to_crates_io: true,
            link_fixer: Some(&make_link_fixer(own_crate_name, base_url, Some(&check))),
        }, false, self.deadline);
        if !warnings.is_empty() {
            warn!("{} readme: {warnings:?}", self.ver.short_name());
        }
        templates::Html(html)
        })
    }

    pub(crate) fn page_title(&self) -> String {
        format!("How to install the {} crate", self.ver.short_name())
    }
}
