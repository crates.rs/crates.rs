use crate::urler::Urler;
use crate::{templates, Page};
use util::{FxHashMap as HashMap, FxHashSet as HashSet};
use futures::stream::StreamExt;
use futures::Stream;
use itertools::Itertools;
use kitchen_sink::{
    comparator, ArcRichCrateVersion, CError, CResult, CrateOwnerRow, Edition, KitchenSink, Kontext, MaintenanceStatus, Origin, RendCtx, RichAuthor, RichCrate, SemVer, UserType, VersionReq, Warning
};
use log::{debug, warn};
use render_readme::{Links, LinksContext, Renderer};
use rich_crate::Markup;
use std::borrow::Cow;
use std::cell::RefCell;
use std::cmp::Reverse;
use std::time::{Duration, Instant};
use tokio::time::timeout_at;
use util::pick_top_n_unstable_by;

/// For `maintainer_dashboard.rs.html`
pub struct MaintainerDashboard<'a> {
    pub(crate) aut: &'a RichAuthor,
    pub(crate) markup: &'a Renderer,
    pub(crate) warnings: Vec<(f32, Vec<Origin>, Vec<StructuredWarning>)>,
    pub(crate) okay_crates: Vec<Origin>,
    pub(crate) printed_extended_desc: RefCell<HashSet<&'static str>>,
    deadline: Instant,
}

#[derive(Default)]
pub struct StructuredWarning {
    pub(crate) title: Cow<'static, str>,
    pub(crate) desc: Cow<'static, str>,
    pub(crate) extended_desc: Option<&'static str>,
    pub(crate) url: Option<(Cow<'static, str>, Cow<'static, str>)>,
    pub(crate) severity: u8,
}

impl<'a> MaintainerDashboard<'a> {
    pub(crate) async fn new(aut: &'a RichAuthor, mut rows: Vec<CrateOwnerRow>, kitchen_sink: &'a KitchenSink, urler: &Urler, make_common_groups: bool, markup: &'a Renderer, rend: &RendCtx) -> CResult<MaintainerDashboard<'a>> {
        pick_top_n_unstable_by(&mut rows, 400, |a, b| b.latest_release.cmp(&a.latest_release));

        let deadline = (Instant::now() + Duration::from_secs(12)).min(rend.deadline);
        let tmp: Vec<_> = Self::look_up(kitchen_sink, rows, deadline)
            .map(move |(origin, crate_ranking, res)| {
                elaborate_warnings(origin, crate_ranking, res, urler, kitchen_sink, deadline.min(Instant::now() + Duration::from_secs(8)), rend)
            })
            .buffered(8)
            .collect().await;

        let mut okay_crates = Vec::new();
        let mut warnings: Vec<_> = tmp.into_iter()
            .filter_map(|mut res| if res.2.is_empty() {
                okay_crates.push(res.0);
                None
            } else {
                res.2.sort_unstable_by(|a, b| b.severity.cmp(&a.severity).then(a.title.cmp(&b.title)));
                Some((res.1, vec![res.0], res.2))
            }).collect();

        if !warnings.is_empty() && warnings.iter().all(|(_, _, w)| w.iter().all(|w| w.title == "Internal error")) {
            rend.set_incomplete();
            return Err(anyhow::anyhow!("Temporary error fetching crates. Please try again."));
        }

        if make_common_groups {
            warnings.sort_unstable_by(|a, b| b.0.total_cmp(&a.0));
            if warnings.len() >= 20 {
                // don't merge junk with top crates
                let half = warnings.len() / 2;
                let mut junk = warnings.split_off(half);
                Self::make_common_groups(&mut warnings);
                Self::make_common_groups(&mut junk);
                warnings.append(&mut junk);
            } else {
                Self::make_common_groups(&mut warnings);
            }
        }

        for (rank, _, w) in &mut warnings {
            // this is probably just an abandoned crate
            if w.len() >= 10 {
                *rank /= w.len() as f32 / 5.;
            }
        }

        /// Rows were sorted by most recent first. Keep few most recent crates at the top, which are more relevant than most horribly deprecated crates full of (t)errors.
        fn sort_by_severity(warnings: &mut [(f32, Vec<Origin>, Vec<StructuredWarning>)]) {
            warnings.sort_unstable_by_key(|(crate_ranking, _, w)| {
                Reverse(((w.iter().map(|w| u32::from(w.severity.pow(2))).sum::<u32>() * 1000 + 5000) as f32 * crate_ranking) as u32)
            });
        }

        warnings.sort_unstable_by(|a, b| b.0.total_cmp(&a.0));
        let split_point = warnings.len() / 8;
        let (top, rest) = warnings.split_at_mut(split_point);
        sort_by_severity(top);
        sort_by_severity(rest);

        Ok(Self {
            aut,
            markup,
            warnings,
            okay_crates,
            deadline,
            printed_extended_desc: RefCell::new(HashSet::default()),
        })
    }

    fn look_up(kitchen_sink: &KitchenSink, rows: Vec<CrateOwnerRow>, deadline: Instant) -> impl Stream<Item=(Origin, f32, CResult<(ArcRichCrateVersion, RichCrate, HashSet<Warning>, Option<comparator::Report>)>)> + '_ {
        futures::stream::iter(rows)
            .map(move |row| async move {
                let origin = row.origin;
                let crate_ranking = row.crate_ranking * if row.invited_by_github_id.is_some() { 0.8 } else { 1. }; // move it lower if this isn't the primary maintainer
                let cw = timeout_at(deadline.into(), Box::pin(async {
                    let c = kitchen_sink.rich_crate_version_async(&origin).await.with_kontext(|| format!("Crate needed for the dashboard {origin:?}"))?;
                    let all = kitchen_sink.rich_crate_async(&origin).await?;
                    let (w, r) = validator::warnings_for_crate(kitchen_sink, &c, &all).await?;
                    Ok::<_, CError>((c, all, w, r))
                })).await.map_err(CError::from).and_then(|x| x);
                (origin, crate_ranking, cw)
            })
            .buffered(8)
    }

    pub(crate) fn is_org(&self) -> bool {
        self.aut.github.user_type == UserType::Org
    }

    pub(crate) fn should_print_extended_description(&self, desc: &'static str) -> bool {
        self.printed_extended_desc.borrow_mut().insert(desc)
    }

    pub(crate) fn login(&self) -> &str {
        &self.aut.github.login
    }

    pub(crate) fn page(&self) -> Page {
        Page {
            title: format!("@{}'s Rust crates", self.login()),
            critical_css_data: Some(include_str!("../../style/public/maintainer_dashboard.css")),
            critical_css_dev_url: Some("/maintainer_dashboard.css"),
            noindex: true,
            ..Default::default()
        }
    }

    pub(crate) fn atom_id_for(&self, origin: &Origin, warn: &StructuredWarning) -> String {
        let mut hasher = blake3::Hasher::new();
        hasher.update(warn.title.as_bytes());
        hasher.update(origin.package_name_icase().as_bytes());
        let hash = hasher.finalize().to_hex();
        format!("https://lib.rs/crates/{}?atom-{hash}", origin.package_name_icase())
    }

    pub(crate) fn now(&self) -> String {
        chrono::Utc::now().to_rfc3339()
    }

    fn make_common_groups(warnings: &mut Vec<(f32, Vec<Origin>, Vec<StructuredWarning>)>) {
        let mut common = HashMap::default();
        for (.., warnings) in &*warnings {
            for w in warnings {
                common.entry((w.title.clone(), w.desc.clone())).or_insert((0, Vec::new(), None, 0f32)).0 += if warnings.len() == 1 { 2 } else { 1 };
            }
        }
        common.retain(|_, (num, ..)| *num >= 3);
        if common.is_empty() || common.len() > warnings.len() / 2 {
            return;
        }
        warnings.retain_mut(|(rank, origins, warnings)| {
            warnings.retain_mut(|w| {
                let dest = match common.get_mut(&(w.title.clone(), w.desc.clone())) {
                    Some(d) => d,
                    None => return true,
                };
                dest.1.extend(origins.iter().cloned());
                dest.3 = dest.3.max(*rank);
                match &mut dest.2 {
                    None => {
                        dest.2 = Some(std::mem::take(w));
                    },
                    Some(old) => {
                        if old.extended_desc.is_none() && w.extended_desc.is_some() {
                            old.extended_desc = std::mem::take(&mut w.extended_desc);
                        }
                        if old.url.is_none() && w.url.is_some() {
                            old.url = std::mem::take(&mut w.url);
                        }
                        old.severity = old.severity.max(w.severity);
                    },
                };
                false
            });
            !warnings.is_empty()
        });

        warnings.reserve(common.len());
        for (_, origins, w, rank) in common.into_values() {
            if let Some(w) = w {
                warnings.push((rank, origins, vec![w]));
            }
        }
    }

    pub(crate) fn render_text(&self, markdown: &str) -> templates::Html<String> {
        let (html, warnings) = self.markup.page(&Markup::Markdown(markdown.to_string()), &LinksContext {
            base_url: None,
            own_crate_name: None,
            link_own_crate_to_crates_io: false,
            nofollow: Links::Ugc,
            link_fixer: None,
        }, false, self.deadline);
        if !warnings.is_empty() {
            warn!("dash {}: {warnings:#?}", self.aut.github.login);
        }
        templates::Html(html)
    }
}

async fn elaborate_warnings(origin: Origin, mut crate_ranking: f32, res: CResult<(ArcRichCrateVersion, RichCrate, HashSet<Warning>, Option<comparator::Report>)>, urler: &Urler, kitchen_sink: &KitchenSink, deadline: Instant, rend: &RendCtx) -> (Origin, f32, Vec<StructuredWarning>) {
    let (k, _all, w, repo_report) = match res {
        Ok(res) => res,
        Err(e) => {
            rend.set_incomplete();
            return (origin, 0., vec![StructuredWarning {
                title: Cow::Borrowed("Internal error"),
                desc: Cow::Owned(format!("We couldn't check this crate at this time, because: {e}. Please try again later.")),
                url: None,
                extended_desc: None,
                severity: 0,
            }]);
        },
    };
    if w.is_empty() || k.is_yanked() || k.maintenance() == MaintenanceStatus::Deprecated {
        (origin, crate_ranking, vec![])
    } else {
        // Ranking is used for sorting, so focus on maintenance here
        crate_ranking *= match k.maintenance() {
            MaintenanceStatus::Experimental => 2.,
            MaintenanceStatus::ActivelyDeveloped => 2.,
            MaintenanceStatus::None => 1.,
            MaintenanceStatus::PassivelyMaintained => 0.5,
            MaintenanceStatus::AsIs => 0.11,
            MaintenanceStatus::LookingForMaintainer => 0.1,
            MaintenanceStatus::Deprecated => 0.01,
        };
        const LOW: u8 = 1;
        const MED: u8 = 2;
        const HI: u8 = 3;
        // repetitive fix suggestions feel patronizing
        let mut fixes_dedupe_combined = HashSet::default();
        let mut warnings = Vec::with_capacity(w.len());
        for w in w.into_iter().filter(|w| !matches!(w, Warning::BrokenLink(..))) { // FIXME: BrokenLink these are unreliable ;(
            let mut extended_desc = None;
            // severity = higher is worse/more severe
            let (severity, title, desc, url) = match w {
                Warning::NoRepositoryProperty => (HI,
                    Cow::Borrowed("No repository property"),
                    Cow::Borrowed("Specify git repository URL in `Cargo.toml` to help users find more information, contribute, and for lib.rs to read more info."),
                    None::<(Cow<'static, str>, Cow<'static, str>)>),
                Warning::NoReadmeProperty => (
                    if k.readme().is_some() {LOW} else {MED},
                    "No readme property".into(),
                    "Specify path to a `README` file for the project, so that information about is included in the crates.io tarball.".into(), None),
                Warning::NoReadmePackaged => (
                    if k.readme().is_some() {LOW} else {MED},
                    "README missing from crate tarball".into(),
                    "Cargo sometimes fails to package the `README` file. Ensure the path to the `README` in `Cargo.toml` is valid, and points to a file inside the crate's directory.".into(), None),
                Warning::NoReadmeInRepo(url) => (
                    if k.readme().is_some() {LOW} else {HI},
                    "README missing from the repository".into(),
                    format!("We've searched `{url_esc}` and could not find a README file there.", url_esc = markdown_escape(url.into_string())).into(), None),
                Warning::EscapingReadmePath(path) => (
                    if k.readme().is_some() {LOW} else {HI},
                    "Buggy README path".into(),
                    format!("The non-local path to readme specified as `{path_esc}` exposes a bug in Cargo. Please use a path inside the crate's directory. Symlinks are okay. Please verify the change doesn't break any repo-relative URLs in the README.", path_esc = markdown_escape(path.into_string())).into(), None),
                Warning::ErrorCloning(url) => {
                    extended_desc = Some("At the moment we only support git, and attempt fetching when we index a new release. Cloning is necessary for lib.rs to gather data that is missing on crates.io, e.g. to correctly resolve relative URLs in README files, which depend on repository layout and non-standard URL schemes of repository hosts.");
                    (MED,
                        "Could not fetch repository".into(),
                        format!("We've had trouble cloning git repo from `{url_esc}`", url_esc = markdown_escape(url.into_string())).into(), None)
                },
                Warning::BrokenLink(kind, url) => {
                    (LOW, format!("Broken link to {kind}").into(),
                        format!("We did not get a successful HTTP response from `{url_esc}` (these checks are cached, so the problem may have been temporary)", url_esc = markdown_escape(url.into_string())).into(), None)
                },
                Warning::BadCategory(name) => {
                    let name_esc = markdown_escape(name.into_string());
                    extended_desc = Some("lib.rs has simplified and merged some of crates.io categories. Please file a bug if we got it wrong.");
                    (if k.category_slugs().is_empty() {MED} else {LOW},
                        "Incorrect category".into(),
                        format!("Crate's categories property in `Cargo.toml` contains '{name_esc}', which isn't a category we recognize").into(),
                        Some(("List of available categories".into(), "https://crates.io/category_slugs".into())))
                },
                Warning::NoCategories => {
                    let Ok(manifest) = kitchen_sink.crate_manifest(k.origin()).await else { continue };
                    let original_categories = manifest.package().categories();
                    extended_desc = Some("Even if there are no categories that fit precisely, pick one that is least bad. You can also propose new categories in crates.io issue tracker.");
                    if !original_categories.is_empty() {
                        let mut uniq = HashSet::default();
                        let categories_iter = k.category_slugs().iter().map(|c| categories::lib_rs_category_to_crates_io_category(c)).chain(original_categories.iter().map(|c| c.as_str())).filter(|&c| uniq.insert(c));
                        (if k.category_slugs().is_empty() {2} else {1},
                            "Needs more categories".into(),
                            format!("Please more specific categories that describe functionality of the crate. Expand `categories = [{}]` in your `Cargo.toml`.", comma_list_escaped(categories_iter)).into(),
                            Some(("List of available categories".into(), "https://crates.io/category_slugs".into())))
                    } else {
                        (if k.category_slugs().is_empty() {HI} else {LOW},
                            "Missing categories".into(),
                            format!("Categories improve browsing of lib.rs and crates.io. Add `categories = [{}]` to the `Cargo.toml`.", comma_list_escaped(k.category_slugs().iter().map(|c| categories::lib_rs_category_to_crates_io_category(c)))).into(),
                            Some(("List of available categories".into(), "https://crates.io/category_slugs".into())))
                    }
                },
                Warning::NoKeywords => (
                    if k.keywords().is_empty() {HI} else {LOW},
                    "Missing keywords".into(),
                    format!("Help users find your crates. Add `keywords = [{}]` (up to 5) to the `Cargo.toml`. Best keywords are alternative terms or their spellings that aren't in the name or description. Also add a keyword that precisely categorizes this crate and groups it with other similar crates.", comma_list_escaped(k.keywords().iter())).into(), None),
                Warning::EditionMSRV(ed, msrv) => {
                    extended_desc = Some("Using the latest edition helps avoid old quirks of the compiler, and ensures Rust code has consistent syntax and behavior across all projects.");
                    (LOW, "Using outdated edition for no reason".into(),
                        format!("We estimate that this crate requires at least Rust 1.{msrv}, which is newer than the last {}-edition compiler. You can upgrade without breaking any compatibility. Run `cargo fix --edition` and update `edition=\"…\"` in `Cargo.toml`.", ed as u16).into(),
                        Some(("The Edition Guide".into(), "https://doc.rust-lang.org/edition-guide/".into())))
                },
                Warning::BadMSRV(needs, says) => {
                    let tmp;
                    let note = if says > 0 { tmp = format!(", but specified Rust 1.{says} as the minimum version"); &tmp } else {""};
                    (LOW, "Needs to specify correct MSRV".into(),
                        format!("We estimate that this crate requires at least Rust 1.{needs}{note}. Add `rust-version = \"1.{needs}\"` to the `Cargo.toml`.").into(),
                        Some((format!("{} versions", k.short_name()).into(), urler.all_versions(k.origin()).unwrap_or_else(|| urler.crate_by_origin(k.origin())).into())))
                },
                Warning::DocsRs => {
                    extended_desc = Some("Docs.rs doesn't need to run or even link any code, so system dependencies can simply be skipped. You can also set `cfg()` flags just for docs.rs and use them to hide problematic code.");
                    (if k.is_sys() {LOW} else {MED},
                        "docs.rs build failed".into(),
                        "docs.rs site failed to build the crate, so users will have trouble finding the documentation. Docs.rs supports multiple platforms and custom configurations, so you can make the build work even if normal crate usage has special requirements.".into(),
                        Some(("Detecting docs.rs".into(), "https://docs.rs/about/builds".into())))
                },
                Warning::DeprecatedDependency(origin, req) => {
                    let name = origin.package_name_icase();
                    (MED, format!("Dependency {name} {req} has issues").into(),
                        "It may not be actively developed any more. Consider changing the dependency.".into(),
                        Some((format!("{name} crate").into(), urler.crate_by_origin(&origin).into())))
                },
                Warning::ObsoleteDependency(origin, why) => {
                    let name = origin.package_name_icase();
                    (MED, format!("Dependency {name} is not needed").into(),
                        markdown_escape(why.into_string()).into_owned().into(),
                        Some((format!("{name} crate").into(), urler.crate_by_origin(&origin).into())))
                },
                Warning::UnpopularDependency(origin, req) => {
                    let name = origin.package_name_icase();
                    (HI, format!("Dependency {name} {req} has issues").into(),
                        "It has been losing active users, which may be a sign it's deprecated or obsolete. Consider replacing it with a different crate.".into(),
                        Some((format!("{name} crate").into(), urler.crate_by_origin(&origin).into())))
                },
                Warning::OutdatedDependency(origin, req, severity) => {
                    let name = origin.package_name_icase();
                    let mut upgrade_version = Cow::Borrowed("the latest version");
                    if let Some(latest) = latest_version_matching_requirement(&req, deadline, kitchen_sink, &origin).await {
                        upgrade_version = Cow::Owned(latest.to_string());
                    }
                    let zero_ver = upgrade_version.starts_with("0.");
                    let upgrade_version_esc = markdown_escape(upgrade_version);
                    extended_desc = Some(if zero_ver {
                            "In Cargo, different 0.x versions are considered incompatible, so this is a semver-major upgrade."
                        } else {
                            "Easy way to bump dependencies: `cargo install cargo-edit; cargo upgrade -i`; Also check out Dependabot service on GitHub."
                        });
                    let title = format!("Dependency {name} {req} is {}outdated", match severity {
                        0..=10 => "slightly ",
                        11..=30 => "a bit ",
                        31..=80 => "",
                        81..=255 => "significantly ",
                    }).into();
                    let desc = if severity > 40 && !k.is_app() {
                        format!("Upgrade to {upgrade_version_esc} to get all the fixes, and avoid causing duplicate dependencies in projects.")
                    } else {
                        format!("Consider upgrading to {upgrade_version_esc} to get all the fixes and improvements.")
                    }.into();
                    (if severity > 81 { MED } else { LOW },
                        title,
                        desc,
                        Some((
                            format!("{name} versions").into(),
                            if severity > 40 { urler.reverse_deps(&origin) } else { urler.all_versions(&origin) }.unwrap_or_else(|| urler.crate_by_origin(&origin)).into(),
                        )))
                },
                Warning::BadSemVer(ver, err) => {
                    (MED, format!("Syntax error in version {ver}").into(),
                        format!("This is not a valid semver: {err_esc}. Cargo enforces semver syntax now, so this version is unusable. Please yank it with `cargo yank --vers {ver_esc}`",
                            err_esc = markdown_escape(err.into_string()),
                            ver_esc = markdown_escape_code(ver.into_string())).into(), None)
                },
                Warning::BadRequirement(origin, req) => {
                    let name = origin.package_name_icase();
                    extended_desc = Some("Cargo used to be more forgiving about the semver syntax, so it's possible that an already-published crate doesn't satisfy the current rules.");
                    (HI, format!("Incorrect dependency requirement {name} = {req}").into(),
                        "We could not parse it. Please check the semver syntax.".into(),
                        Some(("Cargo dependencies manual".into(), "https://doc.rust-lang.org/cargo/reference/specifying-dependencies.html#specifying-dependencies-from-cratesio".into())))
                },
                Warning::ExactRequirement(origin, req) => {
                    let name = origin.package_name_icase();
                    (MED,
                        format!("Locked dependency version {name} {req}").into(),
                        "This can easily cause a dependency resolution conflict. If you must work around a semver-breaking dependency that can't be yanked, use a range of versions or fork it.".into(),
                        Some((format!("{name} versions").into(), urler.all_versions(&origin).unwrap_or_else(|| urler.crate_by_origin(&origin)).into())))
                },
                Warning::LaxRequirement(origin, req, zero_semver_minor_versioning) => {
                    let name = origin.package_name_icase();
                    let name_esc = markdown_escape_code(name);
                    extended_desc = Some(if zero_semver_minor_versioning {
                        "This crate does not bump semver-minor when adding new features, so to be safe you get all the features/APIs/fixes that your crate depends on, require a more specific patch version."
                    } else {
                        "If you want to keep using truly minimal dependency requirements, please make sure you test them in CI with `-Z minimal-versions` Cargo option, because it's very easy to accidentally use a feature added in a later version."
                    });

                    let mut upgrade_note = String::new();
                    if let Some(latest) = latest_version_matching_requirement(&req, deadline, kitchen_sink, &origin).await {
                        upgrade_note = format!("Specify the version as `{name_esc} = \"{latest}\"`. ", latest = markdown_escape_code(latest.to_string()));
                    }

                    (if zero_semver_minor_versioning { MED } else { LOW },
                        format!("Imprecise dependency requirement {name} = {req}").into(),
                        format!("Cargo does not always pick latest versions of dependencies! {upgrade_note}If `Cargo.lock` ends up having an unexpectedly old version of the dependency, you might get a dependency that lacks features/APIs or important bugfixes that you depend on. This is most likely to happen when using the `minimal-versions` flag, used by users of old Rust versions.").into(),
                        Some((format!("{name} versions").into(), urler.all_versions(&origin).unwrap_or_else(|| urler.crate_by_origin(&origin)).into())))
                },
                Warning::NotAPackage => (HI,
                    "`Cargo.toml` parse error".into(),
                    markdown_escape(w.to_string()).into_owned().into(), None),
                Warning::CryptocurrencyBS => {
                    extended_desc = Some("Author of this site is firmly convinced that cryptocurrencies have a net-negative effect on society.");
                    (HI, "Cryptocurrency crate".into(),
                        "This crate has been classified as related to cyrptocurrencies. If you believe this categorization is a mistake, then please review crate's categories and keywords, or file a bug. If it is related, then please reconsider your choices, and yank it.".into(), None)
                },
                Warning::ContainsBinaries(kinds) => {
                    (LOW, "Published crate contains binary files".into(),
                        format!("The crate contains binary files ({kinds_esc}). Crates are meant to be compiled from source. Please check that you haven't published temporary build files by accident. If you have test fixtures, consider excluding them from crates-io tarball, since Cargo doesn't run tests from crates-io packages.", kinds_esc = markdown_escape(kinds.into_string())).into(),
                    urler.reviews(&origin).map(|url| ("Audit page".into(), url.into())))
                },
                Warning::Chonky(size) => {
                    extended_desc = Some("Check that large files weren't included by accident. Note that tarballs uploaded to crates.io can't be used to run examples or tests, so it's fine to exclude them and their data files. You can use the `include` or `exclude` properties in `Cargo.toml` to minimize crate's download size. Crates.io keeps all versions of all crates forever, so this storage adds up.");
                    (LOW, "Big download".into(),
                        format!("The crate is a {}MB download. You can use `cargo package` command to review crate's archive in `target/package` dir.", size/1000/1000).into(), None)
                },
                Warning::SysNoLinks => {
                    let link_esc = markdown_escape_code(k.short_name().trim_end_matches("-sys").trim_start_matches("lib"));
                    extended_desc = Some("This is also needed to protect your crate from duplicate older versions of itself. C symbols are global, and duplicate symbols can cause all sorts of breakage.");
                    (LOW, "*-sys crate without links property".into(),
                        format!("If this crate uses C libraries with public symbols, consider adding `links = \"{link_esc}\"` to crate's `Cargo.toml` to avoid other libraries colliding with them. Note that the links property adds exclusivity to dependency resolution, but doesn't do any linking.").into(), None)
                },
                Warning::Reserved(reason) => {
                    if reason.contains("junk") {
                        (HI, "Crate contains junk files".into(),
                            "The crate contains boilerplate Rust files with no real functionality.".to_string().into(), None)
                    } else {
                        debug!("identified by: {reason}");
                        extended_desc = Some("It's OK if you intend to publish this project in the near future. Keep in mind that even if you have good intentions, things may not go as planned. crates.io won't reclaim abandoned crates, so reserving good names may end up wasting the good names.");
                        (LOW, "Crate is 'reserved'".into(),
                            "Please be respectful of crates.io and don't squat crate names.".to_string().into(), None)
                    }
                },
                Warning::StaleRelease(days, is_stable, severity) => {
                    let Ok(manifest) = kitchen_sink.crate_manifest(k.origin()).await else { continue };
                    let edition = manifest.package().edition();
                    if k.is_nightly() {
                        extended_desc = Some("Nightly crates tend to have a short lifespan. We're delisting them if they're not updated frequently.");
                    } else if is_stable && k.version_semver().map_or(true, |v| v.major == 0) {
                        extended_desc = Some("If the crate is truly stable, why not make a 1.0.0 release?");
                    } else if edition == Edition::E2015 {
                        extended_desc = Some("It's an opportunity to update it to the current Rust edition.");
                    } else {
                        extended_desc = Some("Users pay attention to the latest release date. Even if the crate is perfectly fine as-is, users may not know that.");
                    }
                    let num = if days > 366*2 { days / 366 } else { days / 31 };
                    let unit = if days > 366*2 { "year" } else { "month" };
                    let q = if k.maintenance() == MaintenanceStatus::Experimental {"How did the experiment go"}
                        else {"Is this crate still maintained"};
                    (severity,
                        format!("Latest {}release is old", if is_stable {"stable "} else {"pre"}).into(),
                        format!("It's been over {num} {unit}{}. {q}? Make a new release, either to refresh it, or to set
```toml
[badges.maintenance]
status = \"deprecated\"
```

(or `\"as-is\"`, `\"passively-maintained\"`).", if num != 1 {"s"} else {""}).into(),
                        Some(("Maintenance status field docs".into(), "https://doc.rust-lang.org/cargo/reference/manifest.html#the-badges-section".into())))
                },
                Warning::NotFoundInRepo => {
                    let repo = k.repository_http_url();
                    let repo_url = repo.map_or(Cow::Borrowed("the crate's repo (which you need to specify)"), |r| r.project_url.into());
                    let repo_url_esc = markdown_escape(repo_url);
                    (HI, "Could not find the crate in the repository".into(),
                        format!("Make sure the main branch of `{repo_url_esc}` contains the `Cargo.toml` for the crate. If you have forked the crate, change the repository property in `Cargo.toml` to your fork's URL.").into(), None)
                },
                Warning::VCSInfoMissing => {
                    extended_desc = Some("To protect against supply chain attacks similar to [CVE-2024-3094](https://tukaani.org/xz-backdoor/), lib.rs will soon start flagging non-reproducible packages without public source code as suspicious. Currently only git repositories are supported (but may be hosted anywhere, not just GitHub). If you'd like a different SCM supported, please file a feature request.");
                    (MED, "The Cargo package has no git commit information".into(),
                        "Before publishing a package, make sure all packaged files are committed to the repository, and there are no \"dirty\" files. Push this commit to crate's public repository.".into(),
                        Some(("More info".into(), "https://users.rust-lang.org/t/psa-check-if-your-cargo-crates-are-clean-and-tagged/109264".into()))
                    )
                },
                Warning::Advisory(id, description) => {
                    (HI, format!("Reported security advisory {id}").into(),
                        markdown_escape(description.into_string()).into_owned().into(),
                        urler.reviews(&origin).map(|url| ("All crate audits".into(), url.into())))
                },
                Warning::ImplicitFeatures(features) => {
                    let feature = features[0].as_str();
                    let url = urler.features(&origin, Some(feature)).into();
                    (LOW, format!("Optional dependency '{feature}' exposed as an implicit feature").into(),
                        format!("Cargo automatically makes publicly-available crate features for every optional dependency, unless the dependencies are referenced using `dep:` syntax. Feature{} '{}' may have been unintentional.", if features.len() == 1 {""} else {"s"}, Itertools::format(features.into_iter().map(|f| markdown_escape(String::from(f))), "', '")).into(),
                        Some(("Crate features".into(), url)))
                },
                Warning::LicenseSpdxSyntax => {
                    let Ok(manifest) = kitchen_sink.crate_manifest(k.origin()).await else { continue };
                    (LOW, format!("License {} is not in SPDX syntax", manifest.package().license().unwrap_or("")).into(),
                        "Use `OR` instead of `/`.".into(), Some(("SPDX license list".into(), "https://spdx.org/licenses/".into())))
                }
            };
            debug_assert!(!title.contains('\\') && !title.contains('`'), "title is not markdown {title} {origin:?}");
            warnings.push(StructuredWarning { title, desc, extended_desc, url, severity });
        }
        if let Some(repo_report) = &repo_report {
            let mut fixes_dedupe = HashSet::default();
            let mut got_important_fix = false;
            let res = repo_report.result();
            if !res.all_files_verified { // TODO: also show smaller problems
                use std::fmt::Write;
                let mut desc = String::new();
                if repo_report.tried_files > 0 {
                    let ok_files = repo_report.verified_files + repo_report.skipped_files;
                    if ok_files == repo_report.tried_files {
                        writeln!(&mut desc, "Partially verified {ok_files} file{} (includes {} Cargo-generated).\n",
                            if repo_report.tried_files != 1 {"s"} else {""}, repo_report.skipped_files).unwrap();
                    } else {
                        writeln!(&mut desc, "Verified {ok_files} out of {} file{} (includes {} Cargo-generated).\n",
                            repo_report.tried_files, if repo_report.tried_files != 1 {"s"} else {""}, repo_report.skipped_files).unwrap();
                    }
                } else {
                    desc.push_str("Could not check any files\n\n");
                }

                let (msgs, paths) = repo_report.messages();
                for msg in &msgs {
                    writeln!(&mut desc, " * {}: {}", if msg.issue.is_fatal() { "error" } else { "warning" }, markdown_escape(msg.issue.message())).unwrap();
                    if let Some(fix) = msg.fix_markdown.as_deref() {
                        if msg.important || !got_important_fix {
                            if (msg.important || !fixes_dedupe_combined.contains(fix)) && fixes_dedupe.insert(fix) {
                                writeln!(&mut desc, "   - {fix}").unwrap();
                                got_important_fix |= msg.important;
                            }
                        }
                    }
                }
                if !paths.is_empty() {
                    desc.push_str("\n\nFiles in the crates.io crate compared to the repository:\n\n");
                    let mut paths: Vec<_> = paths.iter().map(|(p, r)| (&**p, &**r)).collect();
                    paths.sort_by(|a, b| a.0.cmp(b.0));

                    for (dir, entries) in paths {
                        use std::fmt::Write;
                        let is_root = dir.as_os_str() == "" || entries.len() == 1;
                        if !is_root {
                            writeln!(&mut desc, " * `{}/`", markdown_escape_code(dir.to_string_lossy())).unwrap();
                        }
                        for (m, file_name) in entries.iter().take(20) {
                            let message = match m.issue {
                                comparator::Issue::NotFoundInRepo(_) => "not found.".into(),
                                comparator::Issue::FoundInRepoButNotAtPath(_) => "exists, but elsewhere in the repo.".into(),
                                comparator::Issue::FileDiffersFromRepo(_) => "does not match the repository.".into(),
                                _ => m.issue.message(),
                            };
                            writeln!(&mut desc, "{} * `{}` {}",
                                if is_root { "" } else { "  " },
                                markdown_escape_code(file_name.to_string_lossy()),
                                markdown_escape(message)).unwrap();
                        }
                    }
                }
                desc.push_str("\n\n");
                for r in &repo_report.repos_used {
                    use std::fmt::Write;
                    let sha1 = hex::encode(r.sha1);

                    if let Some(c) = &r.crate_path {
                        if !c.is_empty() {
                            write!(&mut desc, "Looked for the crate in `{}/`. ", markdown_escape_code(c)).unwrap();
                        }
                    }

                    desc.push_str("Fetched ");
                    if let Some(sub) = &r.submodule_path {
                        write!(&mut desc, "submodule `{}` from ", markdown_escape_code(sub)).unwrap();
                    }

                    write!(&mut desc, "`{}` ", markdown_escape_code(&r.url)).unwrap();
                    if let Some(tag) = &r.tag {
                        write!(&mut desc, "tagged `{}` ({sha1})", markdown_escape_code(tag)).unwrap();
                    } else {
                        desc.push_str(&sha1);
                    }
                    desc.push_str(".\n\n");
                }
                if let Some(d) = repo_report.created_at {
                    writeln!(&mut desc, "Checked on {}", d.date_naive()).unwrap();
                }
                fixes_dedupe_combined.extend(fixes_dedupe.into_iter().map(Box::from));
                warnings.push(StructuredWarning {
                    title: if res.unclear_vcs_info { "Failed to verify create's content against its repository" } else { "Published crate doesn't match its repository" }.into(),
                    desc: desc.into(),
                    extended_desc: Some("This check is experimental."),
                    url: Some(("Forum thread".into(), "https://internals.rust-lang.org/t/verifying-that-crate-files-match-the-git-repository/20587/1".into())),
                    severity: if repo_report.verified_files <= (repo_report.tried_files + repo_report.skipped_files)/2 { HI }
                        else if !res.unclear_vcs_info { MED } else { LOW },
                });
            }
        }
        (origin, crate_ranking, warnings)
    }
}

fn markdown_special_char(b: u8) -> bool {
    matches!(b, b'!' | b'#' | b'(' | b'*' | b'-' | b'.' | b'<' | b'[' | b'\\' | b']' | b'_' | b'`' | b'{')
}

fn markdown_special_code_char(b: u8) -> bool {
    matches!(b, b'\\' | b'`')
}

fn markdown_escape<'a>(s: impl Into<Cow<'a, str>>) -> Cow<'a, str> {
    markdown_escape_n(s.into(), markdown_special_char)
}

fn markdown_escape_code<'a>(s: impl Into<Cow<'a, str>>) -> Cow<'a, str> {
    markdown_escape_n(s.into(), markdown_special_code_char)
}

fn markdown_escape_n(s: Cow<'_, str>, markdown_special_char: fn(u8) -> bool) -> Cow<'_, str> {
    if s.as_bytes().iter().copied().any(markdown_special_char) {
        Cow::Owned(s.chars().flat_map(|c| {
            if let Ok(b) = c.try_into() {
                if markdown_special_char(b) {
                    return ['\\', c].into_iter().take(2)
                }
            }
            [c, '\0'].into_iter().take(1)
        }).collect())
    } else {
        s
    }
}

async fn latest_version_matching_requirement(req: &str, deadline: Instant, kitchen_sink: &KitchenSink, origin: &Origin) -> Option<SemVer> {
    match VersionReq::parse(req) {
        Ok(req) => {
            if let Ok(Ok(current)) = timeout_at(deadline.into(), kitchen_sink.rich_crate_async(origin)).await {
                let mut versions: Vec<_> = current.versions().iter().filter_map(|v| SemVer::parse(&v.num).ok()).collect();
                if let Some(highest_matching_req) = versions.iter().filter(|v| req.matches(v)).max().cloned() {
                    versions.retain(|v| v >= &highest_matching_req);
                }
                return versions.iter().filter(|v| v.pre.is_empty()).max()
                    .or_else(|| versions.iter().filter(|v| !v.pre.is_empty()).max())
                    .cloned()
                    .map(|mut v| {
                        if v.pre.is_empty() && v.major > 0 && v.minor > 0 && v.patch < 10 {
                            v.patch = 0;
                        }
                        v
                    });
            }
        },
        Err(e) => warn!("{req} parse error: {e}"),
    }
    None
}

fn comma_list_escaped(items: impl Iterator<Item = impl std::fmt::Display>) -> String {
    let mut res = items.take(5).map(|c| {
        format!("\"{}\"", markdown_escape_code(c.to_string()))
    }).collect::<Vec<_>>().join(", ");
    if res.is_empty() {
        res.push_str("\"…\"");
    }
    res
}
