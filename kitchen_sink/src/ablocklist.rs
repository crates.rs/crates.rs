use log::warn;
use crate::CrateOwner;
use util::FxHashMap as HashMap;
use std::io;
use std::path::Path;
use util::{CowAscii, SmolStr};

#[derive(Debug, Clone)]
pub enum ABlockReason {
    /// Their crates are considered spam, malware, or otherwise junk.
    /// reason
    Banned(Box<str>),
    /// Their crates are OK, but they don't want to be on the site.
    /// reason, url
    Hidden(Box<str>, Option<SmolStr>),
    /// Suspicious account
    Distrust,
}

#[derive(Debug, Clone)]
pub enum ABlockAction<'a> {
    Ban { why: &'a str, login: Box<str> },
    Redirect,
    Distrust,
    Block(&'a str, Option<&'a str>),
}

impl ABlockAction<'_> {
    #[inline]
    #[must_use] pub fn is_ban(&self) -> bool {
        matches!(self, ABlockAction::Ban { .. })
    }

    #[inline]
    #[must_use] pub fn is_block(&self) -> bool {
        matches!(self, ABlockAction::Block(..))
    }

    #[inline]
    #[must_use] pub fn is_redirect(&self) -> bool {
        matches!(self, ABlockAction::Redirect)
    }

    #[inline]
    #[must_use] pub fn untrusted(&self) -> bool {
        matches!(self, ABlockAction::Distrust | ABlockAction::Ban {..})
    }
}

pub struct ABlockList {
    by_lc_github_login: HashMap<SmolStr, ABlockReason>,
    by_lc_github_id: HashMap<u32, ABlockReason>,
}

impl ABlockList {
    pub fn new(path: &Path) -> io::Result<Self> {
        let list = std::fs::read_to_string(path)?;
        let (by_lc_github_login, by_lc_github_id) = Self::parse_list(&list)?;

        Ok(Self {
            by_lc_github_login,
            by_lc_github_id,
        })
    }

    #[must_use]
    pub fn actions_for_owners<'a>(&'a self, owners: &[CrateOwner]) -> (bool, Vec<ABlockAction<'a>>) {
        let mut all = true;
        let actions: Vec<_> = owners.iter()
            .filter_map(|owner| {
                let res = self.for_owner(owner);
                match res {
                    None => {
                        all = false;
                        None
                    },
                    Some(why) => {
                        Some((why, owner))
                    },
                }
            })
            .map(|(a, owner)| match a {
                ABlockReason::Banned(s) => ABlockAction::Ban { why: s, login: owner.github_login().unwrap_or_default().into() },
                ABlockReason::Hidden(s, l) if !s.is_empty() => ABlockAction::Block(s, l.as_deref()),
                ABlockReason::Hidden(..) => ABlockAction::Redirect,
                ABlockReason::Distrust => ABlockAction::Distrust,
            })
            .collect();

        // some crates are co-owned by both legit and banned owners,
        // so banning by "any" would interfere with legit users' usage,
        (all, actions)
    }

    #[must_use]
    pub fn for_owner(&self, owner: &CrateOwner) -> Option<&ABlockReason> {
        owner.github_id.and_then(|id| self.get_by_github_id(id))
            .or_else(|| self.get_by_login(owner.github_login()?))
    }

    #[must_use] pub fn get_by_login(&self, username: &str) -> Option<&ABlockReason> {
        self.by_lc_github_login.get(&*username.as_ascii_lowercase())
    }

    #[must_use] pub fn get_by_github_id(&self, gh_id: u32) -> Option<&ABlockReason> {
        self.by_lc_github_id.get(&gh_id)
    }

    fn parse_list(list: &str) -> io::Result<(HashMap<SmolStr, ABlockReason>, HashMap<u32, ABlockReason>)> {
        let mut out = HashMap::default();
        let mut out_ids = HashMap::default();
        for (n, l) in list.lines().enumerate() {
            let line = l.trim();
            if line.is_empty() || line.starts_with('#') {
                continue;
            }
            let ((k, id), v) = Self::parse_line(line).ok_or_else(|| io::Error::new(io::ErrorKind::Other, format!("ablocklist line {n} is borked: {line}")))?;
            if let Some(id) = id {
                out_ids.insert(id, v.clone());
            } else {
                warn!("https://lib.rs/~{k} ablocklist missing GH ID");
            }
            out.insert(k, v);
        }
        Ok((out, out_ids))
    }

    #[cfg(debug_assertions)]
    pub fn append_line(&self, file_path: &Path, login: &str, gh_id: u32, reason: &ABlockReason) -> io::Result<()> {
        let mut file = std::fs::OpenOptions::new().append(true).create(true).open(file_path)?;
        use std::io::Write;
        match reason {
            ABlockReason::Banned(s) => writeln!(file, "{login}:{gh_id},b,,{s}"),
            ABlockReason::Hidden(s, url) => writeln!(file, "{login}:{gh_id},h,{},{s}", url.as_deref().unwrap_or_default()),
            ABlockReason::Distrust => writeln!(file, "{login}:{gh_id},d,,"),
        }
    }

    fn parse_line(line: &str) -> Option<((SmolStr, Option<u32>), ABlockReason)> {
        let mut parts = line.splitn(4, ',');
        let mut username = parts.next()?.trim();
        let mut id = None;
        if let Some((u, id_str)) = username.split_once(':') {
            username = u;
            id = id_str.parse().ok();
        }
        let kind = parts.next()?.trim();
        let url = parts.next()?.trim();
        let reason = parts.next()?.trim();

        let b = match kind {
            "b" => ABlockReason::Banned(reason.into()),
            "h" => ABlockReason::Hidden(reason.into(), (!url.is_empty()).then(|| url.into())),
            "d" => ABlockReason::Distrust,
            _ => return None,
        };
        Some(((username.as_ascii_lowercase().into(), id), b))
    }
}
