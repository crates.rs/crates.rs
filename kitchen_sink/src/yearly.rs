use crate::MiniVer;
use util::FxHashMap as HashMap;
use chrono::{Datelike, Utc};
use parking_lot::Mutex;
use serde::{Deserialize, Serialize};
use serde_big_array::BigArray;
pub use simple_cache::Error;
use simple_cache::TempCache;
use std::collections::hash_map::Entry::{Occupied, Vacant};
use std::panic::{RefUnwindSafe, UnwindSafe};
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;
use util::PushInCapacity;

/// Downloads each day of the year
#[derive(Serialize, Deserialize, Clone)]
pub struct DailyDownloads(#[serde(with = "BigArray")] pub [u32; 366]);

#[derive(Serialize, Deserialize, Clone)]
pub struct AnnualDownloads(#[serde(with = "BigArray")] pub [u64; 366]);

type PerVersionDownloads = HashMap<MiniVer, DailyDownloads>;
type ByYearCache = HashMap<u16, Arc<TempCache<PerVersionDownloads>>>;

pub struct AllDownloads {
    by_year: Mutex<ByYearCache>,
    base_path: PathBuf,
    sum_cache: TempCache<AnnualDownloads, u16>,
}

impl RefUnwindSafe for AllDownloads {}
impl UnwindSafe for AllDownloads {}

impl AllDownloads {
    /// Dir where to store years
    pub fn new(base_path: impl Into<PathBuf>) -> Self {
        let base_path = base_path.into();
        Self {
            sum_cache: TempCache::new(base_path.join("yearly_downloads"), Duration::ZERO).unwrap(),
            by_year: Mutex::new(HashMap::default()),
            base_path,
        }
    }

    fn get_cache<'t>(&self, t: &'t mut parking_lot::MutexGuard<'_, ByYearCache>, year: u16) -> Result<&'t mut Arc<TempCache<HashMap<MiniVer, DailyDownloads>>>, Error> {
        Ok(match t.entry(year) {
            Occupied(e) => e.into_mut(),
            Vacant(e) => {
                e.insert(Arc::new(TempCache::new(self.base_path.join(format!("{year}-big.rmpz")), Duration::ZERO)?))
            },
        })
    }

    /// Crates.io crate name
    pub fn get_crate_year_raw(&self, crate_name: &str, year: u16) -> Result<Option<PerVersionDownloads>, Error> {
        let mut t = self.by_year.try_lock_for(Duration::from_secs(10)).ok_or(Error::Timeout)?;
        let cache = self.get_cache(&mut t, year)?;
        cache.get(crate_name)
    }

    /// Remove noise floor. There is a lot of constant downloads from mirrors and bots.
    pub fn get_crate_year_no_bots(&self, crate_name: &str, year: u16) -> Result<Option<PerVersionDownloads>, Error> {
        Ok(self.get_crate_year_raw(crate_name, year)?.map(|mut data| {
            remove_bot_traffic_noise(&mut data);
            data
        }))
    }

    pub fn set_crate_year_raw(&self, crate_name: &str, year: u16, v: &PerVersionDownloads) -> Result<(), Error> {
        let mut t = self.by_year.try_lock_for(Duration::from_secs(15)).ok_or(Error::Timeout)?;
        self.get_cache(&mut t, year)?.set(crate_name, v)?;
        Ok(())
    }

    pub fn for_each_in_year(&self, year: u16, cb: impl FnMut(&Box<str>, PerVersionDownloads)) -> Result<(), Error> {
        let y = self.get_full_year(year)?;
        y.for_each(cb)
    }

    fn get_full_year(&self, year: u16) -> Result<Arc<TempCache<PerVersionDownloads>>, Error> {
        let mut t = self.by_year.try_lock_for(Duration::from_secs(10)).ok_or(Error::Timeout)?;
        let cache = self.get_cache(&mut t, year)?;
        Ok(Arc::clone(cache))
    }

    pub fn total_year_downloads(&self, year: u16) -> Result<[u64; 366], Error> {
        if let Some(res) = self.sum_cache.get(&year)? {
            return Ok(res.0);
        }
        let summed_days = Mutex::new([0u64; 366]);
        if let Ok(year) = self.get_full_year(year) {
            year.par_for_each(|_, mut crate_year| {
                remove_bot_traffic_noise(&mut crate_year);
                let summed_days = &mut *summed_days.lock();
                for (_, days) in crate_year {
                    for (sd, vd) in summed_days.iter_mut().zip(days.0.iter().copied()) {
                        *sd += u64::from(vd);
                    }
                }
            })?;
        }
        let summed_days = summed_days.into_inner();
        self.sum_cache.set(year, AnnualDownloads(summed_days))?;
        Ok(summed_days)
    }

    pub fn save(&self) -> Result<(), Error> {
        self.sum_cache.save()?;
        let t = self.by_year.try_lock_for(Duration::from_secs(10)).ok_or(Error::Timeout)?;
        for y in t.values() {
            y.save()?;
        }
        Ok(())
    }

    pub fn summed_year_downloads(&self, crate_name: &str, curr_year: u16, remove_sudden_peaks: bool) -> Result<[u32; 366], Error> {
        let curr_year_data = self.get_crate_year_no_bots(crate_name, curr_year)?.unwrap_or_default();
        let mut summed_days = [0; 366];

        for (_v, days) in curr_year_data {
            for (summed, vd) in summed_days.iter_mut().zip(days.0.iter().copied()) {
                *summed += vd;
            }
        }

        if remove_sudden_peaks {
            let mut moving_max = summed_days[..5].iter().copied().max().unwrap_or(0).max(1000);
            for day in summed_days.iter_mut().skip(5) {
                if *day > moving_max {
                    // don't believe in sudden usage spikes
                    *day = (*day).min(moving_max * 3 / 2 + 100);
                    moving_max = *day;
                } else {
                    moving_max = moving_max * 7 / 8;
                }
            }
        }
        Ok(summed_days)
    }

    pub(crate) fn recalc_current_year(&self) -> Result<(), Error> {
        let year = Utc::now().year() as u16;
        self.sum_cache.delete(&year)?;
        self.total_year_downloads(year)?;
        Ok(())
    }
}

fn remove_bot_traffic_noise(dl_per_version: &mut PerVersionDownloads) {
    let mut per_version_count = [0u8; 54];
    let mut per_version_minmax = [(0u16, 0u16); 54];
    let mut version_dls = Vec::with_capacity(dl_per_version.len());

    // use weekly buckets, because weekends are have very different stats
    for (week_num, ((v_min, v_max), unique_ver_cnt)) in per_version_minmax.iter_mut().zip(per_version_count.iter_mut()).enumerate() {
        version_dls.clear();

        for version_days in dl_per_version.values() {
            let dls = version_days.0.chunks(7).nth(week_num).unwrap_or_default().iter().map(|&d| u64::from(d)).sum::<u64>();
            if dls == 0 {
                continue;
            }
            version_dls.push_in_cap(dls.min(u64::from(u16::MAX)) as u16);
        }
        if version_dls.is_empty() {
            continue;
        }

        version_dls.sort_unstable();
        // 16th %-ile or max 100 non-junk versions in use
        let sample_min_from = version_dls.len().saturating_sub(100).max(version_dls.len() / 6);
        *v_min = version_dls[sample_min_from];
        *v_max = version_dls.last().copied().unwrap_or(0);

        *unique_ver_cnt = version_dls.len().min(255) as u8;
    }

    // use 2-week buckets to smooth out sparse data
    for days in dl_per_version.values_mut() {
        for (dls, (minmax, unique_ver_cnt)) in days.0.chunks_mut(7).zip(per_version_minmax.windows(2).zip(per_version_count.windows(2))) {
            let unique_ver_cnt = unique_ver_cnt[0].min(unique_ver_cnt[1]);
            // pick bigger baseline of the 2-week period,
            // because there's clear noise floor in monthly stats,
            // but daily stats are noisy and may have zero hits
            let dl_min = minmax[0].0.max(minmax[1].0);
            let dl_max = minmax[0].1.max(minmax[1].1);

            if dl_max == 0 {
                continue;
            }

            let dl_sum = dls.iter().map(|&d| u64::from(d)).sum::<u64>();
            if dl_sum == 0 {
                continue;
            }

            // take minimum downloads of any version, especially if there are many versions
            // and use it as a noise floor for all versions
            let noise_floor = (u32::from(dl_min).min((100 + u32::from(dl_max)) / 4) / 5_u32.saturating_sub(unique_ver_cnt.into()).max(1)).max(25);
            let keep = dl_sum.saturating_sub(noise_floor.into());
            let round = (dl_sum - keep) / 2;
            for daily in &mut *dls {
                let rescaled = ((u64::from(*daily) * keep + round) / dl_sum) as u32;
                debug_assert!(rescaled <= *daily);
                *daily = rescaled;
            }
        }
    }
}

impl Default for DailyDownloads {
    fn default() -> Self {
        Self([0; 366])
    }
}
