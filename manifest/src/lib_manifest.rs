use std::path::PathBuf;

use itertools::Itertools;
use log::debug;
use std::sync::LazyLock as Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};
use toml_edit::{DocumentMut, Item, Table};
use util::{PushString, SmolStr};

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(debug_assertions, derive(PartialEq, Eq))]
pub struct Detail {
    /// key=="" means before all of them (hack to avoid reindexing everything again)
    pub key: SmolStr,
    pub comment: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(debug_assertions, derive(PartialEq, Eq))]
pub struct DetailedFeatures {
    pub features: Vec<Detail>,

    pub normal: Vec<Detail>,
    pub build: Vec<Detail>,
    pub dev: Vec<Detail>,
}

/// assume trimmed whitespace
static LOOKS_LIKE_TOML: Lazy<Regex> = Lazy::new(|| Regex::new(r#"^[a-z_][a-z0-9_-]+\s*=\s*(?:\[[^];<]*\]|\{[^};<]*}|"[^"]+")(\s*#|$)"#).unwrap());
static LOOKS_LIKE_TOML_DEP: Lazy<Regex> = Lazy::new(|| Regex::new(r#"^(?:\{\s*|,\s*)?(?:version|features|default-features|workspace|registry|git|path|optional|package)\s*=\s*[\["{tfi]"#).unwrap());
static LOOKS_LIKE_LIST: Lazy<Regex> = Lazy::new(|| Regex::new(r#"^(?:,\s*)?(?:\[\s*)?"[^"]+"\s*(,\s*"[^"]+"\s*)*(?:,\s*)?\]($|\s*#)"#).unwrap());

impl DetailedFeatures {
    pub fn from_toml(data: &str) -> Result<Self, toml_edit::TomlError> {
        let doc = data.parse::<DocumentMut>()?;
        let mut features = Vec::new();

        if let Some(Item::Table(f)) = doc.get("features") {
            Self::get_preface(f, &mut features, false);

            for (k, v) in f.get_values() {
                let mut comment = String::new();
                for &k in &k {
                    // TODO: stop at lines that look like commented-out TOML and reject all lines before
                    Self::decor_to_comment(k.leaf_decor().prefix(), true, false, &mut comment);
                }
                // TODO: reject comments that look like commented-out values, like `features = []`
                Self::decor_to_comment(v.decor().suffix(), false, false, &mut comment);
                debug_assert_eq!(1, k.len());
                if let Some(k) = k.first() {
                    features.push(Detail { key: k.get().into(), comment });
                }
            }
        }
        let mut normal = Vec::new();
        if let Some(Item::Table(f)) = doc.get("dependencies") {
            Self::get_preface(f, &mut normal, true);
            Self::read_deps_features_from(f, &mut normal);
        }
        let mut build = Vec::new();
        if let Some(Item::Table(f)) = doc.get("build-dependencies") {
            Self::read_deps_features_from(f, &mut build);
        }
        let mut dev = Vec::new();
        if let Some(Item::Table(f)) = doc.get("dev-dependencies") {
            Self::read_deps_features_from(f, &mut dev);
        }
        Ok(Self { features, normal, build, dev })
    }

    fn get_preface(f: &Table, out: &mut Vec<Detail>, is_dep: bool) {
        let mut comment = String::new();
        Self::decor_to_comment(f.decor().prefix(), true, is_dep, &mut comment);
        Self::decor_to_comment(f.decor().suffix(), false, is_dep, &mut comment);
        if !comment.is_empty() && !comment.starts_with("See more keys and their definitions at ") {
            out.push(Detail { key: "".into(), comment });
        }
    }

    #[inline(never)]
    fn read_deps_features_from(table: &Table, out: &mut Vec<Detail>) {
        for (k, v) in table.get_values() {
            let mut comment = String::new();
            for k in &k {
                Self::decor_to_comment(k.leaf_decor().prefix(), true, true, &mut comment);
            }
            Self::decor_to_comment(v.decor().suffix(), false, true, &mut comment);
            // workspace-inherited deps have a weird structure
            if let Some(k) = k.iter().find(|&&k| k.get() != "workspace") {
                out.push(Detail { key: k.get().into(), comment });
            }
        }
    }

    #[inline(never)]
    fn decor_to_comment(decor: Option<&toml_edit::RawString>, is_prefix: bool, is_dep: bool, comment: &mut String) {
        let mut decor = decor.and_then(|s| s.as_str()).map(str::trim).unwrap_or_default();
        if decor.is_empty() {
            return;
        }

        if is_prefix {
            // crates are almost always lowercase, so don't bother with A-Z
            let looks_like_toml = &*LOOKS_LIKE_TOML;
            let mut to_skip = 0;
            let mut len_seen_so_far = 0;
            let mut in_code_block = false;
            for line in decor.split_inclusive('\n') {
                len_seen_so_far += line.len();
                // this isn't final parse, only for commented-out-code check
                let mut line = line.trim();
                if line.starts_with("#!") { // document-features crate
                    continue;
                }
                line = line.trim_start_matches('#').trim_start(); // uncomment

                if line.starts_with("```") {
                    in_code_block = !in_code_block; // 4-space indent is probably too ambiguous
                }
                if in_code_block {
                    continue;
                }
                if line.starts_with(',') || looks_like_toml.is_match(line) {
                    debug!("ignoring comment up to line {line}, because it looks like commented-out TOML");
                    to_skip = len_seen_so_far;
                }
            }
            if to_skip > 0 {
                decor = decor.get(to_skip..).unwrap_or_default().trim_start();
                if decor.is_empty() {
                    return;
                }
            }
        } else { // suffix
            let line = decor.trim_start_matches('#').trim_start();
            if line.starts_with(',') || LOOKS_LIKE_TOML.is_match(line) || (is_dep && LOOKS_LIKE_TOML_DEP.is_match(line)) || (!is_dep && LOOKS_LIKE_LIST.is_match(line)) {
                debug!("ignoring comment suffix {line}, because it looks like commented-out TOML");
                return;
            }
        }

        comment.reserve(decor.len());

        if !comment.is_empty() {
            comment.push_in_cap('\n');
        }
        // empty first and last line already trimmed out in decor
        let lines = decor.lines()
            .map(|l| {
                let mut line = l.trim_start();
                line = line.strip_prefix("#!") // document-features crate
                    .unwrap_or_else(|| line.trim_matches('#'));
                line.strip_prefix(' ').unwrap_or(line) // just one space, allow markdown code indent
            });
        comment.extend(Itertools::intersperse(lines, "\n"));
    }

    // 1 feature means no useful sort order info
    #[must_use] pub fn is_empty(&self) -> bool {
        self.features.len() <= 1 && self.features.iter()
            .chain(&self.normal).chain(&self.build).chain(&self.dev)
            .all(|d| d.comment.is_empty())
    }
}

#[must_use]
pub fn is_no_std_keyword(c: &str) -> bool {
    if c == "nostd" {
        return true;
    }
    let Some(no_no) = c.strip_prefix("no").or_else(|| c.strip_prefix("use"))
        .and_then(|c| c.strip_prefix(['-', '_'])) else { return false };
    matches!(no_no.strip_prefix("lib").unwrap_or(no_no), "std" | "alloc")
}

#[must_use] pub fn is_no_std_feature(c: &str) -> bool {
    c == "std" || c == "alloc" || is_no_std_keyword(c)
}

// That belongs to tarball.rs, but I have no way of sharing it properly across crates ;(
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct IdentifiedBinFile {
    pub path: PathBuf,
    pub kind: Box<str>,
    pub size: u64,
}


#[test]
fn toml_regexes() {
    assert!(LOOKS_LIKE_TOML.is_match("feature = []"));
    assert!(LOOKS_LIKE_TOML.is_match(r#"features=["x"]"#));

    assert!(LOOKS_LIKE_TOML.is_match(r#"dep = "1.2""#));
    assert!(LOOKS_LIKE_TOML.is_match(r#"_dep = "1.2" # x"#));
    assert!(LOOKS_LIKE_TOML.is_match(r#"a_de-pend_dency = { path = "foo" }"#));
    assert!(LOOKS_LIKE_TOML.is_match(r#"a_de-pend_dency = { path = "foo", version = "2" }"#));
    assert!(!LOOKS_LIKE_TOML.is_match(r#"1 = { path = "foo" }"#));
    assert!(!LOOKS_LIKE_TOML.is_match(r#"-a = "123""#));

    assert!(LOOKS_LIKE_TOML_DEP.is_match(r#"{ path = "foo" }"#));
    assert!(LOOKS_LIKE_TOML_DEP.is_match(r#"path = "foo" }"#));
    assert!(LOOKS_LIKE_TOML_DEP.is_match(r#", path = "foo" }"#));
    assert!(LOOKS_LIKE_TOML_DEP.is_match(r#",  path = "foo" } # xxxx"#));
    assert!(LOOKS_LIKE_TOML_DEP.is_match(r#"version = "2", path="" }"#));
    assert!(LOOKS_LIKE_TOML_DEP.is_match(r#"git = "yellow" }"#));
    assert!(!LOOKS_LIKE_TOML_DEP.is_match(r#"color = "yellow" }"#));

    assert!(LOOKS_LIKE_LIST.is_match(r#""foo","bar",]"#));
    assert!(LOOKS_LIKE_LIST.is_match(r#""foo","bar",] # baz"#));
    assert!(LOOKS_LIKE_LIST.is_match(r#""foo", "bar"] # baz"#));
    assert!(LOOKS_LIKE_LIST.is_match(r#", "foo", "bar"] # baz"#));
    assert!(LOOKS_LIKE_LIST.is_match(r#","foo", "bar"] # baz"#));
    assert!(LOOKS_LIKE_LIST.is_match(r#"["foo", "bar"] # baz"#));
    assert!(LOOKS_LIKE_LIST.is_match(r#"["foo", "bar",] # baz"#));
    assert!(LOOKS_LIKE_LIST.is_match(r#"["foo",   ] # baz"#));
    assert!(LOOKS_LIKE_LIST.is_match(r#"[   "foo", "bar"] # baz"#));
    assert!(LOOKS_LIKE_LIST.is_match(r#"[   "foo"]#"#));
    assert!(!LOOKS_LIKE_LIST.is_match(r#"{   "foo"]#"#));
    assert!(!LOOKS_LIKE_LIST.is_match(r#"foo""#));
    assert!(!LOOKS_LIKE_LIST.is_match(r#"foo" #xxx "#));
}

#[test]
fn nostd_kw() {
    assert!(is_no_std_keyword("no-std"));
    assert!(is_no_std_keyword("no_std"));
    assert!(is_no_std_keyword("nostd"));
}

#[test]
fn detailedfeatures_test() {
    let _ = env_logger::try_init();

    let d = DetailedFeatures::from_toml(r#"
    # Intro to features
    [features] # they're cool

    #### first feature ####
    first = []

    # not relevant
    # commented-out = "123"

    # yes relevant
    second = ["first"] # second extra

    third = [] # bad_comment = []
    fourth = [] # ["nope"]

    # ignore = []
    [dependencies]
    # nope = { version = "1" }
    # yup
    dep = "2"
    "#).unwrap();

    debug_assert_eq!(d, DetailedFeatures {
    features: vec![
        Detail {
            key: "".into(),
            comment: "Intro to features\nthey're cool".into(),
        },
        Detail {
            key: "first".into(),
            comment: "first feature ".into(),
        },
        Detail {
            key: "second".into(),
            comment: "yes relevant\nsecond extra".into(),
        },
        Detail {
            key: "third".into(),
            comment: String::new(),
        },
        Detail {
            key: "fourth".into(),
            comment: String::new(),
        },
    ],
    normal: vec![
        Detail {
            key: "dep".into(),
            comment: "yup".into(),
        },
    ],
    build: vec![],
    dev: vec![],
    });
}
