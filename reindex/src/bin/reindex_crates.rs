use rand::rngs::SmallRng;
use util::{FxHashMap as HashMap, FxHashSet as HashSet};
use chrono::prelude::*;
use anyhow::anyhow;
use debcargo_list::DebcargoList;
use feat_extractor::{is_autopublished, is_deprecated, is_squatspam, wlita};
use futures::future::join_all;
use futures::stream::StreamExt;
use futures::Future;
use itertools::Itertools;
use kitchen_sink::{
    running, stop, stopped, ArcRichCrateVersion, CrateOwners, KitchenSink, Origin, RichCrate, RichCrateVersion, SemVer, SpawnAbortOnDrop, VersionPopularity
};
use kitchen_sink::CrateAuthor;
use log::{debug, info, warn};
use parking_lot::Mutex;
use rand::seq::SliceRandom;
use rand::{Rng, SeedableRng};
use ranking::{CrateTemporalInputs, CrateVersionInputs, OverallScoreInputs, ExperienceAtReleaseDate};
use render_readme::{Links, LinksContext, Renderer};
use repo_url::Repo;
use rich_crate::Dependency;
use search_index::{CrateSearchIndex, Indexer, IndexerData};
use simple_cache::TempCache;
use std::borrow::Cow;
use std::cmp::Reverse;
use std::pin::Pin;
use std::process::ExitCode;
use std::time::{Duration, Instant};
use tokio::sync::mpsc;
use triomphe::Arc;
use udedokei::LanguageExt;
use util::{pick_top_n_unstable_by, SmolStr};

struct Reindexer {
    crates: KitchenSink,
    deblist: DebcargoList,
}

/// `(ver, downloads_per_month, source_code_texts, score)`
type SearchIndexDataTmp = (ArcRichCrateVersion, usize, Vec<SmolStr>, f64);

fn main() -> ExitCode {
    if let Err(e) = run() {
        eprintln!("Failed: {e}");
        for c in e.chain() {
            eprintln!("  {c}");
        }
        ExitCode::FAILURE
    } else {
        ExitCode::SUCCESS
    }
}

fn run() -> anyhow::Result<()> {
    let (options, crate_filters) = std::env::args().skip(1).partition::<Vec<_>, _>(|a| a.starts_with('-'));

    let everything = options.iter().any(|a| a == "--all");
    let search_only = options.iter().any(|a| a == "--search");
    let use_filter = options.iter().any(|a| a == "--filter");
    let specific: Vec<_> = if everything || use_filter { vec![] } else { crate_filters.iter().map(|name| {
        Origin::try_from_crates_io_name(name).unwrap_or_else(|| Origin::from_str(name))
    }).collect() };
    let filters = if everything || search_only { crate_filters } else { vec![] };
    let has_no_filters = filters.is_empty();

    let repos = !everything && !search_only && specific.len() < 20;
    let rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .thread_name("reindex")
        .build()?;

    running()?;

    let crates = rt.block_on(KitchenSink::new_default())?;
    let deblist = DebcargoList::new(crates.main_data_dir()).expect("deblist");
    if everything && !search_only && has_no_filters {
        deblist.update().expect("debcargo"); // it needs to be updated sometime, but not frequently
        crates.crate_db.update_keyword_precision()?;
    }
    let r = Arc::new(Reindexer { crates, deblist });
    let mut indexer = Indexer::new(CrateSearchIndex::new(r.crates.main_data_dir()).expect("init search")).expect("init search indexer");
    let lines = TempCache::new(r.crates.main_data_dir().join("search-uniq-lines.dat"), Duration::ZERO).expect("init lines cache");
    let (tx, mut rx) = mpsc::channel::<SearchIndexDataTmp>(64);

    running()?;
    let r2 = r.clone();
    let async_cleanup_thread = std::thread::spawn(move || loop {
        std::thread::sleep(Duration::from_millis(300));
        if stopped() {
            eprintln!("Flushing caches...");
            r2.crates.cleanup();
            eprintln!("Flushed");
            return;
        }
    });

    let index_thread = SpawnAbortOnDrop(rt.spawn({
        async move {
            running()?;
            let renderer = Arc::new(Renderer::new(None));
            let mut n = 0usize;
            let mut next_n = 100usize;
            while let Some((ver, downloads_per_month, extra_auto_keywords, score)) = rx.recv().await {
                if stopped() {break;}
                blocking::block_in_place("indxsear", || {
                    index_search(&mut indexer, &lines, &renderer, &ver, downloads_per_month, &extra_auto_keywords, score)?;
                    n += 1;
                    if n == next_n {
                        next_n *= 2;
                        println!("savepoint…");
                        indexer.commit()?;
                    }
                    Ok::<_, anyhow::Error>(())
                })?;
            }
            blocking::block_in_place("indx-cmmit", || indexer.commit())?;
            let _ = indexer.bye()?;
            Ok::<_, anyhow::Error>(())
        }
    }));

    rt.spawn_blocking({
        let r = r.clone();
        move || {
            r.crates.prewarm();
        }
    });
    // locks the db and causes timeouts everywhere
    let _ = rt.block_on(rt.spawn({
        let r = r.clone();
        async move {
            r.crates.prewarm_reindexing().await;
        }
    }));
    running()?;

    let filter = move |c: &Origin| {
        if has_no_filters {
            return true;
        }
        let name = c.package_name_icase();
        if let [only] = &filters[..] {
            name.contains(only)
        } else  {
            filters.iter().any(|p| p == name)
        }
    };

    let c: Box<dyn Iterator<Item = Origin> + Send> = if !specific.is_empty() {
        Box::new(specific.into_iter().filter(filter))
    } else if everything || search_only {
        let mut urgent = if use_filter { vec![] } else { rt.block_on(r.crates.crates_to_reindex(2000))?
            .into_iter().filter(&filter).collect::<Vec<_>>() };
        let mut rng = rand::rngs::StdRng::from_entropy();
        urgent.shuffle(&mut rng);
        let mut all = r.crates.index()?.all_crates()?;
        all.retain(filter);
        all.shuffle(&mut SmallRng::from_entropy());
        let r = Arc::clone(&r);
        let all = all.into_iter().filter(move |origin| {
            // reindexing of the bulk of sturgeon's law takes too much time
            if has_no_filters && r.crates.crate_ranking_for_builder(origin).is_ok_and(|r| r < 0.33 && r < rng.gen_range(0.0..0.33)) {
                log::debug!("skipping low-ranking {origin:?}");
                return false;
            }
            true
        });
        Box::new(urgent.into_iter().chain(all))
    } else {
        Box::new(rt.block_on(r.crates.crates_to_reindex(200))?.into_iter().filter(filter))
    };
    running()?;

    rt.block_on(SpawnAbortOnDrop(rt.spawn(async move {
        is_send(main_indexing_loop(r, c, tx, repos, search_only, everything)).await;
        if stopped() {return;}
        index_thread.await.unwrap().unwrap();
    })))?;

    stop();
    let _ = async_cleanup_thread.join();
    Ok(())
}

async fn main_indexing_loop(r: Arc<Reindexer>, crate_origins: Box<dyn Iterator<Item=Origin> + Send>, tx: mpsc::Sender<SearchIndexDataTmp>, repos: bool, search_only: bool, skip_heavy_indexing: bool) {
    let renderer = Arc::new(Renderer::new(None));
    let seen_repos = Arc::new(Mutex::new(HashSet::default()));
    let repo_concurrency = Arc::new(tokio::sync::Semaphore::new(4));
    futures::stream::iter(crate_origins.enumerate()).for_each_concurrent(8, move |(i, origin)| {
        let tx = tx.clone();
        let r = Arc::clone(&r);
        let renderer = Arc::clone(&renderer);
        let seen_repos = Arc::clone(&seen_repos);
        let repo_concurrency = Arc::clone(&repo_concurrency);
        async move {
        tokio::spawn(async move {
            if stopped() {return;}
            println!("{i}…");
            if !search_only {
                match run_timeout(&format!("hv-{origin:?}"), 62, Box::pin(r.crates.index_crate_highest_version(&origin, skip_heavy_indexing))).await {
                    Ok(()) => {},
                    err => {
                        print_res(err);
                        if skip_heavy_indexing { return; }
                        // on hourly updates, try to continue and update score, search, etc.
                    },
                }
            }
            if stopped() {return;}
            if i%3000 == 0 {
                info!("flushing caches");
                r.crates.cleanup(); // reduce mem usage, write stuff to disk
            }
            match run_timeout(&format!("rc-{origin:?}"), 59, Box::pin(is_send(r.index_crate(&origin, &renderer, &tx, search_only)))).await {
                Ok(v) => {
                    if repos && !v.is_spam() {
                        for repo in r.repositories_for_crate(&v).await {
                            {
                                let mut s = seen_repos.lock();
                                let url = repo.canonical_git_url();
                                if s.contains(&*url) {
                                    continue;
                                }
                                println!("Indexing repo of {}: {url}", v.short_name());
                                s.insert(url);
                            }
                            let _finished = repo_concurrency.acquire().await;
                            if stopped() {return;}
                            print_res(run_timeout(&format!("rep-{repo:?}"), 500, Box::pin(r.crates.index_repo(&repo, v.version()))).await);
                        }
                    }
                },
                err => print_res(err),
            }
        }).await.unwrap();
    }}).await;
}

impl Reindexer {
    async fn repositories_for_crate<'a>(&self, k: &'a RichCrateVersion) -> Vec<Cow<'a, Repo>> {
        if let Some(repo) = k.repository() {
            return vec![Cow::Borrowed(repo)];
        }

        let crate_name = k.short_name();
        let owners = self.crates.crate_owners(k.origin(), CrateOwners::Strict).await.unwrap_or_default();
        debug!("{crate_name} has no repo property, checking by {} owners", owners.len());

        let repos = join_all(owners.iter().filter_map(|o| o.github_login()).map(|login| async move {
            self.crates.gh.all_repos(login).await.map_err(|e| warn!("{login} repos fail: {e}")).unwrap_or_default().unwrap_or_default()
        })).await;
        repos.into_iter().flatten().filter(|repo| {
            debug!("{crate_name} is it in {:?}", repo);
            repo.name.contains(crate_name) || repo.name.eq_ignore_ascii_case(crate_name)
        })
        .filter_map(|repo| {
            let url = format!("https://github.com/{}/{}", repo.owner.as_ref()?.login, repo.name);
            info!("will try to find crate {crate_name} in repo {url}");
            Repo::new(&url).ok().map(Cow::Owned)
        }).take(3).collect()
    }

async fn index_crate(&self, origin: &Origin, renderer: &Renderer, search_sender: &mpsc::Sender<SearchIndexDataTmp>, search_only: bool) -> Result<ArcRichCrateVersion, anyhow::Error> {
    let crates = &self.crates;
    let (k, v) = futures::try_join!(
        run_timeout("rca", 31, Box::pin(is_send(crates.rich_crate_async(origin)))),
        run_timeout("rcv", 45, Box::pin(is_send(crates.rich_crate_version_async(origin)))),
    )?;

    let crate_version = v.version();
    let origin_str = &*origin.to_str();
    let extra_auto_keywords = crates.extra_derived_storage.get_deserialized::<Vec<(f32, SmolStr)>>((&format!("{origin_str}!kw"), crate_version))?.unwrap_or_default();
    let extra_auto_keywords = extra_auto_keywords.into_iter().map(|(_, w)| w).collect();

    let (downloads_per_month, score) = run_timeout("score", 41, Box::pin(is_send(self.crate_overall_score(&k, &v, renderer)))).await?;
    debug!("{origin:?} has score {score} and {downloads_per_month}dl/mo");
    let (_, index_res) = futures::join!(
        run_timeout("ssend", 10, Box::pin(async {
            search_sender.send((v.clone(), downloads_per_month, extra_auto_keywords, score)).await
                .inspect_err(|_| stop())
        })),
        run_timeout("ic", 50, Box::pin(async move { if !search_only { crates.index_crate(&k, score).await } else {Ok(())} }))
    );
    index_res?;
    Ok(v)
}
}

fn index_search(indexer: &mut Indexer, lines: &TempCache<(String, f64), [u8; 16]>, renderer: &Renderer, k: &RichCrateVersion, downloads_per_month: usize, extra_auto_keywords: &[SmolStr], score: f64) -> Result<(), anyhow::Error> {
    let to_delete = k.is_spam() || k.is_hidden() || score < 0.0001;

    if to_delete {
        indexer.delete(k.origin());
        return Ok(());
    }

    if stopped() {
        anyhow::bail!("aborted");
    }

    let keywords: Vec<_> = if !k.is_spam() { k.keywords().iter().map(|s| s.as_str()).collect() } else { Vec::new() };
    let version = k.version();
    let mut unique_text = String::with_capacity(4000);

        let mut variables = Vec::with_capacity(15);
        variables.push(k.short_name());
        let repo;
        if let Some(r) = k.repository() {
            if let Some(name) = r.repo_name() {
                variables.push(name);
            }
            repo = r.canonical_http_url("", None);
            variables.push(&repo);
        }
        let authors: Vec<_> = k.authors().filter_map(|a| a.name).collect();
        for name in &authors {
            variables.push(name);
        }
        variables.sort_unstable_by_key(|a| Reverse(a.len())); // longest first

        let mut dupe_sentences = HashSet::default();
        let mut cb = |key: &str, orig: &str| {
            let key: [u8; 16] = blake3::hash(key.as_bytes()).as_bytes()[..16].try_into().unwrap();
            if dupe_sentences.insert(key) {
                let (matches, wins) = if let Ok(Some((their_crate_name, their_score))) = lines.get(&key) {
                    (k.short_name() == their_crate_name, score > their_score)
                } else {
                    (true, true)
                };
                if wins {
                    lines.set(key, (k.short_name().to_string(), score)).expect("upd lines");
                }
                if matches || wins {
                    unique_text.push_str(orig);
                    unique_text.push('\n');
                }
            }
        };

        let mut dedup = wlita::WLITA::new(variables.iter(), &mut cb);
        if let Some(markup) = k.readme().map(|readme| &readme.markup) {
            for (s, par) in &renderer.visible_text_by_section(markup) {
                dedup.add_text(s);
                dedup.add_text(par);
            }
        }
        if let Some(markup) = k.lib_file_markdown() {
            for (s, par) in &renderer.visible_text_by_section(&markup) {
                dedup.add_text(s);
                dedup.add_text(par);
            }
        }

    let mut extra_keywords = extra_auto_keywords.iter().map(|s| s.as_str()).collect::<Vec<_>>();

    let crate_name = k.short_name();
    let tmp2;
    if crate_name.as_bytes().iter().any(|&c| c == b'_' || c == b'-') {
        // Make serde-json searchable by 'json serde'
        tmp2 = crate_name.chars().map(|c| if c.is_ascii_alphabetic() {c} else {' '}).collect::<String>();
        extra_keywords.push(&tmp2);
    }

    // Make serde-json searchable by serdejson
    let keywordswithoutdashes = std::iter::once(crate_name).chain(keywords.iter().copied())
        .filter(|s| s.contains(['_', '-']))
        .map(|s| s.chars().filter(|&c| c != '_' && c != '-').collect::<String>()).collect::<Vec<_>>();
    extra_keywords.extend(keywordswithoutdashes.iter().map(|s| s.as_str()));

    let individual_words: HashSet<_> = extra_keywords.iter().chain(keywords.iter())
        .filter(|w| w.contains('-')) // aws-s3 should be findable by "s3"
        .flat_map(|w| w.split('-'))
        .collect();

    let extra_keywords = Itertools::intersperse(extra_keywords.iter().copied()
        .chain(individual_words.iter().copied()), " ")
        .collect::<String>();

    indexer.add(IndexerData {
        origin: k.origin(),
        crate_name,
        version,
        description: k.description().unwrap_or(""),
        keywords: &keywords,
        readme: Some(unique_text.as_str()).filter(|s| !s.trim_start().is_empty()),
        monthly_downloads: downloads_per_month as u64,
        extra_keywords: Some(extra_keywords),
        score,
    })?;
    Ok(())
}

impl Reindexer {
    async fn crate_overall_score(&self, all: &RichCrate, k: &RichCrateVersion, renderer: &Renderer) -> Result<(usize, f64), anyhow::Error> {
        let crates = &self.crates;
        let origin = all.origin();
        let image_deadline = Instant::now() + Duration::from_secs(2);
        let stats = crates.index()?.deps_stats()?;

        let (traction_stats, downloads_per_month_or_equivalent, has_docs_rs, has_verified_repository_link, (do_block, blocklist_actions), is_sub_component, is_internal_crate, vet_reviews, manifest, changelog) = futures::join!(
            run_timeout("ts", 10, Box::pin(crates.traction_stats(origin, stats))),
            run_timeout("dl", 10, Box::pin(crates.downloads_per_month_or_equivalent(origin))),
            crates.has_docs_rs(origin, k.short_name(), k.version()),
            crates.has_verified_repository_link(k),
            crates.crate_blocklist_actions(all.origin()),
            crates.is_sub_component(k),
            crates.is_internal_crate(k),
            crates.vets_for_crate(origin),
            blocking::awatch("score-manifest", crates.crate_manifest(origin)),
            crates.changelog_url(k),
        );
        let manifest = manifest?;
        let has_changelog = changelog.is_some();
        let package = manifest.package();
        let has_verified_repository_link = has_verified_repository_link?;
        let crev_reviews = crates.reviews_for_crate(origin);
        let distrusted = blocklist_actions.iter().any(|b| b.untrusted());
        let ablocklist_banned = do_block && blocklist_actions.iter().any(|b| b.is_ban());
        let ablocklist_hidden = do_block && blocklist_actions.iter().any(|b| b.is_block());
        let ablocklist_redirect = do_block && blocklist_actions.iter().any(|b| b.is_redirect());
        let traction_stats = traction_stats?;
        let vet_reviews = vet_reviews?;
        let has_vet_reviews = !distrusted && vet_reviews.iter().any(|r| r.version().is_some() && !r.is_wildcard_trust()); // version is on full code reviews
        let has_vet_trust = !distrusted && vet_reviews.iter().any(|r| r.is_wildcard_trust());
        let downloads_per_month_or_equivalent = downloads_per_month_or_equivalent?.unwrap_or(0) as u32;
        let advisories = crates.advisories_for_crate(origin)?;
        let semver: SemVer = k.version_semver()?;
        let is_unmaintained = crates.is_unmaintained_in_advisories(k.origin(), &advisories, &crev_reviews, &semver).unwrap_or(false);
        let has_crev_reviews = !distrusted && !crev_reviews.is_empty();

        let has_parent = crates.parent_crate_same_repo_unverified(k).await.is_some();
        let (contrib_info, owners, github_repo, downloads_per_month) = futures::try_join!(
            run_timeout("contr", 20, Box::pin(crates.all_contributors(k))),
            run_timeout("own", 20, Box::pin(crates.crate_owners(k.origin(), CrateOwners::All))),
            run_timeout("gh-repo", 20, Box::pin(async {
                if let Some(repo) = k.repository() {
                    crates.github_repo(repo).await
                } else {
                    Ok(None)
                }
            })),
            run_timeout("dl", 20, Box::pin(crates.downloads_per_month(origin, false)))
        )?;
        let downloads_per_month = downloads_per_month.unwrap_or(0);
        let versions = all.versions();

        let latest_release_date = versions.iter().map(|v| v.created_at).max().expect("0 ver");
        let release_contributors = data_for_contributor_experience(latest_release_date, &contrib_info.0, crates);

        let contributors_count = contrib_info.0.len() as u32;
        let is_repo_archived = github_repo.as_ref().is_some_and(|r| r.archived);
        // can't be too harsh on repo verification, because it may not be available on the first indexing of the first release
        let crates_share_of_the_repo = if has_verified_repository_link { 1.0 } else { 0.4 }
            * if !has_parent { 1.0 } else { 0.75 }
            * if !is_repo_archived { 1.0 } else { 0.25 }; // if the repo is closed, let's not reward for its stats

        let langs = k.language_stats();
        let (rust_code_lines, rust_comment_lines) = langs.langs.get(&udedokei::Language::Rust).map(|rs| (rs.code, rs.comments)).unwrap_or_default();
        let total_code_lines = langs.langs.iter().filter(|(k, _)| k.is_code()).map(|(_, l)| l.code).sum::<u32>();
        let authors: Vec<_> = k.authors().collect();
        let has_badges = manifest.badges.appveyor.is_some() ||
                manifest.badges.circle_ci.is_some() ||
                manifest.badges.gitlab.is_some() ||
                manifest.badges.codecov.is_some() ||
                manifest.badges.coveralls.is_some();
        let base_score = ranking::crate_score_version(&CrateVersionInputs {
            versions,
            description: k.description().unwrap_or(""),
            readme: k.readme().map(|readme| {
                renderer.page_node(&readme.markup, &LinksContext {
                    base_url: None,
                    nofollow: Links::Ugc,
                    own_crate_name: Some(k.short_name()),
                    link_own_crate_to_crates_io: false,
                    link_fixer: None,
                }, false, image_deadline)
            }).as_ref(),
            owners: &owners,
            authors: &authors,
            contributors: Some(contributors_count),

            open_issues_count: is_repo_archived.then_some(0).or(github_repo.as_ref().and_then(|r| r.open_issues_count)),
            stargazers_count: github_repo.as_ref().map(|r| r.stargazers_count),
            repo_is_forked: github_repo.as_ref().is_some_and(|r| r.fork),
            crates_share_of_the_repo,
            release_contributors,

            edition: package.edition(),
            total_code_lines,
            rust_code_lines,
            rust_comment_lines,
            is_app: k.is_app(),
            has_build_rs: k.has_buildrs(),
            has_links: package.links().is_some(),
            has_documentation_link: k.documentation().is_some(),
            has_homepage_link: k.homepage().is_some(),
            has_repository_link: k.repository().is_some(),
            has_verified_repository_link,
            has_keywords: !package.keywords().is_empty(),
            has_own_categories: !package.categories().is_empty(),
            has_features: !manifest.features.is_empty(),
            has_code_of_conduct: k.has_code_of_conduct(),
            has_changelog,
            license: package.license().unwrap_or(""),
            has_badges,
            maintenance: k.maintenance(),
            is_nightly: k.is_nightly(),
            clippy_fail_score: clippy_fail_score(crates, origin),
        });

        let runtime = manifest.dependencies.iter().map(|(k, d)| (k.as_str(), d, false))
            .chain(manifest.target.values().flat_map(|deps| deps.dependencies.iter().map(|(k, d)| (k.as_str(), d, true))));
        let build = manifest.build_dependencies.iter().map(|(k, d)| (k.as_str(), d, false))
            .chain(manifest.target.values().flat_map(|deps| deps.build_dependencies.iter().map(|(k, d)| (k.as_str(), d, true))));

        // outdated dev deps don't matter
        let dependency_freshness = join_all(runtime.chain(build).filter_map(move |(dep_key, dep, target_specific)| {
                if let Dependency::Inherited(_) = dep {
                    return None;
                }
                if !dep.is_crates_io() {
                    return None;
                }
                let req = dep.req().parse().ok()?;
                let origin = Origin::try_from_crates_io_name(dep.package().unwrap_or(dep_key))?;
                Some(async move {
                    let pop = run_timeout("depf", 15, Box::pin(is_send(crates.version_popularity(&origin, &req, stats)))).await
                        .map_err(|e| log::error!("ver1pop {e:?}")).unwrap_or(None);
                    (dep.optional() || target_specific, pop.unwrap_or(VersionPopularity { matches_latest: true, pop: 0., lost_popularity: false, deprecated: false}))
                })
            })).await
            .into_iter().map(|(is_optional, pop)| {
                if pop.matches_latest {1.0} // don't penalize pioneers
                else if is_optional {pop.pop.mul_add(0.2, 0.8)} // not a big deal when it's off
                else {pop.pop}
            })
            .collect();

        let is_in_debian = self.deblist.has(k.short_name()).map_err(|e| log::error!("debcargo check: {}", e)).unwrap_or(false);

        let owner_trust = crates.crate_owners_trust(&owners)?;
        let mut temp_inp = CrateTemporalInputs {
            owners: &owners,
            owner_trust: &owner_trust,
            traction_stats,
            versions: all.versions(),
            is_app: k.is_app(),
            is_proc_macro: k.is_proc_macro(),
            has_docs_rs,
            is_nightly: k.is_nightly(),
            downloads_per_month: downloads_per_month_or_equivalent,
            downloads_per_month_minus_most_downloaded_user: downloads_per_month_or_equivalent, // fixed below
            number_of_direct_reverse_deps: 0,
            number_of_indirect_reverse_deps: 0,
            number_of_indirect_reverse_optional_deps: 0,
            dependency_freshness,
            is_in_debian,
            has_vet_reviews,
            has_crev_reviews,
            has_vet_trust,
        };

        let mut has_many_direct_rev_deps = false;
        if let Some(deps) = stats.crates_io_full_dependents_stats_of(origin) {
            let direct_rev_deps = deps.counters.direct.all();
            if direct_rev_deps > 50 {
                has_many_direct_rev_deps = true;
            }
            let indirect_reverse_optional_deps = (deps.counters.runtime.def + deps.counters.runtime.opt)
                .max(u32::from(deps.counters.dev))
                .max(deps.counters.build.def + deps.counters.build.opt);

            temp_inp.number_of_direct_reverse_deps = direct_rev_deps;
            temp_inp.number_of_indirect_reverse_deps = deps.counters.runtime.def.max(deps.counters.build.def);
            temp_inp.number_of_indirect_reverse_optional_deps = indirect_reverse_optional_deps;
            let tmp = join_all(
                deps.rev_dep_names_default.iter().map(|n| (n, false)).chain(
                deps.rev_dep_names_optional.iter().map(|n| (n, true)))
                .filter_map(|(name, opt)| Some((Origin::try_from_crates_io_name(name)?, opt)))
                .map(|(o, opt)| async move {
                    let dl = run_timeout("dlpm", 5, Box::pin(crates.downloads_per_month(&o, true))).await.unwrap_or_default().unwrap_or_default();
                    if opt { dl / 2 } else { dl } // hyper-tls vs reqwest
                })).await;
            let biggest = tmp.into_iter().max().unwrap_or(0);
            temp_inp.downloads_per_month_minus_most_downloaded_user = downloads_per_month_or_equivalent.saturating_sub(biggest as u32);
        }

        let mut is_deprecated = is_deprecated(k);
        crates.update_deprecation_status(k.origin(), is_deprecated);
        if k.version() == "0.0.0" || k.version() == "0.0.1" {
            is_deprecated = Some("0ver");
        }

        let temp_score = ranking::crate_score_temporal(&temp_inp);
        let overall = OverallScoreInputs {
            former_glory: traction_stats.map_or(1., |t| t.former_glory),
            is_proc_macro: k.is_proc_macro(),
            is_sys: k.is_sys(),
            is_sub_component,
            is_internal: is_internal_crate && !has_many_direct_rev_deps,
            is_autopublished: is_autopublished(k),
            is_deprecated: is_deprecated.is_some() && !too_big_to_fail(k.origin()),
            is_crates_io_published: k.origin().is_crates_io(),
            is_yanked: k.is_yanked(),
            is_squatspam: ablocklist_banned || distrusted || is_squatspam(k).map(|s| debug!("spam crate {s}")).is_some(),
            // Check out https://web3isgoinggreat.com
            is_vaporware_or_ponzi_scheme: k.category_slugs().iter().any(|c| &**c == "cryptography::cryptocurrencies"),
            ablocklist_deadend: ablocklist_hidden || ablocklist_banned,
            ablocklist_redirect,
            traction_stats,
            is_unmaintained: is_unmaintained || is_repo_archived,
        };

        debug!("score {} {base_score:?} {temp_score:?} {overall:?}", k.short_name());
        let score = ranking::combined_score(&base_score, &temp_score, &overall);

        Ok((downloads_per_month, score))
    }
}

fn clippy_fail_score(kitchen_sink: &KitchenSink, origin: &Origin) -> f32 {
    // scores are typically 1-2 for trivial, 10-15 for bad stuff
    let mut max_score = 0.;
    let mut issues: Vec<_> = kitchen_sink.clippy_issues_of_crate(origin)
        .inspect_err(|e| log::error!("{origin:?} clippy: {e}"))
        .unwrap_or_default()
        .into_iter()
        .map(|(s, _)| {
            if s > max_score { max_score = s; }
            s
        })
        .collect();
    if issues.is_empty() {
        return 0.;
    }

    debug!("Clippy score max {max_score:0.2} for {origin:?}");

    // only nitpicks
    if max_score < 1.1 {
        return 0.;
    }

    // if there are only nitpicks, ignore most of them
    // if there are serious issues, let them all add up
    let max_items = 1 + (max_score * 3.).ceil() as usize;

    pick_top_n_unstable_by(&mut issues, max_items, |a, b| b.total_cmp(a));
    let sum = issues.into_iter().sum::<f32>();

    if sum > 10. {
        info!("Clippy sum {sum:0.1} for {origin:?} ({max_items} items, worst {max_score:0.2})");
    }

    sum.min(150.)/150.
}

fn data_for_contributor_experience(latest_release_date: DateTime<Utc>, contrib_info: &HashMap<u32, CrateAuthor>, crates: &KitchenSink) -> Vec<ExperienceAtReleaseDate> {
    let release_age_frac = Utc::now().signed_duration_since(latest_release_date).num_days().max(0) as f64 / (365. * 2.);
    let trust_relevance = 1. - release_age_frac.min(1.);

    let release_contributors: Vec<_> = contrib_info.iter().filter_map(|(gh_id, c)| {
        if !c.owner && c.contribution == 0. {
            return None;
        }
        // if added as an owner after the code has been published,
        // they couldn't have contributed to the released crate
        if c.invited_at.is_some_and(|invited| invited > latest_release_date) {
            return None;
        }
        // for crate ranking actual owners are more important; cap because contribs can vary by many orders of magnitude
        let relative_contrib = (if c.owner { c.contribution + 1. } else { c.contribution / 4. }).sqrt().min(10.);
        let trust = c.trust.unwrap_or(0.) * trust_relevance;

        let account_created = c.github.as_ref()
            .and_then(|g| DateTime::parse_from_rfc3339(g.created_at.as_deref()?).ok());

        debug!("contributor {gh_id}: {} c={relative_contrib:0.2} trust={trust:0.2} gh={account_created:?}", c.name());

        Some((*gh_id, relative_contrib, account_created, trust))
    })
    .filter_map(|(id, contrib_weight, joined_github, trust)| {
        let earliest_crate = crates.all_crates_of_github_id(id).ok()?
            .iter().map(|k| k.invited_at.unwrap_or(k.oldest_release)).min()?;

        // experience at time of publishing the crate
        let rust_age = latest_release_date.signed_duration_since(earliest_crate).num_days().max(0) as u32;
        let gh_age = joined_github.map(|j| latest_release_date.signed_duration_since(j).num_days().max(0) as u32).unwrap_or(rust_age);

        debug!("contributor {id}: gh={gh_age}d cio={rust_age}d as of {latest_release_date}");

        Some(ExperienceAtReleaseDate { rust_age, gh_age, contrib_weight, trust })
    })
    .collect();
    release_contributors
}

/// The premature deprecatio of the YAML crates made a mess.
/// Keep them anyway, because they're still the best option.
fn too_big_to_fail(origin: &Origin) -> bool {
    matches!(origin, Origin::CratesIo(name) if name == "serde_yaml" || name == "yaml-rust")
}

fn print_res<T>(res: Result<T, anyhow::Error>) {
    if let Err(e) = res {
        let s = e.to_string();
        if s.starts_with("Too many open files") {
            stop();
            panic!("{}", s);
        }
        log::error!("••• Reindex Error: {}", s);
        for c in e.chain().skip(1) {
            let s = c.to_string();
            log::error!("•   error: -- {}", s);
            if s.starts_with("Too many open files") {
                stop();
                panic!("{}", s);
            }
        }
    }
}

async fn run_timeout<T, E>(label: &str, secs: u64, fut: Pin<Box<dyn Future<Output = Result<T, E>> + Send + '_>>) -> Result<T, anyhow::Error>
    where anyhow::Error: From<E> {
    let res = tokio::time::timeout(Duration::from_secs(secs), blocking::awatch(label, fut)).await.map_err(move |_| anyhow!("{label} timed out {secs}"))?;
    res.map_err(From::from)
}

#[inline(always)]
fn is_send<T: Send>(t: T) -> T {t}
