use crate::{Author, Markup, Origin, Readme, SmolStr};
use util::FxHashMap as HashMap;
pub use cargo_toml::{Dependency, DepsSet, FeatureSet, MaintenanceStatus, Resolver, TargetDepsSet};
use cargo_toml::{Manifest, OptionalFile};
use either::Either;
use manifest::{is_no_std_feature, is_no_std_keyword, IdentifiedBinFile};
pub use manifest::{Detail, DetailedFeatures};
pub use parse_cfg::{Cfg, Target};
use repo_url::Repo;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use std::cmp::Reverse;
use std::collections::{BTreeMap, BTreeSet};
use std::iter::FusedIterator;
use udedokei::synscrape::{FeaturesEnableItems, TextFromSourceCode};
use util::{CowAscii, PushString};

#[derive(Debug, Clone)]
pub struct RichCrateVersion {
    origin: Origin,
    derived: Derived,
    repo: Option<Repo>,

    is_proc_macro: bool,
    has_bin: bool,

    package_name: SmolStr,
    package_description: Option<String>,
    package_homepage: Option<String>,
    package_version: SmolStr,
    package_authors: Vec<String>,
    package_documentation: Option<String>,
    package_links: Option<SmolStr>,
    package_license: Option<SmolStr>,
    package_build: Option<OptionalFile>,
    bin_names: Vec<Option<String>>,
    lib_name: Option<SmolStr>,
    has_lib_section: bool,
    is_no_std_category: bool,
    external_ffi_bindings: bool,
    maintenance: MaintenanceStatus,
}

pub struct RichCrateRepoInfo<'a> {
    pub repo: &'a Repo,
    pub exact_url: Option<String>,
    pub project_url: String,
}

/// Anti-DoS
const MAX_DEPS: usize = 1024;

/// Data for a specific version of a crate.
///
/// Crates.rs uses this only for the latest version of a crate.
impl RichCrateVersion {
    pub fn new(origin: Origin, manifest: Manifest, derived: Derived) -> Self {
        let is_proc_macro = manifest.is_proc_macro();
        let has_bin = manifest.has_bin();
        let package = manifest.package.unwrap();
        if let Origin::GitHub { .. } = &origin {
            debug_assert!(package.repository().is_some());
        }
        let s = Self {
            origin,
            repo: package.repository().and_then(|r| Repo::new(r).ok()),
            derived,
            is_no_std_category: package.categories().iter().any(|c| c == "no-std" || c == "no-std::no-alloc") ||
                package.keywords().iter().any(|c| is_no_std_keyword(c)) ||
                manifest.features.keys().any(|k| is_no_std_feature(k)),
            external_ffi_bindings: package.categories().iter().any(|c| c == "external-ffi-bindings"),
            is_proc_macro,
            has_bin,
            bin_names: manifest.bin.into_iter().map(|bin| bin.name).collect(),
            has_lib_section: manifest.lib.is_some(),
            lib_name: manifest.lib.and_then(|l| l.name).map(SmolStr::from),
            package_name: package.name.into(),
            package_version: package.version.unwrap().into(),
            package_description: package.description.map(|inh| inh.unwrap()),
            package_homepage: package.homepage.map(|inh| inh.unwrap()),
            package_authors: package.authors.unwrap(),
            package_documentation: package.documentation.map(|inh| inh.unwrap()),
            package_license: package.license.map(|inh| inh.unwrap()).map(SmolStr::from),
            package_build: package.build,
            package_links: package.links.map(SmolStr::from),
            maintenance: manifest.badges.maintenance.status,
        };
        if let Origin::GitHub { .. } = &s.origin {
            debug_assert!(s.repo.is_some());
        }
        s
    }

    #[inline]
    #[must_use] pub fn homepage(&self) -> Option<&str> {
        self.package_homepage.as_deref()
    }

    #[must_use] pub fn documentation(&self) -> Option<&str> {
        self.package_documentation.as_deref()
    }

    /// Finds preferred capitalization for the name
    #[must_use] pub fn capitalized_name(&self) -> &str {
        &self.derived.capitalized_name
    }

    #[must_use] pub fn category_slugs(&self) -> &[SmolStr] {
        &self.derived.categories
    }

    #[must_use] pub fn license(&self) -> Option<&str> {
        self.package_license.as_deref()
    }

    /// Either original keywords or guessed ones
    #[must_use] pub fn keywords(&self) -> &[SmolStr] {
        &self.derived.keywords
    }

    /// Globally unique URL-like string identifying source & the crate within that source
    #[inline]
    #[must_use] pub fn origin(&self) -> &Origin {
        &self.origin
    }

    #[must_use] pub fn docs_rs_url(&self) -> Option<String> {
        Some(format!("https://docs.rs/{}/{}/{}", self.short_name(), self.version(), self.short_name()))
    }

    /// Readable name
    #[inline]
    #[must_use] pub fn short_name(&self) -> &str {
        &self.package_name
    }

    /// Without trailing '.' to match website's style
    #[must_use] pub fn description(&self) -> Option<&str> {
        let d = self.package_description.as_deref()
            .filter(|&d| d != "description")? // win crates
            .trim();

        if d.contains(". ") { // multiple sentences, leave them alone
            return Some(d)
        }
        Some(d.trim_end_matches('.'))
            .filter(|&d| d != self.short_name()) // spams
    }

    /// Only explicitly-specified authors
    #[must_use]
    pub fn authors(&self) -> impl FusedIterator<Item = Author> + '_ {
        match self.package_authors.as_slice() {
            [one] => Either::Left(one.split(',')), // common mistake to use comma-separated string
            rest => Either::Right(rest.iter().map(|s| s.as_str())),
        }.map(Author::new)
    }

    #[inline]
    #[must_use] pub fn repository(&self) -> Option<&Repo> {
        self.repo.as_ref()
    }

    #[inline]
    #[must_use] pub fn repository_vcs_info(&self) -> (Option<&str>, Option<&[u8; 20]>) {
        (self.derived.path_in_repo.as_deref(), self.derived.vcs_info_git_sha1.as_ref())
    }

    #[must_use]
    pub fn file_in_http_repo_url(&self, file_name: &str) -> Option<String> {
        self.repository().map(|repo| {
            let relpath = self.derived.path_in_repo.as_deref().unwrap_or("").trim_end_matches('/');
            let file_rel_path = format!("{relpath}{}{file_name}", if !relpath.is_empty() { "/" } else { "" });

            let sha1 = self.derived.vcs_info_git_sha1.filter(|_| !self.derived.canonical_http_url_sha1_broken);
            repo.canonical_http_url_sha1(&file_rel_path, sha1)
        })
    }

    #[must_use]
    pub fn repository_http_url(&self) -> Option<RichCrateRepoInfo<'_>> {
        self.repository().map(|repo| {
            let relpath = self.derived.path_in_repo.as_deref().unwrap_or("");
            // URLs with sha1 are ugly, so use them only if we know the sha1 is valid.
            let sha1 = self.derived.vcs_info_git_sha1.filter(|_| !self.derived.canonical_http_url_sha1_broken);
            let project_url = repo.canonical_http_url("", None);
            let exact_url = if !relpath.is_empty() || sha1.is_some() {
                Some(repo.canonical_http_url_sha1(relpath, sha1)).filter(|u| u != &project_url)
            } else { None };
            RichCrateRepoInfo {
                repo,
                exact_url,
                project_url,
            }
        })
    }

    #[must_use]
    pub fn repository_http_url_at_sha1(&self, sha1: &[u8; 20]) -> Option<String> {
        self.repository().map(|repo| {
            let relpath = self.derived.path_in_repo.as_deref().unwrap_or("");
            repo.canonical_http_url_sha1(relpath, Some(*sha1))
        })
    }

    /// The path may be from `vcs_info` (not trusted)
    #[must_use] pub fn has_path_in_repo(&self) -> bool {
        self.derived.path_in_repo.is_some()
    }

    #[must_use] pub fn readme(&self) -> Option<&Readme> {
        self.derived.readme.as_ref()
    }

    #[must_use] pub fn readme_was_missing(&self) -> bool {
        self.derived.readme_was_missing
    }

    /// Contents of the `src/lib.rs` from the crate, if available
    #[must_use] pub fn lib_file(&self) -> Option<&str> {
        self.derived.lib_file.as_deref()
    }

    /// Contents of the `src/main.rs` from the crate, if available
    #[must_use] pub fn bin_file(&self) -> Option<&str> {
        self.derived.bin_file.as_deref()
    }

    #[must_use]
    pub fn lib_file_markdown(&self) -> Option<Markup> {
        self.derived.lib_file.as_ref().and_then(|code| {
            let out = extract_doc_comments(code);
            if !out.trim_start().is_empty() {
                Some(Markup::Markdown(out))
            } else {
                None
            }
        })
    }

    #[must_use] pub fn has_buildrs(&self) -> bool {
        self.derived.has_buildrs || matches!(self.package_build, Some(OptionalFile::Path(_) | OptionalFile::Flag(true)))
    }

    #[must_use] pub fn has_code_of_conduct(&self) -> bool {
        self.derived.has_code_of_conduct
    }

    #[must_use] pub fn maintenance(&self) -> MaintenanceStatus {
        self.maintenance
    }

    #[inline]
    #[must_use] pub fn version(&self) -> &str {
        &self.package_version
    }

    pub fn version_semver(&self) -> Result<semver::Version, semver::Error> {
        semver::Version::parse(self.version())
    }

    #[must_use] pub fn is_yanked(&self) -> bool {
        self.derived.is_yanked
    }

    #[must_use] pub fn is_spam(&self) -> bool {
        self.derived.spam
    }

    #[must_use] pub fn is_hidden(&self) -> bool {
        self.derived.hidden
    }

    /// With comments
    #[must_use] pub fn detailed_features(&self) -> Option<&DetailedFeatures> {
        self.derived.features_comments.as_ref()
    }

    /// Feature to item mapping
    #[must_use] pub fn features_enable_items(&self) -> &FeaturesEnableItems {
        &self.derived.features_enable_items
    }

    #[must_use] pub fn lib_name(&self) -> Cow<'_, str> {
        self.lib_name.as_deref()
            .map(Cow::Borrowed)
            .unwrap_or_else(move || {
                self.short_name().as_ascii_normalized(|ch| if ch == b'-' { b'_'} else { ch })
            })
    }

    #[must_use] pub fn has_lib(&self) -> bool {
        !self.is_proc_macro && (self.derived.lib_file.is_some() || self.has_lib_section)
    }

    #[must_use] pub fn has_bin(&self) -> bool {
        !self.bin_names.is_empty()
    }

    #[must_use] pub fn bin_names(&self) -> &[Option<String>] {
        &self.bin_names
    }

    #[must_use] pub fn is_proc_macro(&self) -> bool {
        self.is_proc_macro
    }

    #[must_use] pub fn is_app(&self) -> bool {
        self.has_bin && !self.is_proc_macro && !self.has_lib()
    }

    /// Does it use nightly-only features
    #[must_use] pub fn is_nightly(&self) -> bool {
        self.derived.is_nightly
    }

    #[must_use] pub fn is_no_std(&self) -> bool {
        self.is_no_std_category
    }

    #[must_use] pub fn is_sys(&self) -> bool {
        check_is_sys(!self.bin_names.is_empty(), self.is_proc_macro(), self.has_buildrs(), self.package_links.is_some(), &self.package_name, self.external_ffi_bindings)
    }

    #[must_use] pub fn language_stats(&self) -> &udedokei::Stats {
        &self.derived.language_stats
    }

    /// compressed (whole tarball) and decompressed (extracted files only)
    #[inline]
    #[must_use] pub fn crate_size(&self) -> (u64, u64) {
        (u64::from(self.derived.crate_compressed_size), u64::from(self.derived.crate_decompressed_size))
    }
}

pub trait ManifestExt {
    fn direct_dependencies(&self) -> (Vec<RichDep>, Vec<RichDep>, Vec<RichDep>);
    fn has_bin(&self) -> bool;
    fn has_cargo_bin(&self) -> bool;
    fn is_proc_macro(&self) -> bool;
    fn license_name(&self) -> Option<&str>;
    fn is_sys(&self, has_buildrs: bool) -> bool;
}

impl ManifestExt for Manifest {
    fn license_name(&self) -> Option<&str> {
        self.package().license().map(|s| match s {
            "" => "(unspecified)",
            "MIT OR Apache-2.0" | "MIT/Apache-2.0" | "MIT / Apache-2.0" => "MIT/Apache",
            "Apache-2.0/ISC/MIT" => "MIT/Apache/ISC",
            "BSD-3-Clause AND Zlib" => "BSD+Zlib",
            "CC0-1.0" => "CC0",
            s => s,
        })
    }

    fn is_proc_macro(&self) -> bool {
        self.lib.as_ref().is_some_and(|lib| lib.proc_macro)
    }

    fn has_bin(&self) -> bool {
        !self.bin.is_empty()
    }

    fn has_cargo_bin(&self) -> bool {
        // we get binaries normalized, so no need to check for package name
        self.bin.iter().any(|b| b.name.as_ref().is_some_and(|n| n.starts_with("cargo-")))
    }

    fn is_sys(&self, has_buildrs: bool) -> bool {
        let p = self.package();
        let ext = p.categories().iter().any(|c| c == "external-ffi-bindings");
        check_is_sys(self.has_bin(), self.is_proc_macro(), has_buildrs, p.links.is_some(), &p.name, ext)
    }

    /// run dev build
    fn direct_dependencies(&self) -> (Vec<RichDep>, Vec<RichDep>, Vec<RichDep>) {
        let ft = cargo_toml::features::Resolver::<util::FxBuildHasher>::new_with_hasher_and_filter(&|_| false).parse(self);

        let (all_default_features, all_default_deps) = ft.features.get("default").map(|f| f.enables_recursive(&ft.features)).unwrap_or_default();

        let mut all_deps_enabled_by_features_shallow = HashMap::default();
        ft.features.values().for_each(|feature| {
            feature.enables_deps.iter().for_each(|(&dep_key, action)| {
                // implied optional deps are features for themselves, but that is usually unintened and looks repetitive
                let key = if feature.enabled_by.is_empty() || (feature.explicit && !feature.key.starts_with('_')) {
                    feature.key
                } else {
                    // try to find a default-enabled feature first, since that may look more relevant
                    feature.enabled_by.iter().copied().find(|&f| !f.starts_with('_') && all_default_features.contains_key(f))
                        .unwrap_or_else(|| feature.enabled_by.iter().copied().next().unwrap())
                };
                all_deps_enabled_by_features_shallow.entry(dep_key)
                    .or_insert_with(Vec::new)
                    .push((key, action));
            });
        });

        fn new_dep(dep_key: &str, dep: &Dependency, target: Option<Target<SmolStr>>) -> RichDep {
            RichDep {
                display_as_optional: false,
                enabled_by_default: false,
                target_specific: target.is_some(),
                package: dep.package().unwrap_or(dep_key).into(),
                user_alias: dep_key.into(),
                dep: dep.clone(),
                only_for_features: Default::default(),
                only_for_targets: target.into_iter().collect(),
                with_features: Default::default(),
            }
        }

        let mut normal: BTreeMap<SmolStr, RichDep> = self.dependencies.iter().take(MAX_DEPS).map(|(k, v)| (k.into(), new_dep(k, v, None))).collect();
        let mut build: BTreeMap<SmolStr, RichDep> = self.build_dependencies.iter().take(MAX_DEPS).map(|(k, v)| (k.into(), new_dep(k, v, None))).collect();
        let mut dev: BTreeMap<SmolStr, RichDep> = self.dev_dependencies.iter().take(MAX_DEPS).map(|(k, v)| (k.into(), new_dep(k, v, None))).collect();

        fn add_targets(dest: &mut BTreeMap<SmolStr, RichDep>, src: &DepsSet, target: &str) {
            if dest.len() > MAX_DEPS { return; }

            let target = Target::<SmolStr>::parse_generic(target).unwrap_or_else(|_| {
                Target::Cfg(Cfg::Is(target.into()))
            });

            for (dep_key, dep) in src.iter().take(MAX_DEPS) {
                dest.entry(dep_key.as_str().into())
                    .and_modify(|d| {
                        // otherwise don't add platform info to existing cross-platform deps
                        if !d.only_for_targets.is_empty() {
                            d.only_for_targets.push(target.clone());
                        }
                    })
                    .or_insert_with(|| new_dep(dep_key, dep, Some(target.clone())));
            }
        }

        for (target_str, plat) in self.target.iter().take(MAX_DEPS) {
            add_targets(&mut normal, &plat.dependencies, target_str);
            add_targets(&mut build, &plat.build_dependencies, target_str);
            add_targets(&mut dev, &plat.dev_dependencies, target_str);
        }

        let mut feature_use_count = HashMap::default();

        for (dep_key, rdep) in [&mut normal, &mut build, &mut dev].into_iter().flatten().take(MAX_DEPS) {
            let dep_key = dep_key.as_str();

            let by_default = all_default_deps.get(dep_key).map(|v| v.as_slice()).unwrap_or_default();
            let is_target_specific = rdep.only_for_targets.iter().any(|t| match t {
                Target::Cfg(Cfg::Not(_)) => false, // not(wasm) or not(windows) is still default for everything else
                _ => true,
            });
            rdep.target_specific = is_target_specific;

            let is_dep_enabled_by_default = !is_target_specific && (!rdep.dep.optional() || by_default.iter().any(|(_, a)| !a.is_conditional));

            if is_dep_enabled_by_default {
                rdep.enabled_by_default = true;
            }

            // Display dep features that are always enabled + dep features from default-enabled features,
            // so this is the whole set that users expect to get by default
            rdep.with_features.extend(rdep.dep.req_features().iter().map(|s| s.as_str())
                .chain(by_default.iter().flat_map(|(_, a)| a.dep_features.iter().map(|s| &**s)))
                .map(SmolStr::from));

            if rdep.dep.optional() {
                if let Some(only_for) = all_deps_enabled_by_features_shallow.get(dep_key) {
                    for &(feature_key, action) in only_for {
                        // is_conditional conflates things a bit, because UI will show the feature is not enabled, but it's the dep that is maybe-not enabled
                        let is_feature_enabled_by_default = ((!action.is_conditional && !action.is_dep_only) || is_dep_enabled_by_default) &&
                            all_default_features.contains_key(feature_key);
                        rdep.only_for_features.entry(feature_key.into())
                            .and_modify(|old| *old |= is_feature_enabled_by_default)
                            .or_insert(is_feature_enabled_by_default);

                        // if it's an optional and disabled dep, then show all dep features that will be used if it's enabled
                        if !is_dep_enabled_by_default {
                            rdep.with_features.extend(action.dep_features.iter().map(|s| SmolStr::from(&**s)));
                        }
                    }
                }
            }

            // it is visibly enabled by default by other default feature, so hide extra "default" text
            if rdep.only_for_features.iter().filter(|(k, _)| *k != "default").any(|(_, &on)| on) {
                rdep.only_for_features.remove("default");
            }

            for f in rdep.only_for_features.keys() {
                *feature_use_count.entry(f.clone()).or_insert(0u32) += 1;
            }
        }

        // if there's only one serde using serde feature, make it "[optional] serde", not "[serde] serde"
        for (_, rdep) in [&mut normal, &mut build, &mut dev].into_iter().flatten().take(MAX_DEPS) {
            if rdep.dep.optional() && rdep.only_for_features.len() == 1 && rdep.user_alias == rdep.package {
                let (f, &on) = rdep.only_for_features.iter().next().unwrap();
                if *f == rdep.package && on == rdep.enabled_by_default && feature_use_count.get(f.as_str()).copied().unwrap_or(0) == 1 {
                    rdep.display_as_optional = true;
                }
            }
        }

        // Don't display deps twice if they're required anyway
        for (dep_key, normal_dep) in &mut normal {
            if normal_dep.enabled_by_default {
                if let Some(mut build) = build.remove(dep_key.as_str()) {
                    // merge the data a bit
                    if build.only_for_targets.is_empty() {
                        normal_dep.with_features.append(&mut build.with_features);
                    }
                }
            }
            // dev deps are completely unimportant, and omit for any excuse
            dev.remove(dep_key.as_str());
        }
        for dep_key in build.keys() {
            dev.remove(dep_key.as_str());
        }

        fn convsort(deps: BTreeMap<SmolStr, RichDep>) -> Vec<RichDep> {
            fn flatten_target(t: &Target<SmolStr>) -> &[Cfg<SmolStr>] {
                match t {
                    Target::Cfg(Cfg::All(t) | Cfg::Any(t)) => t.as_slice(),
                    Target::Cfg(Cfg::Not(t)) => std::slice::from_ref(&**t),
                    Target::Cfg(t) => std::slice::from_ref(t),
                    Target::Triple{ .. } => &[],
                }
            }
            fn stringy_cfg(t: &Cfg<SmolStr>) -> &str {
                match t {
                    Cfg::Any(c) | Cfg::All(c) => c.get(0).map(stringy_cfg).unwrap_or(""),
                    Cfg::Not(c) => stringy_cfg(c),
                    Cfg::Equal(_, c) | Cfg::Is(c) => c,
                }
            }

            let num_enabled = deps.values().filter(|d| d.enabled_by_default).count();
            let use_enabled_by_default = deps.len() > 8 && num_enabled > deps.len() / 3 && num_enabled < deps.len() * 7 / 8;

            let mut deps: Vec<_> = deps.into_values().collect();
            let mut group_counts = HashMap::<_, (_, _, _)>::default();
            if deps.len() > 4 {
                deps.iter().for_each(|dep| {
                    let show_first = use_enabled_by_default && dep.enabled_by_default && (dep.only_for_features.len() + dep.only_for_targets.len()) < 5;
                    let w = if dep.only_for_features.len() > 2 { 3_u32 } else { 4 };
                    dep.only_for_features.keys().map(move |f| (w, f.as_str()))
                        .chain(dep.only_for_targets.iter().take(if dep.target_specific { 5 } else { 0 }).flat_map(flatten_target).map(stringy_cfg).map(|s| (2, s)))
                        .take(8).for_each(|(w, k)| {
                            group_counts.entry(SmolStr::from(k))
                                .and_modify(|e| {
                                    e.0 += w;
                                    if dep.package < e.1 { e.1 = dep.package.clone(); }
                                    if show_first { e.2 = true; }
                                })
                                .or_insert_with(|| (w, dep.package.clone(), show_first));
                        });
                });
                group_counts.retain(|_, (c, ..)| {
                    // too-big groups are not that useful, because items within groups don't get secondary grouping
                    let limit = 30.min(2 * deps.len() as u32);
                    if *c > limit {
                        *c -= (*c - limit) / 2;
                    }
                    *c > 4
                });
            }

            deps.sort_by_cached_key(|dep| {
                let group_by = dep.only_for_features.keys().map(|f| f.as_str())
                    .chain(dep.only_for_targets.iter().take(if dep.target_specific { 5 } else { 0 }).flat_map(flatten_target).map(stringy_cfg))
                    .filter_map(|k| group_counts.get_key_value(k))
                    // biggest group, but prefer keys sorted earlier, becaue that's the leftmost feature label in the list
                    .max_by_key(|&(k, &(c, _, _))| (c, Reverse(k.as_str())))
                    // put group where it's first package is
                    .map(|(key, (c, package, b))| (!*b, package.clone(), Reverse(*c), key.as_str()))
                    // prefer enabled_by_default first
                    .unwrap_or_else(|| {
                        let show_first = use_enabled_by_default && dep.enabled_by_default && (dep.only_for_features.len() + dep.only_for_targets.len()) < 5;
                        (!show_first, dep.package.clone(), Reverse(!0), "")
                    });
                (group_by, dep.user_alias.clone())
            });
            deps
        }
        (convsort(normal), convsort(dev), convsort(build))
    }
}

fn check_is_sys(has_bin: bool, is_proc_macro: bool, has_buildrs: bool, has_links: bool, name: &str, external_ffi_bindings: bool) -> bool {
    !has_bin &&
        has_buildrs &&
        !is_proc_macro &&
        (has_links ||
            (
                external_ffi_bindings ||
                name.ends_with("-sys") || name.ends_with("-src") || name.ends_with("_sys")
                // _dll suffix is a false positive
            ))
}

pub struct RichDep {
    pub enabled_by_default: bool,
    pub target_specific: bool,
    /// Don't show `only_for_features`
    pub display_as_optional: bool,
    pub package: SmolStr,
    pub user_alias: SmolStr,
    pub dep: Dependency,
    /// it's optional, used only for a platform
    pub only_for_targets: Vec<Target<SmolStr>>,
    /// it's optional, used only if parent crate's feature is enabled. The boolean says whether the feature itself is enabled by default.
    pub only_for_features: BTreeMap<SmolStr, bool>,
    /// When used, these features of this dependency are enabled
    pub with_features: BTreeSet<SmolStr>,
}

impl RichDep {
    #[must_use]
    pub fn origin(&self) -> Origin {
        if let Some(repo) = self.dep.git().and_then(|git| Repo::new(git).ok()).and_then(|r| Origin::from_repo(&r, &self.package)) {
            repo
        } else {
            Origin::from_crates_io_name(&self.package)
        }
    }

    pub fn for_features(&self) -> impl Iterator<Item = (&str, bool)> + '_ {
        self.only_for_features.iter().map(|(k, v)| (&**k, *v))
    }

    #[must_use] pub fn is_optional(&self) -> bool {
        !self.only_for_features.is_empty()
    }
}

/// Metadata guessed
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Derived {
    pub categories: Vec<SmolStr>,
    pub keywords: Vec<SmolStr>,
    pub path_in_repo: Option<String>,
    pub language_stats: udedokei::Stats,
    pub crate_compressed_size: u32,
    pub crate_decompressed_size: u32,
    pub is_nightly: bool,
    pub capitalized_name: String,
    pub readme: Option<Readme>,
    pub lib_file: Option<String>,
    pub has_buildrs: bool,
    pub has_code_of_conduct: bool,
    pub is_yanked: bool,
    #[serde(default)]
    pub bin_file: Option<String>,
    #[serde(default)]
    pub vcs_info_git_sha1: Option<[u8; 20]>,

    #[serde(default)]
    pub hidden: bool,
    #[serde(default)]
    pub spam: bool,
    /// If true, the readme may be scraped from root of the repo,
    /// and not be specific to this crate.
    #[serde(default)]
    pub readme_was_missing: bool,

    #[serde(default)]
    pub canonical_http_url_sha1_broken: bool,

    #[serde(default)]
    pub features_comments: Option<DetailedFeatures>,
    #[serde(default)]
    pub features_enable_items: FeaturesEnableItems,
}

/// Metadata guessed
#[derive(Debug, Clone, Default)]
pub struct CrateVersionSourceData {
    pub github_keywords: Option<Vec<String>>,
    pub github_description: Option<String>,
    pub language_stats: udedokei::Stats,
    pub crate_compressed_size: u32,
    pub crate_decompressed_size: u32,
    pub is_nightly: bool,
    pub capitalized_name: String,
    pub readme: Option<Readme>,
    pub lib_file: Option<String>,
    /// src/main.rs
    pub bin_file: Option<String>,
    pub path_in_repo: Option<String>,
    pub vcs_info_git_sha1: Option<[u8; 20]>,
    pub has_buildrs: bool,
    pub has_code_of_conduct: bool,
    pub is_yanked: bool,
    pub readme_was_missing: bool,
    pub canonical_http_url_sha1_broken: bool,
    /// names (idents) and doctext weighed
    pub text_from_source_code: TextFromSourceCode,

    pub features_comments: Option<DetailedFeatures>,
    pub features_enable_items: FeaturesEnableItems,
    pub suspicious_binary_files: Vec<IdentifiedBinFile>,
}

#[must_use]
pub fn extract_doc_comments(code: &str) -> String {
    let mut out = String::with_capacity(code.len() / 2);
    let mut is_in_block_mode = false;
    for l in code.lines() {
        let l = l.trim_start();
        if is_in_block_mode {
            if let Some(offset) = l.find("*/") {
                is_in_block_mode = false;
                out.push_str(&l[0..offset]);
            } else {
                out.push_str(l);
            }
            out.push_ascii(b'\n');
        } else if l.starts_with("/*!") && !l.contains("*/") {
            is_in_block_mode = true;
            let rest = &l[3..];
            out.push_str(rest);
            if !rest.trim().is_empty() {
                out.push_ascii(b'\n');
            }
        } else if let Some(doccomment) = l.strip_prefix("//!") {
            out.push_str(doccomment);
            out.push_ascii(b'\n');
        }
    }
    out
}

#[test]
fn parse() {
    assert_eq!("hello\nworld", extract_doc_comments("/*!\nhello\nworld */").trim());
}
