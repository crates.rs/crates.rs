use util::FxHashMap as HashMap;
use categories::Synonyms;
use std::sync::LazyLock as Lazy;
use rust_stemmers::Algorithm;
use std::borrow::Cow;
use std::mem;
use std::sync::Arc;
use tantivy::tokenizer::{Token, TokenFilter, TokenStream, Tokenizer};
use util::CowAscii;

static CUSTOM_STEM: Lazy<HashMap<&'static str, &'static str>> = Lazy::new(|| HashMap::from_iter([
    ("string", "str"),

    ("internal", "internal"),
    ("internally", "internal"),

    // stemmer makes it intern too!
    ("international", "i18n"),
    ("internationally", "i18n"),
    ("internationalization", "i18n"),
    ("internationalisation", "i18n"),
    ("internationalise", "i18n"),
    ("internationalize", "i18n"),

    ("interning", "interning"),
    ("interner", "interning"),
    ("internment", "interning"),
    ("interns", "interning"),
    ("rust", "rs"),
]));

#[derive(Clone)]
pub struct CustomNormalizer {
    synonyms: Arc<Synonyms>,
}

impl CustomNormalizer {
    pub fn new(synonyms: Arc<Synonyms>) -> Self {
        Self { synonyms }
    }
}

impl TokenFilter for CustomNormalizer {
    type Tokenizer<T: Tokenizer> = CustomNormalizerFilter<T>;

    fn transform<T: Tokenizer>(self, tokenizer: T) -> CustomNormalizerFilter<T> {
        CustomNormalizerFilter {
            inner: tokenizer,
            synonyms: self.synonyms,
        }
    }
}

#[derive(Clone)]
pub struct CustomNormalizerFilter<T> {
    inner: T,
    synonyms: Arc<Synonyms>,
}

impl<T: Tokenizer> Tokenizer for CustomNormalizerFilter<T> {
    type TokenStream<'a> = CustomTokenStream<T::TokenStream<'a>>;

    fn token_stream<'a>(&'a mut self, text: &'a str) -> Self::TokenStream<'a> {
        let stemmer = rust_stemmers::Stemmer::create(Algorithm::English);
        CustomTokenStream {
            tail: self.inner.token_stream(text),
            synonyms: self.synonyms.clone(),
            stemmer,
            buffer: String::new(),
        }
    }
}

pub struct CustomTokenStream<T> {
    tail: T,
    stemmer: rust_stemmers::Stemmer,
    synonyms: Arc<Synonyms>,
    buffer: String,
}

impl<T: TokenStream> TokenStream for CustomTokenStream<T> {
    fn advance(&mut self) -> bool {
        if !self.tail.advance() {
            return false;
        }
        let token = self.tail.token_mut();
        token.text.make_ascii_lowercase();

        // make text formatted like synonyms expect (real keyword normalize supports camelcase,
        // but maybe it's not necessary here)
        let lowercased = &*token.text.as_ascii_normalized(|c| {
            if c != b'_' { c.to_ascii_lowercase() } else { b'-' }
        });

        let new_token = if let Some(&replace) = CUSTOM_STEM.get(lowercased) {
            Cow::Borrowed(replace)
        } else {
            self.stemmer.stem(self.synonyms.normalize(lowercased, 2))
        };
        if new_token != token.text {
            match new_token {
                Cow::Owned(s) => token.text = s,
                Cow::Borrowed(s) => {
                    self.buffer.clear();
                    self.buffer.push_str(s);
                    mem::swap(&mut token.text, &mut self.buffer);
                },
            }
        }
        true
    }

    fn token(&self) -> &Token {
        self.tail.token()
    }

    fn token_mut(&mut self) -> &mut Token {
        self.tail.token_mut()
    }
}
