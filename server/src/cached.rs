use actix_web::http::StatusCode;
use categories::Category;
use chrono::prelude::*;
use kitchen_sink::Origin;
use log::error;
use std::borrow::Cow;
use std::path::Path;
use std::{fs, io};


const CACHE_MAGIC_TAG: &[u8; 4] = b"  <c";


#[derive(Debug, Clone)]
pub(crate) enum Page {
    Html(StatusCode, Vec<u8>),
    Feed(StatusCode, Vec<u8>),
    Redirect(String),
}

#[derive(Debug, Clone)]
pub(crate) struct Rendered {
    pub page: Page,
    // s
    pub cache_time: u32,
    /// for purgeable urls
    pub cdn_cache: bool,
    pub refresh: bool,
    pub enc: Option<String>,
    pub last_modified: Option<DateTime<Utc>>,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub(crate) struct CachedPage<'a> {
    pub html: Cow<'a, [u8]>,
    pub last_modified: Option<DateTime<Utc>>,
    pub cache_time: Option<u32>,
    pub content_encoding: Option<String>,
}

impl Rendered {
    pub fn compress(&mut self) -> Result<(), anyhow::Error> {
        if self.enc.is_some() {
            return Ok(());
        }
        let body = match &mut self.page {
            Page::Html(_, body) | Page::Feed(_, body) => body,
            Page::Redirect(..) => return Ok(()),
        };
        let mut out = Vec::with_capacity(body.len() / 6 + 1000);
        let params = brotli::enc::BrotliEncoderParams {
            quality: 11,
            size_hint: body.len(),
            high_entropy_detection_quality: 0,
            large_window: false,
            appendable: false,
            ..Default::default()
        };
        brotli::enc::BrotliCompress(&mut body.as_slice(), &mut out, &params)?;
        log::debug!("br-compressed {} to {}", body.len(), out.len());
        *body = out;
        self.enc = Some("br".into());
        Ok(())
    }
}

impl CachedPage<'_> {
    pub fn get_cached_page(cache_file: &Path, is_acceptable: bool) -> Result<CachedPage<'static>, anyhow::Error> {
        let mut page_cached = fs::read(cache_file)?;

        if !is_acceptable || page_cached.len() <= 8 {
            let _ = fs::remove_file(cache_file); // next req will block instead of an endless refresh loop
        }
        if page_cached.len() <= 8 {
            anyhow::bail!("bad cache");
        }

        let trailer_pos = page_cached.len() - 8; // The worst data format :)
        let is_awful_format = page_cached[trailer_pos..trailer_pos + 4] == CACHE_MAGIC_TAG[..];

        if !is_awful_format {
            Ok(rmp_serde::from_slice::<CachedPage<'_>>(&page_cached).map_err(|e| {
                error!("bad cache: {e:?} {}", cache_file.display());
                let _ = fs::remove_file(cache_file);
                e
            })?)
        } else {
            let trailer_pos = page_cached.len() - 8; // The worst data format :)
            let timestamp = u32::from_le_bytes(page_cached.get(trailer_pos + 4..).unwrap().try_into().unwrap());
            let last_modified = if timestamp > 0 { Utc.timestamp_opt(i64::from(timestamp), 0).single() } else { None };
            page_cached.truncate(trailer_pos);
            Ok(CachedPage {
                html: page_cached.into(),
                last_modified,
                cache_time: None,
                content_encoding: None,
            })
        }
    }

    pub fn write_serialized_cache(&self, cache_file: &Path) -> Result<(), anyhow::Error> {
        let mut file = io::BufWriter::new(fs::File::create(cache_file)?);
        rmp_serde::encode::write(&mut file, &self).map_err(|e| {
            error!("warning: Failed writing to {}: {e}", cache_file.display());
            drop(file);
            let _ = fs::remove_file(cache_file);
            e
        })?;
        Ok(())
    }
}

pub(crate) fn cache_file_name_for_origin(origin: &Origin) -> String {
    match origin {
        Origin::CratesIo(crate_name) => {
            assert!(!crate_name.as_bytes().iter().any(|&b| b == b'/' || b == b'.'));
            format!("{crate_name}.html")
        },
        Origin::GitHub(r) | Origin::GitLab(r) => {
            assert!(!r.package.as_bytes().iter().any(|&b| b == b'/' || b == b'.'));
            let slug = if let Origin::GitHub { .. } = origin { "gh" } else { "lab" };
            format!("{slug},{},{},{}.html", r.repo.owner, r.repo.repo, r.package)
        },
    }
}



#[derive(Debug)]
pub(crate) enum Purge {
    Nothing,
    Trending,
    Category(&'static Category),
    Stats,
    Crate(Origin),
}
