use fetcher::header::{HeaderValue, AUTHORIZATION};
use fetcher::Fetcher;

pub(crate) struct Cdn {
    fetcher: Fetcher,
    purge_url: String,
}

impl Cdn {
    pub(crate) fn new(zone: &str, bearer: &str) -> Result<Self, anyhow::Error> {
        Ok(Self {
            purge_url: format!("https://api.cloudflare.com/client/v4/zones/{zone}/purge_cache"),
            fetcher: Fetcher::new_with_headers(5, [
                (AUTHORIZATION, HeaderValue::from_str(&format!("Bearer {bearer}"))?),
            ].into_iter().collect()),
        })
    }

    pub(crate) async fn purge(&self, files: &[String]) -> Result<(), anyhow::Error> {
        if files.is_empty() {
            return Ok(());
        }
        let res: Resp = self.fetcher
            .post(&self.purge_url, &Purge { files })
            .await?;
        if !res.success {
            anyhow::bail!("purge failed on {files:?}: {res:#?}");
        }
        log::debug!("purged {files:?}: {res:#?}");
        Ok(())
    }
}

#[derive(serde::Serialize)]
struct Purge<'a> {
    files: &'a [String],
}

#[allow(dead_code)]
#[derive(serde::Deserialize, Debug)]
struct Resp {
    #[serde(default)]
    pub errors: Vec<CfMsg>,
    #[serde(default)]
    pub messages: Vec<CfMsg>,
    pub success: bool,
}

#[allow(dead_code)]
#[derive(serde::Deserialize, Debug)]
struct CfMsg {
    #[serde(default)]
    pub code: i64,
    #[serde(default)]
    pub message: String,
}

#[cfg(test)]
mod tests {
    use super::Cdn;
    use std::env;

    #[tokio::test]
    async fn purge_test() {
        let c = env::var("CRATE_CDN_CREDENTIALS").ok().as_ref()
            .and_then(|c| c.trim().split_once(' ')).map(|(a,b)| Cdn::new(a, b)).transpose().unwrap();
        let Some(cdn) = c else { return };
        cdn.purge(&["https://lib.rs/new".into()]).await.unwrap();
    }
}
