use actix_web::body::BoxBody;
use actix_web::http::{header, StatusCode};
use actix_web::HttpResponse;
use kitchen_sink::stop;
use log::error;
use std::time::Duration;
use crate::renderlock::WorkInProgressError;

pub(crate) struct ServerError {
    pub err: anyhow::Error,
}

impl ServerError {
    #[cold]
    pub fn new(err: anyhow::Error) -> Self {
        for cause in err.chain() {
            let mut s = cause.to_string();
            error!("• {}", s);
            // The server is stuck and useless
            s.make_ascii_lowercase();
            if s.contains("too many open files") || s.contains("instance has previously been poisoned") ||
               s.contains("cannot allocate memory") ||
               s.contains("inconsistent park state") || s.contains("failed to allocate an alternative stack") {
                error!("Fatal error: {}", s);
                stop();
                std::thread::sleep(Duration::from_secs(1));
                std::process::exit(2);
            }
        }
        Self { err }
    }
}

impl From<anyhow::Error> for ServerError {
    #[cold]
    fn from(err: anyhow::Error) -> Self {
        Self::new(err)
    }
}

impl From<tokio::task::JoinError> for ServerError {
    #[cold]
    fn from(err: tokio::task::JoinError) -> Self {
        Self::new(err.into())
    }
}

use std::fmt;
impl fmt::Display for ServerError {
    #[cold]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.err.fmt(f)
    }
}
impl fmt::Debug for ServerError {
    #[cold]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.err.fmt(f)
    }
}
impl std::error::Error for ServerError {
}


impl actix_web::ResponseError for ServerError {
    fn status_code(&self) -> StatusCode {
        StatusCode::INTERNAL_SERVER_ERROR
    }

    #[cold]
    fn error_response(&self) -> HttpResponse<BoxBody> {
        let (mut res, title, headline) = if let Some(wip) = self.err.downcast_ref::<WorkInProgressError>() {
            let mut r = HttpResponse::Accepted(); // CDN shows error page for gateway timeout
            let jobs = wip.0;
            r.insert_header((header::REFRESH, jobs.to_string()));
            r.insert_header((header::CACHE_CONTROL, "no-cache, no-store, must-revalidate"));
            r.insert_header(("X-Robots-Tag", "noindex, nofollow"));
            (r, "Please wait…", "Loading…")
        } else {
            (HttpResponse::InternalServerError(), "error[E0500]: lib.rs:1", "Sorry, something went wrong!")
        };

        let mut page = Vec::with_capacity(20000);
        front_end::render_error(&mut page, title.into(), headline, &self.err);

        res.content_type("text/html;charset=UTF-8")
            .no_chunking(page.len() as u64)
            .body(page)
    }
}
