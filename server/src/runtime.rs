use std::fmt;
use anyhow::Context;
use futures::future::{Future, FutureExt};
use kitchen_sink::SpawnAbortOnDrop;
use std::pin::Pin;
use std::time::Duration;
use tokio::runtime::Handle;
use tokio::time::timeout;

#[inline]
pub(crate) fn run_timeout<'a, S, R, T: 'static + Send>(label: S, secs: u32, fut: R) -> Pin<Box<dyn Future<Output = Result<T, anyhow::Error>> + Send + 'a>>
where
    S: 'a + fmt::Debug + AsRef<str> + Send + fmt::Display + Clone,
    // + tracing::Value,
    R: 'static + Send + Future<Output = Result<T, anyhow::Error>>,
{
    let fut = timeout(Duration::from_secs(secs.into()), fut);
    let fut = kitchen_sink::NonBlock::new(label.clone(), fut);
    let timeout = fut.map(move |res| {
        res.with_context(|| format!("{label} timed out after >{secs}s"))?
    });
    Box::pin(timeout)
}

#[inline]
#[track_caller]
#[allow(unexpected_cfgs)]
pub(crate) fn rt_run_timeout<R, T: 'static + Send>(rt: &Handle, label: &'static str, secs: u32, fut: R) -> Pin<Box<dyn Future<Output=Result<T, anyhow::Error>> + 'static + Send>>
where
    R: 'static + Send + Future<Output = Result<T, anyhow::Error>>,
{
    let fut = kitchen_sink::NonBlock::new(label, timeout(Duration::from_secs(secs.into()), fut));
    #[cfg(tokio_unstable)]
    let task = tokio::task::Builder::new().name(label).spawn_on(fut, rt).unwrap();
    #[cfg(not(tokio_unstable))]
    let task = rt.spawn(fut);

    Box::pin(SpawnAbortOnDrop(task).map(move |res| -> Result<T, anyhow::Error> {
        res?.with_context(|| format!("{label} timed out after >{secs}s"))?
    }))
}

#[track_caller]
#[allow(unexpected_cfgs)]
#[inline]
pub(crate) fn rt_run_timeout_bg<'h, R, T: 'static + Send>(rt: &'h Handle, label: &'static str, secs: u32, fut: R) -> impl Future<Output=Result<T, anyhow::Error>> + 'static + Send + use<R, T>
where
    R: 'static + Send + Future<Output = Result<T, anyhow::Error>>,
{
    let fut = kitchen_sink::NonBlock::new(label, timeout(Duration::from_secs(secs.into()), fut));
    #[cfg(tokio_unstable)]
    let task = tokio::task::Builder::new().name(label).spawn_on(fut, rt).unwrap();
    #[cfg(not(tokio_unstable))]
    let task = rt.spawn(fut);

    task.map(move |res| -> Result<T, anyhow::Error> {
        res?.with_context(|| format!("{label} timed out after >{secs}s"))?
    })
}
