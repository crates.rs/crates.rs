use crate::periodic_new_page_refresh;
use crate::HUP_SIGNAL;
#[cfg(feature = "cap-alloc")]
use crate::MAX_MEM;
use blocking::block_in_place;
use kitchen_sink::{stop, KitchenSink, KitchenSinkErr};
use log::{error, info, warn};
use std::panic::catch_unwind;
use std::path::PathBuf;
use std::sync::atomic::{AtomicBool, AtomicU32, Ordering};
use std::sync::Arc;
use std::time::{Duration, Instant};
use crate::AServerState;

#[cfg(feature = "cap-alloc")]
use crate::ALLOCATOR;

pub(crate) fn start_watchdog_threads(state: &AServerState, enabled: &Arc<AtomicBool>, data_dir: PathBuf, github_token: String) -> tokio::task::JoinHandle<()> {
    let timestamp = Arc::new(AtomicU32::new(0));

    // refresher thread
    let watchdog = state.rt.spawn({
        let state: AServerState = state.clone();
        let timestamp = timestamp.clone();
        let enabled = enabled.clone();
        async move {
            let mut last_full_reload = Instant::now();
            let mut last_quick_update = Instant::now();
            state.rt.spawn_blocking({
                let state = state.clone();
                blocking::closure("prewarm", move || {
                    state.crates.load().prewarm();
                })
            }).await.expect("prewarm");
            info!("prewarmed initial data, starting async watchdog\nPost-prewarm mem is {}MB; churned through {}MB", ALLOCATOR.max_allocated()/1_000_000, ALLOCATOR.total_allocated()/1_000_000);

            let mut latency_failures = 0;
            loop {
                if !enabled.load(Ordering::SeqCst) {
                    info!("watchdog task end");
                    return;
                }
                tokio::time::sleep(Duration::from_secs(2)).await;

                // test that runtime hasn't deadlocked
                let ls_start = Instant::now();
                tokio::task::spawn(async {}).await.unwrap();
                tokio::task::spawn_blocking(|| {}).await.unwrap();
                let latency = ls_start.elapsed();
                if latency > Duration::from_millis(500) {
                    #[cfg(feature = "cap-alloc")]
                    let allocated = ALLOCATOR.allocated()/1_000_000;
                    #[cfg(not(feature = "cap-alloc"))]
                    let allocated = 0;

                    latency_failures += 1;
                    warn!("NOP latency is {}ms; sem free: {}bg {}fg; {}MB mem used; {latency_failures} failed in a row",
                        latency.as_millis(),
                        state.background_job.available_permits(),
                        state.foreground_job.available_permits(),
                        allocated);
                    if latency_failures == 2 {
                        eprintln!("Flushing due to async latency");
                        blocking::dump_all();
                        block_in_place("cleanupf", || state.crates.load().cleanup());
                    } else if latency_failures > 4 {
                        eprintln!("Aborting due to async latency");
                        blocking::dump_all();
                        std::process::abort();
                    }
                } else {
                    latency_failures = 0;
                }

                timestamp.store(state.start_time.elapsed().as_secs() as u32, Ordering::SeqCst);
                let should_do_full_reload = if 1 == HUP_SIGNAL.swap(0, Ordering::SeqCst) {
                    #[cfg(feature = "cap-alloc")]
                    info!("HUP! peak mem was {}MB", ALLOCATOR.max_allocated() / 1_000_000);

                    true
                } else if last_full_reload.elapsed() > Duration::from_secs(3 * 3600) {
                    info!("Reloading state on a timer (3h)");
                    true
                } else {
                    false
                };
                if should_do_full_reload {
                    // reload will take some time, this pauses watchdog for a bit
                    timestamp.store(state.start_time.elapsed().as_secs() as u32 + 60, Ordering::SeqCst);
                    match blocking::awatch("loading newksink", KitchenSink::new(&data_dir, &github_token)).await {
                        Ok(k) => {
                            #[cfg(feature = "cap-alloc")]
                            let mem_before = ALLOCATOR.allocated() as isize;
                            info!("Reloading state");
                            timestamp.store(state.start_time.elapsed().as_secs() as u32 + 60, Ordering::SeqCst);

                            block_in_place("upd_repos", || {
                                let old_crates = state.crates.load();
                                old_crates.update_repositories(); // runs git update now, so that next instance will load fresh repos
                                old_crates.cleanup();
                            });
                            let k = Arc::new(k);
                            let preload_result = state.rt.spawn_blocking({
                                let k = k.clone();
                                blocking::closure("prewarm2", move || {
                                    catch_unwind(move || {
                                        k.prewarm();
                                        info!("prewarmed; peak mem was {}MB", ALLOCATOR.max_allocated()/1_000_000);
                                    })
                                    .map_err(KitchenSinkErr::from_panic)
                                })
                            }).await.map_err(KitchenSinkErr::from).and_then(|x| x);
                            if let Err(e) = preload_result {
                                error!("Reload failed: {e}");
                                std::process::exit(5);
                            }
                            last_full_reload = Instant::now();
                            let old = state.crates.swap(k);
                            info!("Swapped state; old has {} ref", Arc::strong_count(&old));
                            drop(old);

                            #[cfg(feature = "cap-alloc")] {
                                let mem_after = ALLOCATOR.allocated() as isize;
                                info!("Reloaded state; added {}KB mem", mem_after - mem_before);
                            }
                            periodic_new_page_refresh(state.clone());
                        },
                        Err(e) => {
                            error!("Refresh failed: {e}");
                            std::process::exit(1);
                        },
                    }
                } else if last_quick_update.elapsed() > Duration::from_secs(15 * 60) {
                    info!("Periodic index update");
                    timestamp.store(state.start_time.elapsed().as_secs() as u32 + 15, Ordering::SeqCst);
                    blocking::watch("updindx", || state.crates.load().update_crate_index());
                    periodic_new_page_refresh(state.clone());
                    last_quick_update = Instant::now();
                }

                #[cfg(feature = "cap-alloc")]
                if ALLOCATOR.allocated() > MAX_MEM * 8 / 10 {
                    warn!("High memory usage {}MB; flushing caches", ALLOCATOR.allocated() / 1_000_000);
                    timestamp.store(state.start_time.elapsed().as_secs() as u32 + 15, Ordering::SeqCst);
                    block_in_place("mem-clean", || state.crates.load().cleanup());
                    if ALLOCATOR.allocated() > MAX_MEM * 8 / 10 {
                        blocking::dump_all();
                        error!("Flushing caches didn't help: {}MB used", ALLOCATOR.allocated() / 1_000_000);
                        std::process::exit(8);
                    }
                }
            }
        }
    });

    // watchdog
    std::thread::spawn({
        let state: AServerState = state.clone();
        let enabled = enabled.clone();
        move || {
        std::thread::sleep(Duration::from_secs(60)); // give startup some time
        loop {
            if !enabled.load(Ordering::SeqCst) {
                info!("watchdog thread end");
                return;
            }
            std::thread::sleep(Duration::from_secs(2));
            let expected = state.start_time.elapsed().as_secs() as u32;
            let rt_timestamp = timestamp.load(Ordering::SeqCst);
            if rt_timestamp + 8 < expected {

                if rt_timestamp + 16 == expected {
                    blocking::dump_all();
                }

                #[cfg(feature = "cap-alloc")]
                let allocated = ALLOCATOR.allocated()/1_000_000;
                #[cfg(not(feature = "cap-alloc"))]
                let allocated = 0;

                warn!("Update loop is {}s behind; sem free: {}bg {}fg; {}MB mem used", expected - rt_timestamp,
                    state.background_job.available_permits(),
                    state.foreground_job.available_permits(),
                    allocated);
                if rt_timestamp + 45 < expected {
                    error!("tokio is dead");
                    blocking::dump_all();
                    stop();
                    std::thread::sleep(Duration::from_secs(2));
                    std::process::exit(4);
                }
            }
            let response_timestamp = state.last_ok_response.load(Ordering::SeqCst);
            if response_timestamp + 60*2 < expected {
                warn!("no requests for 2 minutes? probably a deadlock");
                // don't exit in debug mode, because it's legitimately idling
                if !cfg!(debug_assertions) {
                    blocking::dump_all();
                    stop();
                    std::thread::sleep(Duration::from_secs(1));
                    std::process::exit(2);
                }
            }
        }
    }});
    watchdog
}
