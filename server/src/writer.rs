use util::error::IntoIoError;
use actix_web::web::Bytes;
use futures::channel::mpsc;
use futures::sink::SinkExt;
use futures::{Future, Stream};
use std::io;

pub struct Writer<T: 'static, E: 'static> {
    sender: mpsc::Sender<Result<T, E>>,
    rt: tokio::runtime::Handle,
}

impl<T, E: std::fmt::Display> Writer<T, E> {
    pub fn fail(&mut self, error: E) -> impl Future<Output = ()> + '_ {
        eprintln!("async write aborted: {error}");
        let sent = self.sender.send(Err(error));
        async move { let _ = sent.await; }
    }
}

impl<E: 'static> io::Write for Writer<Bytes, E>
where E: Send + Sync + 'static
{
    fn write(&mut self, d: &[u8]) -> io::Result<usize> {
        let len = d.len();
        let sent = self.sender.send(Ok(Bytes::copy_from_slice(d)));
        let _g = self.rt.enter();
        futures::executor::block_on(sent)
            .map_err(|e| io::ErrorKind::BrokenPipe.into_io_error_with(e))?;
        Ok(len)
    }

    fn write_all(&mut self, d: &[u8]) -> io::Result<()> {
        self.write(d).map(|_| ())
    }

    fn flush(&mut self) -> io::Result<()> {
        let flushed = self.sender.flush();
        let _g = self.rt.enter();
        futures::executor::block_on(flushed)
            .map_err(|e| io::ErrorKind::BrokenPipe.into_io_error_with(e))
    }
}

pub async fn writer<T: 'static, E: 'static + std::fmt::Debug>() -> (Writer<T, E>, impl Stream<Item = Result<T, E>>) {
    let rt = tokio::runtime::Handle::current();
    let (tx, rx) = mpsc::channel(3);
    let w = Writer { sender: tx, rt };
    (w, rx)
}
