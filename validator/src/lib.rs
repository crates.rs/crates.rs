use util::FxHashSet as HashSet;
use cargo_toml::Manifest;
use chrono::{DateTime, Utc};
use feat_extractor::{is_deprecated_requirement, is_squatspam};
use itertools::Itertools;
use kitchen_sink::{comparator::Report, CResult, CrateVersion, DepsStats, Edition, KitchenSink, MaintenanceStatus, RichCrate, RichCrateVersion, RichDep, VersionReq, Warning};
use log::error;
use rich_crate::ManifestExt;
use semver::{Op, Version as SemVer};
use std::collections::BTreeSet;
use util::SmolStr;

pub async fn warnings_for_crate(c: &KitchenSink, k: &RichCrateVersion, all: &RichCrate) -> CResult<(HashSet<Warning>, Option<Report>)> {
    if k.category_slugs().iter().any(|c| &**c == "cryptography::cryptocurrencies") {
        return Ok(([Warning::CryptocurrencyBS].into_iter().collect(), None));
    }

    let mut warnings = c.rich_crate_warnings(k.origin()).await?;
    let compat = c.rustc_compatibility(all).await?;

    let (tarball_byte_size, _) = k.crate_size();
    if tarball_byte_size > 10_000_000 {
        warnings.insert(Warning::Chonky(tarball_byte_size));
    }

    let sus_files = c.get_suspicious_binary_files(k.origin(), k.version())?;
    if !sus_files.is_empty() {
        let kinds = sus_files.iter().map(|f| &*f.kind).collect::<BTreeSet<_>>();
        let kinds = kinds.into_iter().join(", ");
        warnings.insert(Warning::ContainsBinaries(kinds.into()));
    }

    if let Some(reason) = is_squatspam(k) {
        warnings.retain(|w| !matches!(w, Warning::NoKeywords | Warning::NoCategories | Warning::NoRepositoryProperty));
        warnings.insert(Warning::Reserved(reason.into()));
        return Ok((warnings, None));
    }

    let bad_ver = all.versions().iter()
        .filter(|v| !v.yanked)
        .find_map(|v| match SemVer::parse(&v.num) {
            Ok(_) => None,
            Err(err) => Some((&v.num, err))
        });
    if let Some((version, err)) = bad_ver {
        warnings.insert(Warning::BadSemVer(version.as_str().into(), err.to_string().into()));
    }

    let versions = all.versions().iter().filter(|v| !v.yanked).filter_map(|v| {
        Some((v.num.parse::<SemVer>().ok()?, v))
    }).collect::<Vec<_>>();

    let mut repo_report = c.repo_comparison_report_for_crate(k.short_name(), k.version());
    let comp_result = repo_report.as_ref().map(|r| r.result());
    let repo_is_fine = comp_result.as_ref().is_some_and(|r| r.has_a_good_tag && !r.unclear_vcs_info);
    let files_are_ok = comp_result.as_ref().is_some_and(|r| r.all_files_verified);
    let not_found_in_repo = comp_result.as_ref().is_some_and(|r| !r.all_files_verified && r.unclear_vcs_info);

    if !files_are_ok || !repo_is_fine {
        if !repo_is_fine && k.repository_vcs_info().1.is_none() {
            warnings.insert(Warning::VCSInfoMissing);
        } else if repo_report.is_none() && !files_are_ok {
            // we check git repo in the background, so first release may not have it indexed yet
            // so only complain about crates with multiple releases
            let reported_already = warnings.contains(&Warning::NoRepositoryProperty) || warnings.iter().any(|w| matches!(w, Warning::ErrorCloning(_)));
            let repo_check_failed = not_found_in_repo || (all.versions().len() > 1 && !k.has_path_in_repo());
            if !reported_already && repo_check_failed {
                warnings.insert(Warning::NotFoundInRepo);
            }
        }
    }
    if files_are_ok { // hide pedantic reports
        repo_report = None;
    }

    // This uses dates, not semvers, because we care about crates giving signs of life,
    // even if by patching old semvers.
    let latest_stable = find_most_recent_release(&versions, false);
    let latest_unstable = find_most_recent_release(&versions, true)
        // stabilized unstable releases are not relevant
        .filter(|(_, unstable_date)| latest_stable.as_ref().map_or(true, |(_, stable_date)| unstable_date > stable_date));

    let manifest = match c.crate_manifest(k.origin()).await {
        Ok(m) => m,
        Err(e) => {
            error!("Can't parse manifest: {e} of {:?}", k.origin());
            return Err(e.into());
        },
    };
    let package = manifest.package();

    if package.license().is_some_and(|l| l.contains('/')) {
        warnings.insert(Warning::LicenseSpdxSyntax);
    }

    let now = Utc::now();

    fn maintenance_status_factor(m: MaintenanceStatus) -> u32 {
        match m {
            MaintenanceStatus::Experimental => 1,
            MaintenanceStatus::ActivelyDeveloped => 2,
            MaintenanceStatus::None => 3,
            _ => 8,
        }
    }

    if k.maintenance() != MaintenanceStatus::AsIs && k.maintenance() != MaintenanceStatus::Deprecated {
        if let Some((version, _)) = &latest_stable {
            warnings.extend(c.advisories_for_crate(k.origin())?.iter()
                .filter(|a| !a.withdrawn() && a.versions.is_vulnerable(version))
                .map(|a| Warning::Advisory(a.id().as_str().into(), a.title().into())));
        }

        if let Some((_, reldate)) = &latest_stable {
            let days_since = now.signed_duration_since(*reldate).num_days() as u32;
            let stale_after = (5*466).min(if k.is_nightly() { 6*31 } else { 36*31 } * if latest_unstable.is_some() { 2 } else { 1 } * maintenance_status_factor(k.maintenance())/3);
            if days_since > stale_after {
                warnings.insert(Warning::StaleRelease(days_since, true, (days_since / stale_after).min(3) as u8));
            }
        }
        if let Some((_, reldate)) = &latest_unstable {
            let days_since = now.signed_duration_since(*reldate).num_days() as u32;
            let stale_after = (366).min(if k.is_nightly() { 2*31 } else { 3*31 } * if latest_stable.is_some() { 1 } else { 3 } * maintenance_status_factor(k.maintenance())/3);
            if days_since > stale_after {
                warnings.insert(Warning::StaleRelease(days_since, false, (days_since / stale_after).min(3) as u8));
            }
        }
    }

    // don't bother about old crates for now
    if k.maintenance() == MaintenanceStatus::Deprecated || latest_stable.as_ref().is_some_and(|(_, reldate)| now.signed_duration_since(*reldate).num_days() > 365*2) {
        repo_report = None;
    }

    // Some crates are internal details and don't need to be listed in a category
    let last_word = k.short_name().rsplit(['_', '-']).next().unwrap_or("");
    if k.is_proc_macro() || last_word == "impl" || last_word == "internal" {
        warnings.remove(&Warning::NoCategories);
        warnings.remove(&Warning::NoKeywords);
    }

    if k.is_sys() && package.links().is_none() && k.short_name() != "libc" {
        warnings.insert(Warning::SysNoLinks);
    }

    if let Some(readme_raw_path) = package.readme().as_path().and_then(|p| p.to_str()) {
        if readme_raw_path.starts_with("..") || readme_raw_path.starts_with('/') {
            warnings.insert(Warning::EscapingReadmePath(readme_raw_path.into()));
        }
    }

    // rust-version should be set for msrv > 1.56
    // but not if it's the deps that cause the breakage - then it's up to the deps to declare correctly
    let explicit_msrv = package.rust_version()
        .and_then(|v| v.strip_prefix("1."))
        .and_then(|v| v.parse().ok());

    let newest_bad = compat.values().rev().find_map(|c| c.newest_bad_certain()).unwrap_or(0); // serde is an odd one with too-old-to-fail msrv
    let oldest_ok = compat.values().rev().find_map(|c| c.oldest_ok_certain()).unwrap_or(999);
    let verified_breaking = newest_bad.min(oldest_ok.saturating_sub(1));
    let estimated_real_msrv = compat.values().rev().find_map(|c| c.newest_bad()).map(|bad| bad+1).unwrap_or(0)
        .max(explicit_msrv.unwrap_or(0));

    // if it's not compatible with the old compiler, there's no point using an old-compiler edition
    let edition = package.edition();
    if (verified_breaking >= 55 && edition < Edition::E2021) || (verified_breaking >= 30 && edition < Edition::E2018) {
        warnings.insert(Warning::EditionMSRV(edition, estimated_real_msrv));
    }
    if let Some(explicit_msrv) = explicit_msrv {
        if explicit_msrv < verified_breaking && estimated_real_msrv != explicit_msrv {
            let msrv_no_deps = c.rustc_compatibility_no_deps(all)?.values().rev().find_map(|c| c.newest_bad_certain()).unwrap_or(0);
            if msrv_no_deps >= 56 {
                warnings.insert(Warning::BadMSRV(estimated_real_msrv, explicit_msrv)); // for UI consistency display MSRV with deps
            }
        }
    }

    if !k.is_app() && !c.has_docs_rs(k.origin(), k.short_name(), k.version()).await {
        warnings.insert(Warning::DocsRs);
    }

    let stats = c.index()?.deps_stats()?;
    let (runtime, dev, build): (Vec<_>, Vec<_>, Vec<_>) = manifest.direct_dependencies();
    warn_outdated_deps(&runtime, &mut warnings, c, stats, estimated_real_msrv).await;
    warn_outdated_deps(&build, &mut warnings, c, stats, estimated_real_msrv).await;
    warn_bad_requirements(k, &runtime, &mut warnings, c).await;
    warn_bad_requirements(k, &build, &mut warnings, c).await;
    // dev deps are very low priority, so don't warn about them unless there's nothing else to do
    if warnings.is_empty() {
        warn_outdated_deps(&dev, &mut warnings, c, stats, estimated_real_msrv).await;
    }

    // 1.60 is the first to support dep:
    if estimated_real_msrv >= 60 {
        warn_unused_features(&manifest, &mut warnings);
    }
    Ok((warnings, repo_report))
}

fn warn_unused_features(manifest: &Manifest, warnings: &mut HashSet<Warning>) {
    let ft = cargo_toml::features::Resolver::<util::FxBuildHasher>::new_with_hasher_and_filter(&|_| true).parse(manifest);
    let sus_features = ft.features.iter()
        .filter(|&(_, f)| !f.explicit && !f.key.starts_with('_') && f.is_referenced() && !f.enabled_by.contains("default"))
        .map(|(&k, _)| SmolStr::from(k))
        .collect::<Vec<_>>();
    if !sus_features.is_empty() {
        warnings.insert(Warning::ImplicitFeatures(sus_features));
    }
}

fn find_most_recent_release<'a>(versions: &'a [(SemVer, &CrateVersion)], pre: bool) -> Option<(&'a SemVer, DateTime<Utc>)> {
    versions.iter().filter(move |(v, _)| pre != v.pre.is_empty()).max_by(|a,b| a.1.created_at.cmp(&b.1.created_at)).map(|(v, c)| (v, c.created_at))
}

async fn warn_bad_requirements(k: &RichCrateVersion, dependencies: &[RichDep], warnings: &mut HashSet<Warning>, c: &KitchenSink) {
    let mut lax_dep_warned = false;
    for richdep in dependencies {
        let req_str = richdep.dep.req().trim();
        let origin = richdep.origin();
        if req_str == "*" || !richdep.dep.is_crates_io() {
            warnings.insert(Warning::BadRequirement(origin, req_str.into()));
            continue;
        }

        if !req_str.contains('.') {
            let mut uses_zero_semver_minor_versioning = uses_zero_semver_minor_versioning(&richdep.package);
            let mut its_fine = false;

            if let Ok(k) = c.rich_crate_version_stale_is_ok(&origin).await {
                if let Ok(v) = k.version_semver() {
                    if v.minor > 0 {
                        uses_zero_semver_minor_versioning = false; // they've fixed it?
                    }
                    if !uses_zero_semver_minor_versioning {
                        its_fine = v.minor == 0; // if there's only 'x.0.y' version, then requirement 'x' is fine
                    }
                }
            }

            if !its_fine && !lax_dep_warned {
                lax_dep_warned = true; // this is a boring common issue
                warnings.insert(Warning::LaxRequirement(origin, req_str.into(), uses_zero_semver_minor_versioning));
            }
        }

        match req_str.parse::<VersionReq>() {
            Ok(req) => {
                // allow prerelease match to be exact; binary release likely needs to match
                if req.comparators.iter().all(|c| c.op == Op::Exact) && !req_str.split('+').next().unwrap().contains('-') && !richdep.package.contains("x86_64-") && !richdep.package.contains("aarch64-") {
                    let origin = richdep.origin();
                    if let Ok(other_crate) = c.rich_crate_version_stale_is_ok(&origin).await {
                        // if they belong to the same repo, they're probably versioned together
                        if other_crate.repository() == k.repository() {
                            continue;
                        }
                    }
                    // app-only crates get a free pass (outdated reqs are handled separately)
                    if k.has_lib() {
                        warnings.insert(Warning::ExactRequirement(origin, req_str.into()));
                    }
                }
            },
            Err(err) => {
                warnings.insert(Warning::BadRequirement(richdep.origin(), err.to_string().into()));
            },
        }
    }
}

async fn warn_outdated_deps(dependencies: &[RichDep], warnings: &mut HashSet<Warning>, c: &KitchenSink, stats: &DepsStats, msrv: u16) {
    for richdep in dependencies {
        if let Ok(req) = richdep.dep.req().parse::<VersionReq>() {
            let package = richdep.package.as_str();
            let origin = richdep.origin();
            match package {
                "matches" if msrv >= 42 => {
                    warnings.insert(Warning::ObsoleteDependency(origin, "matches! macro in the standard library".into()));
                    continue;
                },
                "try_from" => {
                    warnings.insert(Warning::ObsoleteDependency(origin, "try_from is in the standard library".into()));
                    continue;
                },
                "is-terminal" if msrv >= 70 => {
                    warnings.insert(Warning::ObsoleteDependency(origin, "IsTerminal trait is in the standard library".into()));
                    continue;
                },
                "file" => {
                    warnings.insert(Warning::ObsoleteDependency(origin, "std::fs::read is in the standard library".into()));
                    continue;
                },
                "time" if !req.matches(&"0.3.36".parse().unwrap()) => {
                    warnings.insert(Warning::OutdatedDependency(origin, richdep.dep.req().into(), 100));
                    continue;
                },
                _ => {},
            }
            if let Ok(Some(pop)) = c.version_popularity(&origin, &req, stats).await {
                if pop.deprecated {
                    warnings.insert(Warning::DeprecatedDependency(origin, richdep.dep.req().into()));
                    continue;
                }
                if pop.lost_popularity && pop.pop < 0.2 {
                    warnings.insert(Warning::UnpopularDependency(origin, richdep.dep.req().into()));
                    continue;
                }
                if !pop.matches_latest {
                    let outdated_percent = ((1. - pop.pop) * 100.).round() as u8;
                    warnings.insert(Warning::OutdatedDependency(origin, richdep.dep.req().into(), outdated_percent));
                    continue;
                }
            }
            // this may be version-specific, but the wording on the maintainer dashboard doesn't make it clear
            if is_deprecated_requirement(package, &req) {
                warnings.insert(Warning::DeprecatedDependency(origin, richdep.dep.req().into()));
                continue;
            }
        }
    }
}

fn uses_zero_semver_minor_versioning(name: &str) -> bool {
    // it is ironic that the new semver maintainer passionately hates semver feature rules.
    matches!(name, "serde" | "serde_derive" | "serde_json" | "cc" | "anyhow" | "thiserror" | "cxx" | "cxx-build" | "serde_test" | "syn" | "trybuild")
}
